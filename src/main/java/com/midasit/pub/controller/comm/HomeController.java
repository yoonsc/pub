package com.midasit.pub.controller.comm;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by lsh on 2018-11-19.
 */
@Controller
public class HomeController {

    @RequestMapping(value = {"","/index" }, method = RequestMethod.GET)
    public String index() {
        return "index";
    }
}
