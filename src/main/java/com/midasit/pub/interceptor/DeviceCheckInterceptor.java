package com.midasit.pub.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by lsh on 2018-08-17.
 */
@Component
public class DeviceCheckInterceptor extends HandlerInterceptorAdapter {

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler ) throws Exception{

        Device device = DeviceUtils.getCurrentDevice(request);

        String msg = null;
        if (device.isMobile()) {
            msg = "mobile";
        } else if (device.isTablet()) {
            msg = "tablet";
        } else {
            msg = "desktop";
        }
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("device", msg);
        return true;
    }
}
