<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents bg_white store_view">

	<div class="ly_container store_top">
		<div class="grid_left">
			<span class="logoimg">
				<img src="/resources/user/img/contents/store/top_thumb.jpg" alt="">
			</span>
		</div>
		<!-- //grid_left -->
		<div class="grid_right">
			<dl class="store_name">
				<dt>스타벅스 <br />건대스타시티점</dt>
				<dd>Starbucks konkuk City</dd>
			</dl>
			<div class="store_info">
				<dl>
					<dt><i class="ico ico_store_time"></i> 영업시간</dt>
					<dd>
						<ul class="detail_list">
							<li><span class="time">09:00 ~ 23:00</span> <span class="issue">주중 - 월,화,수,목,일</span></li>
							<li><span class="time">09:00 ~ 23:00</span> <span class="issue">주말 - 금, 토</span></li>
							<li><span class="time">09:00 ~ 23:00</span> <span class="issue">브레이크 타임</span></li>
						</ul>
					</dd>
				</dl>
				<dl>
					<dt><i class="ico ico_store_phone"></i> 전화번호</dt>
					<dd>
						<ul class="detail_list">
							<li>02-738-8197</li>
							<li>010-1234-1234</li>
						</ul>
					</dd>
				</dl>
				<dl>
					<dt><i class="ico ico_store_map"></i> 매장위치</dt>
					<dd>
						스타존 1F<a href="javascript:;" class="btn btn_round_nm">위치보기 <i class="lnr lnr-chevron-right"></i></a>
					</dd>
				</dl>
			</div>
			<div class="button_group txt-cnt">
				<span><a href="javascript:;" class="btn box-blue"><i class="far fa-heart"></i> 관심매장</a></span>
				<span><a href="javascript:;" class="btn box-white">사이트 바로가기</a></span>
			</div>
		</div>
		<!-- //grid_right -->
	</div>
	<!-- //store_top -->

	<div class="ly_container store_body" id="store_body">
		<dl class="store_intro store_body_tit">
			<dt>Store Info</dt>
			<dd>
				<p>스타벅스 건대스타시티점 입니다.</p>
			</dd>
		</dl>
		<dl class="store_location store_body_tit">
			<dt>Location</dt>
			<dd>
				<p>스타벅스 건대시티점의 위치는 <a href="javascript:mapviewPop();" class="txt-line-blue">스타존<i class="ico ico_i_blue"></i></a> 1F 에 있습니다.</p>
			</dd>
		</dl>

		<div class="map_wrap">
			<div class="store_map">
				<img src="/resources/user/img/contents/store/store_map_img.jpg" alt="">
				<!-- 매장위치아이콘 -->
				<i class="pick_icon" style="left: 10%; top: 10%;"></i>
			</div>


			<!-- 간략단지안내 팝업 -->
			<div id="mapviewPop" class="layerPopup map_layer">
				<div class="layerPopup_in">
					<span class="closeLayer">
						<a href="javascript:LayerPopups.close('mapviewPop');" class="btn"><i class="lnr lnr-cross"></i></a>
					</span>
					<div class="map_img">
						<img src="/resources/user/img/contents/about/overview_map.jpg" alt="">
					</div>
					<!-- //layer_txt -->
				</div>
				<!-- //layerPopup_in -->
			</div>
			<!-- //간략단지안내 팝업 -->
		</div>


	</div>
	<!-- //store_body -->




</div>

<script type="text/javascript">
	function mapviewPop(){
		LayerPopups.open('mapviewPop', true);
	}
</script>


<%@include file="../inc/inc_gl_bottom.jsp"%>
