<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents bg_white store_view">

	<div class="ly_container store_top">
		<div class="grid_left">
			<span class="logoimg">
				<img src="/resources/user/img/contents/store/top_thumb.jpg" alt="">
			</span>
		</div>
		<!-- //grid_left -->
		<div class="grid_right">
			<dl class="store_name">
				<dt>스타벅스 <br />건대스타시티점</dt>
				<dd>Starbucks konkuk City</dd>
			</dl>
			<div class="store_info">
				<dl>
					<dt><i class="ico ico_store_time"></i> 영업시간</dt>
					<dd>
						<ul class="detail_list">
							<li><span class="time">09:00 ~ 23:00</span> <span class="issue">주중 - 월,화,수,목,일</span></li>
							<li><span class="time">09:00 ~ 23:00</span> <span class="issue">주말 - 금, 토</span></li>
							<li><span class="time">09:00 ~ 23:00</span> <span class="issue">브레이크 타임</span></li>
						</ul>
					</dd>
				</dl>
				<dl>
					<dt><i class="ico ico_store_phone"></i> 전화번호</dt>
					<dd>
						<ul class="detail_list">
							<li>02-738-8197</li>
							<li>010-1234-1234</li>
						</ul>
					</dd>
				</dl>
				<dl>
					<dt><i class="ico ico_store_map"></i> 매장위치</dt>
					<dd>
						스타존 1F<a href="javascript:;" class="btn btn_round_nm" id="goLocation">위치보기 <i class="lnr lnr-chevron-right"></i></a>
					</dd>
				</dl>
			</div>
			<div class="button_group txt-cnt">
				<span><a href="javascript:;" class="btn btn-favorite on"><i class="far fa-heart"></i> 관심매장</a></span>
				<span><a href="javascript:;" class="btn box-white">사이트 바로가기</a></span>
			</div>
		</div>
		<!-- //grid_right -->
	</div>
	<!-- //store_top -->

	<div class="ly_container store_body">
		<dl class="store_intro store_body_tit">
			<dt>Store Info</dt>
			<dd>
				<p>스타벅스 건대스타시티점 입니다.</p>
			</dd>
		</dl>
		<div class="store_editer">
			<dl class="tit_sec">
				<dd class="summay_1">나의 일상이 되는 또 다른 공간</dd>
				<dt><strong class="e_name">스타벅스</strong> 건대스타시티점</dt>
				<dd class="summay_2">다채로운 커피의 매력을 느껴볼 수 있는 특별한 시간을 경험해보세요.</dd>
			</dl>

			<div class="store_view_img">
				<img src="/resources/user/img/contents/store/shop_view.jpg" alt="">
			</div>

			<div class="store_txt">
				<p>건대스타시티점은 좀더 다양한 혜택 및 커피에 대한 지식을 공유하고자<br /> 2개월에 한번 커피교실을 운영하고 있습니다.</p>
				<p>많은 고객님들의 참여 부탁드립니다. </p>
			</div>

			<ul class="store_thumb">
				<li><img src="/resources/user/img/contents/store/shop_view_thumb_1.jpg" alt=""></li>
				<li><img src="/resources/user/img/contents/store/shop_view_thumb_2.jpg" alt=""></li>
				<li><img src="/resources/user/img/contents/store/shop_view_thumb_3.jpg" alt=""></li>
			</ul>

		</div>
		<!-- //store_editer -->

		<dl class="store_gallery store_body_tit">
			<dt>Gallery
				<span class="nav_control">
					<a href="javascript:;" id="slidePrev"><i class="lnr lnr-chevron-left"></i></a>
					<a href="javascript:;" id="slideNext"><i class="lnr lnr-chevron-right"></i></a>
				</span>
			</dt>
			<dd>
				<ul class="gal_list">
					<li><a href="javascript:;"><img src="/resources/user/img/contents/store/shop_menu_img01.jpg" alt=""></a></li>
					<li><a href="javascript:;"><img src="/resources/user/img/contents/store/shop_menu_img01.jpg" alt=""></a></li>
					<li><a href="javascript:;"><img src="/resources/user/img/contents/store/shop_menu_img02.jpg" alt=""></a></li>
					<li><a href="javascript:;"><img src="/resources/user/img/contents/store/shop_menu_img02.jpg" alt=""></a></li>
					<li><a href="javascript:;"><img src="/resources/user/img/contents/store/shop_menu_img01.jpg" alt=""></a></li>
					<li><a href="javascript:;"><img src="/resources/user/img/contents/store/shop_menu_img01.jpg" alt=""></a></li>
					<li><a href="javascript:;"><img src="/resources/user/img/contents/store/shop_menu_img02.jpg" alt=""></a></li>
					<li><a href="javascript:;"><img src="/resources/user/img/contents/store/shop_menu_img02.jpg" alt=""></a></li>
				</ul>
			</dd>
		</dl>

		<div class="map_wrap" id="storeMap">
			<dl class="store_location store_body_tit" id="store_body">
				<dt>Location</dt>
				<dd>
					<p>스타벅스 건대시티점의 위치는 <a href="javascript:mapviewPop();" class="txt-line-blue">스타존<i class="ico ico_i_blue"></i></a> 1F 에 있습니다.</p>
				</dd>
			</dl>

			<div class="store_map">
				<img src="/resources/user/img/contents/store/store_map_img.jpg" alt="">
				<!-- 매장위치아이콘 -->
				<i class="pick_icon" style="left: 10%; top: 10%;"></i>
			</div>

			<!-- 간략단지안내 팝업 -->
			<div id="mapviewPop" class="layerPopup map_layer">
				<div class="layerPopup_in">
					<span class="closeLayer">
						<a href="javascript:LayerPopups.close('mapviewPop');" class="btn"><i class="lnr lnr-cross"></i></a>
					</span>
					<div class="map_img">
						<img src="/resources/user/img/contents/about/overview_map.jpg" alt="">
					</div>
					<!-- //layer_txt -->
				</div>
				<!-- //layerPopup_in -->
			</div>
			<!-- //간략단지안내 팝업 -->
		</div>

	</div>
	<!-- //store_body -->



<script type="text/javascript">

	function mapviewPop(){
		LayerPopups.open('mapviewPop', true);
	}

     (function (ns) {
        $(function(){
        	var $mapBtn = $("#goLocation");
        	var $mapCon = $("#store_body");
        	var $Top = $mapCon.offset().top;

        	$mapBtn.on("click", function(){
			 TweenMax.to($("html, body"), 1, {scrollTop:$mapCon.offset().top, ease:Cubic.easeInOut});
            });

           ns.shopGallery.init(".gal_list");
        });
     })(APP || {});
</script>





</div>

<%@include file="../inc/inc_gl_bottom.jsp"%>
