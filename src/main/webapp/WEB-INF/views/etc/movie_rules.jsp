<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

 
<div class="sub_contents etc">
    <div class="ly_container bg_white">

        <h2>영상정보처리기기 운영관리 방침</h2>
        <div class="privacy_con mt20">
            <h3 class="pb20">제1조 영상정보처리기기의 설치근거 및 설치목적</h3>
            <p class="tit_txt">건국AMC는 개인정보보호법 제25조 제1항에 따라 다음과 같은 목적으로 영상정보처리기기를 설치 및 운영합니다.</p>

            <ul class="bl_line">
                <li>시설안전 및 화재예방</li>
                <li>고객의 안전을 위한 범죄 예방</li>
                <li>차량도난 및 파손방지</li>
            </ul>

            <h3 class="pb20">제2조 영상정보처리기기 설치 현황</h3>
            <table class="common_tb">
                <caption></caption>
                <colgroup>
                    <col width="20%">
                    <col width="20%">
                    <col width="20%">
                    <col width="20%">
                    <col width="20%">
                </colgroup>
                <thead>
                    <tr>
                        <th rowspan="2">구분</th>
                        <th colspan="3">설치대수</th>
                        <th rowspan="2">설치위치 및 <br />촬영범위</th>
                    </tr>
                    <tr>
                        <th>스타존</th>
                        <th>시티존</th>
                        <th>영존</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>CCTV</th>
                        <td>166</td>
                        <td>160</td>
                        <td>134</td>
                        <td>건물 내&middot;외곽 및 주차장</td>
                    </tr>
                    <tr>
                        <th>차량번호 <br />인식카메라</th>
                        <td>4</td>
                        <td>4</td>
                        <td>1</td>
                        <td>주차장 입&middot;출구</td>
                    </tr>
                    <tr>
                        <th>사면 카메라</th>
                        <td>8</td>
                        <td>4</td>
                        <td>4</td>
                        <td>주차장 입&middot;출구</td>
                    </tr>
                </tbody>
            </table>


            <h3 class="pb20">제3조 관리책임자 및 접근권한자</h3>
            <p class="tit_txt">건국AMC는 영상정보에 대한 안전한 관리를 위해 아래와 같이 개인영상정보 보호책임자를 두고 있습니다.</p>
            <table class="common_tb">
                <caption></caption>
                <colgroup>
                    <col width="20%">
                    <col width="20%">
                    <col width="20%">
                    <col width="20%">
                    <col width="20%">
                </colgroup>
                <thead>
                    <tr>
                        <th>구분</th>
                        <th>이름</th>
                        <th>직위</th>
                        <th>소속</th>
                        <th>연락처</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>관리책임자</th>
                        <td>김경환</td>
                        <td>주임</td>
                        <td>더클래식500</td>
                        <td>02-2218-5531</td>
                    </tr>
                    <tr>
                        <th>접근권한자</th>
                        <td colspan="4">보안팀/주차팀/시설관리팀</td>
                    </tr>
                </tbody>
            </table>

            <h3 class="pb20">제4조 영상정보의 촬영시간, 보관기간, 보관장소 및 처리방법</h3>
            <table class="common_tb">
                <caption></caption>
                <colgroup>
                    <col width="33%">
                    <col width="34%">
                    <col width="33%">
                </colgroup>
                <thead>
                    <tr>
                        <th>촬영시간</th>
                        <th>보관기간</th>
                        <th>보관장소</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>24시간</th>
                        <td>촬영일로부터 자동 저장 30일</td>
                        <td>방재실</td>
                    </tr>
                </tbody>
            </table>

            <p class="pt20">
            처리방법 : 개인영상정보의 목적 외 이용, 제3자 제공, 파기, 열람 등 요구에 관한 사항을 기록·관리하고, 보관기간 만료 시 복원이 불가능한 방법으로 영구 삭제(출력물의 경우 파쇄 또는 소각)합니다.
            </p>

            <h3>제5조 영상정보처리기기 설치 및 관리 등의 위탁에 관한 사항</h3>
            <p class="tit_txt">
                건국AMC는 아래와 같이 영상정보처리기기 설치 및 관리 등을 위탁하고 있으며, 관계법령에 따라 위탁 계약 시 개인정보가 안전하게 관리될 수 있도록 필요한 사항을 규정하고 있습니다.
            </p>
            <table class="common_tb mt10">
                <caption></caption>
                <colgroup>
                    <col width="25%">
                    <col width="25%">
                    <col width="25%">
                    <col width="25%">
                </colgroup>
                <thead>
                    <tr>
                        <th>구분</th>
                        <th>업체명</th>
                        <th>담당자</th>
                        <th>연락처</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th rowspan="2">스타존</th>
                        <td>㈜동우유니온(시설)</td>
                        <td>김해봉 소장</td>
                        <td rowspan="2">02-2218-3395</td>
                    </tr>
                    <tr>
                        <td>㈜삼구INC(보안,주차)</td>
                        <td>이건상 팀장</td>
                    </tr>
                    <tr>
                        <th rowspan="2">시티존</th>
                        <td>㈜동우유니온(시설)</td>
                        <td>권대성 소장</td>
                        <td rowspan="2">02-2024-1515</td>
                    </tr>
                    <tr>
                        <td>㈜삼구INC(보안,주차)</td>
                        <td>오인규 팀장</td>
                    </tr>
                    <tr>
                        <th rowspan="2">영존</th>
                        <td>㈜동우유니온(시설)</td>
                        <td>배상언 소장</td>
                        <td rowspan="2">02-2218-4562</td>
                    </tr>
                    <tr>
                        <td>㈜삼구INC(보안,주차)</td>
                        <td>최환수 팀장</td>
                    </tr>
                </tbody>
            </table>


            <h3 class="pb20">제6조 개인영상정보의 확인방법 및 장소에 관한 사항</h3>
            <ol class="num_list">
                <li>확인방법 : 영상정보 접근권한자에게 미리 연락 및 경찰관 동행하여 열람이 가능합니다.</li>
                <li>확인장소 : 정보를 확인하고자 하는 구역의 방재실</li>
            </ol>

            <h3>제7조 정보주체의 영상정보 열람 등 요구에 대한 조치</h3>
            <p>귀하는 개인영상정보에 관하여 열람 또는 존재확인, 삭제를 원하는 경우 언제든지 영상정보처리기기 운영자에게 요구하실 수 있습니다. 단, 귀하가 촬영된 개인영상정보 및 명백히 정보주체의 급박한 생명, 신체, 재산의 이익을 위하여 필요한 개인영상정보에 한정 됩니다. 건국AMC는 개인영상정보에 관하여 열람 또는 존재확인, 삭제를 요구한 경우 지체없이 필요한 조치를 하겠습니다.</p>

            <h3>제8조 영상정보의 안전성 확보조치</h3>
            <p>건국AMC에서 처리하는 영상정보는 암호화 조치 등을 통해 안전하게 관리되고 있습니다. 또한 개인 영상정보의 위.변조 방지를 위해 개인영상정보의 생성 일시, 열람 시 목적.열람자.열람 일시 등을 기록하여 관리하고 있습니다. 영상정보의 보관장소는 보안구역으로 정하고 사전 승인된 인력 이외에는 출입을 통제하고 있습니다.</p>

            <h3>제9조 개인정보 처리방침 변경에 관한 사항</h3>
            <p>이 영상정보처리기기 운영·관리방침은 2019년 1월 1일에 개정되었으며 법령·정책 또는 보안기술의 변경에 따라 내용의 추가·삭제 및 수정이 있을 시에는 시행하기 최소 7일전에 본 홈페이지를 통해 변경사유 및 내용 등을 공지하도록 하겠습니다.</p>


        </div>
        <!-- //privacy_con -->

    </div>
    <!-- //ly_container -->


</div>
<!-- //sub_contents -->

<%@include file="../inc/inc_gl_bottom.jsp"%>
