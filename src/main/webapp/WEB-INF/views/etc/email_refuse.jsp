<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

 
<div class="sub_contents etc">
    <div class="ly_container bg_white">
        <h2>이메일집단수집거부</h2>
        <div class="privacy_con mt20">
            <div class="line_box">
                본 웹사이트에 게시된 이메일 주소가 전자우편 수집 프로그램이나 그 밖의 기술적 장치를 이용하여 무단으로 수집되는 것을 거부하며, 이를 위반 시 정보통신망법에 의해 형사처벌됨을 유념하시기 바랍니다.
            </div>

            <h3 class="pb20">정보통신망법 제 50조의 2 (전자우편주소의 무단 수집행위 등 금지)</h3>
            <ol class="num_list">
                <li>누구든지 인터넷 홈페이지 운영자 또는 관리자의 사전 동의 없이 인터넷 홈페이지에서 자동으로 전자우편주소를 수집하는 프로그램이나 그 밖의 기술적 장치를 이용하여 전자우편주소를 수집하여서는 아니 된다.</li>
                <li>누구든지 제1항을 위반하여 수집된 전자우편주소를 판매.유통하여서는 아니 된다.</li>
                <li>누구든지 제1항과 제2항에 따라 수집.판매 및 유통이 금지된 전자우편주소임을 알면서 이를 정보 전송에 이용 하여서는 아니 된다.</li>
            </ol>
        </div>
        <!-- //privacy_con -->

    </div>
    <!-- //ly_container -->


</div>
<!-- //sub_contents -->

<%@include file="../inc/inc_gl_bottom.jsp"%>
