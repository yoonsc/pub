<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

 
<div class="sub_contents etc">
    <div class="ly_container bg_white">
        <h2>개인정보처리방침</h2>
        <div class="privacy_con mt20">
            <h4>제1조 수집하는 개인정보의 항목</h4>
            <ul class="bl_line">
                <li>필수 : 성명, 생년월일, 주소, 이메일 주소, 핸드폰 번호, 아이디, 비밀번호, 성별</li>
                <li>수집방법 : 홈페이지, 이벤트 응모, 이메일, 콜센터</li>
            </ul>

            <h4>제2조 개인정보 수집 이용 목적</h4>
            <ul class="bl_line">
                <li>회원 서비스 제공 및 본인 확인 절차에 활용</li>
                <li>회원에 대한 각종 편의 서비스 및 혜택 제공, 공지사항 전달 및 본인의사 확인, 민원처리, 사고조사 등을 위한 원활한 의사소통 경로 확보</li>
            </ul>

            <h4>제3조 개인정보 보유 및 이용기간</h4>
            <ul class="bl_line">
                <li>개인정보는 개인정보 삭제 및 회원 탈퇴를 요청할 때까지 보유ㆍ이용합니다. 고객으로부터 개인정보 즉시 파기 요청이 없을 경우 탈퇴 신청 후 즉시 파기합니다.</li>
            </ul>

            <h4>제4조 개인정보의 수집 주체</h4>
            <ul class="bl_line">
                <li>수집 주체 : 건국AMC</li>
            </ul>

            <ul class="comment">
                <li>고객님께서는 개인정보 수집/이용 동의를 거부할 권리가 있습니다. 단, 동의 거부 시에는 회원가입이 불가하여 상기 이용목적에 명시된 서비스를 받으실 수 없습니다.</li>
            </ul>
        </div>
        <!-- //privacy_con -->

    </div>
    <!-- //ly_container -->

</div>
<!-- //sub_contents -->

<%@include file="../inc/inc_gl_bottom.jsp"%>
