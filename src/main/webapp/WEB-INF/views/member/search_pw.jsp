<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents member">
	<div class="ly_container bg_white">
		<div class="search_member">
			
			<ul class="tab_boxline tablist_2">
	            <li><a href="search_id">아이디 찾기</a></li>
	            <li><a href="search_pw" class="on">비밀번호 찾기</a></li>
	        </ul>
	        <p class="tit">비밀번호를 찾고자 하는 아이디를 입력해주세요.</p>

	        <div class="line_wrap mb20 ph50">
	        	<div class="search_box_pw">
        			<table class="common_tb_col register_form">
			          <caption>아이디 등록서식</caption>
			          <colgroup>
			            <col width="120px">
			            <col width="*">
			          </colgroup>					          
			          <tbody>					          	                				              				             
			              <tr>
			                  <th scope="row">아이디 </th>
			                  <td>
								<input type="text" name="hID" id="hID">
			                  </td>
			              </tr>			              
		              	</tbody>
		              </table>
		        </div>
		        <!-- //search_box_inner -->        	

	        </div>
	        <!-- //line_wrap -->

	        <div class="button_group txt-cnt pt50">					        	
				<span><a href="javascript:;" class="btn box-blue">다음</a></span>
			</div>


		</div>
	</div>
	<!-- //ly_container -->	

</div>
<!-- //sub_contents -->

<%@include file="../inc/inc_gl_bottom.jsp"%>
