<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents member">
	<div class="ly_container bg_white">
		<div class="join_agree">
			<ul class="step_list sns_step">				
				<li>
					<span class="step_icon step_1"></span>
					<dl>
						<dt>Step1</dt>
						<dd>약관동의</dd>
					</dl>
				</li>				
				<li>
					<span class="step_icon step_3"></span>
					<dl>
						<dt>Step3</dt>
						<dd>정보입력</dd>
					</dl>
				</li>
				<li class="on">
					<span class="step_icon step_4"></span>
					<dl>
						<dt>Step4</dt>
						<dd>가입완료</dd>
					</dl>
				</li>
			</ul>

			<div class="register_end">
				<dl>
					<dt>스타시티 <strong>회원가입</strong>이 완료되었습니다. </dt>
					<dd>
						<p>쇼핑, 문화, 레저, 편의시설 그리고 인근에 주거공간까지 갖춘<br /> 국내 최초의 진정한 One-stop Life 쇼핑공간 스타시티몰의 회원이 되신 것을 축하드립니다.</p>
						<p>이제 스타시티몰에서 더욱더 즐거운 라이프생활을 즐기시길 바랍니다.</p>
						<p>감사합니다.</p>
					</dd>
				</dl>

				<div class="button_group txt-cnt">
					<span><a href="javascript:;" class="btn box-white">홈 화면으로 이동</a></span>
				</div>

			</div>

		</div>
	</div>
	<!-- //ly_container -->
	

</div>
<!-- //sub_contents -->

<script language="javascript">
	$(function(){	 
		
	});
</script>


<%@include file="../inc/inc_gl_bottom.jsp"%>
