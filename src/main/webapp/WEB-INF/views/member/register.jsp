<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents member">
	<div class="ly_container bg_white">
		<div class="join_agree">
			<ul class="step_list">				
				<li>
					<span class="step_icon step_1"></span>
					<dl>
						<dt>Step1</dt>
						<dd>약관동의</dd>
					</dl>
				</li>
				<li>
					<span class="step_icon step_2"></span>
					<dl>
						<dt>Step2</dt>
						<dd>본인인증</dd>
					</dl>
				</li>
				<li class="on">
					<span class="step_icon step_3"></span>
					<dl>
						<dt>Step3</dt>
						<dd>정보입력</dd>
					</dl>
				</li>
				<li>
					<span class="step_icon step_4"></span>
					<dl>
						<dt>Step4</dt>
						<dd>가입완료</dd>
					</dl>
				</li>
			</ul>


			<form id="joinForm" method="get" action=""> 

				<div class="register_wrap">				
					<h3 class="register_tit">회원정보 입력 <span><i class="require">*</i> 는 필수 입력 항목 입니다.</span></h3>				

		            <table class="common_tb_col register_form  join_register">
				          <caption>문의분야, 이름, 이메일, 전화번호, 내용을 작성하는 등록서식</caption>
				          <colgroup>
				            <col width="150px">
				            <col width="*">
				          </colgroup>					          
				          <tbody>	
				          	  <tr>
				                  <th scope="row">아이디 <i class="require">*</i></th>
				                  <td class="btn_in">			                  	
									<input type="text" name="hID" id="hID">								
									<a href="javascript:chkIdPop();" class="btn box-navy">중복확인</a>							
				                  </td>
				              </tr>	
				              <tr>
				                  <th scope="row">비밀번호 <i class="require">*</i></th>
				                  <td>			                  	
									<input type="password" name="hPW" id="hPW" required>
				                  </td>
				              </tr>	              				              				             
				              <tr>
				                  <th scope="row">이름 <i class="require">*</i></th>
				                  <td>
									<input type="text" name="name" id="hName" readonly value="홍길동" class="read-only">
				                  </td>
				              </tr>
				              <tr>
				                  <th scope="row">생년월일 <i class="require">*</i></th>
				                  <td>
				                  	<input type="text" name="hBirth" id="hBirth" readonly value="19810101" class="read-only">
				                  </td>
				              </tr>
				              <tr>
				                  <th scope="row">휴대전화 번호 <i class="require">*</i></th>
				                  <td class="phone_cell">
									<span class="select-wrapper line_select">
										<select title="전화번호선택" id="hTel1" name="hTel1" readonly class="read-only">
											<option value="010">010</option>
											<option value="011">011</option>
											<option value="018">018</option>
											<option value="016">016</option>
											<option value="017">017</option>
										</select>     
									</span> - 
									<span>
										<input type="text" name="hTel2" id="hTel2" class="number-check read-only" value="1111" maxlength="4" title="전화번호중간번호" style="width:150px;" onkeydown="onlyNumber(this);" readonl> -
									</span>						
									<span>									
										<input type="text" name="hTel3" id="hTel3" class="number-check read-only" value="1111" maxlength="4" title="전화번호끝번호" style="width:150px;" onkeydown="onlyNumber(this);" readonly>
									</span>
									<input type="hidden" name="tel">
				                  </td>
				              </tr>
				              <tr>
				                  <th scope="row" class="vtop">이메일 <i class="require">*</i></th>
				                  <td>					                  	
			                  		<div class="placeholder">
										<label for="hEmail1">이메일</label>
										<input type="text" name="hEmail1" class="boxline" id="hEmail1" style="width: 250px;">
									</div> 
									<div class="email_input">	
										@
										<span>
											<input type="text" name="hEmail2" class="boxline" id="hEmail2" title="입력" style="width: 200px;"> 
										</span> 						
										<span class="select-wrapper line_select">
											<select name="mailserver" title="이메일 서버선택" onchange="$('#hEmail2').val( $(this).val() == 'D' ? '' : $(this).val() ); " style="width: 200px;">
												<option value="D">직접입력</option>
												<option value="naver.com">naver.com</option>
												<option value="gmail.com">gmail.com</option>
												<option value="daum.net">daum.net</option>
												<option value="hanmail.net">hanmail.net</option>
												<option value="hotmail.com">hotmail.com</option>
												<option value="icloud.com">icloud.com</option>
												<option value="yahoo.com">yahoo.com</option>
											</select>														
										</span>											
									</div>	
									<input type="hidden" name="email">
									<ul class="comment">
				                 		<li>중요소식 및 뉴스레터 수신을 위해 정확한 이메일 주소를 입력해주세요.</li>
				                 	</ul>
								  </td>
				              </tr>
				              <tr>
				                  <th scope="row">성별 </th>
				                  <td>
				                  	<label>
				                  		<input type="radio" name="hSec" class="radio" >
				                  		<span class="label"></span> 남성
				                  	</label>
				                  	<label>
				                  		<input type="radio" name="hSec" class="radio" >
				                  		<span class="label"></span> 여성
				                  	</label>
				                  	<label>
				                  		<input type="radio" name="hSec" class="radio" >
				                  		<span class="label"></span> 선택안함
				                  	</label>
				                  </td>
				              </tr>
				              
				              <tr>
				                  <th scope="row" class="vtop"><i class="require"></i> 주소</th>
				                  <td class="btn_in">			                  	
				                  	 <p class="pb10">
				                  	 	<span><input type="text" name="hZip" class="boxline" id="hZip" style="width: 150px;"></span>
										 <a href="javascript:;" class="btn box-navy vtop">우편번호 찾기</a>
				                  	 </p>
				                  	 <p>
				                  	 	<span><input type="text" name="hZip" class="boxline" id="hZip2" style="width: 350px;"></span>
				                  	 	<span><input type="text" name="hZip" class="boxline" id="hZip3" style="width: 300px;"></span>
				                  	 </p>
				                  </td>
				              </tr>			              
				          </tbody>
				    </table>

				    <h3 class="register_tit">부가정보 입력</h3>
				    <p class="letterchk">
				    	<label>
	                  		<input type="checkbox" name="hLetter" class="checkbox" >
	                  		<span class="label"></span> 경품당첨 소식 및 뉴스레터, 이벤트등의 정보 수신에 동의합니다. 
	                  	</label>
				    </p>
				    <table class="common_tb_col register_form join_register">
				    	<caption>문의분야, 이름, 이메일, 전화번호, 내용을 작성하는 등록서식</caption>
				          <colgroup>
				            <col width="150px">
				            <col width="*">
				          </colgroup>					          
				          <tbody>	
				          	  <tr>
				                  <th scope="row">이메일 수신</th>
				                  <td>
				                  	<label>
				                  		<input type="radio" name="hAgEmail" class="radio" >
				                  		<span class="label"></span> 동의
				                  	</label>
				                  	<label>
				                  		<input type="radio" name="hAgEmail" class="radio" >
				                  		<span class="label"></span> 미동의
				                  	</label>		                  	
				                  </td>
				              </tr>	
				              <tr>
				                  <th scope="row">SMS 수신</th>
				                  <td>
				                  	<label>
				                  		<input type="radio" name="hAgSms" class="radio" >
				                  		<span class="label"></span> 동의
				                  	</label>
				                  	<label>
				                  		<input type="radio" name="hAgSms" class="radio" >
				                  		<span class="label"></span> 미동의
				                  	</label>		                  	
				                  </td>
				              </tr>	
				          </tbody>
				      </table>
				    </table>
				</div>

				<div class="button_group txt-cnt">
					<span><a href="javascript:;" class="btn box-white">취소</a></span>
					<span><a href="javascript:;" class="btn box-blue submit">가입완료</a></span>
				</div>

			</form>


		</div>
	</div>
	<!-- //ly_container -->

	<!-- 아이디중복체크 팝업 시작-->
	<div id="idCheckPop" class="layerPopup check_layer">
		<div class="layerPopup_in">
			<span class="closeLayer">
				<a href="javascript:LayerPopups.close('idCheckPop');" class="btn"><i class="lnr lnr-cross"></i></a>
			</span>
			<div class="layer_txt">	
				<h3 class="agree_tit">아이디 중복체크</h3>
				<div class="check_form">
					<dl>
						<dt>아이디</dt>
						<dd>
							<p>
								<input type="text" name="hZip" class="boxline" id="checkId" style="width: 290px;">
								<a href="javascript:;" class="btn box-navy">중복확인</a>
							</p>
							<p class="comt">※ 아이디는 영문(소문자)또는 숫자를 활용하여 4 -16자 이내로 생성 가능합니다.</p>
						</dd>
					</dl>

					<p class="result_txt"><strong>“test”</strong>아이디는 사용이 가능합니다.</p>
				</div>
				<!--// check_form -->						
			</div>
			<!-- //layer_txt -->

			<div class="button_group txt-cnt">
				<span><a href="javascript:LayerPopups.close('idCheckPop');" class="btn btn_round_blue_bg">아이디사용</a></span>
			</div>

		</div>
		<!-- //layerPopup_in -->
	</div> 
	<!-- //개인정보 팝업 -->

</div>
<!-- //sub_contents -->

<script type="text/javascript" src="/resources/user/js/libs/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="/resources/user/js/libs/jquery/jquery.validate.messages_ko.js"></script>

<script language="javascript">

	//ID중복체크
	function chkIdPop(){
		LayerPopups.open('idCheckPop', true);
	}


	(function (ns) {
		$(function(){			   
		   ns.placeholder.init();
		});
	 })(APP || {});
</script>


<%@include file="../inc/inc_gl_bottom.jsp"%>
