<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

 
<div class="sub_contents member">
	<div class="ly_container bg_white pb70">

		<div class="error_icon">
            <i class="ico ico_error"></i>
        </div>

        <dl class="error_tit">
            <dt>죄송합니다.<br />요청하신 페이지를 찾을 수 없거나, 오류가 발생하였습니다.</dt>
            <dd>
                <p>페이지의 주소가 잘못 입력되었거나, 요청하신 페이지의 주소가 변경, 삭제 되어 찾을 수 없습니다.<br />
               이용에 불편을 드려 죄송합니다. </p>
            </dd>
        </dl>

        <div class="button_group error_btns txt-cnt">
            <span><a href="javascript:history.back();" class="btn box-white">이전 페이지 이동</a></span>
            <span><a href="/" class="btn box-blue submit">홈으로 이동</a></span>
        </div>

	</div>
	<!-- //ly_container -->	

</div>
<!-- //sub_contents -->

<script language="javascript">

    $(function(){    
        $(".aside_menu").hide();
    });
</script>


<%@include file="../inc/inc_gl_bottom.jsp"%>
