<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents member">
	<div class="ly_container bg_white">
		<div class="search_member">     
	       	        	
	        	<dl class="inactive_tit"> 
					<dt><strong>스타시티 휴면계정</strong>해제 안내</dt>
					<dd>
						<p>스타시티를 다시 방문해주셔서 감사합니다.<br />
						회원님의 계정 휴면 해제가 완료되어 이제 정상적으로 스타시티를 이용하실 수 있습니다.</p>
					</dd>
				</dl>

				<div class="inactive_con">
		        	<div class="line_wrap ph50">
		        		<p class="tit pt0 pb40">회원님의 소중한 개인정보 보호를 위해 비밀번호를 변경해주세요.</p>	
			        	<div class="search_box_pw">
		        			<table class="common_tb_col register_form">
					          <caption>아이디, 패스워드</caption>
					          <colgroup>
					            <col width="150px">
					            <col width="*">
					          </colgroup>					          
					          <tbody>					          	                				              				             
					          	 <tr>
					                  <th scope="row">현재 비밀번호 </th>
					                  <td>
										<input type="password" name="hPW">
					                  </td>
					              </tr>
					              <tr>
					                  <th scope="row">새 비밀번호 </th>
					                  <td>
										<input type="password" name="hNewPW2">
					                  </td>
					              </tr>			              
					              <tr>
					                  <th scope="row">새 비밀번호 확인 </th>
					                  <td>
										<input type="password" name="hNewPW2">
					                  </td>
					              </tr>
				              	</tbody>
				              </table>
				        </div>
				        <!-- //search_box_inner -->  

				        <div class="button_group txt-cnt pt50">			
			        		<span><a href="#" class="btn box-white">다음에변경</a></span>	
							<span><a href="javascript:inaLayer.open();" class="btn box-blue submit">변경완료</a></span>
						</div>
		        	</div>
		        	<!-- //line_wrap -->

		        	<p class="icon_alert_tit">안내</p>
			        <ul class="bl_line">
						<li>비밀번호는 8~20자의 영문 대 소문자, 숫자, 특수문자를 이용하여 입력해주세요.</li>
						<li>연속된 번호 또는 주민등록번호, 생일, 전화번호 등의 개인정보 관련된 숫자는 고객님의 소중한 개인정보 보호를 위해 사용하지 않도록 주의하시기 바랍니다.  </li>
					</ul> 
				</div>

				<div class="inactive_ok" style="display: none;">
					<div class="button_group txt-cnt">			
						<span><a href="/" class="btn box-blue submit">홈화면으로 이동</a></span>
					</div>
				</div>

		</div>
	</div>
	<!-- //ly_container -->	

</div>
<!-- //sub_contents -->

<script language="javascript">	

	//인증레이어 
	var inaLayer = {
	    open: function () {
	        $(".inactive_ok").css("display","block");
	        $(".inactive_con").css("display","none");
	    },
	    close: function () {        	
	    	$(".inactive_ok").css("display","none");
	        $(".inactive_con").css("display","block");
    	}
    };	

</script>


<%@include file="../inc/inc_gl_bottom.jsp"%>
