<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents member">
	<div class="ly_container bg_white">
		<p class="join_logo"><img src="/resources/user/img/contents/member/join_logo.png" alt="starcity"></p>
		<div class="join_gate">			
			<div class="left_sec">
				<dl>
					<dt>일반 회원가입</dt>
					<dd>스타시티회원은 만 14세 이상 성인만 가입 가능합니다.</dd>
					<dd class="btn_in">
						<a href="#" class="btn bg_join_blue">일반 회원가입</a>		
					</dd>					
				</dl>				
			</div>
			<div class="right_sec">
				<dl>
					<dt>SNS계정으로 회원가입</dt>
					<dd>SNS계정인증을 통해 스타시티 회원으로 가입합니다.</dd>
					<dd class="btn_in">						
						<a href="#" class="btn bg_naver"><i class="ico ico_join_naver"></i>네이버 아이디로 회원가입</a>
						<a href="#" class="btn bg_facebook"><i class="ico ico_join_face"></i>페이스북 아이디로 회원가입</a>
						<a href="#" class="btn bg_cacao"><i class="ico ico_join_cacao"></i>카카오 아이디로 회원가입</a>
					</dd>					
				</dl>
			</div>
		</div>
	</div>
	<!-- //ly_container -->
</div>
<!-- //sub_contents -->


<%@include file="../inc/inc_gl_bottom.jsp"%>
