<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents member">
	<div class="ly_container bg_white">
		<div class="search_member">
			
			<ul class="tab_boxline tablist_2">
	            <li><a href="search_id" class="on">아이디 찾기</a></li>
	            <li><a href="search_pw">비밀번호 찾기</a></li>
	        </ul>
	        <p class="tit">고객님이 조회하신 아이디 입니다.</p>

	        <div class="line_wrap mb20">
	        	<dl class="result_txt">
        			<dt>FACEBOOK_***</dt>
        			<dd>가입 : 2019.04.30</dd>
	        	</dl>
	        	<dl class="result_txt">
        			<dt>FACEBOOK_***</dt>
        			<dd>가입 : 2019.04.30</dd>
	        	</dl>
	        	<dl class="result_txt">
        			<dt>FACEBOOK_***</dt>
        			<dd>가입 : 2019.04.30</dd>
	        	</dl>

	        	<div class="button_group txt-cnt pt60">				
					<span><a href="javascript:;" class="btn box-navy">본인인증을 통해 전체 아이디 조회</a></span>
				</div>

	        </div>
	        <!-- //line_wrap -->

	        <p class="icon_alert_tit">안내</p>
	        <ul class="bl_line">
				<li>필수 : 성명, 생년월일, 주소, 이메일 주소, 핸드폰 번호, 아이디, 비밀번호, 성별</li>
				<li>수집방법 : 홈페이지, 이벤트 응모, 이메일, 콜센터</li>
			</ul>



	        <div class="button_group txt-cnt pt50">				
	        	<span><a href="search_pw" class="btn box-white">비밀번호 찾기</a></span>
				<span><a href="javascript:;" class="btn box-blue">아이디 찾기</a></span>
			</div>


		</div>
	</div>
	<!-- //ly_container -->	

</div>
<!-- //sub_contents -->


<!-- sns로그인 팝업 시작-->
<div id="confirmLayerBox" class="layerPopup confirm_layer">
	<div class="layerPopup_in">
		<span class="closeLayer">
			<a href="javascript:LayerPopups.close('confirmLayerBox');" class="btn"><i class="lnr lnr-cross"></i></a>
		</span>
		<div class="layer_txt">
			<dl class="submit_txt">
				<dt class="pt40">SNS 간편 로그인 안내</dt>
				<dd>SNS 간편 로그인은 해당 SNS계정으로<br />
				회원가입 후 이용 가능합니다.<br />
				회원가입 페이지로 이동하시겠습니까?
				</dd>
			</dl>				
		</div>
		<div class="button_group txt-cnt">
			<span><a href="javascript:LayerPopups.close('confirmLayerBox');" class="btn btn_round_nm">취소</a></span>
			<span><a href="javascript:;" class="btn btn_round_blue_bg">확인</a></span>
		</div>
	</div>
</div> 
<!-- //sns로그인 팝업 -->


<script language="javascript">
	
	function loginSns() {
		javascript:LayerPopups.open('confirmLayerBox');
	}
	
	(function (ns) {
		$(function(){	
			
			$('input[type=radio][name=fInfo]').on('change', function() {
			     switch($(this).val()) {
			         case 'myinfo':
			             $(".mem_infocon").show();
			             $(".mem_authcon").hide();
			             break;
			         case 'myauth':
			             $(".mem_infocon").hide();
			             $(".mem_authcon").show();
			             break;
			     }
			});

		   ns.placeholder.init();
		});
	 })(APP || {});
</script>

<%@include file="../inc/inc_gl_bottom.jsp"%>
