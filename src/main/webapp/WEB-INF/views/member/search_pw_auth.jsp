<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents member">
	<div class="ly_container bg_white">
		<div class="search_member">
			
			<ul class="tab_boxline tablist_2">
	            <li><a href="search_id">아이디 찾기</a></li>
	            <li><a href="search_pw" class="on">비밀번호 찾기</a></li>
	        </ul>	        

	        <div class="auth_con">
		        <div class="line_wrap mt50 ph50">
		        	<p class="tit pt0 pb20">본인인증을 통해 비밀번호를 재설정하실 수 있습니다.</p>
		        	<div class="auth_list">							
						<ul>
							<li>
								<a href="javascript:authLayer.open();">
									<span class="auth_icon auth_1"></span>휴대전화 인증
								</a>					
				        	</li>
				        	<li>
								<a href="javascript:authLayer.open();">
									<span class="auth_icon auth_2"></span>아이핀 인증
								</a>					
				        	</li>
			        	</ul>
					</div>      	
		        </div>
		        <!-- //line_wrap -->
	    	</div>
	    	<!-- //auth_con -->

	        <div class="auth_ok_con" style="display: none;">
	        	<p class="tit">새로운 비밀번호를 등록해주세요.</p>
	        	<div class="line_wrap ph50">	        	
		        	<div class="search_box_pw">
	        			<table class="common_tb_col register_form">
				          <caption>아이디, 패스워드</caption>
				          <colgroup>
				            <col width="150px">
				            <col width="*">
				          </colgroup>					          
				          <tbody>					          	                				              				             
				              <tr>
				                  <th scope="row">새 비밀번호 </th>
				                  <td>
									<input type="password" name="hNewPW">
				                  </td>
				              </tr>			              
				              <tr>
				                  <th scope="row">새 비밀번호 확인 </th>
				                  <td>
									<input type="password" name="hNewPW">
				                  </td>
				              </tr>
			              	</tbody>
			              </table>
			        </div>
			        <!-- //search_box_inner -->  
	        	</div>
	        	<!-- //line_wrap -->

	        	<p class="icon_alert_tit">안내</p>
		        <ul class="bl_line">
					<li>필수 : 성명, 생년월일, 주소, 이메일 주소, 핸드폰 번호, 아이디, 비밀번호, 성별</li>
					<li>수집방법 : 홈페이지, 이벤트 응모, 이메일, 콜센터</li>
				</ul> 

	        	<div class="button_group txt-cnt pt50">					
					<span><a href="javascript:authLayer.close();" class="btn box-blue submit">확인</a></span>
				</div>
	        </div>
	        <!-- //auth_ok_con -->

		</div>
	</div>
	<!-- //ly_container -->	

</div>
<!-- //sub_contents -->

<script language="javascript">	

	//인증레이어 
	var authLayer = {
	    open: function () {
	        $(".auth_ok_con").css("display","block");
	        $(".auth_con").css("display","none");
	    },
	    close: function () {        	
	    	$(".auth_ok_con").css("display","none");
	        $(".auth_con").css("display","block");
    	}
    };	

</script>


<%@include file="../inc/inc_gl_bottom.jsp"%>
