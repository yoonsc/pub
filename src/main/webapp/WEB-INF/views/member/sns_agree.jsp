<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents member">
	<div class="ly_container bg_white">
		<div class="join_agree">
			<ul class="step_list sns_step">				
				<li class="on">
					<span class="step_icon step_1"></span>
					<dl>
						<dt>Step1</dt>
						<dd>약관동의</dd>
					</dl>
				</li>
				<li>
					<span class="step_icon step_3"></span>
					<dl>
						<dt>Step2</dt>
						<dd>정보입력</dd>
					</dl>
				</li>
				<li>
					<span class="step_icon step_4"></span>
					<dl>
						<dt>Step3</dt>
						<dd>가입완료</dd>
					</dl>
				</li>
			</ul>

			<ul class="agree_list">
				<li>
					<input type="checkbox" name="agreechk_1" class="checkbox" ><label class="label"></label> 
	            	<span class="txt">스타시티 이용약관, 개인정보 수집 및 이용동의, 이벤트 및 프로모션 안내메일 수신(선택)에 모두 동의 합니다.</span>
	        	</li>
	        	<li>
					<input type="checkbox" name="agreechk_2" class="checkbox" ><label class="label"></label> 
	            	<span class="txt">개인정보 수집 및 이용동의 <strong class="txt-color-blue">(필수)</strong></span>
	            	<span class="more_btn"><a href="javascript:LayerPopups.open('agreeLayerBox_1', true);" class="btn box-navy">자세히 보기</a></span>
	        	</li>
	        	<li>
					<input type="checkbox" name="agreechk_4" class="checkbox" ><label class="label"></label> 
	            	<span class="txt">만 14세 이상 성인입니다 <strong class="txt-color-blue">(필수)</strong></span>
	            	<span class="more_btn"><a href="javascript:LayerPopups.open('agreeLayerBox_3', true);" class="btn box-navy">자세히 보기</a></span>
	        	</li>
	        	<li>
					<input type="checkbox" name="agreechk_3" class="checkbox" ><label class="label"></label> 
	            	<span class="txt">이벤트 및 프로모션 알림 메일 수신 <strong class="txt-color-blue">(선택)</strong></span>
	            	<span class="more_btn"><a href="javascript:LayerPopups.open('agreeLayerBox_2', true);" class="btn box-navy">자세히 보기</a></span>
	        	</li>
			</ul>


			<div class="button_group txt-cnt">
				<span><a href="javascript:;" class="btn box-white">비동의</a></span>
				<span><a href="javascript:;" class="btn box-blue">동의</a></span>
			</div>


		</div>
	</div>
	<!-- //ly_container -->

	<!-- 개인정보 팝업 시작-->
	<div id="agreeLayerBox_1" class="layerPopup agree_layer">
		<div class="layerPopup_in">
			<span class="closeLayer">
				<a href="javascript:LayerPopups.close('agreeLayerBox_1');" class="btn"><i class="lnr lnr-cross"></i></a>
			</span>
			<div class="layer_txt">	
				<h3 class="agree_tit">개인정보 수집 및 이용동의</h3>
				<div class="privacy_con register_form">
					<h3>개인정보 수집 및 이용에 대한 동의</h3>
					<h4>제1조 수집하는 개인정보의 항목</h4>
					<ul class="bl_line">
						<li>필수 : 성명, 생년월일, 주소, 이메일 주소, 핸드폰 번호, 아이디, 비밀번호, 성별</li>
						<li>수집방법 : 홈페이지, 이벤트 응모, 이메일, 콜센터</li>
					</ul>

					<h4>제2조 개인정보 수집 이용 목적</h4>
					<ul class="bl_line">
						<li>회원 서비스 제공 및 본인 확인 절차에 활용</li>
						<li>회원에 대한 각종 편의 서비스 및 혜택 제공, 공지사항 전달 및 본인의사 확인, 민원처리, 사고조사 등을 위한 원활한 의사소통 경로 확보</li>
					</ul>

					<h4>제3조 개인정보 보유 및 이용기간</h4>
					<ul class="bl_line">
						<li>개인정보는 개인정보 삭제 및 회원 탈퇴를 요청할 때까지 보유ㆍ이용합니다. 고객으로부터 개인정보 즉시 파기 요청이 없을 경우 탈퇴 신청 후 즉시 파기합니다.</li>
					</ul>

					<h4>제4조 개인정보의 수집 주체</h4>
					<ul class="bl_line">
						<li>수집 주체 : 건국AMC</li>
					</ul>

					<ul class="comment">
                 		<li>고객님께서는 개인정보 수집/이용 동의를 거부할 권리가 있습니다. 단, 동의 거부 시에는 회원가입이 불가하여 상기 이용목적에 명시된 서비스를 받으실 수 없습니다.</li>
                 	</ul>

				</div>
				<!--// privacy_con -->						
			</div>
			<!-- //layer_txt -->
		</div>
		<!-- //layerPopup_in -->
	</div> 
	<!-- //개인정보 팝업 -->

	<!-- 마케팅 활용 팝업 시작-->
	<div id="agreeLayerBox_2" class="layerPopup agree_layer">
		<div class="layerPopup_in">
			<span class="closeLayer">
				<a href="javascript:LayerPopups.close('agreeLayerBox_2');" class="btn"><i class="lnr lnr-cross"></i></a>
			</span>
			<div class="layer_txt">	
				<h3 class="agree_tit">개인정보 마케팅 활용 동의</h3>
				<div class="privacy_con register_form">
					<h3>개인정보 마케팅 활용 동의</h3>
					<h4>제1조 수집하는 개인정보 항목</h4>
					<ul class="bl_line">
						<li>성명, 생년월일, 주소, 이메일주소, 핸드폰번호, 아이디, 비밀번호, 성별</li>
					</ul>

					<h4>제2조  개인정보의 수집 및 이용 목적</h4>
					<ul class="bl_line">
						<li>건국AMC 및 관계사가 제공하는 상품 및 서비스 안내, 소식지 제공, 이벤트 정보 제공, 제휴행사 및 서비스 홍보를 위한 마케팅 활용, 마케팅을 위한 고객정보분석 및<br/> 서비스 개발</li>
					</ul>

					<h4>제3조 개인정보 보유 및 이용기간</h4>
					<ul class="bl_line">
						<li>관계 법령의 규정에 따라 고객의 개인정보를 보존할 의무가 있는 경우가 아닌 한, 회원 탈퇴 신청 후 지체없이 파기</li>
						<li>마케팅 활용 정보 수신 여부에 대해서는 동의를 거부할 권리가 있습니다. 이 경우 회원가입은 가능하지만 이메일과 문자를 통해 자사의 뉴스레터, 경품 당첨 소식, 이벤트 정보 등의 유용한 정보안내를 받으실 수 없습니다.
						</li>
					</ul>
				</div>
				<!--// privacy_con -->						
			</div>
			<!-- //layer_txt -->
		</div>
		<!-- //layerPopup_in -->
	</div> 
	<!-- //마케팅 활용 팝업 -->

	<!--  만 14세 이상 팝업 시작-->
	<div id="agreeLayerBox_3" class="layerPopup agree_layer">
		<div class="layerPopup_in">
			<span class="closeLayer">
				<a href="javascript:LayerPopups.close('agreeLayerBox_3');" class="btn"><i class="lnr lnr-cross"></i></a>
			</span>
			<div class="layer_txt">	
				<h3 class="agree_tit">만 14세 이하 가입불가 안내</h3>
				<div class="privacy_con register_form">
					<div class="summary">
						<p>㈜건국AMC가 운영하는 스타시티 웹사이트에서는 만 14세 미만 아동의 회원가입을 제한하고 있어, 회원가입 하실 수 없습니다.</p>
						<p>만 14세 미만 아동이 회원가입을 하는 경우 회원탈퇴 또는 서비스이용이 제한 될 수 있습니다.</p>
					</div>

				</div>
				<!--// privacy_con -->						
			</div>
			<!-- //layer_txt -->
		</div>
		<!-- //layerPopup_in -->
	</div> 
	<!-- //만 14세 이상 팝업 -->


</div>
<!-- //sub_contents -->

<script language="javascript">

	$(function(){	 
		var chkthis = $(".agree_list>li").find(":checkbox");
	    chkthis.click(function(){     
	        if($(this).is(":checked")){  
	            $(this).parent().addClass('on');   
	        } else {
	            $(this).parent().removeClass('on');      
	        }
	    });
	});
</script>


<%@include file="../inc/inc_gl_bottom.jsp"%>
