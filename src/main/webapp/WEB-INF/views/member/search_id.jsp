<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents member">
	<div class="ly_container bg_white">
		<div class="search_member">
			
			<ul class="tab_boxline tablist_2">
	            <li><a href="search_id" class="on">아이디 찾기</a></li>
	            <li><a href="search_pw">비밀번호 찾기</a></li>
	        </ul>
	        <p class="tit">등록된 회원정보 또는 본인인증을 통해 아이디를 찾으실 수 있습니다.</p>

	        <div class="line_wrap">
	        	<dl class="search_chg_box">
	        		<dt>
	        			<label><input type="radio" name="fInfo" class="radio" value="myinfo" checked>
	        			<span class="label"></span> 등록된 회원정보로 찾기</label> 	        			
                    </dt>
                    <dd class="mem_infocon">
                    	<ul class="bl_dot">
	        				<li>회원정보에 등록된 이름, 생년월일, 휴대전화 번호가 일치해야지만 아이디를 확인할 수 있습니다.</li>
	        				<li>조회된 아이디는 일부 마스킹처리되어 표시되며, 전체 아이디를 확인하시려면 본인인증을 진행해주세요.</li>
	        			</ul>

                    	<div class="search_box_inner">
			        		<table class="common_tb_col register_form join_register">
				          <caption>문의분야, 이름, 이메일, 전화번호, 내용을 작성하는 등록서식</caption>
				          <colgroup>
				            <col width="120px">
				            <col width="*">
				          </colgroup>					          
					          <tbody>					          	                				              				             
					              <tr>
					                  <th scope="row">이름 </th>
					                  <td>
										<input type="text" name="name" id="hName">
					                  </td>
					              </tr>
					              <tr>
						              <th scope="row">생년월일 </th>
					                  <td>
					                  	<input type="text" name="hBirth" id="hBirth">					                  	
					                  </td>
				              	  </tr>
				              	  <tr>
						              <th scope="row"></th>
					                  <td>					                  	
					                  	<ul class="comment pt0">
					                 		<li>생년월일 8자리 입력.  ex) 19810507</li>
					                 	</ul>
					                  </td>
				              	  </tr>

				                  <tr>
					                  <th scope="row">휴대전화 번호</th>
					                  <td class="phone_cell">
										<span class="select-wrapper line_select">
											<select title="전화번호선택" id="hTel1" name="hTel1" >
												<option value="">선택</option>
												<option value="010">010</option>
												<option value="011">011</option>
												<option value="018">018</option>
												<option value="016">016</option>
												<option value="017">017</option>
											</select>     
										</span> - 
										<span>
											<input type="text" name="hTel2" id="hTel2" class="number-check"  maxlength="4" title="전화번호중간번호" style="width:150px;" onkeydown="onlyNumber(this);" > - 
										</span>						
										<span>									
											<input type="text" name="hTel3" id="hTel3" class="number-check" maxlength="4" title="전화번호끝번호" style="width:150px;" onkeydown="onlyNumber(this);" >
										</span>
										<input type="hidden" name="tel">
					                  </td>
					              </tr>
					          </tbody>
					      </table>
					  	</div>
					  	<!-- //search_box_inner -->
                    </dd>
	        	</dl>
	        	<!-- //search_chg_box -->

	        	<dl class="search_chg_box">
	        		<dt><label><input type="radio" name="fInfo" class="radio" value="myauth" >
	        			<span class="label"></span> 본인인증으로 찾기</label> 
                    </dt>
                    <dd class="mem_authcon" style="display: none;">
                    	<div class="auth_list">							
							<ul>
								<li>
									<a href="#"><!-- 인증시 : <a href="#" class="on"> -->
										<span class="auth_icon auth_1"></span>휴대전화 인증
									</a>					
					        	</li>
				        	</ul>
						</div>
                    </dd>
	        	</dl>
	        	<!-- //search_chg_box -->

	        </div>
	        <!-- //line_wrap -->


	        <div class="button_group txt-cnt">				
				<span><a href="javascript:;" class="btn box-blue">아이디 찾기</a></span>
			</div>


		</div>
	</div>
	<!-- //ly_container -->	

</div>
<!-- //sub_contents -->


<!-- sns로그인 팝업 시작-->
<div id="confirmLayerBox" class="layerPopup confirm_layer">
	<div class="layerPopup_in">
		<span class="closeLayer">
			<a href="javascript:LayerPopups.close('confirmLayerBox');" class="btn"><i class="lnr lnr-cross"></i></a>
		</span>
		<div class="layer_txt">
			<dl class="submit_txt">
				<dt class="pt40">SNS 간편 로그인 안내</dt>
				<dd>SNS 간편 로그인은 해당 SNS계정으로<br />
				회원가입 후 이용 가능합니다.<br />
				회원가입 페이지로 이동하시겠습니까?
				</dd>
			</dl>				
		</div>
		<div class="button_group txt-cnt">
			<span><a href="javascript:LayerPopups.close('confirmLayerBox');" class="btn btn_round_nm">취소</a></span>
			<span><a href="javascript:;" class="btn btn_round_blue_bg">확인</a></span>
		</div>
	</div>
</div> 
<!-- //sns로그인 팝업 -->


<script language="javascript">
	
	function loginSns() {
		javascript:LayerPopups.open('confirmLayerBox');
	}
	
	(function (ns) {
		$(function(){	
			
			$('input[type=radio][name=fInfo]').on('change', function() {
			     switch($(this).val()) {
			         case 'myinfo':
			             $(".mem_infocon").show();
			             $(".mem_authcon").hide();
			             break;
			         case 'myauth':
			             $(".mem_infocon").hide();
			             $(".mem_authcon").show();
			             break;
			     }
			});

		   ns.placeholder.init();
		});
	 })(APP || {});
</script>

<%@include file="../inc/inc_gl_bottom.jsp"%>
