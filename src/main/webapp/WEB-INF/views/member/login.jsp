<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents member">
	<div class="ly_container bg_white">
		<p class="join_logo"><img src="/resources/user/img/contents/member/join_logo.png" alt="starcity"></p>
		<div class="join_gate">			
			<div class="left_sec">
				<dl>
					<dt>일반 로그인</dt>
					<dd class="login_form">
						<div class="placeholder">
							<label for="hID">아이디</label>
							<input type="text" name="hID" class="boxline" id="hID" style="width: 100%;">
						</div> 
						<div class="placeholder">
							<label for="hID">비밀번호</label>
							<input type="password" name="hPW" class="boxline" id="hPW" style="width: 100%;">
						</div> 
					</dd>
					<dd class="btn_in pt20">
						<a href="#" class="btn bg_join_blue">로그인</a>
					</dd>
					<dd class="chk_in">
						<label>
	                  		<input type="checkbox" name="idChk" class="checkbox" >
	                  		<span class="label"></span>아이디 저장
	                  	</label>
	                  	<ul class="login_link">
	                  		<li><a href="#">아이디 찾기</a></li>
	                  		<li><a href="#">비밀번호 찾기</a></li>
	                  	</ul>
					</dd>										
				</dl>				
			</div>
			<div class="right_sec">
				<dl>
					<dt>간편 로그인</dt>				
					<dd class="btn_in">						
						<a href="javascript:loginSns();" class="btn bg_naver"><i class="ico ico_join_naver"></i>네이버로 로그인</a>
						<a href="javascript:loginSns();" class="btn bg_facebook"><i class="ico ico_join_face"></i>페이스북 로그인</a>
						<a href="javascript:loginSns();" class="btn bg_cacao"><i class="ico ico_join_cacao"></i>카카오톡 로그인</a>
					</dd>					
				</dl>
			</div>
			<!-- //right_sec -->
		</div>
		<!-- //join_gate -->

		<div class="join_info">
			<div class="grid_con">
				<div class="grid_left">
					<div class="tit">
						아직 스타시티 <br/>회원이 아니신가요?
					</div>
				</div>
				<div class="grid_right">
					<div class="txt">지금 <strong class="txt-color-blue">회원가입</strong>을 하시면 스타시티의 다양한 이벤트 및 프로모션에<br/ >참여가 가능하며 정기 뉴스레터를 받으실 수 있습니다. 
					</div>

					<div class="button_group">
						<a href="agree" class="btn btn_round_nm"><i class="ico ico_login_people"></i> 스타시티 회원가입</a>
					</div>

				</div>
			</div>
		</div>

	</div>
	<!-- //ly_container -->	

</div>
<!-- //sub_contents -->


<!-- sns로그인 팝업 시작-->
<div id="confirmLayerBox" class="layerPopup confirm_layer">
	<div class="layerPopup_in">
		<span class="closeLayer">
			<a href="javascript:LayerPopups.close('confirmLayerBox');" class="btn"><i class="lnr lnr-cross"></i></a>
		</span>
		<div class="layer_txt">
			<dl class="submit_txt">
				<dt class="pt40">SNS 간편 로그인 안내</dt>
				<dd>SNS 간편 로그인은 해당 SNS계정으로<br />
				회원가입 후 이용 가능합니다.<br />
				회원가입 페이지로 이동하시겠습니까?
				</dd>
			</dl>				
		</div>
		<div class="button_group txt-cnt">
			<span><a href="javascript:LayerPopups.close('confirmLayerBox');" class="btn btn_round_nm">취소</a></span>
			<span><a href="javascript:;" class="btn btn_round_blue_bg">확인</a></span>
		</div>
	</div>
</div> 
<!-- //sns로그인 팝업 -->


<script language="javascript">
	
	function loginSns() {
		javascript:LayerPopups.open('confirmLayerBox');
	}
	
	(function (ns) {
		$(function(){			   
		   ns.placeholder.init();
		});
	 })(APP || {});
</script>

<%@include file="../inc/inc_gl_bottom.jsp"%>
