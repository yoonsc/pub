<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents floor_convenience">
	<div class="ly_container">
		<div class="bg_info_box">
			편의시설을 선택 하시면 아래 지도에 <strong class="txt-line-blue">해당 편의시설</strong>에 대한 정보가 표시됩니다.
		</div>

		<ul class="conven_icons">
			<li v-for="( obj, k ) in facilities" :key="k">
				<a href="#" :class="{ on : facilitiesId == k }" @click.prevent="changeFacilities( k )"><i class="ico" :class="'ico_conven_' + obj.class"></i>{{ obj.name }}</a>
			</li>
		</ul>

		<div class="map_view_sec">
			<div class="map_pop_box" v-show="facilitiesId != -1">
				<dl>
					<dt>{{ actFacilities.name }}</dt>
					<dd>단지 내 이용 가능한 {{ actFacilities.name }}는<br /><strong>총 {{ actFacilities.total }}곳</strong> 입니다.</dd>
				</dl>
				<p class="infotxt"><em class="txt-color-blue">*</em> 일부 은행에 설치된 현금인출기는 이용시간에 제한이 있을 수 있습니다.</p>
			</div>

			<ul class="info_list">
				<li class="cf_num1 on">
					<dl>
						<dt>시티존</dt>
						<dd><strong>1</strong></dd>
					</dl>
				</li>
				<li class="cf_num2">
					<dl>
						<dt>스타존</dt>
						<dd><strong>2</strong></dd>
					</dl>
				</li>
				<li class="cf_num3">
					<dl>
						<dt>영존</dt>
						<dd><strong>3</strong></dd>
					</dl>
				</li>
				<li class="cf_num4">
					<dl>
						<dt>더 클래식 500</dt>
						<dd><strong>4</strong></dd>
					</dl>
				</li>
			</ul>
			<p><img src="/resources/user/img/contents/about/convenience_map.jpg" alt=""></p>
		</div>
		<!-- //map_view_sec -->
	</div>
	<!-- //ly_container -->


	<div class="ly_container tab_map_con">
		<ul class="tab_boxtype tablist_4 mb30">
			<li class="on"><a href="#">시티존</a></li>
			<li><a href="#">스타존</a></li>
			<li><a href="#">영존</a></li>
			<li><a href="#">더 클래식 500</a></li>
		</ul>

		<div class="floor_map_con">

			<p class="floor_num">B1</p>

			<!-- 지도 편의시설 아이콘 리스트 -->
			<span class="label_icon icon_atm">atm</span>
			<span class="label_icon icon_disabled" style="left: 50px;">장애인</span>
			<span class="label_icon icon_stroller" style="left: 100px;">유모차</span>
			<span class="label_icon icon_smoking" style="left: 150px;">흡연</span>
			<span class="label_icon icon_parking" style="left: 200px;">주차</span>
			<span class="label_icon icon_print" style="left: 250px;">발급기</span>
			<span class="label_icon icon_baby" style="left: 300px;">수유실</span>
			<span class="label_icon icon_meet" style="left: 350px;">만남</span>


			<ul class="floor_navi">
				<li><a href="#">3F</a></li>
				<li><a href="#">2F</a></li>
				<li><a href="#">1F</a></li>
				<li class="parking_f"><a href="#" class="on">B1</a></li>
				<li class="parking_f"><a href="#">B2</a></li>
				<li class="parking_f"><a href="#">B3</a></li>
			</ul>

			<ul class="info_icons">
				<li><i class="ico ico_conven_info1"></i>에스컬레이터</li>
				<li><i class="ico ico_conven_info2"></i>엘리베이터</li>
				<li><i class="ico ico_conven_info3"></i>화장실</li>
				<li><i class="ico ico_conven_info4"></i>현금지급기</li>
				<li><i class="ico ico_conven_info5"></i>지하철 연결</li>
			</ul>
			<p class="floor_map_img"><img src="/resources/user/img/contents/about/floor_map_b1.jpg"></p>

		</div>
		<!-- //floor_map_con -->
	</div>
	<!-- ly_container -->


</div>
<!-- //floor_info -->
<script type="text/javascript" src="/resources/user/js/libs/vue.min.js"></script>
<script type="text/javascript">
	$(function () {
		APP.Convenience.init();
	});
</script>

<%@include file="../inc/inc_gl_bottom.jsp"%>
