<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents overview_bginfo bginfo1">	
	<div class="ly_container">
		<dl>
			<dt class="en_gothic">CITY OF<br />DYNAMIC LIFE</dt>
			<dd>국내 유일의 생활복합 쇼핑공간</dd>
		</dl>	
	</div>
</div>

<div class="sub_contents bg_white ph80">	
	<div class="ly_container lg600 img_line_box">
		<div class="grid_left">
			<img src="/resources/user/img/contents/about/overview_thumb_1.jpg" alt="">
		</div>
		<div class="grid_right line_con">
			<p class="top_line_tit en_gothic t_upper">START CITY shopping Mall</p>
			<dl>
				<dt><strong>별처럼 오랫동안 꿈꿔오던</strong><br />스타시티</dt>
				<dd>2007년 3월 문을 연 스타시티몰은<br />
				쇼핑, 문화, 레저, 편의시설, 그리고 인근에 주거공간까지 갖춘<br />
				국내 최초의 진정한 One-stop Life 쇼핑공간입니다. </dd>
			</dl>			
		</div>

	</div>
	<!-- //ly_container -->
</div>


<div class="sub_contents overview_bginfo bginfo2">	
	<div class="ly_container">
		<dl>
			<dt>강남과 강북이 만나는<br /><strong>이상적인 입지조건</strong></dt>
			<dd>
				<p class="tit">지하철 2호선과 7호선이 만나는 건대입구역과 1분 거리에 위치하고, 올림픽대로와 걍변북로가 가까워 접근이 수월하며,<br />
	주변에 한강, 건국대 캠퍼스, 어린이 대공원, 아차산 등이 위치하고 있습니다.</p>
				<p class="txt">
					지하 1F에는 국내 최대 할인점인 이마트가 입점하고 있으며, 1F에는 패션잡화, 음식점, 커피전문점, 2층~3층에는<br />
					첨단의 시설을 자랑하는 11개관의 롯데시네마와 게임존이 입점하여 특별한 만족과 감동이 있는 공간을 제공해 드립니다.<br />
					롯데백화점 스타시티점은 최고의 품격있는 서비스와 고품격 문화생활 공간이 어우러진 젊은 감각의<br />
					New Life Style을 지향하는 고품격 백화점입니다.
				</p>
			</dd>
		</dl>	
	</div>
</div>

<div class="sub_contents bg_white ph80">	
	<div class="ly_container rg600 over_h img_line_box">
		<div class="grid_left line_con">
			<p class="top_line_tit en_gothic t_upper">Starcity Broadcasting Center</p>
			<dl>
				<dt>스타시티<br /><strong>방송국</strong></dt>
				<dd>스타시티몰 내 단독 방송국인 스타시티방송국(SBC)을 개설하여,<br /> 
				고객들이 즐겁고 색다른 쇼핑을 즐기실 수 있도록 <br />
				국내 최고의 커뮤니티형 음악방송과 테마토크쇼를 방송하고 있습니다. 
				</dd>
			</dl>					
		</div>
		<div class="grid_right">
			<img src="/resources/user/img/contents/about/overview_thumb_2.jpg" alt="">
		</div>

	</div>
	<!-- //ly_container -->
</div>


<div class="sub_contents bg_white pb50">	
	<div class="ly_container lg600 tit_txt_box">
		<div class="grid_left">
			<div class="tit">스타시티 <strong>단지 안내</strong></div>
		</div>
		<div class="grid_right">
			<div class="txt">스타시티는 롯데백화점이 입점해 있는 스타존,  롯데시네마&이마트가 입점해 있는 시티존, <br />건대입구역 3번 출구 영존, 펜타즈 호텔이 있는 더 클래식 500 으로 이루어져 있습니다.</div>
		</div>
	</div>
	<!-- //ly_container -->
</div>

<div class="sub_contents pb100">
	<div class="ly_container">
		<img src="/resources/user/img/contents/about/overview_map.jpg" alt="">
	</div>
</div>



<%@include file="../inc/inc_gl_bottom.jsp"%>