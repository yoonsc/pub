<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<p class="contents_title">스타시티 오시는 길을 안내해드립니다.</p>

<div class="sub_contents bg_white">	
	<div class="ly_container list_con_wrap">
		<ul class="location_box">
			<li>
				<div class="list_con">
					<dl>
						<dt>시티존<br /><strong>(롯데시네마, 이마트 입점 단지)</strong></dt>
						<dd>
							서울특별시 광진구 아차산로 272<br />
							(서울특별시 광진구 자양동 227-7)	
						</dd>
					</dl>
					<div class="button_group txt-cnt">
						<span><a href="javascript:;" class="btn btn_round_blue">지도에서 보기</a></span>
					</div>	
				</div>
			</li>
			<li>
				<div class="list_con">
					<dl>
						<dt>스타존<br /><strong>(롯데백화점 입점 단지)</strong></dt>
						<dd>
							서울특별시 광진구 능동로 92<br />(서울특별시 광진구 자양동 227-342)</dd>
					</dl>
					<div class="button_group txt-cnt">
						<span><a href="javascript:;" class="btn btn_round_blue">지도에서 보기</a></span>
					</div>	
				</div>
			</li>
			<li>
				<div class="list_con">
					<dl>
						<dt>더 클래식 500</dt>
						<dd>서울특별시 광진구 능동로 90<br />(서울특별시 광진구 자양동 227-342)</dd>
					</dl>
					<div class="button_group txt-cnt">
						<span><a href="javascript:;" class="btn btn_round_blue">지도에서 보기</a></span>
					</div>	
				</div>
			</li>
			<li>
				<div class="list_con">
					<dl>
						<dt>영존</dt>
						<dd>서울 광진구 능동로 110 스타시티 영존<br />(서울특별시 광진구 화양동 4-20 )</dd>
					</dl>
					<div class="button_group txt-cnt">
						<span><a href="javascript:;" class="btn btn_round_blue">지도에서 보기</a></span>
					</div>	
				</div>
			</li>
		<ul>
	</div>
</div>

<div class="sub_contents bg_white">	
	<div class="ly_container pt50">
		<img src="/resources/user/img/contents/about/location_map.jpg" alt="">
	</div>
	<div class="ly_container ph40 location_info">
		<div class="grid_left">
			<div class="locaton_wrap">
				<span><i class="ico ico_location_car"></i></span>
				<p class="tit">자동차로 오시는 길 </p>
				<dl>
					<dt>영동대교 이용 </dt>
					<dd>영동대교 북단 → 성수사거리 우회전 →  구의로 →  건대입구역 사거리 
					→ 스타시티 입구 우회전 </dd>
					</dl>
				<dl>
					<dt>청담대교 이용</dt>
					<dd>청담대교 북단 → 건대입구역 사거리 우회전 → 스타시티 입구 우회전</dd>
					</dl>
				<dl>
					<dt>잠실대교 이용</dt>
					<dd>잠실대교 북단 → 자양사거리 좌회전 → 구의로 
					→ 건국대학교 일감문 사거리 자회전 </dd>
					</dl>
				<dl>
					<dt>경부고속도로 </dt>
					<dd>서울톨게이트 → 구리.판교간 고속도로 → 올림픽대로 → 잠실대교 → 구의로 
					→ 건국대학교 일감문사거리 좌회전</dd>
					</dl>
				<dl>	
					<dt>중부고속도로 </dt>
					<dd>동서울 톨게이트 → 서하남 → 올림픽대로 → 구의로 
					→  건국대학교 일감문 사거리 좌회전</dd>
				</dl>
			</div>
		</div>
		<div class="grid_right">
			<div class="locaton_wrap">
				<span><i class="ico ico_location_subway"></i></span>
				<p class="tit">지하철로 오시는 길 </p>
				<p class="txt">2호선/7호선 건대입구역 5번 출구에서 190m (도보 3분) </p>
			</div>
			<div class="locaton_wrap">
				<span><i class="ico ico_location_bus"></i></span>
				<p class="tit">버스로 오시는 길 </p>
				<p class="txt pb10">건대역(05204) 에서 하차 하여 148m (도보 2분)</p>
				<p class="txt">
				- 간선 : 102, 240, 721 <br />
				- 지선 : 2222, 2223, 2224, 3217, 3220, 4212<br />
				- 광역 : 8133<br />
				- 공항버스 : 6013
				</p>
			</div>
		</div>
	</div>
</div>




<%@include file="../inc/inc_gl_bottom.jsp"%>