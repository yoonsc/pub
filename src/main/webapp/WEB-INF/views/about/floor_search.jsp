<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>


<p class="contents_title">찾으시는 매장의 카테고리 선택 또는 키워드를 입력해 주세요.</p>
<div class="sub_contents floor_search_con">
	<div class="sub_menu_bg"></div>
	<nav class="floor_nav">
		<ul class="floor_nav_menu">
		   <li><a href=""><i class="ico ico_nav_all"></i><span class="m_txt">전체</span></a>
		   		<ul class="sub_menu">
		   		</ul>
		   </li>
		   <li><a href=""><i class="ico ico_nav_shop"></i><span class="m_txt">라이프스타일</span><em></em></a>
		   		<ul class="sub_menu">
		   			<li><a href="#">전체2</a></li>
		   			<li><a href="#">대형마트&middot;편의점</a></li>
		   			<li><a href="#">국·건강식품&middot;세탁</a></li>
		   			<li><a href="#">엔터테인먼트</a></li>
		   			<li><a href="#">은행</a></li>
		   			<li><a href="#">헤어&middot;뷰티</a></li>
		   			<li><a href="#">이동통신</a></li>
		   			<li><a href="#">인테리어&middot;부동산&middot;기타</a></li>
		   		</ul>
		   </li>
		   <li><a href=""><i class="ico ico_nav_food"></i><span class="m_txt">푸드</span><em></em></a>
		   		<ul class="sub_menu">
		   			<li><a href="#">전체3</a></li>
		   			<li><a href="#">대형마트&middot;편의점</a></li>
		   			<li><a href="#">국·건강식품&middot;세탁</a></li>
		   			<li><a href="#">엔터테인먼트</a></li>
		   			<li><a href="#">은행</a></li>
		   			<li><a href="#">헤어&middot;뷰티</a></li>
		   			<li><a href="#">이동통신</a></li>
		   			<li><a href="#">인테리어&middot;부동산&middot;기타</a></li>
		   		</ul>
		   </li>
		   <li><a href=""><i class="ico ico_nav_coffee"></i><span class="m_txt">카페&디저트</span><em></em></a>
		   		<ul class="sub_menu">
		   			<li><a href="#">전체4</a></li>
		   			<li><a href="#">대형마트&middot;편의점</a></li>
		   			<li><a href="#">국·건강식품&middot;세탁</a></li>
		   			<li><a href="#">엔터테인먼트</a></li>
		   			<li><a href="#">은행</a></li>
		   			<li><a href="#">헤어&middot;뷰티</a></li>
		   			<li><a href="#">이동통신</a></li>
		   			<li><a href="#">인테리어&middot;부동산&middot;기타</a></li>
		   		</ul>
		   </li>
		   <li><a href=""><i class="ico ico_nav_beauty"></i><span class="m_txt">뷰티</span><em></em></a>
		   		<ul class="sub_menu">
		   			<li><a href="#">전체5</a></li>
		   			<li><a href="#">대형마트&middot;편의점</a></li>
		   			<li><a href="#">국·건강식품&middot;세탁</a></li>
		   			<li><a href="#">엔터테인먼트</a></li>
		   			<li><a href="#">은행</a></li>
		   			<li><a href="#">헤어&middot;뷰티</a></li>
		   			<li><a href="#">이동통신</a></li>
		   			<li><a href="#">인테리어&middot;부동산&middot;기타</a></li>
		   		</ul>
		   </li>
		   <li><a href=""><i class="ico ico_nav_fashion"></i><span class="m_txt">패션</span><em></em></a>
		   		<ul class="sub_menu">
		   			<li><a href="#">전체6</a></li>
		   			<li><a href="#">대형마트&middot;편의점</a></li>
		   			<li><a href="#">국·건강식품&middot;세탁</a></li>
		   			<li><a href="#">엔터테인먼트</a></li>
		   			<li><a href="#">은행</a></li>
		   			<li><a href="#">헤어&middot;뷰티</a></li>
		   			<li><a href="#">이동통신</a></li>
		   			<li><a href="#">인테리어&middot;부동산&middot;기타</a></li>
		   		</ul>
		   </li>
		   <li><a href=""><i class="ico ico_nav_other"></i><span class="m_txt">잡화</span><em></em></a>
		   		<ul class="sub_menu">
		   			<li><a href="#">전체7</a></li>
		   			<li><a href="#">대형마트&middot;편의점</a></li>
		   			<li><a href="#">국·건강식품&middot;세탁</a></li>
		   			<li><a href="#">엔터테인먼트</a></li>
		   			<li><a href="#">은행</a></li>
		   			<li><a href="#">헤어&middot;뷰티</a></li>
		   			<li><a href="#">이동통신</a></li>
		   			<li><a href="#">인테리어&middot;부동산&middot;기타</a></li>
		   		</ul>
		   </li>
		   <li><a href=""><i class="ico ico_nav_service"></i><span class="m_txt">서비스</span><em></em></a>
		   		<ul class="sub_menu">
		   			<li><a href="#">전체8</a></li>
		   			<li><a href="#">대형마트&middot;편의점</a></li>
		   			<li><a href="#">국·건강식품&middot;세탁</a></li>
		   			<li><a href="#">엔터테인먼트</a></li>
		   			<li><a href="#">은행</a></li>
		   			<li><a href="#">헤어&middot;뷰티</a></li>
		   			<li><a href="#">이동통신</a></li>
		   			<li><a href="#">인테리어&middot;부동산&middot;기타</a></li>
		   		</ul>
		   </li>
		</ul>
	</nav>
	<!-- //floor_nav -->

	<div class="select_box_top">
		<span class="search_in">
			<div class="placeholder">
				<label for="text">매장명 또는 키워드를 입력하세요</label>
				<input type="text" name="text" id="text" value="">
			</div>
			<a href="javascript:fn_search(document.listForm);" class="btn btn-search">검색</a>
		</span>
	</div>

</div>
<!-- //floor_search -->

<div class="floor_list_wrap">
	<div class="ly_container">
		<ul class="tab_boxtype tablist_5">
		    <li class="on"><a href="javascript:;">전체(250)</a></li>
		    <li><a href="javascript:;">스타존(95)</a></li>
		    <li><a href="javascript:;">시티존(26)</a></li>
		    <li><a href="javascript:;">영존(35)</a></li>
		    <li><a href="javascript:;">더 클래식 500(24)</a></li>
		</ul>
	</div>
	<!-- //ly_container -->


	<div class="ly_container list_con_wrap">
		<ul class="floor_shop_list">
			<li class="no_result">
				<img src="/resources/user/img/floor_search_no.png" alt="검색어와 일치하는 매장이 없습니다.">
			</li>
		    <li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">매드포갈릭매드포갈릭매드포갈릭매드포갈릭매드포갈릭매드포갈릭매드포갈릭</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_0.jpg" alt="매드포갈릭"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>매드포갈릭</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">롯데백화점</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit on">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_1.jpg" alt="롯데백화점"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>롯데백화점</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">윈더플레이스</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_2.jpg" alt="윈더플레이스"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>윈더플레이스</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">스타시티</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_3.jpg" alt="스타시티"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>스타시티</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">세이코</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_4.jpg" alt="세이코"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>세이코</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">매드포갈릭매드포갈릭매드포갈릭매드포갈릭매드포갈릭매드포갈릭매드포갈릭</span></p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_0.jpg" alt="매드포갈릭"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>매드포갈릭</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">롯데백화점</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_1.jpg" alt="롯데백화점"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>롯데백화점</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">윈더플레이스</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_2.jpg" alt="윈더플레이스"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>윈더플레이스</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">스타시티</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_3.jpg" alt="스타시티"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>스타시티</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">세이코</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_4.jpg" alt="세이코"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>세이코</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>

			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">매드포갈릭매드포갈릭매드포갈릭매드포갈릭매드포갈릭매드포갈릭매드포갈릭</span></p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_0.jpg" alt="매드포갈릭"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>매드포갈릭</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">롯데백화점</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_1.jpg" alt="롯데백화점"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>롯데백화점</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">윈더플레이스</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_2.jpg" alt="윈더플레이스"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>윈더플레이스</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">스타시티</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_3.jpg" alt="스타시티"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>스타시티</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">세이코</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_4.jpg" alt="세이코"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>세이코</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>

			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">매드포갈릭매드포갈릭매드포갈릭매드포갈릭매드포갈릭매드포갈릭매드포갈릭</span></p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_0.jpg" alt="매드포갈릭"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>매드포갈릭</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">롯데백화점</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_1.jpg" alt="롯데백화점"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>롯데백화점</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">윈더플레이스</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_2.jpg" alt="윈더플레이스"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>윈더플레이스</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">스타시티</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_3.jpg" alt="스타시티"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>스타시티</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="detail_box">
		    			<p class="shop_tit">세이코</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon favorit">즐겨찾기</a>
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_4.jpg" alt="세이코"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>세이코</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>
		    	</div>
			</li>
		</ul>

		<div class="pagination">
			<ul>
				<!-- <li><a href="#none" onclick="fn_page();" class="first pagbtn">처음</a></li> -->
				<li><a href="#" onclick="fn_page();" class="prev pagbtn">이전</a></li>
				<li><strong>1</strong></li>
				<li><a href="#none" onclick="fn_page(2);">2</a></li>
				<li><a href="#none" onclick="fn_page(3);">3</a></li>
				<li><a href="#none" onclick="fn_page(4);">4</a></li>
				<li><a href="#none" onclick="fn_page(5);">5</a></li>
				<li><a href="#none" onclick="fn_page(6);">6</a></li>
				<li><a href="#none" onclick="fn_page(7);">7</a></li>
				<li><a href="#none" onclick="fn_page(8);">8</a></li>
				<li><a href="#none" onclick="fn_page();" class="next pagbtn">다음</a></li>
				<!-- <li><a href="#" onclick="fn_page();" class="last pagbtn">마지막</a></li> -->
			</ul>
		</div>

	</div>


</div>





<script type="text/javascript">
     (function (ns) {
        $(function(){
        	$('#searchText').keydown(function (key) {
			   if (key.keyCode == 13) {
					$(".btn-search").trigger("click");
			   }
		   });
		   ns.placeholder.init();
		   ns.fl_listHover.init(".list_con");

		   //샵메뉴세팅
		   ns.shopgnb.init();
		   ns.shopgnb.setMenu(0);
        });
     })(APP || {});
</script>




<%@include file="../inc/inc_gl_bottom.jsp"%>
