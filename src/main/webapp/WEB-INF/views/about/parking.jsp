<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents bg_white">	
	<div class="ly_container">
		<ul class="tab_boxtype tablist_4" id="parkingTab">
		    <li class="on"><a href="javascript:;">시티존</a></li>
		    <li><a href="javascript:;">스타존</a></li>
		    <li><a href="javascript:;">영존</a></li>
		    <li><a href="javascript:;">더 클래식 500</a></li>
		</ul>

		<div class="tab_content_wrap ph50">
			
			<div class="tab_content">
				<p><img src="/resources/user/img/contents/about/parking_city.jpg" alt=""></p>
				<div class="parking_list">
					<h3>시티존 주차 안내 (이마트/롯데시네마 방향)</h3>
					<div class="con_wrap">
						<div class="grid_left">
							<dl>
								<dt><i class="ico ico_parking_time"></i></dt>
								<dd class="tit_cell">운영시간</dd>
								<dd><p>24시간 운영</p></dd>
							</dl>
							<dl>
								<dt><i class="ico ico_parking_phone"></i></dt>
								<dd class="tit_cell">문의전화</dd>
								<dd>
									<p>주간: 02-2024-1590</p>
									<p>야간: 02-2024-1500</p>
								</dd>
							</dl>
						</div> 
						<div class="grid_right">
							<ul class="bl_list">
								<li>* 롯데백화점 주차장과 쇼핑몰 상호간 통합적용 되지 않습니다.</li>
								<li>* 전 매장에 주차 할인 인증기가 설치돼 편리하게  주차인증을 받으실 수 있습니다.</li>
								<li><span class="txt-color-blue">* 무료주차 시간에는 최초 30분이 포함되어 있습니다.</span></li>
								<li><span class="txt-color-blue">* 주차요금 정산 후에는 재정산 및 환불이 불가합니다.</span></li>
							</ul>
						</div>
					</div>					
				</div>
				<!-- //parking_list-->

				<div class="parking_list">
					<h3>주차요금 안내</h3>
					<div class="con_wrap">
						<div class="grid_left">
							<dl>
								<dt><i class="ico ico_parking_money"></i></dt>
								<dd>
									<p>최초 30분 무료 </p>
									<p>초과 시 15분당 700원</p>
									<p>당일 최대요금: 3만원 (24시간)</p>
								</dd>
							</dl>
						</div>
					</div>					
				</div>
				<!-- //parking_list-->

				<div class="parking_list">
					<h3>무료주차 안내</h3>
					<div class="con_wrap">
						<div class="grid_left">
							<dl>
								<dt><i class="ico ico_parking_info"></i></dt>
								<dd>
									<p>1만원 이상 구매 시: 1시간 무료 </p>
									<p>3만원 이상 구매 시: 2시간 무료</p>
									<p>5만원 이상 구매 시: 3시간 무료 </p>
									<p>20만원 이상 구매 시: 5시간 무료 </p>
								</dd>
							</dl>
						</div>
						<div class="grid_right">
							<ul class="bl_list">
								<li>* 롯데시네마: 2시간 30분 무료(롯데시네마 주차할인권 보유 고객)</li>
								<li>* 비터스윗코리아/퐁퐁플라워: 4시간 무료</li>
							</ul>
						</div>
					</div>					
				</div>
				<!-- //parking_list-->
			</div>
			<!-- //tab_content-->

			<div class="tab_content">
				<p><img src="/resources/user/img/contents/about/parking_star.jpg" alt=""></p>
				<div class="parking_list">
					<h3>스타존 주차 안내 (리테일/롯데백화점 방향)</h3>
					<div class="con_wrap">
						<div class="grid_left">
							<dl>
								<dt><i class="ico ico_parking_time"></i></dt>
								<dd class="tit_cell">운영시간</dd>
								<dd><p>24시간 운영</p></dd>
							</dl>
							<dl>
								<dt><i class="ico ico_parking_phone"></i></dt>
								<dd class="tit_cell">문의전화</dd>
								<dd>
									<p>주간: 02-2218-3385</p>
									<p>야간: 02-2218-3395</p>
								</dd>
							</dl>
						</div>
						<div class="grid_right">
							<ul class="bl_list">
								<li>* 이마트/ 롯데시네마 주차장과 상호간 통합적용 되지 않습니다.</li>
								<li>* 주차 할인 인증기는 리테일 매장 내 포스와 백화점 검품장, 고객상담실, 문화센타, 안내데스크에 설치되어 있습니다.</li>
								<li><span class="txt-color-blue">* 무료주차 시간에는 최초 30분이 포함되어 있습니다.</span></li>
								<li><span class="txt-color-blue">* 주차요금 정산 후에는 재정산 및 환불이 불가합니다.</span></li>
							</ul>
						</div>
					</div>					
				</div>
				<!-- //parking_list-->

				<div class="parking_list">
					<h3>주차요금 안내</h3>
					<div class="con_wrap">
						<div class="grid_left">
							<dl>
								<dt><i class="ico ico_parking_money"></i></dt>
								<dd>
									<p>최초 30분 무료 </p>
									<p>초과 시 15분당 700원</p>
									<p>당일 최대요금: 3만원 (24시간)</p>
								</dd>
							</dl>
						</div>
					</div>					
				</div>
				<!-- //parking_list-->

				<div class="parking_list">
					<h3>무료주차 안내</h3>
					<div class="con_wrap">
						<div class="grid_left">
							<dl>
								<dt><i class="ico ico_parking_info"></i></dt>
								<dd>
									<p>1만원 이상 구매 시: 1시간 무료 </p>
									<p>3만원 이상 구매 시: 2시간 무료</p>
									<p>5만원 이상 구매 시: 3시간 무료 </p>
									<p>20만원 이상 구매 시: 5시간 무료 </p>
								</dd>
							</dl>
						</div>
						<div class="grid_right">
							<ul class="bl_list">
								<li>* DM및 스마트쿠폰은 정산소에 제시하여 주시기 바랍니다.</li>
							</ul>
						</div>
					</div>					
				</div>
				<!-- //parking_list-->
			</div>
			<!-- //tab_content-->

			<div class="tab_content">
				<p><img src="/resources/user/img/contents/about/parking_young.jpg" alt=""></p>
				<div class="parking_list">
					<h3>영존 주차 안내 (건국대학교 병원 방향)</h3>
					<div class="con_wrap">
						<div class="grid_left">
							<dl>
								<dt><i class="ico ico_parking_time"></i></dt>
								<dd class="tit_cell">운영시간</dd>
								<dd><p>24시간 운영</p></dd>
							</dl>
							<dl>
								<dt><i class="ico ico_parking_phone"></i></dt>
								<dd class="tit_cell">문의전화</dd>
								<dd>
									<p>주간: 02-455-6680</p>
									<p>야간: 02-2218-4562</p>
								</dd>
							</dl>
						</div>
						<div class="grid_right">
							<ul class="bl_list">
								<li>* 이건국대 병원 주차장과 상호간 통합적용 되지 않습니다. 건국대 병원 이용고객은 건국대학교 병원 주차장을 이용바랍니다.</li>
								<li><span class="txt-color-blue">* 무료주차 시간에는 최초 30분이 포함되어 있습니다.</span></li>
								<li><span class="txt-color-blue">* 주차요금 정산 후에는 재정산 및 환불이 불가합니다.</span></li>
							</ul>
						</div>
					</div>					
				</div>
				<!-- //parking_list-->

				<div class="parking_list">
					<h3>주차요금 안내</h3>
					<div class="con_wrap">
						<div class="grid_left">
							<dl>
								<dt><i class="ico ico_parking_money"></i></dt>
								<dd>
									<p>최초 30분 무료 </p>
									<p>초과 시 15분당 700원</p>
									<p>당일 최대요금: 3만원 (24시간)</p>
								</dd>
							</dl>
						</div>
					</div>					
				</div>
				<!-- //parking_list-->

				<div class="parking_list">
					<h3>무료주차 안내</h3>
					<div class="con_wrap">
						<div class="grid_left">
							<dl>
								<dt><i class="ico ico_parking_info"></i></dt>
								<dd>
									<p>1만원 이상 구매 시: 1시간 무료 </p>
									<p>3만원 이상 구매 시: 2시간 무료</p>
									<p>5만원 이상 구매 시: 3시간 무료 </p>
									<p>20만원 이상 구매 시: 5시간 무료 </p>
								</dd>
							</dl>
						</div>
						
					</div>					
				</div>
				<!-- //parking_list-->
			</div>
			<!-- //tab_content-->

			<div class="tab_content">
				<p><img src="/resources/user/img/contents/about/parking_classic.jpg" alt=""></p>
				<div class="parking_list">
					<h3>더 클래식 500 주차 안내 (더 클래식 500 펜타즈 호텔 방향)</h3>
					<div class="con_wrap">
						<div class="grid_left">
							<dl>
								<dt><i class="ico ico_parking_time"></i></dt>
								<dd class="tit_cell">운영시간</dd>
								<dd><p>24시간 운영</p></dd>
							</dl>
							<dl>
								<dt><i class="ico ico_parking_phone"></i></dt>
								<dd class="tit_cell">문의전화</dd>
								<dd>
									<p>02-2218-5596</p>
								</dd>
							</dl>
						</div>
						<div class="grid_right">
							<ul class="bl_list">
								<li>* 이마트/롯데시네마, 롯데백화점 주차장과 상호간 통합적용 되지 않습니다. </li>
								<li><span class="txt-color-blue">* 무료주차 시간에는 최초 30분이 포함되어 있습니다.</span></li>
								<li><span class="txt-color-blue">* 주차요금 정산 후에는 재정산 및 환불이 불가합니다.</span></li>
							</ul>
						</div>
					</div>					
				</div>
				<!-- //parking_list-->

				<div class="parking_list">
					<h3>주차요금 안내</h3>
					<div class="con_wrap">
						<div class="grid_left">
							<dl>
								<dt><i class="ico ico_parking_money"></i></dt>
								<dd>
									<p>최초 30분 무료 </p>
									<p>초과 시 10분당 700원</p>
									<p>당일 최대요금: 제한없음(이용시간 별 대비 부과)</p>
								</dd>
							</dl>
						</div>
					</div>					
				</div>
				<!-- //parking_list-->

				<div class="parking_list">
					<h3>무료주차 안내</h3>
					<div class="con_wrap">
						<div class="grid_left">
							<dl>
								<dt><i class="ico ico_parking_info"></i></dt>
								<dd>
									<p>임대업장 구매 금액별 주차시간 할인(1만원/1시간, 2만원/2시간, 3만원/3시간) </p>
									<p>투숙객 : 투숙기간 무료</p>
									<p>멤버십 : 8시간 무료 </p>
									<p>입주세대방문 : 4시간 무료 </p>
									<p>라구뜨/라비앙로즈 : 3시간,6시간 무료(행사 진행도에 따라 할인부과)</p>
								</dd>
							</dl>
						</div>
						
					</div>					
				</div>
				<!-- //parking_list-->
			</div>
			<!-- //tab_content-->
		</div>
	</div>
	<!-- //ly_container-->	
</div>


<script type="text/javascript">
     (function (ns) {
        $(function(){
           ns.ui.tabMenu($('#parkingTab'),$('.tab_content_wrap'));
        });
     })(APP || {});
</script>


<%@include file="../inc/inc_gl_bottom.jsp"%>