<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>
<style>
	.layout_visual {display: none;}
	.aside_menu {display: none;}
	.layout_lnb {}
	.guide_page h2 {color: #222222; font-size: 35px; line-height: 1.3; padding-bottom: 20px; margin-bottom: 20px; border-bottom: 1px solid #ddd;}
	.guide_page h3 {color: #222222; font-size: 22px; line-height: 1.3; padding: 20px 0;}

</style>

<div class="guide_page">
	<div class="sub_contents pb100">
		<div class="ly_container">
			<h2>Tab Style</h2>
			<ul class="tab_boxtype tablist_5">
			    <li class="on"><a href="#">나의 문의내역</a></li>
			    <li><a href="#">나의 문의내역</a></li>
			    <li><a href="#">나의 문의내역</a></li>
			    <li><a href="#">나의 문의내역</a></li>
			    <li><a href="#">나의 문의내역</a></li>
			</ul>

		</div>
	</div>

	<div class="sub_contents pb100">
		<div class="ly_container">
			<h2>Table Style</h2>
			<h3>: 게시판 가로형</h3>



			<div class="select_box_top">						
				<span class="select">
					<span class="select-wrapper">
						<select id="type" name="type">
                   			<option value="0">전체</option>							
							<option value="1">제목</option>							
							<option value="2">내용</option>							
						</select>
					</span>
				</span>	
				<span class="search_in">
					<div class="placeholder">
						<label for="text">검색어를 입력해주세요.</label>
						<input type="text" name="text" id="text" class="btmline" value="">
					</div>									
					<a href="javascript:fn_search(document.listForm);" class="btn btn-search">검색</a>
				</span>
			</div>

			<table class="common_tb_row" >
			  <caption>No, 구분, 제목, 작성일</caption>
			  <colgroup>
			    <col width="100px">
			    <col width="120px">
			    <col width="*">
			    <col width="150px">
			  </colgroup>	
			  <thead>
			  	<tr>
			  		<th>No</th>
			  		<th>구분</th>
			  		<th>제목</th>
			  		<th>작성일</th>
			  	</tr>
			  </thead>				          
			  <tbody>
				  	<tr>
				  		<td colspan="4" class="nodata">등록된 내용이 없습니다.</td>
				  	</tr>
			      	<tr class="notice">
			      		<td><span class="lable_round label_sm box-blue">공지</span></td>
			      		<td>구분</td>
				  		<td class="sbj"><a href="#">HMC 환불진행 관련 문의드려요</a></td>
				  		<td class="date">2019.05.10</td>					              
			      	</tr>	
			      	<tr class="notice">
			      		<td><span class="lable_round label_sm box-blue">공지</span></td>
			      		<td>구분</td>
				  		<td class="sbj"><a href="#">HMC 환불진행 관련 문의드려요</a></td>
				  		<td class="date">2019.05.10</td>					              
			      	</tr>
			      	<tr>
			      		<td class="num">99</td>
			      		<td>구분</td>
				  		<td class="sbj"><a href="#">HMC 환불진행 관련 문의드려요</a></td>
				  		<td class="date">2019.05.10</td>					              
			      	</tr>
			      	<tr>
			      		<td class="num">99</td>
			      		<td>구분</td>
				  		<td class="sbj"><a href="#">HMC 환불진행 관련 문의드려요</a></td>
				  		<td class="date">2019.05.10</td>					              
			      	</tr>
			      	<tr>
			      		<td class="num">99</td>
			      		<td>구분</td>
				  		<td class="sbj"><a href="#">HMC 환불진행 관련 문의드려요</a></td>
				  		<td class="date">2019.05.10</td>					              
			      	</tr>
			      	<tr>
			      		<td class="num">99</td>
			      		<td>구분</td>
				  		<td class="sbj"><a href="#">HMC 환불진행 관련 문의드려요</a></td>
				  		<td class="date">2019.05.10</td>					              
			      	</tr>
			  </tbody>
			</table>

			<div class="pagination">
				<ul>
					<!-- <li><a href="#none" onclick="fn_page();" class="first pagbtn">처음</a></li> -->
					<li><a href="#" onclick="fn_page();" class="prev pagbtn">이전</a></li>
					<li><strong>1</strong></li>
					<li><a href="#none" onclick="fn_page(2);">2</a></li>
					<li><a href="#none" onclick="fn_page(3);">3</a></li>
					<li><a href="#none" onclick="fn_page(4);">4</a></li>
					<li><a href="#none" onclick="fn_page(5);">5</a></li>
					<li><a href="#none" onclick="fn_page(6);">6</a></li>
					<li><a href="#none" onclick="fn_page(7);">7</a></li>
					<li><a href="#none" onclick="fn_page(8);">8</a></li>
					<li><a href="#none" onclick="fn_page();" class="next pagbtn">다음</a></li>
					<!-- <li><a href="#" onclick="fn_page();" class="last pagbtn">마지막</a></li> -->
				</ul>
			</div>

		</div>
		<!-- //ly_container -->

		<script type="text/javascript">
			(function (ns) {
				$(function(){
				   $('#searchText').keydown(function (key) {
					   if (key.keyCode == 13) {
							$(".btn-search").trigger("click");
					   }
				   });
				   ns.placeholder.init();
				});
			 })(APP || {});
		</script>

	</div>
	<!-- //sub_contents -->



	<div class="sub_contents pb100">
		<div class="ly_container">
			<h3>: 게시판 상세</h3>

			<dl class="board_tit">
				<dt>
					<span class="label box-event">이벤트</span>
					<span class="txt">겨울맞이 슈퍼세일 이벤트</span>
				</dt>
				<dd>
					<ul class="sns_link">
						<li><a href="#"><i class="fab fa-facebook-f"></i>facebook</a></li>
						<li><a href="#"><i class="fab fa-twitter"></i>twitter</a></li>
						<li><a href="#"><i class="fas fa-link"></i>link</a></li>
					</ul>
				</dd>
			</dl>
			<p class="board_subtit">
				<span class="date"><i class="ico ico_cal_sm"></i> <strong>기간</strong> 2018.10.01 ~ 2018.12.31</span>
				<span class="txt"><i class="ico ico_cal_sm"></i> <strong>공연일시</strong> 2018.12.18 (화) 15:00</span>
				<span class="txt"><i class="ico ico_loc_sm"></i> <strong>공연장소</strong> 건대입구역 사거리 라플라스 광장</span>
				<span class="txt"><i class="ico ico_time_sm"></i> <strong>공연시간</strong> 60분공연</span>
			</p>

			<div class="board_data">
				<div class="data_txt">
					<p>5월 문화공연을 장식해준 팀은 현대적인 퍼포먼스에 한국의 전통을 가미한 퓨전 난타를 선보인 <퀸즈타>라는 난타 전문 공연팀 입니다.</p>
					<p>5월 문화공연을 장식해준 팀은 현대적인 퍼포먼스에 한국의 전통을 가미한 퓨전 난타를 선보인 <퀸즈타>라는 난타 전문 공연팀 입니다.</p>
					<p>5월 문화공연을 장식해준 팀은 현대적인 퍼포먼스에 한국의 전통을 가미한 퓨전 난타를 선보인 <퀸즈타>라는 난타 전문 공연팀 입니다.</p>
				</div>
			</div>

			<div class="board_navi txt-right">												
				<span><a href="javascript:;" class="btn btn_round_nm"><i class="ico ico_list_box"></i> 목록</a></span>					
			</div>
		</div>
	</div>
	<!-- //sub_contents -->

</div>




<%@include file="../inc/inc_gl_bottom.jsp"%>