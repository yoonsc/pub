<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents floor_info" style="visibility: hidden;" v-bind:style="'visibility: visible;'">
	<div class="ly_container">
		<ul class="tab_boxtype tablist_4">
		    <li v-for="( obj, k ) in zoneList" v-bind:class="{ 'on' : k + 1 == zoneType }">
				<a href="#" v-on:click.prevent="changeZone( k )">{{ [ '시티존', '스타존', '영존', '더 클래식 500' ][ k ] }}</a>
			</li>
		</ul>

		<div class="ph50">
			<!--  시티존 -->
			<div style="display: none;" v-show="zoneType == 1">
				<table class="common_tb_col floor_info_tbl">
		          <caption>층별안내</caption>
		          <colgroup>
		            <col width="140px">
		            <col width="320px">
		            <col width="*">
		          </colgroup>
		          <tbody>
		              <tr>
		                  <th scope="row">3F</th>
		                  <td class="txt-cnt"><img src="/resources/user/img/contents/about/logo_overthedish.jpg" alt=""></td>
		                  <td><my-list v-bind:list="zoneList[ 0 ]" v-bind:floor="'3F'"></my-list></td>
		              </tr>
		              <tr>
		                  <th scope="row">2F</th>
		                  <td class="txt-cnt"><img src="/resources/user/img/contents/about/logo_lottecinema.jpg" alt=""></td>
		                  <td><my-list v-bind:list="zoneList[ 0 ]" v-bind:floor="'2F'"></my-list></td>
		              </tr>
		              <tr>
		                  <th scope="row">1F</th>
						  <td class="txt-cnt"><img src="/resources/user/img/contents/about/logo_shinhanbank.jpg" alt=""></td>
		                  <td><my-list v-bind:list="zoneList[ 0 ]" v-bind:floor="'1F'"></my-list></td>
		              </tr>
		              <tr>
		                  <th scope="row">B1</th>
		                  <td class="txt-cnt"><img src="/resources/user/img/contents/about/logo_emart.jpg" alt=""></td>
		                  <td><my-list v-bind:list="zoneList[ 0 ]" v-bind:floor="'B1'"></my-list></td>
		              </tr>
		              <tr>
		                  <th scope="row">B2</th>
		                  <td>
		                  	<dl>
		                  		<dt><span class="ico ico-parking">주차장</span></dt>
		                  		<dd>
		                  			<ul class="item_list">
		                  				<li>주차장</li>
		                  			</ul>
		                  		</dd>
		                  	</dl>
		                  </td>
		                  <td><my-list v-bind:list="zoneList[ 0 ]" v-bind:floor="'B2'"></my-list></td>
		              </tr>
		              <tr>
		                  <th scope="row">B3</th>
		                  <td>
		                  	<dl>
		                  		<dt><span class="ico ico-parking">주차장</span></dt>
		                  		<dd>
		                  			<ul class="item_list">
		                  				<li>주차장</li>
		                  			</ul>
		                  		</dd>
		                  	</dl>
		                  </td>
		                  <td><my-list v-bind:list="zoneList[ 0 ]" v-bind:floor="'B3'"></my-list></td>
		              </tr>
		          </tbody>
				</table>
			</div>
			<!-- // 시티존 -->

			<!--  스타존 -->
			<div style="display: none;" v-show="zoneType == 2">
				<table class="common_tb_col floor_info_tbl">
		          <caption>층별안내</caption>
		          <colgroup>
		            <col width="140px">
		            <col width="320px">
		            <col width="250px">
		            <col width="*">
		          </colgroup>
		          <tbody>
		              <tr>
		                  <th scope="row">4F ~ 10F</th>
		                  <td class="txt-cnt br_btm0 bg_whgrey2" colspan="3"></td>
		              </tr>
		              <tr>
		                  <th scope="row">3F</th>
		                  <td class="txt-cnt bg_whgrey2" rowspan="4"><img src="/resources/user/img/contents/about/logo_lottedepart.jpg" alt="롯데백화점"></td>
		                  <td class="txt-cnt br_top_line"><img src="/resources/user/img/contents/about/logo_lotteculture.jpg" alt="롯데문화센터"></td>
		                  <td class="br_top_line"><my-list v-bind:list="zoneList[ 1 ]" v-bind:floor="'3F'"></my-list></td>
		              </tr>
		              <tr>
		                  <th scope="row">2F</th>
		                  <td colspan="2"><my-list v-bind:list="zoneList[ 1 ]" v-bind:floor="'2F'"></my-list></td>
		              </tr>
		              <tr>
		                  <th scope="row">1F</th>
		                  <td colspan="2"><my-list v-bind:list="zoneList[ 1 ]" v-bind:floor="'1F'"></my-list></td>
		              </tr>
		              <tr>
		                  <th scope="row">B1</th>
		                  <td class="txt-cnt"><img src="/resources/user/img/contents/about/logo_bandi.jpg" alt="bandi lunis"></td>
		                  <td><my-list v-bind:list="zoneList[ 1 ]" v-bind:floor="'B1'"></my-list></td>
		              </tr>
		              <tr>
		                  <th scope="row">B2</th>
		                  <td>
		                  	<dl>
		                  		<dt><span class="ico ico-parking">주차장</span></dt>
		                  		<dd>
		                  			<ul class="item_list">
		                  				<li>주차장</li>
		                  			</ul>
		                  		</dd>
		                  	</dl>
		                  </td>
		                  <td colspan="2"><my-list v-bind:list="zoneList[ 1 ]" v-bind:floor="'B2'"></my-list></td>
		              </tr>
		              <tr>
		                  <th scope="row">B4</th>
		                  <td>
		                  	<dl>
		                  		<dt><span class="ico ico-parking">주차장</span></dt>
		                  		<dd>
		                  			<ul class="item_list">
		                  				<li>주차장</li>
		                  			</ul>
		                  		</dd>
		                  	</dl>
		                  </td>
		                  <td colspan="2"><my-list v-bind:list="zoneList[ 1 ]" v-bind:floor="'B4'"></my-list></td>
		              </tr>
		              <tr>
		                  <th scope="row">B5 ~ B6</th>
		                  <td colspan="3">
		                  	<dl>
		                  		<dt><span class="ico ico-parking">주차장</span></dt>
		                  		<dd>
		                  			<ul class="item_list">
		                  				<li>주차장</li>
		                  			</ul>
		                  		</dd>
		                  	</dl>
		                  </td>
		              </tr>
		          </tbody>
				</table>
			</div>
			<!-- // 스타존 -->

			<!--  영존 -->
			<div style="display: none;" v-show="zoneType == 3">
				<%-- <table class="floor_info_tbl2">
				  <caption>층별안내</caption>
  		          <colgroup>
  		            <col width="140px">
  		            <col width="530px">
  		            <col width="*">
		          </colgroup>
		          <thead>
				    <tr>
					  <th scope="row"></th>
					  <th>A동</th>
					  <th>B동</th>
				    </tr>
				  </thead>
		  	    </table> --%>
				<table class="common_tb_col floor_info_tbl">
		          <caption>층별안내</caption>
		          <colgroup>
		            <col width="140px">
		            <col width="530px">
		            <col width="*">
		          </colgroup>
		          <tbody>
		              <tr>
		                  <th scope="row">&nbsp;</th>
		                  <td class="tit_cell txt-cnt">A동</td>
		                  <td class="tit_cell txt-cnt">B동</td>
		              </tr>
		              <tr>
		                  <th scope="row">6F ~ 7F</th>
		                  <td colspan="2" class="txt-cnt">업무시설</td>
		              </tr>
		              <tr>
		                  <th scope="row">5F</th>
		                  <td colspan="2" class="txt-cnt"><my-list v-bind:list="zoneList[ 2 ]" v-bind:floor="'5F'"><</td>
		              </tr>
		              <tr>
		                  <th scope="row">4F</th>
		                  <td><my-list v-bind:list="zoneList[ 2 ]" v-bind:floor="'4F'" v-bind:dong="'A동'"></my-list></td>
		                  <td><my-list v-bind:list="zoneList[ 2 ]" v-bind:floor="'4F'" v-bind:dong="'B동'"></my-list></td>
		              </tr>
		              <tr>
		              	<th scope="row">3F</th>
		                  <td><my-list v-bind:list="zoneList[ 2 ]" v-bind:floor="'3F'" v-bind:dong="'A동'"></my-list></td>
		                  <td><my-list v-bind:list="zoneList[ 2 ]" v-bind:floor="'3F'" v-bind:dong="'B동'"></my-list></td>
		              </tr>
		              <tr>
		              	<th scope="row">2F</th>
		                  <td><my-list v-bind:list="zoneList[ 2 ]" v-bind:floor="'2F'" v-bind:dong="'A동'"></my-list></td>
		                  <td><my-list v-bind:list="zoneList[ 2 ]" v-bind:floor="'2F'" v-bind:dong="'B동'"></my-list></td>
		              </tr>
		              <tr>
		              	<th scope="row">1F</th>
		                  <td><my-list v-bind:list="zoneList[ 2 ]" v-bind:floor="'1F'" v-bind:dong="'A동'"></my-list></td>
	                  	  <td><my-list v-bind:list="zoneList[ 2 ]" v-bind:floor="'1F'" v-bind:dong="'B동'"></my-list></td>
		              </tr>
		              <tr>
		              	<th scope="row">B1</th>
		                  <td><my-list v-bind:list="zoneList[ 2 ]" v-bind:floor="'B1'" v-bind:dong="'A동'"></my-list></td>
		                  <td>
		                  	<dl>
		                  		<dt><span class="ico ico-parking">주차장</span></dt>
		                  		<dd>
		                  			<ul class="item_list">
		                  				<li>주차장</li>
		                  			</ul>
		                  		</dd>
		                  	</dl>
	                 	</td>
		              </tr>
		              <tr>
		              	<th scope="row">B2</th>
		                  <td><my-list v-bind:list="zoneList[ 2 ]" v-bind:floor="'B2'" v-bind:dong="'A동'"></my-list></td>
		                  <td>
		                  	<dl>
		                  		<dt><span class="ico ico-parking">주차장</span></dt>
		                  		<dd>
		                  			<ul class="item_list">
		                  				<li>주차장</li>
		                  			</ul>
		                  		</dd>
		                  	</dl>
	                 	</td>
		              </tr>
		              <tr>
		                  <th scope="row">B4 ~ B5</th>
		                  <td>
		                  	<dl>
		                  		<dt><span class="ico ico-parking">주차장</span></dt>
		                  		<dd>
		                  			<ul class="item_list">
		                  				<li>주차장</li>
		                  			</ul>
		                  		</dd>
		                  	</dl>
		                  </td>
		                  <td>
		                  	<dl>
		                  		<dt><span class="ico ico-parking">주차장</span></dt>
		                  		<dd>
		                  			<ul class="item_list">
		                  				<li>주차장</li>
		                  			</ul>
		                  		</dd>
		                  	</dl>
		                  </td>
		              </tr>
		          </tbody>
				</table>
			</div>
			<!-- // 영존 -->

			<!-- 더클래식 500 -->
			<div style="display: none;" v-show="zoneType == 4">
				<%-- <table class="floor_info_tbl2">
				  <caption>층별안내</caption>
  		          <colgroup>
  		            <col width="140px">
  		            <col width="530px">
  		            <col width="*">
		          </colgroup>
		          <thead>
				    <tr>
					  <th scope="row"></th>
					  <th>A동</th>
					  <th>B동</th>
				    </tr>
				  </thead>
		  	    </table> --%>
				<table class="common_tb_col floor_info_tbl">
		          <caption>층별안내</caption>
		          <colgroup>
		            <col width="140px">
		            <col width="460px">
		            <col width="140px">
		            <col width="*">
		          </colgroup>
		          <tbody>
		              <tr>
		                  <%-- <th scope="row">&nbsp;</th> --%>
		                  <td colspan="2" class="tit_cell txt-cnt" style="border-left: 0;">A동</td>
						  <%-- <th>&nbsp;</th> --%>
		                  <td colspan="2" class="tit_cell txt-cnt">B동</td>
		              </tr>
		              <tr>
		                  <th scope="row">41F ~ 50F</th>
		                  <td class="txt-cnt">주거시설</td>
						  <th></th>
		                  <td class="txt-cnt"></td>
		              </tr>
		              <tr>
		                  <th scope="row">21F ~ 40F</th>
		                  <td class="txt-cnt">주거시설</td>
						  <th rowspan="5">5F ~ 40F</th>
		                  <td class="txt-cnt" rowspan="5">주거시설</td>
		              </tr>
		              <tr>
		                  <th scope="row">8F ~ 20F</th>
		                  <td class="txt-cnt"><img src="/resources/user/img/contents/about/logo_pentaz.jpg" alt=""><br>펜타즈 호텔</td>
		              </tr>
		              <tr>
		                  <th scope="row">7F</th>
		                  <td class="txt-cnt">입주자 전용시설</td>
		              </tr>
		              <tr>
		                  <th scope="row">6F</th>
		                  <td><my-list v-bind:list="zoneList[ 3 ]" v-bind:floor="'6F'" v-bind:dong="'A동'"></my-list></td>
		              </tr>
		              <tr>
		                  <th scope="row">5F</th>
		                  <td><my-list v-bind:list="zoneList[ 3 ]" v-bind:floor="'5F'" v-bind:dong="'A동'"></my-list></td>
		              </tr>
		              <tr>
		                  <th scope="row">4F</th>
		                  <td><my-list v-bind:list="zoneList[ 3 ]" v-bind:floor="'4F'" v-bind:dong="'A동'"></my-list></td>
						  <th>4F</th>
		                  <td><my-list v-bind:list="zoneList[ 3 ]" v-bind:floor="'4F'" v-bind:dong="'B동'"></my-list></td>
		              </tr>
		              <tr>
		                  <th scope="row">3F</th>
		                  <td><my-list v-bind:list="zoneList[ 3 ]" v-bind:floor="'3F'" v-bind:dong="'A동'"></my-list></td>
						  <th>3F</th>
	                  	  <td><my-list v-bind:list="zoneList[ 3 ]" v-bind:floor="'3F'" v-bind:dong="'B동'"></my-list></td>
		              </tr>
		              <tr>
		                  <th scope="row">2F</th>
		                  <td class="txt-cnt">통합지원센터 </td>
						  <th>2F</th>
	                  	  <td><my-list v-bind:list="zoneList[ 3 ]" v-bind:floor="'2F'" v-bind:dong="'B동'"></my-list></td>
		              </tr>
		              <tr>
		                  <th scope="row">1F</th>
		                  <td><my-list v-bind:list="zoneList[ 3 ]" v-bind:floor="'1F'" v-bind:dong="'A동'"></my-list></td>
						  <th>1F</th>
	                  	  <td><my-list v-bind:list="zoneList[ 3 ]" v-bind:floor="'1F'" v-bind:dong="'B동'"></my-list></td>
		              </tr>
		              <tr>
		                  <th scope="row">B1</th>
	                  	  <td><my-list v-bind:list="zoneList[ 3 ]" v-bind:floor="'B1'" v-bind:dong="'A동'"></my-list></td>
						  <th>B1</th>
	                  	  <td><my-list v-bind:list="zoneList[ 3 ]" v-bind:floor="'B1'" v-bind:dong="'B동'"></my-list></td>
		              </tr>
		              <tr>
		                  <th scope="row">B2</th>
	                  	  <td>
	                  	  	<dl>
		                  		<dt><span class="ico ico-parking">주차장</span></dt>
		                  		<dd>
		                  			<ul class="item_list">
		                  				<li>주차장</li>
		                  			</ul>
		                  		</dd>
		                  	</dl>
							<my-list v-bind:list="zoneList[ 3 ]" v-bind:floor="'B2'" v-bind:dong="'A동'"></my-list>
						  </td>
						  <th>B2</th>
	                  	  <td>
	                  	  	<dl>
		                  		<dt><span class="ico ico-parking">주차장</span></dt>
		                  		<dd>
		                  			<ul class="item_list">
		                  				<li>주차장</li>
		                  			</ul>
		                  		</dd>
		                  	</dl>
	                  	  </td>
		              </tr>
		              <tr>
		                  <th scope="row">B3 ~ B6</th>
	                  	  <td>
	                  	  	<dl>
		                  		<dt><span class="ico ico-parking">주차장</span></dt>
		                  		<dd>
		                  			<ul class="item_list">
		                  				<li>주차장</li>
		                  			</ul>
		                  		</dd>
		                  	</dl>
	                  	  </td>
						  <th>B3 ~ B6</th>
	                  	  <td>
	                  	  	<dl>
		                  		<dt><span class="ico ico-parking">주차장</span></dt>
		                  		<dd>
		                  			<ul class="item_list">
		                  				<li>주차장</li>
		                  			</ul>
		                  		</dd>
		                  	</dl>
	                  	  </td>
		              </tr>
		          </tbody>
				</table>
			</div>
			<!-- // 더클래식 500 -->
		</div>
		<div class="floor_info_link mb80">
			<p class="floor_info_link_title"><strong>스타시티 층별 상세 지도 및 매장위치</strong>를 알고 싶으신가요?</p>
			<p class="floor_info_link_sub">스타시티 가이드에서 스타시티 단지 내 층별 상세 지도 및 매장 위치를 확인하실 수 있습니다.</p>
			<a href="javascript:goGuidePage();">스타시티 가이드</a>
		</div>
		<!-- //floor_info_link -->
	</div>
	<!-- //ly_container -->
</div>
<!-- //floor_info -->

<script type="text/javascript" src="/resources/user/js/libs/vue.min.js"></script>
<script type="text/x-template" id="list-template">
	<div>
		<dl v-for="(obj, k) in list" v-bind:key="k" v-if="floor == obj.floor && dong == obj.dong">
		  <dt><span class="ico" v-bind:class="[ '', 'ico-life', 'ico-food', 'ico-cafe', 'ico-beauty', 'ico-fashion', 'ico-miscellaneous', 'ico-service' ][ obj.categoryType ]"></span></dt>
		  <dd>
			  <ul class="item_list">
				  <li v-for="str in getStores( obj.stores )">{{ str }}</li>
			  </ul>
		  </dd>
		</dl>
	</div>
</script>
<script type="text/javascript">
	$(function () {
		APP.FloorInfo.init();
	});
	function goGuidePage(){
		location.href="/guide/guide#"+APP.FloorInfo.getParam().zoneType;
	}
</script>

<%@include file="../inc/inc_gl_bottom.jsp"%>
