<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>


<div class="sub_contents floor_convenience">
                <div class="ly_container">
                    <div class="bg_info_box">
                        편의시설을 선택 하시면 아래 지도에 <strong class="txt-line-blue">해당 편의시설</strong>에 대한 정보가 표시됩니다.
                    </div>

                    <ul class="conven_icons">
                        <li><a href="convenience_1" class="on"><i class="ico ico_conven_atm"></i>현금인출기</a></li>
                        <li><a href="convenience_2"><i class="ico ico_conven_disabled"></i>장애인 화장실</a></li>
                        <li><a href="convenience_3"><i class="ico ico_conven_stroller"></i>유모차 대여</a></li>
                        <li><a href="convenience_4"><i class="ico ico_conven_smoking"></i>흡연부스</a></li>
                        <li><a href="convenience_5"><i class="ico ico_conven_parking"></i>주차 무인정산기</a></li>
                        <li><a href="convenience_6"><i class="ico ico_conven_print"></i>무인민원 발급기</a></li>
                        <li><a href="convenience_7"><i class="ico ico_conven_baby"></i>수유실</a></li>
                        <li><a href="convenience_8"><i class="ico ico_conven_meet"></i>만남의 광장</a></li>
                    </ul>

                    <div class="map_view_sec">
                        <div class="map_pop_box">
                            <dl>
                                <dt>현금인출기</dt>
                                <dd>단지 내 이용 가능한 현금인출기는<br /><strong>총 19곳</strong> 입니다.</dd>
                            </dl>
                            <p class="infotxt"><em class="txt-color-blue">*</em> 일부 은행에 설치된 현금인출기는 이용시간에 제한이 있을 수 있습니다.</p>
                        </div>

                        <ul class="info_list">
                            <li class="cf_num1 on">
                                <dl>
                                    <dt>시티존</dt>
                                    <dd><strong>9</strong></dd>
                                </dl>
                            </li>
                            <li class="cf_num2">
                                <dl>
                                    <dt>스타존</dt>
                                    <dd><strong>3</strong></dd>
                                </dl>
                            </li>
                            <li class="cf_num3">
                                <dl>
                                    <dt>영존</dt>
                                    <dd><strong>3</strong></dd>
                                </dl>
                            </li>
                            <li class="cf_num4">
                                <dl>
                                    <dt>더클래식500</dt>
                                    <dd><strong>4</strong></dd>
                                </dl>
                            </li>
                        </ul>
                        <p><img src="/resources/user/img/contents/about/convenience_map.jpg" alt=""></p>
                    </div>
                    <!-- //map_view_sec -->
                </div>
                <!-- //ly_container -->


                <div class="ly_container tab_map_con pb0">
                    <ul class="tab_boxtype tablist_4 pb30">
                        <li class="on"><a href="javascript:;">시티존</a></li>
                        <li><a href="javascript:;">스타존</a></li>
                        <li><a href="javascript:;">영존</a></li>
                        <li><a href="javascript:;">더클래식500</a></li>
                    </ul>
                </div>
                <!-- ly_container -->
                
                <!-- 지도 편의시설 아이콘 리스트
                <span class="label_icon icon_atm">atm</span>
                <span class="label_icon icon_disabled" style="left: 50px;">장애인</span>
                <span class="label_icon icon_stroller" style="left: 100px;">유모차</span>
                <span class="label_icon icon_smoking" style="left: 150px;">흡연</span>
                <span class="label_icon icon_parking" style="left: 200px;">주차</span>
                <span class="label_icon icon_print" style="left: 250px;">발급기</span>
                <span class="label_icon icon_baby" style="left: 300px;">수유실</span>
                <span class="label_icon icon_meet" style="left: 350px;">만남</span>
                 -->

                <div class="ly_container" id="floorTabCon">
                    <div class="floor_map_con">                       
                                                
                        <ul class="floor_navi" id="floor_navi_1">
                            <li><a href="#floor_map_2">2F</a></li>
                            <li><a href="#floor_map_1">1F</a></li>                            
                        </ul>

                        <div class="inner_wrap"> 
                            <div class="inner_map" id="floor_map_2">      
                                <p class="floor_map_img"><img src="/resources/user/img/contents/about/floor/atm/city_2f.jpg" alt=""></p>
                            </div>

                            <div class="inner_map" id="floor_map_1"> 
                                <p class="floor_map_img"><img src="/resources/user/img/contents/about/floor/atm/city_1f.jpg" alt=""></p>
                            </div>
                        </div>
                        <!-- //inner_wrap -->
                    </div>
                    <!-- //floor_map_con :시티존 -->

                    <div class="floor_map_con">       
                        <ul class="floor_navi">
                            <li class="on"><a href="#">B1</a></li>
                        </ul>                        
                        <p class="floor_map_img"><img src="/resources/user/img/contents/about/floor/atm/star_b1.jpg" alt=""></p>

                        <section class="floor_btm_box">
                            <div class="box_con">                    
                                <h3>그 외 롯데백화점 내 ATM 설치 안내</h3>
                                <div class="box_con_wrap">
                                    <div class="grid_left">
                                        <dl>
                                            <dt><span>4f</span></dt>
                                            <dd>6,7,8 호기 E/L홀 내 - 설치수량 1대</dd>                                    
                                        </dl>
                                        <dl>
                                            <dt><span>8f</span></dt>
                                            <dd>롯데카드센터 내 - 설치수량 1대</dd>                                    
                                        </dl>
                                    </div>
                                    <div class="grid_right">
                                        <span class="txt-color-blue">*</span> 4층~10층까지는 롯데백화점이 입점해 있으며 롯데백화점 내 <br />
                                        편의시설 및 위치는 롯데백화점 웹사이트에서 확인이 가능합니다.

                                        <div class="button_group pb0">
                                            <span><a href="https://www.lotteshopping.com/branchShopGuide/floorGuideSub?cstr=0028" target="_blank" class="btn box-blue">롯데백화점 웹사이트 바로가기</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                    <!-- //floor_map_con :스타존 -->


                    <div class="floor_map_con">                                               
                        <ul class="floor_navi">
                            <li class="on"><a href="#">2F</a></li>
                        </ul>                        
                        <p class="floor_map_img"><img src="/resources/user/img/contents/about/floor/atm/young_2f.jpg" alt=""></p>
                    </div>
                    <!-- //floor_map_con  :영존 -->


                    <div class="floor_map_con">                
                        <ul class="floor_navi">
                            <li class="on"><a href="#">1F</a></li>
                        </ul>                        
                        <p class="floor_map_img"><img src="/resources/user/img/contents/about/floor/atm/classic_1f.jpg" alt=""></p>
                    </div>
                    <!-- //floor_map_con :더클래식500 -->
                </div>
                <!-- //floorTabCon  -->
                



                <script type="text/javascript">
                     (function (ns) {
                        $(function(){          
                           ns.tabMenufloor($(".tab_boxtype"), $("#floorTabCon"));
                           ns.innerTab.init($("#floor_navi_1"), 2); //selector, 활성화탭 Number 
                        });
                     })(APP || {});
                </script>


            </div>
            <!-- //floor_convenience -->


<%@include file="../inc/inc_gl_bottom.jsp"%>
