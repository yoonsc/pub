<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>


            <div class="sub_contents floor_convenience">
                <div class="ly_container">
                    <div class="bg_info_box">
                        편의시설을 선택 하시면 아래 지도에 <strong class="txt-line-blue">해당 편의시설</strong>에 대한 정보가 표시됩니다.
                    </div>

                    <ul class="conven_icons">
                        <li><a href="convenience_1"><i class="ico ico_conven_atm"></i>현금인출기</a></li>
                        <li><a href="convenience_2"><i class="ico ico_conven_disabled"></i>장애인 화장실</a></li>
                        <li><a href="convenience_3"><i class="ico ico_conven_stroller"></i>유모차 대여</a></li>
                        <li><a href="convenience_4" class="on"><i class="ico ico_conven_smoking"></i>흡연부스</a></li>
                        <li><a href="convenience_5"><i class="ico ico_conven_parking"></i>주차 무인정산기</a></li>
                        <li><a href="convenience_6"><i class="ico ico_conven_print"></i>무인민원 발급기</a></li>
                        <li><a href="convenience_7"><i class="ico ico_conven_baby"></i>수유실</a></li>
                        <li><a href="convenience_8"><i class="ico ico_conven_meet"></i>만남의 광장</a></li>
                    </ul>

                    <div class="map_view_sec">
                        <div class="map_pop_box">
                            <dl>
                                <dt>흡연부스</dt>
                                <dd>단지 내 이용 가능한 흡연부스는<br /><strong>총 2곳</strong> 입니다.</dd>
                            </dl>
                            <p class="infotxt"><em class="txt-color-blue">*</em> 스타시티의 모든 단지는 금연건물 입니다.<br />
                            흡연은 지정된 아래 장소에서만 가능하며 그 외 흡연 적발 시 [국민건강증진법] 제 9조에 의거과태료가 부가됩니다. </p>
                        </div>

                        <ul class="info_list">
                            <li class="cf_num1">
                                <dl>
                                    <dt>시티존</dt>
                                    <dd><strong>0</strong></dd>
                                </dl>
                            </li>
                            <li class="cf_num2 on">
                                <dl>
                                    <dt>스타존</dt>
                                    <dd><strong>1</strong></dd>
                                </dl>
                            </li>
                            <li class="cf_num3">
                                <dl>
                                    <dt>영존</dt>
                                    <dd><strong>0</strong></dd>
                                </dl>
                            </li>
                            <li class="cf_num4 on">
                                <dl>
                                    <dt>더클래식500</dt>
                                    <dd><strong>1</strong></dd>
                                </dl>
                            </li>
                        </ul>
                        <p><img src="/resources/user/img/contents/about/convenience_map.jpg" alt=""></p>
                    </div>
                    <!-- //map_view_sec -->
                </div>
                <!-- //ly_container -->


                <div class="ly_container tab_map_con pb0">
                    <ul class="tab_boxtype tablist_4 pb30">
                        <li><a href="javascript:;" class="false">시티존</a></li>
                        <li class="active on"><a href="#starzone">스타존</a></li>
                        <li><a href="javascript:;" class="false">영존</a></li>
                        <li class="active"><a href="#classic">더클래식500</a></li>
                    </ul>
                </div>
                <!-- ly_container -->

                <!-- 지도내 편의시설 아이콘 리스트
                <span class="label_icon icon_atm">atm</span>
                <span class="label_icon icon_disabled" style="left: 50px;">장애인</span>
                <span class="label_icon icon_stroller" style="left: 100px;">유모차</span>
                <span class="label_icon icon_smoking" style="left: 150px;">흡연</span>
                <span class="label_icon icon_parking" style="left: 200px;">주차</span>
                <span class="label_icon icon_print" style="left: 250px;">발급기</span>
                <span class="label_icon icon_baby" style="left: 300px;">수유실</span>
                <span class="label_icon icon_meet" style="left: 350px;">만남</span>
                 -->

                <div class="ly_container" id="floorTabCon">                    

                    <div class="floor_map_con" id="starzone" style="display: block; opacity: 1;">    
                        <p class="floor_map_img"><img src="/resources/user/img/contents/about/floor/smoking/star_b1.jpg" alt=""></p>                        
                    </div>
                    <!-- //floor_map_con :스타존 -->  

                     <div class="floor_map_con" id="classic">    
                        <p class="floor_map_img"><img src="/resources/user/img/contents/about/floor/smoking/classic_1f.jpg" alt=""></p>                        
                    </div>
                    <!-- //floor_map_con :스타존 --> 
                </div>
                <!-- //floorTabCon -->    


                <script type="text/javascript">                     
                        $(function(){
                           var $tabBtn = $(".tab_boxtype >li.active");      
                           $tabBtn.find("a").click(function(e){                            
                            e.preventDefault();
                            var $this = $(this);                            
                            $(".tab_boxtype >li").removeClass("on");
                            $(this).parent().addClass("on");                            
                                TweenMax.set($(".floor_map_con"), {"display":"none"}); 
                                TweenMax.to($($this.attr("href")), 0.8, {"opacity":1, "display":"block"});                                                       
                            });
                       });
                </script>


            </div>
            <!-- //floor_convenience -->


<%@include file="../inc/inc_gl_bottom.jsp"%>
