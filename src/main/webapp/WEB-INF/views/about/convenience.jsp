<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>


<div class="sub_contents floor_convenience">
    <div class="ly_container">
        <div class="bg_info_box">
            편의시설을 선택 하시면 아래 지도에 <strong class="txt-line-blue">해당 편의시설</strong>에 대한 정보가 표시됩니다.
        </div>

        <ul class="conven_icons">
            <li><a href="convenience_1"><i class="ico ico_conven_atm"></i>현금인출기</a></li>
            <li><a href="convenience_2"><i class="ico ico_conven_disabled"></i>장애인 화장실</a></li>
            <li><a href="convenience_3"><i class="ico ico_conven_stroller"></i>유모차 대여</a></li>
            <li><a href="convenience_4"><i class="ico ico_conven_smoking"></i>흡연부스</a></li>
            <li><a href="convenience_5"><i class="ico ico_conven_parking"></i>주차 무인정산기</a></li>
            <li><a href="convenience_6"><i class="ico ico_conven_print"></i>무인민원 발급기</a></li>
            <li><a href="convenience_7"><i class="ico ico_conven_baby"></i>수유실</a></li>
            <li><a href="convenience_8"><i class="ico ico_conven_meet"></i>만남의 광장</a></li>
        </ul>

        <div class="map_view_sec">
            <ul class="info_list">
                <li class="cf_num1 on">
                    <dl>
                        <dt>시티존</dt>
                    </dl>
                </li>
                <li class="cf_num2" style="top: 320px; left: 640px;">
                    <dl>
                        <dt>스타존</dt>
                    </dl>
                </li>
                <li class="cf_num3" style="left: 670px;">
                    <dl>
                        <dt>영존</dt>
                    </dl>
                </li>
                <li class="cf_num4" style="left: 570px; top:450px;">
                    <dl>
                        <dt>더클래식500</dt>
                    </dl>
                </li>
            </ul>
            <p><img src="/resources/user/img/contents/about/convenience_map.jpg" alt=""></p>
        </div>
        <!-- //map_view_sec -->
    </div>
    <!-- //ly_container -->
   

</div>
<!-- //floor_convenience -->


<%@include file="../inc/inc_gl_bottom.jsp"%>
