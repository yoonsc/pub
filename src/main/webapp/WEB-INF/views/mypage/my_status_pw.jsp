<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents mypage">
	<div class="ly_container bg_white">
		<ul class="tab_boxtype tablist_3 mb50">
			<li><a href="my_contact_list">나의 문의내역</a></li> 
			<li><a href="my_favorite">나의 관심매장</a></li> 
			<li class="on"><a href="my_status">회원정보 보기</a></li> 
		</ul>

		<div class="myinfo_pw search_member">
			<dl class="inactive_tit pb50"> 
				<dt><strong>안전한 비밀번호</strong>로 내 정보를 보호하세요. </dt>				
			</dl>

			<div class="inactive_con">	        		
	        	<div class="search_box_pw">
        			<table class="common_tb_col register_form">
			          <caption>아이디, 패스워드</caption>
			          <colgroup>
			            <col width="150px">
			            <col width="*">
			          </colgroup>					          
			          <tbody>					          	                				              				             
			          	 <tr>
			                  <th scope="row">현재 비밀번호 </th>
			                  <td>
								<input type="password" name="hPW">
			                  </td>
			              </tr>
			              <tr>
			                  <th scope="row">새 비밀번호 </th>
			                  <td>
								<input type="password" name="hNewPW2">
			                  </td>
			              </tr>			              
			              <tr>
			                  <th scope="row">새 비밀번호 확인 </th>
			                  <td>
								<input type="password" name="hNewPW2">
			                  </td>
			              </tr>
		              	</tbody>
		              </table>
		        </div>
		        <!-- //search_box_inner -->  

		        <div class="button_group txt-cnt pt50">			
	        		<span><a href="#" class="btn box-white">취소</a></span>	
					<span><a href="#" class="btn box-blue submit">확인</a></span>
				</div>
			        	
			</div>


		</div>


	</div>
	<!-- //ly_container -->	

</div>
<!-- //sub_contents -->


<%@include file="../inc/inc_gl_bottom.jsp"%>
