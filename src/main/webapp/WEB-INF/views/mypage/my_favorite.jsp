<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents mypage">
	<div class="ly_container bg_white">
		<ul class="tab_boxtype tablist_3 mb50">
			<li><a href="my_contact_list">나의 문의내역</a></li> 
			<li class="on"><a href="my_favorite">나의 관심매장</a></li> 
			<li><a href="my_status">회원정보 보기</a></li> 
		</ul>
	</div>
	<!-- //ly_container -->	

	<div class="ly_container floor_list_wrap">	
		<!-- 등록된 매장이 없을경우 -->
		<!--
		<div class="no_favorite_data">			
			<dl>
				<dt><i class="ico ico_no_shop"></i></dt>
				<dd>등록된 관심 매장이 없습니다.</dd>
			</dl>
		</div>
		-->

		<div class="edit_btn_con">
			<label class="all_label">
          		<input type="checkbox" name="f_chkAll" class="checkbox">
          		<span class="label"></span> 전체선택
          	</label>	
			
			<ul class="edit_now">
				<li><a href="javascript:;" class="btn btn_round_nm" id="edit_favorite"><i class="ico ico_check_edit"></i> 편집</a></li>
			</ul>
			<ul class="edit_next">
				<li><a href="javascript:;" class="btn btn_round_nm" id="cancel_favorite"> 취소</a></li>
				<li><a href="javascript:;" class="btn btn_round_blue_bg"><i class="ico ico_check_close"></i> 삭제</a></li>
			</ul>

		</div>
		<ul class="floor_shop_list">		    
			<li>
		    	<div class="list_con">
		    		<div class="box_check_con">
		    			<label class="chk_round">
			          		<input type="checkbox" name="f_chk" class="checkbox">
			          		<span class="label"></span>
			          	</label>
		    			<span class="bg"></span>		    			
		    		</div>

		    		<div class="detail_box">
		    			<p class="shop_tit">롯데백화점</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_1.jpg" alt="롯데백화점"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>롯데백화점</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>			    	
		    	</div>
			</li>			
			<li>
		    	<div class="list_con">
		    		<div class="box_check_con">
		    			<label class="chk_round">
			          		<input type="checkbox" name="f_chk" class="checkbox">
			          		<span class="label"></span>
			          	</label>
		    			<span class="bg"></span>		    			
		    		</div>

		    		<div class="detail_box">
		    			<p class="shop_tit">롯데백화점</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_1.jpg" alt="롯데백화점"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>롯데백화점</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>			    	
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="box_check_con">
		    			<label class="chk_round">
			          		<input type="checkbox" name="f_chk" class="checkbox">
			          		<span class="label"></span>
			          	</label>
		    			<span class="bg"></span>		    			
		    		</div>

		    		<div class="detail_box">
		    			<p class="shop_tit">롯데백화점</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_1.jpg" alt="롯데백화점"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>롯데백화점</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>			    	
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="box_check_con">
		    			<label class="chk_round">
			          		<input type="checkbox" name="f_chk" class="checkbox">
			          		<span class="label"></span>
			          	</label>
		    			<span class="bg"></span>		    			
		    		</div>

		    		<div class="detail_box">
		    			<p class="shop_tit">롯데백화점</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_1.jpg" alt="롯데백화점"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>롯데백화점</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>			    	
		    	</div>
			</li>
			<li>
		    	<div class="list_con">
		    		<div class="box_check_con">
		    			<label class="chk_round">
			          		<input type="checkbox" name="f_chk" class="checkbox">
			          		<span class="label"></span>
			          	</label>
		    			<span class="bg"></span>		    			
		    		</div>

		    		<div class="detail_box">
		    			<p class="shop_tit">롯데백화점</p>
		    			<dl>
		    				<dt>대표전화</dt>
		    				<dd>02.2024.1412</dd>
		    			</dl>
		    			<dl>
		    				<dt>매장위치</dt>
		    				<dd>스타존 1층</dd>
		    			</dl>
		    			<div class="btns">
		    				<a href="#" class="f_icon linkgo">바로가기</a>
		    			</div>
		    		</div>
		    		<div class="shop_box">
		    			<dl>
		    				<dt><img src="/resources/user/img/contents/about/shop_list_logo_1.jpg" alt="롯데백화점"></dt>
		    				<dd>
		    					<div class="shoptxt">
		    						<span>롯데백화점</span>
		    					</div>
		    				</dd>
		    			</dl>
		    		</div>			    	
		    	</div>
			</li>

		</ul>
	</div>
	<!-- //list_con_wrap -->

</div>
<!-- //sub_contents -->

<script type="text/javascript">
     (function (ns) {
        $(function(){   

        	var $label = $(".all_label"),
        		$listBox = $(".floor_shop_list > li > div"),
        		$chkBox = $(".box_check_con"),
        		$detailBox = $(".detail_box"),
        		$editbtn = $(".edit_next"),
        		$nowBtn = $(".edit_now");

	        	$label.css("display","none");
	        	$listBox.addClass("hover_box");
	        	$chkBox.hide();
	        	$detailBox.css("opacity","0");
	        	$editbtn.css("display","none");       	
        	       	
        	$("#edit_favorite").click(function(e){        		
        		$nowBtn.hide();        		
        		$chkBox.show();        		
        		$editbtn.show();        		
        		$detailBox.hide();
        		$label.css("display","inline-block");        		
        		$listBox.removeClass("hover_box");
            });

            $("#cancel_favorite").click(function(e){        		
        		$nowBtn.show();        		
        		$chkBox.hide();        		
        		$editbtn.hide();        		
        		$detailBox.show();
        		$label.css("display","none");
        		$listBox.addClass("hover_box");

        		ns.fl_listHover.init(".hover_box");
        		$(".chk_round").find(":checkbox").prop('checked', false);
        		$("input[name='f_chkAll']").prop('checked', false);
            });   

            $("input[name='f_chkAll']").change(function() {	
            	var selectchk =  $(".chk_round").find(":checkbox");
            	if (this.checked) {
        			selectchk.prop('checked', true);	        			
        		} else {
        			selectchk.prop('checked', false);
        		}
            }); 

           


		   ns.fl_listHover.init(".hover_box");   
        });
     })(APP || {});
</script>



<%@include file="../inc/inc_gl_bottom.jsp"%>
