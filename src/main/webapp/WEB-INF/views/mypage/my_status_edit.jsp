<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents mypage">
	<div class="ly_container bg_white">
		<ul class="tab_boxtype tablist_3 mb50">
			<li><a href="my_contact_list">나의 문의내역</a></li> 
			<li><a href="my_favorite">나의 관심매장</a></li> 
			<li class="on"><a href="my_status">회원정보 보기</a></li> 
		</ul>

            <table class="common_tb_col register_form mypage_form join_register">
	          <caption></caption>
	          <colgroup>
	            <col width="230px">
	            <col width="*">
	          </colgroup>					          
	          <tbody>	
	          	  <tr>
	                  <th scope="row">회원구분</th>
	                  <td>일반회원</td>
	              </tr>
	              <tr>
	                  <th scope="row">아이디</th>
	                  <td>TESTID1234</td>
	              </tr>
	          	  <tr>
	                  <th scope="row">비밀번호</th>
	                  <td class="btn_in">			                  										
						<a href="my_chgpw" class="btn box-blue">비밀번호 변경</a>							
	                  </td>
	              </tr>	
	              <tr>
	                  <th scope="row">이름</th>
	                  <td>홍길동</td>
	              </tr>
	              <tr>
	                  <th scope="row">이메일</th>
	                  <td>					                  	
                  		<div class="placeholder">
							<label for="hEmail1">이메일</label>
							<input type="text" name="hEmail1" class="boxline" id="hEmail1" style="width: 250px;">
						</div> 
						<div class="email_input">	
							@
							<span>
								<input type="text" name="hEmail2" class="boxline" id="hEmail2" title="입력" style="width: 200px;"> 
							</span> 						
							<span class="select-wrapper line_select">
								<select name="mailserver" title="이메일 서버선택" onchange="$('#hEmail2').val( $(this).val() == 'D' ? '' : $(this).val() ); " style="width: 200px;">
									<option value="D">직접입력</option>
									<option value="naver.com">naver.com</option>
									<option value="gmail.com">gmail.com</option>
									<option value="daum.net">daum.net</option>
									<option value="hanmail.net">hanmail.net</option>
									<option value="hotmail.com">hotmail.com</option>
									<option value="icloud.com">icloud.com</option>
									<option value="yahoo.com">yahoo.com</option>
								</select>														
							</span>											
						</div>	
						<input type="hidden" name="email">
						<ul class="comment">
	                 		<li>중요소식 및 뉴스레터 수신을 위해 정확한 이메일 주소를 입력해주세요.</li>
	                 	</ul>
					  </td>
	              </tr>
	              <tr>
	                  <th scope="row">휴대전화 번호 </th>
	                  <td class="phone_cell btn_in">
						<span class="select-wrapper line_select">
							<select title="전화번호선택" id="hTel1" name="hTel1">
								<option value="010">010</option>
								<option value="011">011</option>
								<option value="018">018</option>
								<option value="016">016</option>
								<option value="017">017</option>
							</select>     
						</span> - 
						<span>
							<input type="text" name="hTel2" id="hTel2" class="number-check" value="1111" maxlength="4" title="전화번호중간번호" style="width:150px;" onkeydown="onlyNumber(this);"> - 
						</span>						
						<span>									
							<input type="text" name="hTel3" id="hTel3" class="number-check" value="1111" maxlength="4" title="전화번호끝번호" style="width:150px;" onkeydown="onlyNumber(this);">
						</span>
						<input type="hidden" name="tel">
						<a href="my_status_pw" class="btn box-navy">휴대전화 본인인증</a>	
	                  </td>
	              </tr>
	              <tr>
	                  <th scope="row"> 주소</th>
	                  <td class="btn_in">			                  	
	                  	 <p class="pb10">
	                  	 	<span><input type="text" name="hZip" class="boxline" id="hZip" style="width: 150px;"></span>
							<a href="javascript:;" class="btn box-navy">우편번호 찾기</a>
	                  	 </p>
	                  	 <p>
	                  	 	<span><input type="text" name="hZip" class="boxline" id="hZip2" style="width: 350px;"></span>
	                  	 	<span><input type="text" name="hZip" class="boxline" id="hZip3" style="width: 300px;"></span>
	                  	 </p>
	                  </td>
	              </tr>	
	              <tr>
	                  <th scope="row">성별 </th>
	                  <td>
	                  	<label>
	                  		<input type="radio" name="hSec" class="radio" >
	                  		<span class="label"></span> 남성
	                  	</label>
	                  	<label>
	                  		<input type="radio" name="hSec" class="radio" >
	                  		<span class="label"></span> 여성
	                  	</label>
	                  	<label>
	                  		<input type="radio" name="hSec" class="radio" >
	                  		<span class="label"></span> 선택안함
	                  	</label>
	                  </td>
	              </tr>
	              <tr>
	                  <th scope="row">생년월일</th>
	                  <td>
	                  	<input type="text" name="hBirth" id="hBirth" value="19810101">
	                  </td>
	              </tr>
	              <tr>
	                  <th scope="row">마케팅 정보 수신여부 선택</th>
	                  <td>
	                  	<label>
	                  		<input type="checkbox" name="hAgEmail" class="checkbox" >
	                  		<span class="label"></span> 이메일
	                  	</label>
	                  	<label>
	                  		<input type="checkbox" name="hAgSms" class="checkbox" >
	                  		<span class="label"></span> SMS
	                  	</label>		                  	
	                  </td>
	              </tr>				              			              		              
	          </tbody>
		    </table>

		<div class="button_group txt-right pt40">
			<span><a href="javascript:;" class="btn btn_round_nm">취소</a></span> &nbsp;
			<span><a href="javascript:;" class="btn btn_round_blue_bg">확인</a></span>
		</div>

	</div>
	<!-- //ly_container -->	

</div>
<!-- //sub_contents -->


<%@include file="../inc/inc_gl_bottom.jsp"%>
