<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents mypage">

	<div class="ly_container bg_white notice_view">

		<ul class="tab_boxtype tablist_3 mb50">
			<li class="on"><a href="my_contact_list">나의 문의내역</a></li> 
			<li><a href="my_favorite">나의 관심매장</a></li> 
			<li><a href="my_status">회원정보 보기</a></li> 
		</ul>

		<dl class="board_tit">
			<dt>
				<span class="tit">제목</span>
				<span class="txt">겨울맞이 슈퍼세일 이벤트</span>
			</dt>
			<dd style="width: 100px;">				
				<span class="date">2019.05.10</span>
			</dd>
		</dl>


		<div class="board_data notice_data">
			<div class="data_txt">
				<p>스타시티 5월 문화공연 안내 내용이 들어갑니다. 스타시티 5월 문화공연 안내 내용이 들어갑니다.</p>
				<p>스타시티 5월 문화공연 안내 내용이 들어갑니다. 스타시티 5월 문화공연 안내 내용이 들어갑니다.</p>
				<p>스타시티 5월 문화공연 안내 내용이 들어갑니다. 스타시티 5월 문화공연 안내 내용이 들어갑니다.</p>
				<p>스타시티 5월 문화공연 안내 내용이 들어갑니다. 스타시티 5월 문화공연 안내 내용이 들어갑니다.</p>
				<p>스타시티 5월 문화공연 안내 내용이 들어갑니다. 스타시티 5월 문화공연 안내 내용이 들어갑니다.</p>
			</div>
		</div>

		<div class="board_reply">
			<dl>
				<dt>
					<span class="lable_round label_sm box-blue">답변완료</span>
					<!-- <span class="lable_round label_sm box-grey">답변대기</span> -->
				</dt>
				<dd>
					<span class="date">2019.05.10 17:00</span>
					<p>안녕하세요 홍길동 고객님 <br />
					현재 6월 문화행사는 아직 기획중인 사항으로 확정된 행사는 아직까지 없습니다.<br />
					6월 행사 계획이 완료되면 별도 공지를 통해 안내해드리도록 하겠습니다. <br />
					감사합니다. 
					</p>
				</dd>
			</dl>
		</div>

		<div class="board_file">
			<dl style="padding-left: 120px;">
				<dt><i class="ico ico_file_clip"></i> 첨부파일</dt>
				<dd>
					<ul>
						<li><a href="#">5월 문화공연 일정표.pdf <i>다운로드</i></a></li>
						<li><a href="#">첨부파일명 첨부파일명이 나옵니다.pdf <i>다운로드</i></a></li>
					</ul>
				</dd>
			</dl>
		</div>

		<div class="board_btm txt-right">
			<span><a href="my_contact_list" class="btn btn_round_nm"><i class="ico ico_list_box"></i> 목록</a></span>
			<span><a href="/service/contact" class="btn btn_round_nm"> 다시문의</a></span>
		</div>
	</div>
	<!-- //ly_container  -->
</div>
<!-- //sub_contents -->

<script type="text/javascript">
     (function (ns) {
        $(function(){

        });
     })(APP || {});
</script>



<%@include file="../inc/inc_gl_bottom.jsp"%>
