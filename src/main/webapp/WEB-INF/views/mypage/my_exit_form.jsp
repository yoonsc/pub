<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents mypage">
	<div class="ly_container bg_white">

		<h3 class="page_title_sub">회원탈퇴</h3>

		<div class="myinfo_pw search_member">
			<dl class="inactive_tit pb50">
				<dt>회원 탈퇴 시<strong>회원정보</strong>는 즉시 삭제됩니다. </dt>				
				<dd>
					<p>스타시티를 이용해주셔서 감사합니다.<br />
					회원탈퇴 후 기존 아이디는 3개월 동안 다시 사용하실 수 없습니다.</p>
				</dd>
			</dl>

			<div class="line_wrap">
				<dl class="reason_con mb50">
					<dt>회원탈퇴 사유</dt>
					<dd>
						<ul class="reason_list">					
							<li>
								<label>
			                  		<input type="radio" name="hWhy" class="radio" value="스타시티 사이트 이용빈도가 낮음">
			                  		<span class="label"></span> 스타시티 사이트 이용빈도가 낮음
			                  	</label>
							 </li>
							 <li>
								<label>
			                  		<input type="radio" name="hWhy" class="radio" value="스타시티 사이트 속도 및 안전성 불만">
			                  		<span class="label"></span> 스타시티 사이트 속도 및 안전성 불만
			                  	</label>
							 </li>
							 <li>
								<label>
			                  		<input type="radio" name="hWhy" class="radio" value="개인정보 유출 우려">
			                  		<span class="label"></span> 개인정보 유출 우려
			                  	</label>
							 </li>
							 <li>
								<label>
			                  		<input type="radio" name="hWhy" class="radio" value="스타시티 사이트 이용 콘텐츠 부족 및 서비스 부족">
			                  		<span class="label"></span> 스타시티 사이트 이용 콘텐츠 부족 및 서비스 부족 
			                  	</label>
							 </li>
							 <li>
								<label>
			                  		<input type="radio" name="hWhy" class="radio" value="기타">
			                  		<span class="label"></span> 기타 
			                  	</label>
							 </li>
						</ul>
						<div class="txtarea_con" id="WhyEx" style="display: none;">
							<textarea name="contents" id="hMemo" style="width:100%; height: 150px;" class=""></textarea>
						</div>
					</dd>
				</dl>	

				<dl class="reason_con">
					<dt>스타시티에 바라는 점이 있다면  남겨주세요.</dt>
					<dd>
						<p>고객님의 소중한 의견을 바탕으로 더 나은 스타시티몰이 되도록 노력하겠습니다. </p>						
						<div class="txtarea_con pt20">
							<textarea name="contents" id="hMemo2" style="width:100%; height: 150px;" class=""></textarea>
						</div>
					</dd>
				</dl>

			</div>
			 <!-- //line_wrap --> 

			 <div class="button_group txt-cnt pt30">			
        		<span><a href="#" class="btn box-white">취소</a></span>	
				<span><a href="#" class="btn box-blue submit">회원탈퇴</a></span>
			</div>

		</div>


	</div>
	<!-- //ly_container -->	

</div>
<!-- //sub_contents -->

<script type="text/javascript">
     (function (ns) {
        $(function(){   

            $('input[name=hWhy]').on('change', function() {

            	if ($(this).val() == '기타' ) {
            		$("#WhyEx").show();
            	} else {
            		$("#WhyEx").hide();
            	}			    
			});

        });
     })(APP || {});
</script>


<%@include file="../inc/inc_gl_bottom.jsp"%>
