<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents mypage">
	<div class="ly_container bg_white">
		<ul class="tab_boxtype tablist_3 mb50">
			<li><a href="my_contact_list">나의 문의내역</a></li> 
			<li><a href="my_favorite">나의 관심매장</a></li> 
			<li class="on"><a href="my_status">회원정보 보기</a></li> 
		</ul>

		<div class="myinfo_pw search_member">
			<p class="tit pt0 pb40">회원님의 소중한 개인정보 보호를 위해 비밀번호를 변경해주세요.</p>	

			<div class="inactive_con">	        		
	        	<div class="search_box_pw" style="width: 500px;">
        			<table class="common_tb_col register_form">
			          <caption>아이디, 패스워드</caption>
			          <colgroup>
			            <col width="130px">
			            <col width="*">
			          </colgroup>					          
			          <tbody>				              
			              <tr>
			                  <th scope="row">비밀번호 확인 </th>
			                  <td class="btn_in">
								<input type="password" name="hPW" style="width: 200px;">
								<a href="#" class="btn box-blue txt-cnt" style="height: 50px; line-height: 50px; width: 100px;">확인</a>	
			                  </td>
			              </tr>
		              	</tbody>
		              </table>
		        </div>
		        <!-- //search_box_inner -->  
			</div>


		</div>


	</div>
	<!-- //ly_container -->	

</div>
<!-- //sub_contents -->


<%@include file="../inc/inc_gl_bottom.jsp"%>
