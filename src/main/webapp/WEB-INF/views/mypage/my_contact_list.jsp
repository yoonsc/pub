<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents mypage">
	<div class="ly_container bg_white">
		<ul class="tab_boxtype tablist_3 mb50">
			<li class="on"><a href="my_contact_list">나의 문의내역</a></li> 
			<li><a href="my_favorite">나의 관심매장</a></li> 
			<li><a href="my_status">회원정보 보기</a></li> 
		</ul>

		<table class="common_tb_row" >
		  <caption>No, 구분, 제목, 작성일</caption>
			<colgroup>
				<col width="100px">
				<col width="120px">
				<col width="*">
				<col width="150px">
			</colgroup>	
			<thead>
				<tr>
					<th>No</th>
					<th>구분</th>
					<th>제목</th>
					<th>작성일</th>
				</tr>
			</thead>				          
			<tbody>
			  	<tr>
			  		<td colspan="4" class="nodata">등록된 내용이 없습니다.</td>
			  	</tr>
		      	<tr>
		      		<td class="num">99</td>
		      		<td><span class="lable_round label_sm box-blue">답변완료</span></td>		      		
			  		<td class="sbj"><a href="#">HMC 환불진행 관련 문의드려요</a></td>
			  		<td class="date">2019.05.10</td>					              
		      	</tr>
		      	<tr>
		      		<td class="num">99</td>
		      		<td><span class="lable_round label_sm box-grey">답변대기</span></td>		      		
			  		<td class="sbj"><a href="#">HMC 환불진행 관련 문의드려요</a></td>
			  		<td class="date">2019.05.10</td>					              
		      	</tr>
		      </tbody>	
			</table>			

			<div class="pagination">
				<ul>
					<!-- <li><a href="#none" onclick="fn_page();" class="first pagbtn">처음</a></li> -->
					<li><a href="#" onclick="fn_page();" class="prev pagbtn">이전</a></li>
					<li><strong>1</strong></li>
					<li><a href="#none" onclick="fn_page(2);">2</a></li>
					<li><a href="#none" onclick="fn_page(3);">3</a></li>
					<li><a href="#none" onclick="fn_page(4);">4</a></li>
					<li><a href="#none" onclick="fn_page(5);">5</a></li>
					<li><a href="#none" onclick="fn_page(6);">6</a></li>
					<li><a href="#none" onclick="fn_page(7);">7</a></li>
					<li><a href="#none" onclick="fn_page(8);">8</a></li>
					<li><a href="#none" onclick="fn_page();" class="next pagbtn">다음</a></li>
					<!-- <li><a href="#" onclick="fn_page();" class="last pagbtn">마지막</a></li> -->
				</ul>
			</div>

			<div class="board_btm txt-right bt0 pt0">												
				<span><a href="javascript:;" class="btn btn_round_nm"><i class="ico ico_pencil"></i> 문의하기</a></span>					
			</div>

			




	</div>
	<!-- //ly_container -->	

</div>
<!-- //sub_contents -->


<%@include file="../inc/inc_gl_bottom.jsp"%>
