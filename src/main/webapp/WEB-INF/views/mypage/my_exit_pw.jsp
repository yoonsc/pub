<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents mypage">
	<div class="ly_container bg_white">

		<h3 class="page_title_sub">회원탈퇴</h3>

		<div class="myinfo_pw search_member">
			<p class="tit pt0 pb40">회원탈퇴를 위해 비밀번호를 입력해주세요.</p>

			<div class="inactive_con">	        		
	        	<div class="search_box_pw" style="width: 500px;">
        			<table class="common_tb_col register_form">
			          <caption>아이디, 패스워드</caption>
			          <colgroup>
			            <col width="130px">
			            <col width="*">
			          </colgroup>					          
			          <tbody>				              
			              <tr>
			                  <th scope="row">비밀번호 확인 </th>
			                  <td class="btn_in">
								<input type="password" name="hPW" style="width: 200px;">
								<a href="#" class="btn box-blue txt-cnt" style="height: 50px; line-height: 50px; width: 100px;">확인</a>	
			                  </td>
			              </tr>
		              	</tbody>
		              </table>
		        </div>
		        <!-- //search_box_inner -->  
			</div>


		</div>


	</div>
	<!-- //ly_container -->	

</div>
<!-- //sub_contents -->


<%@include file="../inc/inc_gl_bottom.jsp"%>
