<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="content-language" content="ko">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8 ">
    <meta name="viewport" content="width=1280">
    <meta name="robots" content="all" />
    <title>스타시티</title>
    <meta name="author" content="starcity 스타시티" />
    <meta name="title" content="starcity 스타시티" />
    <meta name="Description" content="starcity 스타시티">
    <meta name="Keywords" content="starcity, 스타시티">
    <meta property="og:type" content="website">
    <meta property="og:title" content="스타시티 | starcity">
    <meta property="og:description" content="starcity">
    <meta property="og:url" content="http://www.starcity.co.kr">
    <meta property="og:image" content="/resources/user/img/sns_image.png">
    <link rel="shortcut icon" href="/resources/user/img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/resources/user/css/style.css">
    <script type="text/javascript" src="/resources/user/js/libs/jquery/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="/resources/user/js/libs/greensock/TweenMax.min.js"></script>
    <script type="text/javascript" src="/resources/user/js/libs/greensock/plugins/ScrollToPlugin.min.js"></script>
    <script type="text/javascript" src="/resources/user/js/libs/jquery/slick.js"></script>
    <script type="text/javascript" src="/resources/user/js/libs/jquery/jquery.filestyle.js"></script>
    <script type="text/javascript" src="/resources/user/js/app.js"></script>
    <script type="text/javascript" src="/resources/user/js/app2.js"></script>
</head>
<body>
<div id="wrap">
    <!-- 바로가기메뉴
    <dl id="skiptoContent">
      <dt><strong class="hidden">바로가기 메뉴</strong></dt>
      <dd><a href="#contents" class="accessibility01">본문바로가기</a></dd>
    </dl>
    // 바로가기메뉴 -->
    <header id="header">
        <div class="header_con">
            <h1><a href="/index"><img src="/resources/user/img/top_logo.png" alt="starcity"></a></h1>
            <nav class="gnb">
                <ul class="gnb_depth_1">
                    <li><a href="/about/overview" data-title="about starcity">스타시티 소개</a>
                        <ul class="gnb_depth_2">
                            <li><a href="/about/overview">스타시티 소개</a></li>
                            <li><a href="/about/floor_info">층별안내</a></li>
                            <li><a href="/about/floor_search">매장검색</a></li>
                            <li><a href="/about/convenience">편의시설</a></li>
                            <li><a href="/about/opening">영업시간</a></li>
                            <li><a href="/about/parking">주차안내</a></li>
                            <li><a href="/about/location">오시는길</a></li>
                        </ul>
                    </li>
                    <li><a href="/guide/guide#" data-title="starcity guide"><strong>스타시티 가이드</strong></a>
                        <ul class="gnb_depth_2">
                            <li><a href="/guide/guide#1">시티존</a></li>
                            <li><a href="/guide/guide#2">스타존</a></li>
                            <li><a href="/guide/guide#3">영존</a></li>
                            <li><a href="/guide/guide#4">더 클래식 500</a></li>
                        </ul>
                    </li>
                    <li><a href="/event/event" data-title="starcity event"><strong>컬처&amp;이벤트</strong></a>
                        <ul class="gnb_depth_2">
                            <li><a href="/event/event" data-url="/event/event_view">이벤트</a></li>
                            <li><a href="/event/schedule" data-url="/event/schedule_view">공연/전시일정</a></li>
                            <li><a href="/event/activity" data-url="/event/activity_view">공연/전시활동</a></li>
                            <li><a href="/event/sales_info">대관안내</a></li>
                        </ul>
                    </li>
                    <li><a href="/service/notice" data-title="starcity service"><strong>고객서비스</strong></a>
                        <ul class="gnb_depth_2">
                            <li><a href="/service/notice" data-url="/service/notice_view">공지사항</a></li>
                            <li><a href="/service/faq">FAQ</a></li>
                            <li><a href="/service/contact">고객문의</a></li>
                            <li><a href="/service/information">입점안내</a></li>
                        </ul>
                    </li>
                    <li class="hidden_menu">
                        <a href="/mypage/my_contact_list" data-title="my page"><strong>마이페이지</strong></a>
                        <ul class="gnb_depth_2">
                            <li><a href="/mypage/my_contact_list" data-url="/mypage/my_contact_view">나의문의내역</a></li>
                            <li><a href="/mypage/my_favorite">나의관심매장</a></li>
                            <li><a href="/mypage/my_status" data-url="/mypage/my_status_edit">회원정보보기</a></li>
                            <li><a href="/mypage/my_cfm_pw" data-url="/mypage/my_status_pw">비밀번호수정</a></li>
                            <li><a href="/mypage/my_exit_pw" data-url="/mypage/my_exit_login">회원탈퇴</a></li>
                            <li><a href="/mypage/my_exit_form">회원탈퇴입력</a></li>
                       </ul>
                    </li>
                    <li class="hidden_menu">
                        <a href="/store/normal_view" data-title="cafe&dessert"><strong>까페&디저트</strong></a>
                        <ul class="gnb_depth_2">
                            <li><a href="/store/normal_view" data-url="/store/special_view">상점</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <!-- //gnb -->
            <div class="header_util">
                <span class="logininfo"><a href="/member/login">로그인</a></span>
                <div class="lang_link">
                    <a href="/eng/" class="is-on">ENG</a>
                    <!--<ul>
                        <li><a href="/">KOR</a></li>
                        <li><a href="/starcity/en/">ENG</a></li>
                    </ul>
                    -->
                </div>
            </div>
        </div>
        <!-- //header_con -->
        <div id="navbg"></div>
    </header>
    <div id="container">
        <section class="layout_visual">
            <div class="ly_container">
                <h2 class="bg_tit" id="titleH2"></h2>
                <p class="bg_summary" id="titleKor"></p>
            </div>
            <!-- //ly_container -->
        </section>
        <!-- //layout_visual -->
        <section class="layout_lnb">
            <div class="ly_container">
                <ul id="cloneLnb">
                </ul>
            </div>
            <!-- //ly_container -->
        </section>
        <!-- //layout_lnb -->

        <section class="page_container">
            <h3 class="page_title" id="titleH3"></h3>
