<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
        <section class="aside_menu">
            <ul class="quick_info">
                <li>
                    <a href="javascript:LayerPopups.open('danziPop', true);">
                        <i class="ico ico_aside_map"></i>
                        <span>단지안내</span>
                    </a>
                </li>
                <li>
                    <a href="/about/floor_info">
                        <i class="ico ico_aside_floor"></i>
                        <span>층별안내</span>
                    </a>
                </li>
                <li>
                    <a href="/about/parking">
                        <i class="ico ico_aside_point"></i>
                        <span>주차안내</span>
                    </a>
                </li>
                <li>
                    <a href="/about/location">
                        <i class="ico ico_aside_location"></i>
                        <span>오시는길</span>
                    </a>
                </li>
            </ul>
            <div class="main_quick_nav">
               <ul class="main_nav">
                    <li class="quick_visual"><a href="#visual">HOME</a></li>
                    <li class="quick_event"><a href="#event">1</a></li>
                    <li class="quick_insta"><a href="#insta">2</a></li>
                    <li class="quick_guide"><a href="#guide">3</a></li>
               </ul>
           </div>
        </section>


    </section>
    <!-- //page_container -->
    
</div>
<!-- //container -->

<footer id="footer">
    <div class="footer_con">
        <ul class="footer_link">
            <li><a href="/etc/service_rules">이용약관</a></li>
            <li><a href="/etc/privacy">개인정보처리방침</a></li>
            <li><a href="/etc/email_refuse">이메일집단수집거부</a></li>
            <li><a href="/etc/movie_rules">영상정보처리기기 운영관리 방침</a></li>
            <li><a href="/service/information">입점안내</a></li>
            <li><a href="/event/sales_info">대관안내</a></li>
        </ul>
        <address>
            <p class="txt_addr"><span>(05065) 서울특별시 광진구 자양동 아차산로 272 (자양동) 스타시티몰</span><span>대표전화 : 02-2024-1500</span></p>
            <p class="txt_copyright">COPYRIGHT ⓒ 2019 KONKUK AMC. ALL RIGHTS RESERVED.</p>
        </address>
        <div class="familysite" id="Fnfamily">
        	<div class="family_wrap">
        		<a href="javascript:;">FAMILY SITE</a>
	            <div class="list_con">
	                <ul class="flist">
	                    <li class="flist1"><a target="_blank" href="http://www.theclassic500.com/">더 클래식 500</a></li>
	                    <li class="flist2"><a target="_blank" href="http://www.konkuk.ac.kr">건국대학교 </a></li>
	                    <li class="flist3"><a target="_blank" href="http://www.kku.ac.kr/">건국대학교 글로컬 캠퍼스</a></li>
	                    <li class="flist4"><a target="_blank" href="http://www.kuh.ac.kr/">건국대학교병원</a></li>
	                    <li class="flist5"><a target="_blank" href="http://www.kuh.co.kr/">건국대학교 충주병원</a></li>
	                    <li class="flist6"><a target="_blank" href="http://www.konkukmilk.co.kr/">건국유업</a></li>
	                    <li class="flist7"><a target="_blank" href="http://kugolf.co.kr/">스마트KU골프파빌리온</a></li>
	                    <li class="flist8"><a target="_blank" href="http://www.psuca.edu/">PSU</a></li>
	                    <li class="flist9"><a target="_blank" href="http://www.konkukham.com/">건국햄</a></li>
	                </ul>
	            </div>
        	</div>
        </div>
        <!-- //familysite -->
        <span class="sns_box"><a href="#"><i class="ico ico_sns_insta"></i>인스타그램</a></span>
    </div>
    <!-- //footer_con -->
</footer>
<!-- //footer -->

<!-- 단지안내 팝업 시작-->
<div id="danziPop" class="layerPopup map_layer">
    <div class="layerPopup_in">
        <span class="closeLayer">
            <a href="javascript:LayerPopups.close('danziPop');" class="btn"><i class="lnr lnr-cross"></i></a>
        </span>
        <div class="map_img">
            <img src="/resources/user/img/contents/about/overview_map.jpg" alt="">
        </div>
        <!-- //layer_txt -->
    </div>
    <!-- //layerPopup_in -->
</div>
<!-- //단지안내 팝업 -->

<div id="layerScreen"></div>

</div>
<!-- //wrap -->

<script type="text/javascript">
     (function (ns) {
        $(function(){          
           ns.gnb.init();
           ns.Numbering.init();
           ns.familybox.init();
           ns.navHover.init();
        });
     })(APP || {});
</script>

</body>
</html>
