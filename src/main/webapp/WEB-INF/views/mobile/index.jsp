<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="inc/inc_gl_top.jsp"%>

<section class="popup_wrap">
    <div class="popup_list" id="popList">
        <!-- 신규매장 슬라이드팝업 -->
        <div class="popup" id="pop0">
        	<ul class="popup_slide">
        		<li>
		            <p class="img">
		        		<a href="http://www.naver.com" target="_blank">
		        		   <img src="/resources/user/img/pop_sample.jpg" alt="" />
		                </a>
		            </p>
	        	</li>
	        	<li>
		            <p class="img">
		        		<a href="http://www.naver.com" target="_blank">
		        		   <img src="/resources/user/img/pop_sample.jpg" alt="" />
		                </a>
		            </p>
	        	</li>
	        	<li>
		            <p class="img">
		        		<a href="http://www.naver.com" target="_blank">
		        		   <img src="/resources/user/img/pop_sample.jpg" alt="" />
		                </a>
		            </p>
	        	</li>
        	</ul>
            <div class="today">
                <label><input type="checkbox" id="chkPop0" class="type-checkbox">오늘 이창을 열지 않음</label>
                <a href="javascript:;">닫기</a>
            </div>
        </div>
        <!-- 일반 레이어팝 -->
        <div class="popup nmpop" id="pop1">
            <p class="img">
        		<a href="http://www.naver.com" target="_blank">
        		   <img src="/resources/user/img/main_pop.jpg" alt="" />
                </a>
            </p>
            <div class="today">
                <label><input type="checkbox" id="chkPop1" class="type-checkbox">오늘 이창을 열지 않음</label>
                <a href="javascript:;">닫기</a>
            </div>
        </div>
    </div><!--//popup_list-->
</section><!--//popup_wrap-->


<section class="main_sec event_sec" id="event">   
    <p class="slider_num"><span id="currentNum">01</span><em>/</em><span id="totalNum"></span></p>         
    <ul class="event_slide">
        <li class="slider_item">
            <a href="/event/event"><img src="/resources/user/mobile/img/main/event_banner_img1.jpg" alt=""></a>
        </li>
        <li class="slider_item">
            <a href="/event/event"><img src="/resources/user/mobile/img/main/event_banner_img1.jpg" alt=""></a>
        </li>
        <li class="slider_item">
            <a href="/event/event"><img src="/resources/user/mobile/img/main/event_banner_img1.jpg" alt=""></a>
        </li>              
    </ul>
</section>
<!-- //event -->

<section class="main_sec guide_sec" id="guide">  
    <dl class="guide_tit">
        <dt>스타시티 <strong>guide</strong></dt>
        <dd>
            <p class="txt">New Life Style을 지향하는<br />
            스타시티의 라이프를 소개합니다.</p>
            <p class="btn_in">
                <span><a href="javascript:;" class="btn btn_round_nm">자세히보기<i class="bar"></i></a></span>
            </p>                        
        </dd>
    </dl>           
   
    <div class="lotte_box">
        <img src="/resources/user/mobile/img/main/shop_banner_00.jpg" alt="">
    </div>
    <ul>                    
        <li>
            <a href="#">
                <dl>
                    <dt><img src="/resources/user/mobile/img/main/shop_banner_01.jpg" alt=""></dt>
                    <dd>롯데시네마 건대입구점</dd>
                </dl>                                                    
            </a>
        </li>
        <li>
            <a href="#">
                <dl>
                    <dt><img src="/resources/user/mobile/img/main/shop_banner_02.jpg" alt=""></dt>
                    <dd>반디앤루니스 스타시티점</dd>
                </dl>                                                    
            </a>
        </li>
        <li>
            <a href="#">
                <dl>
                    <dt><img src="/resources/user/mobile/img/main/shop_banner_03.jpg" alt=""></dt>
                    <dd>종합 서적과 문구 팬시 서점</dd>
                </dl>                                                    
            </a>
        </li>
        <li>
            <a href="#">
                <dl>
                    <dt><img src="/resources/user/mobile/img/main/shop_banner_04.jpg" alt=""></dt>
                    <dd>스타시티웨딩홀</dd>
                </dl>                                                    
            </a>
        </li>
    </ul>
           
</section>
<!-- //guide -->

<section class="main_sec insta_sec" id="insta">
    <h3>instagram</h3>
    <ul class="insta_list">
        <li>
            <a href="#">
                <div class="txt_con">
                    <i class="fab fa-instagram"></i>
                    <p>날씨 뚝! 오늘 너무 춥지 않아요? 저녁에 뜨끈~ 한 쌀국수 호로록~ 스타시티가 직접 맛보았다! #포베이 #스타시티포베이#포베이 추천메뉴 #쌀국수C</p>
                </div>
                <div class="thumb"><img src="/resources/user/mobile/img/main/insta_thumb.jpg" alt=""></div>
            </a>
        </li>
        <li>
            <a href="#">
                <div class="txt_con">
                    <i class="fab fa-instagram"></i>
                    <p>날씨 뚝! 오늘 너무 춥지 않아요? 저녁에 뜨끈~ 한 쌀국수 호로록~ 스타시티가 직접 맛보았다! #포베이 #스타시티포베이#포베이 추천메뉴 #쌀국수C</p>
                </div>
                <div class="thumb"><img src="/resources/user/mobile/img/main/insta_thumb.jpg" alt=""></div>
            </a>
        </li>
        <li>
            <a href="#">
                <div class="txt_con">
                    <i class="fab fa-instagram"></i>
                    <p>날씨 뚝! 오늘 너무 춥지 않아요? 저녁에 뜨끈~ 한 쌀국수 호로록~ 스타시티가 직접 맛보았다! #포베이 #스타시티포베이#포베이 추천메뉴 #쌀국수C</p>
                </div>
                <div class="thumb"><img src="/resources/user/mobile/img/main/insta_thumb.jpg" alt=""></div>
            </a>
        </li>
        <li>
            <a href="#">
                <div class="txt_con">
                    <i class="fab fa-instagram"></i>
                    <p>날씨 뚝! 오늘 너무 춥지 않아요? 저녁에 뜨끈~ 한 쌀국수 호로록~ 스타시티가 직접 맛보았다! #포베이 #스타시티포베이#포베이 추천메뉴 #쌀국수C</p>
                </div>
                <div class="thumb"><img src="/resources/user/mobile/img/main/insta_thumb.jpg" alt="" id="imgheight"></div>
            </a>
        </li>
    </ul>
</section>
<!-- //insta -->

<script type="text/javascript">
	$(function(){
        popControl.init();

        var $slide = $(".event_slide");
        $slide.slick({
            fade:false,
            slidesToShow:1,
            slidesToScroll:1,
            arrows:false,
            dots:true,
            infinite: true,
            autoplay:false,
            autoplaySpeed:3000,
            draggable:true,
            speed:500,
            zIndex:10,
            pauseOnHover:false
        });
        var $tnum = $(".slider_item").not(".slick-cloned").length;
        $("#totalNum").text("0"+$tnum);

        $slide.on("beforeChange", function(event, slick, currentSlide, nextSlide){
            var $nnum = "0" + (nextSlide +1);
            $("#currentNum").text($nnum);
        });


        $( "ul.insta_list li:nth-child(2), ul.insta_list li:nth-child(4)").addClass("other");

         function instaResize($image){
            var $image = $(".insta_list li .thumb").find("img");
            var $imageH = $(".insta_list li .thumb").outerHeight();
            var $txtCon = $(".insta_list li").find(".txt_con");
            $txtCon.css({"height":$imageH});
         }
         $(window).resize(instaResize);
         instaResize();
	});


</script>


<%@include file="inc/inc_gl_bottom.jsp"%>
