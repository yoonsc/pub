<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

        <div class="sub_title">
            <h2>이벤트</h2>
            <p class="en_summary">EVENT</p>
        </div>



        <div class="layout_body">   

            <div class="ly_container">
                <ul class="tab_boxtype tablist_2" id="eventTab">
                    <li class="on"><a href="javascript:;">진행중 이벤트</a></li>
                    <li><a href="javascript:;">지난 이벤트</a></li>
                </ul>

                <div class="tab_content_wrap">

                    <!--  진행중 이벤트 -->
                    <div class="tab_content">
                        <ul class="evt_list open_evt_list">     
                            <li data-val="">
                                <a href="/event/event_view">
                                    <div class="box_wrap">
                                        <span class="evt_label">이벤트</span>
                                        <div class="thumb_box"> 
                                            <p class="img"><img src="/resources/user/mobile/img/contents/event/event_list.jpg" alt=""></p>    
                                        </div>
                                        <dl>
                                            <dt>스타시티 신규가입회원 스타벅스 이벤트</dt>
                                            <dd>기간 : 2019-03-13 ~ 2019-03-14</dd>
                                        </dl>
                                    </div>
                                </a>
                            </li>
                            <li data-val="">
                                <a href="/event/event_view">
                                    <div class="box_wrap">
                                        <span class="evt_label">이벤트</span>
                                        <div class="thumb_box"> 
                                            <p class="img"><img src="/resources/user/mobile/img/contents/event/event_list.jpg" alt=""></p>    
                                        </div>
                                        <dl>
                                            <dt>스타시티 신규가입회원 스타벅스 이벤트</dt>
                                            <dd>기간 : 2019-03-13 ~ 2019-03-14</dd>
                                        </dl>
                                    </div>
                                </a>
                            </li>
                        </ul> 
                        <!--  더보기  -->
                        <div class="btn_page_more_con open_list_more_btn">           
                            <a href="javascript:openListMore();" class="more_item">
                                <div class="page_more_wrap">                                
                                    <div class="page_more_box">
                                        <span class="more_txt">더보기</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!--  //더보기 -->
                    </div>
                    <!--  //진행중 이벤트 -->


                    <!--  지난 이벤트 -->
                    <div class="tab_content">
                        <ul class="evt_list closed_evt_list">                            
                            <li class="evt_end">
                                <a href="javascript:alert('종료된 이벤트입니다.');">
                                    <div class="box_wrap">
                                        <span class="evt_label">문화</span>
                                        <div class="thumb_box"> 
                                            <p class="img"><img src="/resources/user/mobile/img/contents/event/event_list.jpg" alt=""></p>        
                                        </div>
                                        <dl>
                                            <dt>스타시티 신규가입회원 스타벅스 이벤트</dt>
                                            <dd>기간 : 2019-01-01 ~ 2019-02-16</dd>
                                        </dl>
                                    </div>
                                </a>
                            </li> 
                            <li class="evt_end">
                                <a href="javascript:alert('종료된 이벤트입니다.');">
                                    <div class="box_wrap">
                                        <span class="evt_label">문화</span>
                                        <div class="thumb_box"> 
                                            <p class="img"><img src="/resources/user/mobile/img/contents/event/event_list.jpg" alt=""></p>        
                                        </div>
                                        <dl>
                                            <dt>스타시티 신규가입회원 스타벅스 이벤트</dt>
                                            <dd>기간 : 2019-01-01 ~ 2019-02-16</dd>
                                        </dl>
                                    </div>
                                </a>
                            </li> 
                        </ul>
                        <div class="btn_page_more_con closed_list_more_btn">           
                            <a href="javascript:closedListMore();" class="more_item">
                                <div class="page_more_wrap">                               
                                    <div class="page_more_box">
                                        <span class="more_txt">더보기</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!--  //더보기 -->
                    </div>  
                    <!--  //지난 이벤트 -->
                </div>
                <!-- //tab_content_wrap -->
            </div>
            <!-- //ly_container -->
        </div>      
        <!-- //layout_body -->

        <script type="text/javascript"> 
            $(function(){      
                ui_tabMenu($('#eventTab'),$('.tab_content_wrap'));
            });
        </script>

<%@include file="../inc/inc_gl_bottom.jsp"%>