<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

    <div class="sub_title">
        <h2>공연/문화활동</h2>
        <p class="en_summary">CONCERT & EXHIBITION STORY</p>
    </div>


    <div class="layout_body">   

        <div class="ly_container">
            
            <ul class="evt_list activity_list">     
                <li data-val="">
                    <a href="/event/event_view">
                        <div class="box_wrap">                                
                            <div class="thumb_box"> 
                                <p class="img"><img src="/resources/user/mobile/img/contents/event/activity_list.jpg" alt=""></p>    
                            </div>
                            <dl>
                                <dt>건대 스타시티 쇼핑몰 문화공연 <난타></dt>
                                <dd>2019년 05월 10일</dd>
                            </dl>
                        </div>
                    </a>
                </li>
                <li data-val="">
                    <a href="/event/event_view">
                        <div class="box_wrap">
                            <div class="thumb_box">
                                <p class="img"><img src="/resources/user/mobile/img/contents/event/activity_list.jpg" alt=""></p>
                            </div>
                            <dl>
                                <dt>건대 스타시티 쇼핑몰 문화공연 <난타></dt>
                                <dd>2019년 05월 10일</dd>
                            </dl>
                        </div>
                    </a>
                </li>
                <li data-val="">
                    <a href="/event/event_view">
                        <div class="box_wrap">
                            <div class="thumb_box">
                                <p class="img"><img src="/resources/user/mobile/img/contents/event/activity_list.jpg" alt=""></p>
                            </div>
                            <dl>
                                <dt>건대 스타시티 쇼핑몰 문화공연 <난타></dt>
                                <dd>2019년 05월 10일</dd>
                            </dl>
                        </div>
                    </a>
                </li>
                
            </ul> 
            <!--  더보기  -->
            <div class="btn_page_more_con open_list_more_btn">           
                <a href="javascript:openListMore();" class="more_item">
                    <div class="page_more_wrap">                                
                        <div class="page_more_box">
                            <span class="more_txt">더보기</span>
                        </div>
                    </div>
                </a>
            </div>
            <!--  //더보기 -->

        </div>
        <!-- //ly_container -->
    </div>      
    <!-- //layout_body -->

<%@include file="../inc/inc_gl_bottom.jsp"%>