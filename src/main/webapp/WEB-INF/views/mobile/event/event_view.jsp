<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

    <div class="prev_title">
        <a href="javascript:history.go(-1);"><span class="lnr lnr-chevron-left"></span></a>
        <h2>이벤트</h2>
    </div>



    <div class="layout_body mt25">

        <div class="ly_container">

            <dl class="board_tit">
                <dt>
                    <span class="label box-event">이벤트</span>
                    <span class="txt">노출기간 테스트용노출기간 테스트용노출기간 테스트용노출기간 테스트용</span>
                </dt>
                <dd class="board_subtit">
                    <span class="date"><i class="ico ico_cal_sm"></i> <strong>기간</strong> 2019-03-13 ~ 2019-03-14</span>
                </dd>
                <dd>
                    <ul class="sns_link">
                        <li><a href="javascript:shareSns(1);"><i class="fab fa-facebook-f"></i>facebook</a></li>
                        <li><a href="javascript:shareSns(2);"><i class="fab fa-twitter"></i>twitter</a></li>
                        <li><a id="clipboard"><i class="fas fa-link"></i>link</a></li>
                    </ul>
                </dd>
            </dl>

            <div class="board_data">
                <div class="data_txt">
                    <p><img src="http://starcity.midashelp.com/resources/upload/editorImage/20190313100615267010.jpg" alt="" /></p>
                </div>
            </div>

            <div class="board_btm txt-cnt">
                <span><a href="/event/event?seq=117&pageIndex=1" class="btn btn_round_nm"><i class="fas fa-bars"></i> 목록</a></span>
            </div>
        </div>
        <!-- //ly_container -->

    </div>
    <!-- //layout_body -->


<%@include file="../inc/inc_gl_bottom.jsp"%>