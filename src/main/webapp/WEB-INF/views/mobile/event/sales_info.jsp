<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

        <div class="sub_title">
            <h2>대관안내</h2>
            <p class="en_summary">INFORMATION FOR HIRING HALL</p>
        </div>

        <div class="layout_body bg_grey pb0">

            <div class="ly_container pt20">                   
                <div class="steplist_con">
                    <h4 class="content_tit">스타시티 대관 절차</h4>
                    <ul class="steplist"> 
                        <li>                
                            <div class="line_wrap">   
                                <div class="left_sec">
                                    <span class="icon_img"><img src="/resources/user/mobile/img/contents/event/step_icon_1.png" alt=""></span>    
                                </div>
                                <div class="right_sec">
                                    <dl>
                                        <dt><strong>STEP.1</strong> 신청</dt>
                                        <dd>대관신청서 및 공연계획서를 작성하여 담당자 이메일로 접수 하시기 바랍니다. </dd>
                                    </dl>
                                </div>
                            </div>
                        </li>
                        <li>                
                            <div class="line_wrap">   
                                <div class="left_sec">
                                    <span class="icon_img"><img src="/resources/user/mobile/img/contents/event/step_icon_2.png" alt=""></span>    
                                </div>
                                <div class="right_sec">
                                    <dl>
                                        <dt><strong>STEP.2</strong> 심사</dt>
                                        <dd>대관 심사 기준에 의거하여 대관의 가부를 결정하게 되며 대략 4~10일 정도의 심사기간이 소요 됩니다. </dd>
                                    </dl>
                                </div>
                            </div>
                        </li>
                        <li>                
                            <div class="line_wrap">   
                                <div class="left_sec">
                                    <span class="icon_img"><img src="/resources/user/mobile/img/contents/event/step_icon_3.png" alt=""></span>    
                                </div>
                                <div class="right_sec">
                                    <dl>
                                        <dt><strong>STEP.3</strong> 승인</dt>
                                        <dd>심사기준 결과 대관승인이 결정되면 개별 통보해 드립니다.</dd>
                                    </dl>
                                </div>
                            </div>
                        </li>
                        <li>                
                            <div class="line_wrap">   
                                <div class="left_sec">
                                    <span class="icon_img"><img src="/resources/user/mobile/img/contents/event/step_icon_4.png" alt=""></span>    
                                </div>
                                <div class="right_sec">
                                    <dl>
                                        <dt><strong>STEP.4</strong> 계약</dt>
                                        <dd>대관일 30일 전까지 계약을 체결합니다.</dd>
                                    </dl>
                                </div>
                            </div>
                        </li>
                    </ul>  
                </div> 
                <!-- //steplist_con -->  

                 
            </div>            
            <!-- //ly_container -->

            <div class="ly_container full_box bg_white ph20" style="margin-bottom: 2rem; margin-top: 2rem;">   
                <div class="linebox_info">
                    <dl>
                        <dt>스타시티 대관 <br>신청양식 및 접수 안내</dt>
                        <dd>
                            <p>대관 신청 안내   <strong>02-2218-5704</strong></p>
                            <p>대관 신청 접수 이메일  <strong>yonggong79@theclassic500.co.kr</strong> </p>
                            <p>이제 스타시티몰에서 더욱더 즐거운 라이프생활을 즐기시길 바랍니다.</p>
                            <p class="ph20"><i class="fas fa-exclamation-circle"></i> 심사기준에 따라 대관료가 발생합니다.</p>
                        </dd>
                    </dl>
                    <div class="button_group txt-cnt">  
                        <span><a href="/resources/user/file/대관신청서.zip" target="_blank" class="btn box-blue">대관 신청서 다운로드 <i class="lnr lnr-download"></i></a></span>
                    </div>
                </div>
            </div>


        </div>

<%@include file="../inc/inc_gl_bottom.jsp"%>