<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

    <div class="prev_title">
        <a href="javascript:history.go(-1);"><span class="lnr lnr-chevron-left"></span></a>
        <h2>공연/문화일정</h2>
    </div>


    <div class="layout_body mt25">

        <div class="ly_container">

            <dl class="board_tit">
                <dt>
                    <span class="label box-event">전시</span>
                    <span class="txt">노출기간 테스트용노출기간 테스트용노출기간 테스트용노출기간 테스트용</span>
                </dt>
                <p class="board_subtit">
                    <span class="txt"><i class="far fa-calendar-alt"></i> <strong>날짜</strong> 2019-03-16 ~ 2019-03-25</span>
                    <span class="txt"><i class="fas fa-map-marker-alt"></i> <strong>장소</strong> 스타존 1층 OOOO 광장</span>
                    <span class="txt"><i class="far fa-clock"></i> <strong>시간</strong> PM 08:00 ~ </span>
                </p>
                <dd>
                    <ul class="sns_link">
                        <li><a href="javascript:shareSns(1);"><i class="fab fa-facebook-f"></i>facebook</a></li>
                        <li><a href="javascript:shareSns(2);"><i class="fab fa-twitter"></i>twitter</a></li>
                        <li><a id="clipboard"><i class="fas fa-link"></i>link</a></li>
                    </ul>
                </dd>
            </dl>

            <div class="board_data">
                <p class="data_img"><img src="http://starcity.midashelp.com/resources/upload/schedule/2019/03/20190306092909690003.PNG" alt=""></p>

                <div class="field_data">
                    <dl class="top_bar">
                        <dt>공연내용</dt>
                        <dd>
                            <p>내용이 노출되요~~</p>
                            <p>내용이 노출되요~~</p>
                            <p>내용이 노출되요~~</p>
                            <p>내용이 노출되요~~</p>
                            <p>내용이 노출되요~~</p>
                            <p>내용이 노출되요~~</p>
                            <p>내용이 노출되요~~</p>
                            <p>내용이 노출되요~~</p>
                            <p>내용이 노출되요~~</p>
                        </dd>
                    </dl>
                    <dl class="top_bar">
                        <dt>기타</dt>
                        <dd>
                            기타 자세한 문의는 문의처로 문의 바랍니다.
                        </dd>
                    </dl>
                    <dl class="top_bar">
                        <dt>문의</dt>
                        <dd>
                            02-8415-5171
                        </dd>
                    </dl>
                </div>
                <!-- //field_data -->
            </div>
            <!-- //board_data -->

            <div class="board_btm txt-cnt">
                <span><a href="/event/event?seq=117&pageIndex=1" class="btn btn_round_nm"><i class="fas fa-bars"></i> 목록</a></span>
            </div>
        </div>
        <!-- //ly_container -->

    </div>
    <!-- //layout_body -->

<%@include file="../inc/inc_gl_bottom.jsp"%>