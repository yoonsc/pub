<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

    <div class="prev_title">
        <a href="javascript:history.go(-1);"><span class="lnr lnr-chevron-left"></span></a>
        <h2>공연/문화활동</h2>
    </div>


    <div class="layout_body mt25">

        <div class="ly_container activity_view">

            <dl class="board_tit">
                <dt>
                    <span class="txt">노출기간 테스트용노출기간 테스트용노출기간 테스트용노출기간 테스트용</span>
                </dt>
                <dd>
                    <span class="date">2019.05.10</span>
                </dd>
            </dl>

            <div class="board_data">
                <div class="data_txt">
                    <p><img src="http://starcity.midashelp.com/resources/upload/editorImage/20190313100615267010.jpg" alt="" /></p>
                </div>
            </div>

            <div class="board_nav">
                <dl class="data_nav_prev">
                    <dt>이전글</dt>
                    <dd>
                        등록된 이전글이 없습니다.
                    </dd>

                </dl>
                <dl class="data_nav_next">
                    <dt>다음글</dt>
                    <dd>
                        <a href="#">다음 등록된 게시물입니다.</a>
                    </dd>
                </dl>
            </div>


            <div class="board_btm txt-cnt">
                <span><a href="/event/activity" class="btn btn_round_nm"><i class="fas fa-bars"></i> 목록</a></span>
            </div>
        </div>
        <!-- //ly_container -->

    </div>
    <!-- //layout_body -->


<%@include file="../inc/inc_gl_bottom.jsp"%>