<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

        <div class="sub_title">
            <h2>공연/문화일정</h2>
            <p class="en_summary">CONCERT & EXHIBITION</p>
        </div>

        <div class="layout_body schedule">
            
            <div class="ly_container">

                <div class="month_nav">
                    <a href="#" class="prev_month">이전달</a>
                    <span id="calendarYM" class="nowmonth_txt">2019<strong>년</strong>3<strong>월</strong></span> 
                    <a href="#" class="next_month">다음달</a>
                </div>

                <div class="calendar_list">
                    <dl class="date_list_con">
                        <dt>01.01<em>(화)</em></dt>
                        <dd>
                            <ul>
                                <li>
                                    <a href="/event/schedule_view?seq=20">
                                    <div class="thumb">
                                        <span></span>
                                    </div>
                                    <div class="data_con">
                                        <div class="data_cate">
                                            <span class="cate cate_view">전시</span>
                                        </div>
                                        <div class="data_title">스타시티 라플라스 문화 스타시티 라플라스 문화</div>
                                            <div class="data_des">
                                                <dl class="time">
                                                    <dt>시간</dt>
                                                    <dd>16:00 / 17:00</dd>
                                                </dl>
                                                <dl class="location">
                                                    <dt>장소</dt>
                                                    <dd>시티존 1F</dd>
                                                </dl>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                 <li>
                                    <a href="/event/schedule_view?seq=20">
                                    <div class="thumb">
                                        <span style="background-image: url(/resources/user/mobile/img/contents/event/schedule_thumb_img.jpg);"><img src="/resources/user/mobile/img/contents/event/schedule_thumb_img.jpg" alt=""></span>
                                    </div>
                                    <div class="data_con">
                                        <div class="data_cate">
                                            <span class="cate cate_play">공연</span>
                                        </div>
                                        <div class="data_title">스타시티 라플라스 문화 스타시티 라플라스 문화</div>
                                            <div class="data_des">
                                                <dl class="time">
                                                    <dt>시간</dt>
                                                    <dd>16:00 / 17:00</dd>
                                                </dl>
                                                <dl class="location">
                                                    <dt>장소</dt>
                                                    <dd>시티존 1F</dd>
                                                </dl>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </dd>
                    </dl>


                    <dl class="date_list_con">
                        <dt>01.14<em>(목)</em></dt>
                        <dd>
                            <ul>
                                <li>
                                    <a href="/event/schedule_view?seq=20">
                                    <div class="thumb">
                                        <span></span>
                                    </div>
                                    <div class="data_con">
                                        <div class="data_cate">
                                            <span class="cate cate_view">전시</span>
                                        </div>
                                        <div class="data_title">스타시티 라플라스 문화 스타시티 라플라스 문화</div>
                                            <div class="data_des">
                                                <dl class="time">
                                                    <dt>시간</dt>
                                                    <dd>16:00 / 17:00</dd>
                                                </dl>
                                                <dl class="location">
                                                    <dt>장소</dt>
                                                    <dd>시티존 1F</dd>
                                                </dl>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                 <li>
                                    <a href="/event/schedule_view?seq=20">
                                    <div class="thumb">
                                        <span style="background-image: url(/resources/user/mobile/img/contents/event/schedule_thumb_img.jpg);"><img src="/resources/user/mobile/img/contents/event/schedule_thumb_img.jpg" alt=""></span>
                                    </div>
                                    <div class="data_con">
                                        <div class="data_cate">
                                            <span class="cate cate_play">공연</span>
                                        </div>
                                        <div class="data_title">스타시티 라플라스 문화 스타시티 라플라스 문화</div>
                                            <div class="data_des">
                                                <dl class="time">
                                                    <dt>시간</dt>
                                                    <dd>16:00 / 17:00</dd>
                                                </dl>
                                                <dl class="location">
                                                    <dt>장소</dt>
                                                    <dd>시티존 1F</dd>
                                                </dl>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </dd>
                    </dl>

                </div>
                <!-- //calendar_list -->
            </div>
            <!-- //ly_container -->
        </div>
        <!-- //layout_body -->

<%@include file="../inc/inc_gl_bottom.jsp"%>
