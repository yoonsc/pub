<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

        <div class="sub_title">
            <h2>마이페이지</h2>
            <p class="en_summary">MY PAGE</p>
        </div>

        <div class="layout_body">

            <div class="ly_container">

                <ul class="tab_boxtype tablist_3 mb25">
                    <li><a href="my_contact_list">나의 문의내역</a></li>
                    <li class="on"><a href="my_favorite">나의 관심매장</a></li>
                    <li><a href="my_status">회원정보 보기</a></li>
                </ul>


                <!-- 등록된 매장이 없을경우
                <div class="no_favorite_data">
                    <dl>
                        <dt><i><img src="/resources/user/mobile/img/ico_nodata.png" alt=""></i></dt>
                        <dd>등록된 관심 매장이 없습니다.</dd>
                    </dl>
                </div>
                -->

                <div class="edit_btn_con">
                    <label class="all_label">
                        <input type="checkbox" name="f_chkAll" class="checkbox">
                        <span class="label"></span> 전체선택
                    </label>

                    <ul class="edit_now">
                        <li><a href="javascript:;" class="btn btn_round_nm" id="edit_favorite"><i class="lnr lnr-checkmark-circle"></i> 편집</a></li>
                    </ul>
                    <ul class="edit_next">
                        <li><a href="javascript:;" class="btn btn_round_nm" id="cancel_favorite">취소</a></li>
                        <li><a href="javascript:;" class="btn btn_round_nmbg"><i class="far fa-times-circle"></i> 삭제</a></li>
                    </ul>
                </div>




                <ul class="floor_shop_list">
                    <li>
                        <div class="list_left">
                            <div class="box_check_con">
                                <label class="chk_round">
                                    <input type="checkbox" name="f_chk" class="checkbox">
                                    <span class="label"></span>
                                </label>
                                <span class="bg"></span>
                            </div>
                            <span class="thumb"><img src="/resources/user/mobile/img/contents/about/logo_shop_list.jpg" alt=""></span>
                        </div>
                        <div class="shop_box_con">
                            <dl>
                                <dt>반디앤루니스반디앤루니스반디앤루니스반디앤루니스반디앤루니스</dt>
                                <dd>
                                    <ul>
                                        <li><strong>대표전화 :</strong><span class="data">02-2218-3050</span></li>
                                        <li><strong>매장위치 :</strong><span class="data">스타존 B1</span></li>
                                    </ul>
                                </dd>
                            </dl>
                            <div class="btn_box">
                                <a href="#" class="btn btn_round_nm">상세보기</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="list_left">
                            <div class="box_check_con">
                                <label class="chk_round">
                                    <input type="checkbox" name="f_chk" class="checkbox">
                                    <span class="label"></span>
                                </label>
                                <span class="bg"></span>
                            </div>
                            <span class="thumb"><img src="/resources/user/mobile/img/contents/about/logo_shop_list.jpg" alt=""></span>
                        </div>
                        <div class="shop_box_con">
                            <dl>
                                <dt>반디앤루니스반디앤루니스반디앤루니스반디앤루니스반디앤루니스</dt>
                                <dd>
                                    <ul>
                                        <li><strong>대표전화 :</strong><span class="data">02-2218-3050</span></li>
                                        <li><strong>매장위치 :</strong><span class="data">스타존 B1</span></li>
                                    </ul>
                                </dd>
                            </dl>
                            <div class="btn_box">
                                <a href="#" class="btn btn_round_nm">상세보기</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- //ly_container -->
        </div>
        <!-- //layout_body -->

    <script type="text/javascript">
            $(function(){

                var $label = $(".all_label"),
                    $listBox = $(".list_con"),
                    $chkBox = $(".box_check_con"),
                    $editbtn = $(".edit_next"),
                    $nowBtn = $(".edit_now");

                    $label.css("display","none");
                    $listBox.addClass("hover_box");
                    $chkBox.hide();
                    $editbtn.css("display","none");

                $("#edit_favorite").click(function(e){
                    $nowBtn.hide();
                    $chkBox.show();
                    $editbtn.show();
                    $label.css("display","inline-block");
                    $listBox.removeClass("hover_box");
                });

                $("#cancel_favorite").click(function(e){
                    $nowBtn.show();
                    $chkBox.hide();
                    $editbtn.hide();
                    $label.css("display","none");
                    $listBox.addClass("hover_box");

                    $(".chk_round").find(":checkbox").prop('checked', false);
                    $("input[name='f_chkAll']").prop('checked', false);
                });

                $("input[name='f_chkAll']").change(function() {
                    var selectchk =  $(".chk_round").find(":checkbox");
                    if (this.checked) {
                        selectchk.prop('checked', true);
                    } else {
                        selectchk.prop('checked', false);
                    }
                });
            });
    </script>



<%@include file="../inc/inc_gl_bottom.jsp"%>
