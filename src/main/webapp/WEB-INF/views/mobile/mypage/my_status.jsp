<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

    <div class="sub_title">
        <h2>마이페이지</h2>
        <p class="en_summary">MY PAGE</p>
    </div>

    <div class="layout_body">

        <div class="ly_container">

            <ul class="tab_boxtype tablist_3 mb25">
                <li><a href="my_contact_list">나의 문의내역</a></li>
                <li><a href="my_favorite">나의 관심매장</a></li>
                <li class="on"><a href="my_status">회원정보 보기</a></li>
            </ul>

             <div class="register_form register_view">
                <h3 class="register_tit">일반회원 </h3>
                <dl>
                    <dt>아이디 </dt>
                    <dd>
                       TESTID1234
                    </dd>
                </dl>
                <dl>
                    <dt>비밀번호</dt>
                    <dd class="btn_in">
                        <a href="my_chgpw" class="btn box-blue">비밀번호 변경</a>
                    </dd>
                </dl>
                <dl>
                    <dt>이름</dt>
                    <dd>홍길동</dd>
                </dl>
                <dl>
                    <dt>이메일</dt>
                    <dd>admin@gmail.com</dd>
                </dl>
                <dl>
                    <dt>휴대전화 번호</dt>
                    <dd>010-5358-5381</dd>
                </dl>
                <dl>
                    <dt>주소</dt>
                    <dd class="pt10">우편번호 : 13639</dd>
                    <dd class="pt10">경기 성남시 수정구 위례광장로 97 1111-1111</dd>
                </dl>
                <dl>
                    <dt>성별</dt>
                    <dd>남성</dd>
                </dl>
                <dl>
                    <dt>생년월일</dt>
                    <dd>19780127</dd>
                </dl>
                <dl>
                    <dt>마케팅 정보 수신여부 선택</dt>
                    <dd>
                        <label>
                            <input type="checkbox" name="hAgEmail" class="checkbox" checked="" onclick="return false;">
                            <span class="label"></span> 이메일
                        </label>
                        <label>
                            <input type="checkbox" name="hAgSms" class="checkbox" checked="" onclick="return false;">
                            <span class="label"></span> SMS
                        </label>
                    </dd>
                </dl>
            </div>
            <div class="register_btm">
                <div class="button_group txt-cnt">
                    <span><a href="javascript:;" class="btn box-blue submit">회원정보수정</a></span>
                </div>
            </div>

        </div>
        <!-- //ly_container -->
    </div>
    <!-- //layout_body -->



<%@include file="../inc/inc_gl_bottom.jsp"%>
