<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

    <div class="sub_title">
        <h2>마이페이지</h2>
        <p class="en_summary">MY PAGE</p>
    </div>

    <div class="layout_body">

        <div class="ly_container">

            <ul class="tab_boxtype tablist_3 mb25">
                <li><a href="my_contact_list">나의 문의내역</a></li>
                <li><a href="my_favorite">나의 관심매장</a></li>
                <li class="on"><a href="my_status">회원정보 보기</a></li>
            </ul>
        </div>


        <div class="ly_container">

            <div class="register_form pw_view pt15">
               <h3 class="register_tit txt-cnt">안전한 비밀번호로 내 정보를 보호하세요.</h3>
               <dl>
                   <dt>현재 비밀번호</dt>
                   <dd><input type="password" name="password" title="현재 비밀번호를 입력해주세요." style="width: 100%;"></dd>
               </dl>
               <dl>
                   <dt>새 비밀번호</dt>
                   <dd><input type="password" id="newPassword" name="newPassword" title="새로운 비밀번호를 입력해주세요." style="width: 100%;"></dd>
               </dl>
               <dl>
                   <dt>새 비밀번호 확인</dt>
                   <dd>
                       <input type="password" name="newPasswordExp" title="새로운 비밀번호를 다시 입력해주세요." style="width: 100%;">
                   </dd>
                   <dd>
                       <ul class="comment">
                           <li>안내
                               <ul class="bl_line" style="margin-left: -1.5rem; padding-top: .5rem;">
                                   <li>비밀번호는 8~20자의 영문 대 소문자, 숫자, 특수문자를 이용하여 입력해주세요.</li>
                                   <li>연속된 번호 또는 주민등록번호, 생일, 전화번호 등의 개인정보 관련된
                                     숫자는 고객님의 소중한 개인정보 보호를 위해 사용하지 않도록
                                     주의하시기 바랍니다. </li>
                               </ul>
                           </li>
                       </ul>
                   </dd>
                   <dd class="btn_in txt-cnt pt25">
                       <span class="col-2x"><a href="my_status" class="btn box-white">취소</a></span>
                       <span class="col-2x"><a href="javascript:goChangePw(document.editForm);" class="btn box-blue submit">확인</a></span>
                   </dd>
               </dl>

           </div>

       </div>
       <!-- //ly_container -->

    </div>

<%@include file="../inc/inc_gl_bottom.jsp"%>
