<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>
    <div class="prev_title">
        <a href="javascript:history.go(-1);"><span class="lnr lnr-chevron-left"></span></a>
        <h2>회원정보변경</h2>
    </div>

    <div class="layout_body">

        <div class="ly_container">

             <div class="register_form register_view pt15">
                <h3 class="register_tit txt-cnt">개인정보를 변경을 위해 비밀번호를 입력해주세요.</h3>
                <dl>
                    <dt>비밀번호확인</dt>
                    <dd>
                        <input type="password" name="password" style="width: 100%;">
                    </dd>
                    <dd class="btn_in ph20">
                        <a href="javascript:confirmPw(document.loginForm);" class="btn box-blue">확인</a>
                    </dd>
                </dl>
            </div>

        </div>
        <!-- //ly_container -->
    </div>
    <!-- //layout_body -->


<%@include file="../inc/inc_gl_bottom.jsp"%>
