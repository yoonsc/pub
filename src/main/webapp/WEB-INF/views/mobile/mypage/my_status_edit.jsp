<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

    <div class="prev_title">
        <a href="javascript:history.go(-1);"><span class="lnr lnr-chevron-left"></span></a>
        <h2>회원정보변경</h2>
    </div>

    <div class="layout_body">

        <div class="ly_container">

             <div class="register_form register_view pt15">
                <h3 class="register_tit">일반회원 </h3>
                <dl>
                    <dt>아이디 </dt>
                    <dd>TESTID1234</dd>
                </dl>
                <dl>
                    <dt>비밀번호</dt>
                    <dd class="btn_in">
                        <a href="my_chgpw" class="btn box-blue">비밀번호 변경</a>
                    </dd>
                </dl>
                <dl>
                    <dt>이름</dt>
                    <dd><input type="text" name="name" class="boxline" id="name" value="홍길동" readonly="" style="width: 100%;"></dd>
                </dl>
                <dl>
                    <dt>이메일</dt>
                    <dd class="email_cell">
                        <p>
                            <span><input type="text" id="hEmail1" name="hEmail1" class="boxline" value="admin"></span>
                            <span><input type="text" id="hEmail2" name="hEmail2" class="boxline" value="gmail.com"></span>
                        </p>
                        <p class="pt10">
                            <span class="select-wrapper">
                                <select name="mailserver" title="이메일 서버선택" onchange="$('#hEmail2').val( $(this).val() == 'D' ? '' : $(this).val() );">
                                    <option value="D">직접입력</option>
                                    <option value="naver.com">naver.com</option>
                                    <option value="gmail.com">gmail.com</option>
                                    <option value="daum.net">daum.net</option>
                                    <option value="hanmail.net">hanmail.net</option>
                                    <option value="hotmail.com">hotmail.com</option>
                                    <option value="icloud.com">icloud.com</option>
                                    <option value="yahoo.com">yahoo.com</option>
                                </select>
                            </span>
                        </p>
                        <ul class="comment">
                            <li>중요소식 및 뉴스레터 수신을 위해 정확한 이메일 주소를 입력해주세요.</li>
                        </ul>
                    </dd>
                </dl>
                <dl>
                    <dt>휴대전화 번호</dt>
                    <dd class="phone_cell">
                        <input type="text" name="phone1" id="phone1" class="number-check" value="010" maxlength="4" title="전화번호첫번호"  onkeydown="onlyNumber(this);" readonly="">
                        <span class="phone_dash">
                            <input type="text" name="phone2" id="phone2" class="number-check" value="5358" maxlength="4" title="전화번호중간번호" onkeydown="onlyNumber(this);" readonly="">
                        </span>
                        <span class="phone_dash">
                            <input type="text" name="phone3" id="phone3" class="number-check" value="5381" maxlength="4" title="전화번호끝번호" onkeydown="onlyNumber(this);" readonly="">
                        </span>
                    </dd>
                </dl>
                <dl>
                    <dt>주소</dt>
                    <dd class="check_btn">
                        <span><input type="text" name="zipcode" class="boxline" id="hZip" value="13639" readonly="readonly"></span>
                        <a href="javascript:searchAddr(document.editForm);" class="btn box-navy" style="vertical-align: top;">우편번호 찾기</a>
                    </dd>
                    <dd><input type="text" name="addr1" class="boxline" id="hZip2" style="width: 100%;" value="경기 성남시 수정구 위례광장로 97" readonly="readonly"></dd>
                    <dd><input type="text" name="addr2" class="boxline" id="hZip3" style="width: 100%;" value="1111-1111"></dd>
                </dl>
                <dl>
                    <dt>성별</dt>
                    <dd>
                        <label>
                            <input type="radio" name="genderType" class="radio" checked="" value="1">
                            <span class="label"></span>남성
                        </label>

                        <label>
                            <input type="radio" name="genderType" class="radio" value="2">
                            <span class="label"></span>여성
                        </label>

                        <label>
                            <input type="radio" name="genderType" class="radio" value="3">
                            <span class="label"></span>선택안함
                        </label>
                    </dd>
                </dl>
                <dl>
                    <dt>생년월일</dt>
                    <dd><input type="text" name="birth" id="hBirth" value="19780727" style="width: 100%;" readonly="readonly"></dd>
                </dl>
                <dl>
                    <dt>마케팅 정보 수신여부 선택</dt>
                    <dd>
                        <label>
                            <input type="checkbox" name="hAgEmail" class="checkbox" checked="">
                            <span class="label"></span> 이메일
                        </label>
                        <label>
                            <input type="checkbox" name="hAgSms" class="checkbox" checked="">
                            <span class="label"></span> SMS
                        </label>
                    </dd>
                </dl>
            </div>
            <div class="register_btm">
                <div class="button_group txt-cnt">
                    <span><a href="javascript:;" class="btn box-blue submit">회원정보수정</a></span>
                </div>
            </div>

        </div>
        <!-- //ly_container -->
    </div>
    <!-- //layout_body -->


<%@include file="../inc/inc_gl_bottom.jsp"%>
