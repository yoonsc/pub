<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

        <div class="prev_title">
            <a href="javascript:history.go(-1);"><span class="lnr lnr-chevron-left"></span></a>
            <h2>문의내역 상세</h2>
        </div>



        <div class="layout_body">

            <div class="ly_container activity_view mt30">
                <dl class="board_tit">
                    <dt>
                        <span class="txt">스타시티 5월 문화공연 안내 말줄임 없는 공지사항 제목이 노출됩니다. </span>
                    </dt>
                    <dd>
                        <span class="date">2019.05.10</span>
                    </dd>
                </dl>

                <div class="board_data notice_data">
                    <div class="data_txt">
                        <p>스타시티 5월 문화공연 안내 내용이 들어갑니다. 스타시티 5월 문화공연 안내 내용이 들어갑니다.</p>
                        <p>스타시티 5월 문화공연 안내 내용이 들어갑니다. 스타시티 5월 문화공연 안내 내용이 들어갑니다.</p>
                        <p>스타시티 5월 문화공연 안내 내용이 들어갑니다. 스타시티 5월 문화공연 안내 내용이 들어갑니다.</p>
                        <p>스타시티 5월 문화공연 안내 내용이 들어갑니다. 스타시티 5월 문화공연 안내 내용이 들어갑니다.</p>
                        <p>스타시티 5월 문화공연 안내 내용이 들어갑니다. 스타시티 5월 문화공연 안내 내용이 들어갑니다.</p>
                    </div>
                </div>

                <div class="board_reply">
                    <dl>
                        <dt>
                            <span class="lable_round label_sm box-blue">답변완료</span>
                            <!-- <span class="lable_round label_sm box-grey">답변대기</span> -->
                        </dt>
                        <dd>
                            <p>안녕하세요 홍길동 고객님 <br />
                            현재 6월 문화행사는 아직 기획중인 사항으로 확정된 행사는 아직까지 없습니다.<br />
                            6월 행사 계획이 완료되면 별도 공지를 통해 안내해드리도록 하겠습니다. <br />
                            감사합니다.
                            </p>
                            <span class="date">2019.05.10 17:00</span>
                        </dd>
                    </dl>
                </div>

                <div class="board_file">
                    <dl>
                        <dt><i class="lnr lnr-paperclip"></i> 첨부파일</dt>
                        <dd>
                            <ul>
                                <li><a href="/fileInfo/download?seq=30" target="_blank"><strong>images (9).jpg </strong> <br /><i>다운로드</i></a></li>
                                <li><a href="/fileInfo/download?seq=30" target="_blank"><strong>첨부파일명 첨부파일명이 나옵니다.pdf  </strong> <br /><i>다운로드</i></a></li>
                            </ul>
                        </dd>
                    </dl>
                </div>


                <div class="board_btm txt-cnt">
                    <span><a href="/service/notice" class="btn btn_round_nm"><i class="fas fa-bars"></i> 목록</a></span>
                    <span><a href="/service/contact" class="btn btn_round_nm">다시문의</a></span>
                </div>
            </div>
            <!-- //ly_container -->

        </div>
        <!-- //layout_body -->


<%@include file="../inc/inc_gl_bottom.jsp"%>
