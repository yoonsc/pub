<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

        <div class="sub_title">
            <h2>마이페이지</h2>
            <p class="en_summary">MY PAGE</p>
        </div>

        <div class="layout_body">

            <div class="ly_container">

                <ul class="tab_boxtype tablist_3">
                    <li class="on"><a href="my_contact_list">나의 문의내역</a></li>
                    <li><a href="my_favorite">나의 관심매장</a></li>
                    <li><a href="my_status">회원정보 보기</a></li>
                </ul>

                <ul class="news_list">
                    <!-- 검색값이 없을때 -->
                    <li class="nodata">등록된 내용이 없습니다.</li>
                    <li>
                        <a href="#">
                            <dl>
                                <dt>
                                    <span class="lable_round box-grey">답변대기</span>
                                    <span class="sbj">HMC 환불진행 관련 문의 드려요드려요드려요</span>
                                </dt>
                                <dd>2019-02-15</dd>
                            </dl>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <dl>
                                <dt>
                                    <span class="lable_round box-blue">답변완료</span>
                                    <span class="sbj">HMC 환불진행 관련 문의 드려요드려요드려요</span>
                                </dt>
                                <dd>2019-02-15</dd>
                            </dl>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <dl>
                                <dt>
                                    <span class="lable_round box-blue">답변완료</span>
                                    <span class="sbj">HMC 환불진행 관련 문의 드려요드려요드려요</span>
                                </dt>
                                <dd>2019-02-15</dd>
                            </dl>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <dl>
                                <dt>
                                    <span class="lable_round box-blue">답변완료</span>
                                    <span class="sbj">HMC 환불진행 관련 문의 드려요드려요드려요</span>
                                </dt>
                                <dd>2019-02-15</dd>
                            </dl>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <dl>
                                <dt>
                                    <span class="lable_round box-blue">답변완료</span>
                                    <span class="sbj">HMC 환불진행 관련 문의 드려요드려요드려요</span>
                                </dt>
                                <dd>2019-02-15</dd>
                            </dl>
                        </a>
                    </li>
                </ul>

                <div class="pagination">
                    <ul>
                        <li><a href="javascript:;" onclick="" class="prev pagbtn">이전</a></li>
                        <li><strong>1</strong></li>
                        <li><a href="javascript:onPaging(document.listForm, 2)">2</a></li>
                        <li><a href="javascript:;" onclick="onPaging(document.listForm, 2)" class="next pagbtn">다음</a></li>
                    </ul>
                </div>

            </div>
            <!-- //ly_container -->
        </div>
        <!-- //layout_body -->


<%@include file="../inc/inc_gl_bottom.jsp"%>
