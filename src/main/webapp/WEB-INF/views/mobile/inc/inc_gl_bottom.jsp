<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    </div>
    <!-- //container -->
    
    <!-- footer -->
    <footer>
       <!--  <a href="#" id="go_top"><span class="hidden">위로가기</span></a> -->  
    
       <div class="lang_link">
           <a href="/eng/">ENG</a>
       </div>
       <div class="familysite" id="Fnfamily">
            <a href="javascript:;">FAMILY SITE</a>
            <div>
                <ul class="list">
                    <li><a target="_blank" href="http://www.theclassic500.com/">더 클래식 500</a></li>
                    <li><a target="_blank" href="http://www.konkuk.ac.kr">건국대학교 </a></li>
                    <li><a target="_blank" href="http://www.kku.ac.kr/">건국대학교 글로컬 캠퍼스</a></li>
                    <li><a target="_blank" href="http://www.kuh.ac.kr/">건국대학교병원</a></li>
                    <li><a target="_blank" href="http://www.kuh.co.kr/">건국대학교 충주병원</a></li>
                    <li><a target="_blank" href="http://www.konkukmilk.co.kr/">건국유업</a></li>
                    <li><a target="_blank" href="http://kugolf.co.kr/">스마트KU골프파빌리온</a></li>
                    <li><a target="_blank" href="http://www.psuca.edu/">PSU</a></li>
                    <li><a target="_blank" href="http://www.konkukham.com/">건국햄</a></li>
                </ul>
            </div>
        </div>
        <!-- //familysite -->
    
       <ul class="footer_link">
            <li><a href="/mobile/etc/service_rules">이용약관</a></li>
            <li><a href="/mobile/etc/privacy">개인정보처리방침</a></li>
            <li><a href="/mobile/etc/email_refuse">이메일집단수집거부</a></li>
            <li><a href="/mobile/etc/movie_rules">영상정보처리기기 운영관리 방침</a></li>
            <li><a href="/mobile/service/information">입점안내</a></li>
            <li><a href="/mobile/event/sales_info">대관안내</a></li>
        </ul>
    
        <address><span>(05065) 서울특별시 광진구 자양동 아차산로 272 (자양동) 스타시티몰</span><span>대표전화 : 02-2024-1500</span></address>
        <p class="copyright">COPYRIGHT ⓒ 2019 KONKUK AMC. ALL RIGHTS RESERVED.</p>
        
        <div class="btn_box">
            <a href="#" class="btn btn_verpc">PC버전 보기</a>
        </div>    
    
    </footer>
    <!-- //footer -->
    <div id="mask"></div>
    <div id="layerScreen"></div>
    <!--//mask-->
    </div>
    <!-- //wrap -->

    <script>
        $(function(){
            gnb.init();
            selectUpBox.init("#Fnfamily");
        });
    </script>

    </body>
</html>
