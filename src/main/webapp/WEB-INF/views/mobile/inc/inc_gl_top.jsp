<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width, user-scalable=no" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="robots" content="all" />
    <title>스타시티</title>
    <meta name="author" content="starcity 스타시티" />
    <meta name="title" content="starcity 스타시티" />
    <meta name="Description" content="starcity 스타시티">
    <meta name="Keywords" content="starcity, 스타시티">
    <meta property="og:type" content="website">
    <meta property="og:title" content="스타시티 | starcity">
    <meta property="og:description" content="starcity">
    <meta property="og:url" content="http://www.starcity.co.kr">
    <meta property="og:image" content="/resources/user/mobile/img/sns_image.png">
    <link rel="shortcut icon" href="/resources/user/mobile/img/common/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/resources/user/mobile/css/style.css"/>
    <script src="/resources/user/mobile/js/libs/jquery.min.js"></script>
    <script src="/resources/user/mobile/js/libs/greensock/TweenMax.min.js"></script>
    <script src="/resources/user/mobile/js/libs/slick.min.js"></script>
    <script src="/resources/user/mobile/js/front.js"></script>
</head>

<body>
<!-- wrap -->
<div id="wrap">

    <!-- header -->
    <header id="header">
        <div class="header_in">
            <a href="javascript:;" id="gnb_open" class="gnb_open">
                <em class="line1"></em>
                <em class="line2"></em>
                <em class="line3"></em>
            </a>
            <h1><a href="/mobile/index"><img src="/resources/user/mobile/img/logo.png" alt="" /></a></h1>
            <a href="javascript:;" id="search_open" class="search_open"></a>
        </div>
    </header>
    <div class="header_search">
        <div class="select_box_top">
            <span class="search_in">
                <div class="placeholder">
                    <input type="text" name="searchText" id="searchText" value="" placeholder="매장명 또는 키워드를 입력하세요">
                </div>
                <a href="javascript:getStoreList(document.listForm, 0, 0, 0, 2);" class="btn btn-search">검색</a>
            </span>
        </div>
        <div class="search_close">
            <a href="javascript:;">닫기 <i class="lnr lnr-cross"></i></a>
        </div>
    </div>
    <nav id="gnb">
        <div class="gnb_close">
            <span class="lnb_login">
                <a href="/mobile/member/login" class="lnb_login_btn">로그인</a>
                <a href="/mobile/member/join">회원가입</a>
            </span>
            <a href="javascript:;" id="gnb_close" class="gnb_close"></a>
        </div>
        <div class="gnb_quick">
            <ul>
                <li>
                    <a href="/mobile/index">
                        <dl>
                            <dt><i class="ico ico_lnb_home"></i></dt>
                            <dd>홈</dd>
                        </dl>
                    </a>
                </li>
                <li>
                    <a href="/mobile/about/floor_info">
                        <dl>
                            <dt><i class="ico ico_lnb_danzi"></i></dt>
                            <dd>단지안내</dd>
                        </dl>
                    </a>
                </li>
                <li>
                    <a href="/mobile/about/floor_info">
                        <dl>
                            <dt><i class="ico ico_lnb_floor"></i></dt>
                            <dd>층별안내</dd>
                        </dl>
                    </a>
                </li>
                <li>
                    <a href="/mobile/about/opening">
                        <dl>
                            <dt><i class="ico ico_lnb_time"></i></dt>
                            <dd>영업시간</dd>
                        </dl>
                    </a>
                </li>
                <li>
                    <a href="/mobile/about/parking">
                        <dl>
                            <dt><i class="ico ico_lnb_parking"></i></dt>
                            <dd>주차안내</dd>
                        </dl>
                    </a>
                </li>
                <li>
                    <a href="/mobile/about/location">
                        <dl>
                            <dt><i class="ico ico_lnb_location"></i></dt>
                            <dd>오시는길</dd>
                        </dl>
                    </a>
                </li>
            </ul>
        </div>

        <ul class="depth0">
            <li class="remote"><a href="javascript:;"><strong>스타시티 소개</strong></a>
                <ul class="depth1">
                    <li><a href="/mobile/about/overview">스타시티 소개</a></li>
                    <li><a href="/mobile/about/floor_info">층별안내</a></li>
                    <li><a href="/mobile/about/floor_search">매장검색</a></li>
                    <li><a href="/mobile/about/convenience">편의시설</a></li>
                    <li><a href="/mobile/about/opening">영업시간</a></li>
                    <li><a href="/mobile/about/parking">주차안내</a></li>
                    <li><a href="/mobile/about/location">오시는길</a></li>
                </ul>
            </li>
            <li class="remote"><a href="javascript:;"><strong>스타시티 가이드</strong></a>
                <ul class="depth1">
                    <li><a href="/mobile/guide/guide#1">시티존</a></li>
                    <li><a href="/mobile/guide/guide#2">스타존</a></li>
                    <li><a href="/mobile/guide/guide#3">영존</a></li>
                    <li><a href="/mobile/guide/guide#4">더클래식500</a></li>
                </ul>
            </li>
            <li class="remote"><a href="javascript:;"><strong>컬처&amp;이벤트</strong></a>
                <ul class="depth1">
                    <li><a href="/mobile/event/event" data-link="/event/event_view">이벤트</a></li>
                    <li><a href="/mobile/event/schedule" data-link="/event/schedule_view">공연/전시일정</a></li>
                    <li><a href="/mobile/event/activity" data-link="/event/activity_view">공연/전시활동</a></li>
                    <li><a href="/mobile/event/sales_info">대관안내</a></li>
                </ul>
            </li>
            <li class="remote"><a href="javascript:;"><strong>고객서비스</strong></a>
                <ul class="depth1">
                    <li><a href="/mobile/service/notice" data-link="/service/notice_view">공지사항</a></li>
                    <li><a href="/mobile/service/faq">FAQ</a></li>
                    <li><a href="/mobile/service/contact">고객문의</a></li>
                    <li><a href="/mobile/service/information">입점안내</a></li>
                </ul>
            </li>

        </ul>
    </nav>
    <div class="gnb_mask"></div>
    <!-- // header-->

    <!-- container -->
    <div id="container">
