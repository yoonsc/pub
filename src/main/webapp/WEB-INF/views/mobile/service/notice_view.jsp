<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

    <div class="prev_title">
        <a href="javascript:history.go(-1);"><span class="lnr lnr-chevron-left"></span></a>
        <h2>공지사항</h2>
    </div>


    <div class="layout_body mt25">

        <div class="ly_container activity_view">

            <dl class="board_tit">
                <dt>
                    <span class="txt">스타시티 5월 문화공연 안내 말줄임 없는 공지사항 제목이 노출됩니다. </span>
                </dt>
                <dd>
                    <span class="date">2019.05.10</span>
                </dd>
            </dl>

            <div class="board_data">
                <div class="data_txt">
                    <p><img src="http://starcity.midashelp.com/resources/upload/editorImage/20190313100615267010.jpg" alt="" /></p>
                </div>
            </div>

            <div class="board_file">
                <dl>
                    <dt><i class="lnr lnr-paperclip"></i> 첨부파일</dt>
                    <dd>
                        <ul>
                            <li><a href="/fileInfo/download?seq=30" target="_blank"><strong>images (9).jpg </strong> <br /><i>다운로드</i></a></li>
                            <li><a href="/fileInfo/download?seq=30" target="_blank"><strong>첨부파일명 첨부파일명이 나옵니다.pdf  </strong> <br /><i>다운로드</i></a></li>
                        </ul>
                    </dd>
                </dl>
            </div>


            <div class="board_nav">
                <dl class="data_nav_prev">
                    <dt>이전글</dt>
                    <dd>
                        등록된 이전글이 없습니다.
                    </dd>

                </dl>
                <dl class="data_nav_next">
                    <dt>다음글</dt>
                    <dd>
                        <a href="#">다음 등록된 게시물입니다.</a>
                    </dd>
                </dl>
            </div>


            <div class="board_btm txt-cnt">
                <span><a href="/service/notice" class="btn btn_round_nm"><i class="fas fa-bars"></i> 목록</a></span>
            </div>
        </div>
        <!-- //ly_container -->

    </div>
    <!-- //layout_body -->

<%@include file="../inc/inc_gl_bottom.jsp"%>