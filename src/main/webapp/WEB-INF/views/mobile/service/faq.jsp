<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>
        <div class="sub_title">
            <h2>자주하는 질문</h2>
            <p class="en_summary">FAQ</p>
        </div>

        <div class="layout_body notice">

            <div class="ly_container">
                <div class="select_box_top">
                    <span class="select">
                        <span class="select-wrapper">
                            <select id="searchType" name="searchType">
                                <option value="0">전체</option>
                                <option value="1">제목</option>
                                <option value="2">내용</option>
                            </select>
                        </span>
                    </span>
                    <span class="search_in">
                        <div class="placeholder">
                            <input type="text" name="searchText" id="searchText" class="btmline" placeholder="검색어를 입력해주세요." value="">
                        </div>
                        <a href="javascript:fn_search(document.listForm);" class="btn btn-search">검색</a>
                    </span>
                </div>
                <!-- //select_box_top -->

                <ul class="faq_list" id="faq_list">
                    <li>
                        <dl class="ac-cont">
                            <dt>
                                <a href="javascript:;" title="내용 보기">
                                    <div class="left_sec">
                                        <span class="cate">고객안내</span>
                                        <em class="ico_q">Q</em>
                                    </div>
                                    <div class="right_sec">롯데백화점은 어디있나요?</div>
                                    <i></i>
                                </a>
                            </dt>
                            <dd>
                                <em class="ico_a">A</em>
                                <div>
                                    <p>2번 출구 500m 앞에 있습니다.</p>
                                    <p>네비게이션 켜서 오세요~</p>
                                </div>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl class="ac-cont">
                            <dt>
                                <a href="javascript:;" title="내용 보기">
                                    <div class="left_sec">
                                        <span class="cate">고객안내</span>
                                        <em class="ico_q">Q</em>
                                    </div>
                                    <div class="right_sec">롯데백화점은 어디있나요?</div>
                                    <i></i>
                                </a>
                            </dt>
                            <dd>
                                <em class="ico_a">A</em>
                                <div>
                                    <p>2번 출구 500m 앞에 있습니다.</p>
                                    <p>네비게이션 켜서 오세요~</p>
                                </div>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl class="ac-cont">
                            <dt>
                                <a href="javascript:;" title="내용 보기">
                                    <div class="left_sec">
                                        <span class="cate">고객안내</span>
                                        <em class="ico_q">Q</em>
                                    </div>
                                    <div class="right_sec">롯데백화점은 어디있나요?</div>
                                    <i></i>
                                </a>
                            </dt>
                            <dd>
                                <em class="ico_a">A</em>
                                <div>
                                    <p>2번 출구 500m 앞에 있습니다.</p>
                                    <p>네비게이션 켜서 오세요~</p>
                                </div>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl class="ac-cont">
                            <dt>
                                <a href="javascript:;" title="내용 보기">
                                    <div class="left_sec">
                                        <span class="cate">고객안내</span>
                                        <em class="ico_q">Q</em>
                                    </div>
                                    <div class="right_sec">롯데백화점은 어디있나요?</div>
                                    <i></i>
                                </a>
                            </dt>
                            <dd>
                                <em class="ico_a">A</em>
                                <div>
                                    <p>2번 출구 500m 앞에 있습니다.</p>
                                    <p>네비게이션 켜서 오세요~</p>
                                </div>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl class="ac-cont">
                            <dt>
                                <a href="javascript:;" title="내용 보기">
                                    <div class="left_sec">
                                        <span class="cate">고객안내</span>
                                        <em class="ico_q">Q</em>
                                    </div>
                                    <div class="right_sec">롯데백화점은 어디있나요?</div>
                                    <i></i>
                                </a>
                            </dt>
                            <dd>
                                <em class="ico_a">A</em>
                                <div>
                                    <p>2번 출구 500m 앞에 있습니다.</p>
                                    <p>네비게이션 켜서 오세요~</p>
                                </div>
                            </dd>
                        </dl>
                    </li>
                </ul>



                <div class="pagination">
                    <ul>
                        <li><a href="javascript:;" onclick="" class="prev pagbtn">이전</a></li>
                        <li><strong>1</strong></li>
                        <li><a href="javascript:onPaging(document.listForm, 2)">2</a></li>
                        <li><a href="javascript:;" onclick="onPaging(document.listForm, 2)" class="next pagbtn">다음</a></li>
                    </ul>
                </div>

            </div>
            <!-- //ly_container -->
        </div>
        <!-- //layout_body -->


<script type="text/javascript">
	$(function(){
        ui_faqAcMenu($("#faq_list"), true);
	});
</script>


<%@include file="../inc/inc_gl_bottom.jsp"%>