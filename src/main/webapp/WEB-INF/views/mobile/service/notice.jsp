<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>
    <div class="sub_title">
        <h2>공지사항</h2>
        <p class="en_summary">NOTICE</p>
    </div>

    <div class="layout_body notice">

        <div class="ly_container">
            <div class="select_box_top">
                <span class="select">
                    <span class="select-wrapper">
                        <select id="searchType" name="searchType">
                            <option value="0">전체</option>
                            <option value="1">제목</option>
                            <option value="2">내용</option>
                        </select>
                    </span>
                </span>
                <span class="search_in">
                    <div class="placeholder">
                        <input type="text" name="searchText" id="searchText" class="btmline" placeholder="검색어를 입력해주세요." value="">
                    </div>
                    <a href="javascript:fn_search(document.listForm);" class="btn btn-search">검색</a>
                </span>
            </div>
            <!-- //select_box_top -->


            <ul class="news_list">
                <!-- 검색값이 없을때 -->
                <li class="nodata">등록된 게시물이 없습니다.</li>
                <li class="tp_notice">
                    <a href="#">
                        <dl>
                            <dt>
                                <span class="cate">입찰공고</span>
                                <span class="sbj">숲세권, 학세권, 역세권까지 다양한 정주여건 두루 갖춘 프리미엄 아파트 광진 스위트엠</span>
                            </dt>
                            <dd>2019-02-15</dd>
                        </dl>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <dl>
                            <dt>
                                <span class="cate">입찰공고</span>
                                <span class="sbj">숲세권, 학세권, 역세권까지 다양한 정주여건 두루 갖춘 프리미엄 아파트 광진 스위트엠</span>
                            </dt>
                            <dd>2019-02-15</dd>
                        </dl>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <dl>
                            <dt>
                                <span class="cate">입찰공고</span>
                                <span class="sbj">숲세권, 학세권, 역세권까지 다양한 정주여건 두루 갖춘 프리미엄 아파트 광진 스위트엠</span>
                            </dt>
                            <dd>2019-02-15</dd>
                        </dl>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <dl>
                            <dt>
                                <span class="cate">입찰공고</span>
                                <span class="sbj">숲세권, 학세권, 역세권까지 다양한 정주여건 두루 갖춘 프리미엄 아파트 광진 스위트엠</span>
                            </dt>
                            <dd>2019-02-15</dd>
                        </dl>
                    </a>
                </li>
                 <li>
                    <a href="#">
                        <dl>
                            <dt>
                                <span class="cate">입찰공고</span>
                                <span class="sbj">숲세권, 학세권, 역세권까지 다양한 정주여건 두루 갖춘 프리미엄 아파트 광진 스위트엠</span>
                            </dt>
                            <dd>2019-02-15</dd>
                        </dl>
                    </a>
                </li>
            </ul>

            <div class="pagination">
                <ul>
                    <li><a href="javascript:;" onclick="" class="prev pagbtn">이전</a></li>
                    <li><strong>1</strong></li>
                    <li><a href="javascript:onPaging(document.listForm, 2)">2</a></li>
                    <li><a href="javascript:;" onclick="onPaging(document.listForm, 2)" class="next pagbtn">다음</a></li>
                </ul>
            </div>

        </div>
        <!-- //ly_container -->
    </div>
    <!-- //layout_body -->


<%@include file="../inc/inc_gl_bottom.jsp"%>