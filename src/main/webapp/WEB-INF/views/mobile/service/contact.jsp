<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

    <div class="sub_title">
        <h2>고객문의</h2>
        <p class="en_summary">RECEIVE AN INQUIRY</p>
    </div>

    <div class="layout_body contact">

        <div class="ly_container register_form">
            <div class="register_block">
                <span class="select-wrapper">
                 <select name="type" title="카테고리선택">
                   <option value="">카테고리선택</option>
                   <option value="1">일반문의</option>
                   <option value="2">프로젝트문의</option>
                   <option value="3">투자문의</option>
                   <option value="4">기타문의</option>
                 </select>
                </span>
            </div>

            <div class="register_block">
                <input type="text" name="title" id="hTitle" style="width:100%;" placeholder="제목을 입력하세요" >
            </div>

            <div class="register_block">
                <div class="momo_con">
                    <textarea name="contents" id="hMemo" style="width:100%; height: 280px;" placeholder="내용을 입력하세요 (2,000자 이내)" /></textarea>
                    <div class="remain_box">글자수 : <span class="remain_txt">0</span> / 2000</div>
                </div>

                <ul class="comment">
                    <li>내용은 2,000자(4000byte)까지 작성하실 수 있습니다.</li>
                    <li>문의 주신 내용에 대한 답변은 마이페이지 > 나의 문의내역에서 확인하실 수 있습니다. </li>
                    <li>문의 시 파일 첨부는 PC에서만 가능합니다. </li>
                </ul>
            </div>

            <div class="register_btm txt-cnt bt0">
                <span><a href="javascript:;" class="btn btn_round_nm"> 취소</a></span>
                <span class="ml10"><a href="javascript:submitPop();" class="btn btn_round_blue_bg"> 등록</a></span>
            </div>



        </div>
        <!-- //ly_container -->
    </div>
    <!-- //layout_body -->

    <!-- submit 팝업 시작-->
    <div id="confirmLayerBox" class="layerPopup confirm_layer">
        <div class="layerPopup_in">
            <span class="closeLayer">
                <a href="javascript:LayerPopups.close('confirmLayerBox');" class="btn"><i class="lnr lnr-cross"></i></a>
            </span>
            <div class="layer_txt">
                <dl class="submit_txt">
                    <dt>문의주신 내용이 <br /><strong>접수 완료</strong>되었습니다.</dt>
                    <dd>최대한 빠른 답변을 드릴 수 있도록 노력하겠습니다. <br />
                    답변은 <strong class="txt-line-black">마이페이지 > 나의 문의내역</strong> 에서<br /> 확인하실 수 있습니다. <br />
                    감사합니다. </dd>
                </dl>
            </div>
            <div class="button_group txt-cnt">
                <span><a href="javascript:LayerPopups.close('confirmLayerBox');" class="btn btn_round_blue_bg">확인</a></span>
            </div>
        </div>
    </div>
    <!-- //submit 팝업 -->



<script type="text/javascript">
    function submitPop(){
        LayerPopups.open('confirmLayerBox', true);
    }

	$(function(){
        charLimit.init("#hMemo", 2000);
	});
</script>


<%@include file="../inc/inc_gl_bottom.jsp"%>