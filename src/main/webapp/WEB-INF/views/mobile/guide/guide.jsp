<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

    <div class="sub_title">
        <h2>스타시티 가이드</h2>
        <p class="en_summary">STAR CITY GUIDE</p>
    </div>
   

    <div class="layout_body guide_view">   

        <div class="ly_container">
            <ul class="tab_boxtype tablist_4">
                <li class="on"><a href="#">시티존</a></li>
                <li><a href="#">스타존</a></li>
                <li><a href="#">영존</a></li>
                <li><a href="#">더 클래식 500</a></li>
            </ul>

            <div class="floor_select">               
                <span class="select-wrapper">
                    <select id="selectType" name="selectType">   
                        <option value="3F">3F</option> 
                        <option value="2F">2F</option> 
                        <option value="1F">1F</option> 
                        <option value="B1">B1</option> 
                        <option value="B2">B2</option> 
                        <option value="B3">B3</option> 
                        <option value="B4">B4</option> 
                    </select>
                </span>
            </div>
        

            <div class="floor_map_con">
                <div class="floor_zoom">
                    <a href="#"><i class="ico ico-plus"></i></a>
                    <a href="#"><i class="ico ico-minus"></i></a>
                </div>
                <div class="floor_map_small">
                    <img src="/resources/user/mobile/img/contents/guide/guide_map_view.jpg" alt="">
                </div>
            </div>


            <ul class="floor_shop_list ph20">
                <li>
                    <div class="list_left">
                        <span class="thumb"><img src="/resources/user/mobile/img/contents/about/logo_shop_list.jpg" alt=""></span>
                    </div>
                    <div class="shop_box_con">
                        <dl>
                            <dt>반디앤루니스반디앤루니스반디앤루니스반디앤루니스반디앤루니스</dt>
                            <dd>
                                대표전화 : 02-2218-3050
                                매장위치 : 스타존 B1
                            </dd>
                        </dl>
                        <div class="btn_box">
                            <a href="#" class="btn btn_round_blue">위치보기</a>
                            <a href="#" class="btn btn_round_nm">상세보기</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="list_left">
                        <span class="thumb"><img src="/resources/user/mobile/img/contents/about/logo_shop_list.jpg" alt=""></span>
                    </div>
                    <div class="shop_box_con">
                        <dl>
                            <dt>반디앤루니스반디앤루니스반디앤루니스반디앤루니스반디앤루니스</dt>
                            <dd>
                                대표전화 : 02-2218-3050
                                매장위치 : 스타존 B1
                            </dd>
                        </dl>
                        <div class="btn_box">
                            <a href="#" class="btn btn_round_blue">위치보기</a>
                            <a href="#" class="btn btn_round_nm">상세보기</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="list_left">
                        <span class="thumb"><img src="/resources/user/mobile/img/contents/about/logo_shop_list.jpg" alt=""></span>
                    </div>
                    <div class="shop_box_con">
                        <dl>
                            <dt>반디앤루니스반디앤루니스반디앤루니스반디앤루니스반디앤루니스</dt>
                            <dd>
                                대표전화 : 02-2218-3050
                                매장위치 : 스타존 B1
                            </dd>
                        </dl>
                        <div class="btn_box">
                            <a href="#" class="btn btn_round_blue">위치보기</a>
                            <a href="#" class="btn btn_round_nm">상세보기</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="list_left">
                        <span class="thumb"><img src="/resources/user/mobile/img/contents/about/logo_shop_list.jpg" alt=""></span>
                    </div>
                    <div class="shop_box_con">
                        <dl>
                            <dt>반디앤루니스반디앤루니스반디앤루니스반디앤루니스반디앤루니스</dt>
                            <dd>
                                대표전화 : 02-2218-3050
                                매장위치 : 스타존 B1
                            </dd>
                        </dl>
                        <div class="btn_box">
                            <a href="#" class="btn btn_round_blue">위치보기</a>
                            <a href="#" class="btn btn_round_nm">상세보기</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <!-- //ly_container -->
    </div>      
    <!-- //guide_view -->

<%@include file="../inc/inc_gl_bottom.jsp"%>
