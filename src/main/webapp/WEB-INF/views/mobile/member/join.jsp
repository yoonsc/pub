<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

    <div class="sub_title bg_grey">
        <h2>회원가입</h2>
        <p class="en_summary">JOIN</p>
    </div>

    <div class="layout_body">

        <div class="ly_container register_form">
            <div class="join_gate">
                <div class="normal_sec">
                    <dl>
                        <dt>일반 회원가입</dt>
                        <dd class="txt-cnt pb15">스타시티회원은 만 14세 이상 성인만 가입 가능합니다.</dd>
                        <dd class="btn_in">
                            <a href="step1_normal" class="btn bg_join_blue">일반 회원가입</a>
                        </dd>
                    </dl>
                </div>
                <div class="sns_sec">
                    <dl>
                        <dt>SNS계정으로 회원가입</dt>
                        <dd class="txt-cnt pb15">SNS계정인증을 통해 스타시티 회원으로 가입합니다.</dd>
                        <dd class="btn_in">
                            <a href="javascript:loginNaver('https://nid.naver.com/oauth2.0/authorize?response_type=code&client_id=46UeBZAh46t1pnLrDu7v&redirect_uri=http%3A%2F%2Fstarcity.midashelp.com%3A80%2Fmember%2Fjoin%2Fcallback_naver&state=1285852156824314567020144140892260642655')" id="naverIdLogin" class="btn bg_naver"><i class="ico ico_join_naver"></i>네이버 아이디로 회원가입</a>
                            <a href="javascript:loginFacebook()" class="btn bg_facebook" ><i class="ico ico_join_face"></i>페이스북 아이디로 회원가입</a>
                            <a href="javascript:loginKakao()" class="btn bg_cacao"><i class="ico ico_join_cacao"></i>카카오 아이디로 회원가입</a>
                        </dd>
                    </dl>
                </div>
                <!-- //right_sec -->
            </div>
            <!-- //join_gate -->
        </div>
        <!-- //ly_container -->
    </div>
    <!-- //layout_body -->


<%@include file="../inc/inc_gl_bottom.jsp"%>
