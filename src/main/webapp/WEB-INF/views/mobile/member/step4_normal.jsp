<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

    <div class="prev_title">
        <a href="javascript:history.go(-1);"><span class="lnr lnr-chevron-left"></span></a>
        <h2>일반회원가입</h2>
    </div>

    <div class="layout_body">

        <div class="ly_container">
            <div class="join_agree">

                <ul class="step_list">
                    <li><span class="step_icon step_1"></span></li>
                    <li><span class="step_icon step_2"></span></li>
                    <li><span class="step_icon step_3"></span></li>
                    <li class="on"><span class="step_icon step_4"></span></li>
                </ul>

                <dl class="step_title">
                    <dt>Step4</dt>
                    <dd>가입완료</dd>
                </dl>


                <div class="register_end">
                    <dl>
                        <dt>스타시티 <br /><strong>회원가입</strong>이 완료되었습니다. </dt>
                        <dd>
                            <p>쇼핑, 문화, 레저, 편의시설 그리고 인근에 주거공간까지 갖춘 국내 최초의 진정한 One-stop Life 쇼핑공간 스타시티몰의 회원이 되신 것을 축하드립니다.</p>
                            <p class="pt10">이제 스타시티몰에서 더욱더 즐거운 라이프생활을 즐기시길 바랍니다.</p>
                            <p class="pt10">감사합니다.</p>
                        </dd>
                    </dl>
                </div>


                <div class="button_group txt-cnt">
                    <span><a href="javascript:;" class="btn box-blue">홈 화면으로 이동</a></span>
                </div>

            </div>
            <!-- //join_agree -->
        </div>
        <!-- //ly_container -->
    </div>
    <!-- //layout_body -->


<%@include file="../inc/inc_gl_bottom.jsp"%>
