<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>
    <div class="sub_title bg_grey">
        <h2>로그인</h2>
        <p class="en_summary">LOGIN</p>
    </div>

        <div class="layout_body">

            <div class="ly_container register_form">
                <div class="join_gate">
                    <div class="normal_sec">
                        <dl>
                            <dt>일반 로그인</dt>
                            <dd class="login_form">
                                <div class="register_block">
                                    <input type="text" name="hID" class="boxline" id="hID" placeholder="아이디" style="width: 100%;">
                                </div>
                                <div class="register_block">
                                    <input type="password" name="hPW" class="boxline" id="hPW" placeholder="비밀번호" style="width: 100%;">
                                </div>
                            </dd>
                            <dd class="btn_in">
                                <a href="#" class="btn bg_join_blue">로그인</a>
                            </dd>
                            <dd class="chk_in">
                                <label>
                                    <input type="checkbox" name="idChk" class="checkbox" >
                                    <span class="label"></span> 아이디 저장
                                </label>
                            </dd>
                        </dl>
                    </div>
                    <div class="sns_sec">
                        <dl>
                            <dt>간편 로그인</dt>
                            <dd class="btn_in">
                                <a href="javascript:loginSns();" class="btn bg_naver"><i class="ico ico_join_naver"></i>네이버로 로그인</a>
                                <a href="javascript:loginSns();" class="btn bg_facebook"><i class="ico ico_join_face"></i>페이스북 로그인</a>
                                <a href="javascript:loginSns();" class="btn bg_cacao"><i class="ico ico_join_cacao"></i>카카오톡 로그인</a>
                            </dd>

                            <dd class="btn_in">
                                <ul class="comment">
                                    <li>아이디 비밀번호 찾기는 PC에서만 가능합니다.</li>
                                </ul>
                                <div class="pw20"><a href="#" class="btn bg_navy mt15">PC에서 아이디/비밀번호 찾기</a></div>
                            </dd>

                        </dl>
                    </div>
                    <!-- //right_sec -->
                </div>
                <!-- //join_gate -->

                <div class="join_info">
                    <dl>
                        <dt>아직 스타시티 <br/>회원이 아니신가요?</dt>
                        <dd>
                            지금 <strong class="txt-color-blue">회원가입</strong>을 하시면 스타시티의 <br/ >
                            다양한 이벤트 및 프로모션에 <br/ >
                            참여가 가능하며 정기 뉴스레터를<br/ >
                             받으실 수 있습니다.
                        </dd>
                    </dl>
                    <div class="button_group">
                        <a href="join" class="btn btn_round_nm"><i class="far fa-user-circle"></i> 스타시티 회원가입</a>
                    </div>
                </div>
                <!-- //join_info -->

            </div>
            <!-- //ly_container -->
        </div>
        <!-- //layout_body -->

        <!-- sns로그인 팝업 시작-->
        <div id="confirmLayerBox" class="layerPopup confirm_layer">
            <div class="layerPopup_in">
                <span class="closeLayer">
                    <a href="javascript:LayerPopups.close('confirmLayerBox');" class="btn"><i class="lnr lnr-cross"></i></a>
                </span>
                <div class="layer_txt">
                    <dl class="submit_txt">
                        <dt class="pt40">SNS 간편 로그인 안내</dt>
                        <dd>SNS 간편 로그인은 해당 SNS계정으로<br />
                        회원가입 후 이용 가능합니다.<br />
                        회원가입 페이지로 이동하시겠습니까?
                        </dd>
                    </dl>
                </div>
                <div class="button_group txt-cnt">
                    <span><a href="javascript:LayerPopups.close('confirmLayerBox');" class="btn btn_round_nm">취소</a></span>
                    <span><a href="javascript:;" class="btn btn_round_blue_bg">확인</a></span>
                </div>
            </div>
        </div>
        <!-- //sns로그인 팝업 -->



<script type="text/javascript">
    function loginSns(){
        LayerPopups.open('confirmLayerBox', true);
    }
</script>



<%@include file="../inc/inc_gl_bottom.jsp"%>
