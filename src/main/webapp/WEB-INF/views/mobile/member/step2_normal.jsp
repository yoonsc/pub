<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

    <div class="prev_title">
        <a href="javascript:history.go(-1);"><span class="lnr lnr-chevron-left"></span></a>
        <h2>일반회원가입</h2>            
    </div>

    <div class="layout_body">
        
        <div class="ly_container">
            <div class="join_agree">         
                
                <ul class="step_list">              
                    <li><span class="step_icon step_1"></span></li>
                    <li class="on"><span class="step_icon step_2"></span></li>
                    <li><span class="step_icon step_3"></span></li>
                    <li><span class="step_icon step_4"></span></li>
                </ul>

                <dl class="step_title">
                    <dt>Step2</dt>
                    <dd>본인인증</dd>
                </dl>


                <div class="auth_list">
                    <p class="auth_tit">본인인증을 진행해주세요.</p>
                    <ul>
                        <li>
                            <a href="#">
                                <span class="auth_icon"><img src="/resources/user/mobile/img/phone_auth.png" alt=""></span>휴대전화 인증
                            </a>                    
                        </li>
                    </ul>
                </div>


                <div class="button_group txt-cnt">
                    <span><a href="javascript:;" class="btn box-white">취소</a></span>
                    <span><a href="javascript:;" class="btn box-blue">다음단계</a></span>
                </div>

            </div>
            <!-- //join_agree -->
        </div>
        <!-- //ly_container -->
    </div>
    <!-- //layout_body -->



<%@include file="../inc/inc_gl_bottom.jsp"%>
