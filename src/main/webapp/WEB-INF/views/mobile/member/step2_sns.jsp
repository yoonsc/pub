<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

        <div class="prev_title">
            <a href="javascript:history.go(-1);"><span class="lnr lnr-chevron-left"></span></a>
            <h2>SNS계정으로 회원가입</h2>
        </div>

        <div class="layout_body">

            <div class="ly_container">
                <div class="join_agree">

                    <ul class="step_list sns_step">
                        <li><span class="step_icon step_1"></span></li>
                        <li class="on"><span class="step_icon step_3"></span></li>
                        <li><span class="step_icon step_4"></span></li>
                    </ul>

                    <dl class="step_title">
                        <dt>Step2</dt>
                        <dd>정보입력</dd>
                    </dl>


                    <form id="joinForm" method="get" action="">
                        <div class="register_form">
                            <h3 class="register_tit">회원정보 입력 <span><i class="require">*</i> 는 필수 입력 항목 입니다.</span></h3>
                            <dl>
                                <dt>이름 <i class="require">*</i></dt>
                                <dd><input type="text" name="name" id="hName" style="width: 100%;"></dd>
                            </dl>
                            <dl>
                                <dt>생년월일 <i class="require">*</i></dt>
                                <dd>
                                    <input type="text" name="hBirth" id="hBirth" style="width: 100%;">
                                    <ul class="comment pt5">
                                        <li>생년월일 8자리 입력.  ex) 19810507</li>
                                    </ul>
                                </dd>
                            </dl>
                            <dl>
                                <dt>휴대전화 번호 <i class="require">*</i></dt>
                                <dd class="phone_cell">
                                    <span class="select-wrapper">
                                        <select title="전화번호선택" id="hTel1" name="hTel1">
                                            <option value="010">010</option>
                                            <option value="011">011</option>
                                            <option value="018">018</option>
                                            <option value="016">016</option>
                                            <option value="017">017</option>
                                        </select>
                                    </span>
                                    <span class="phone_dash">
                                        <input type="text" name="hTel2" id="hTel2" class="number-check" maxlength="4" onkeydown="onlyNumber(this);">
                                    </span>
                                    <span class="phone_dash">
                                        <input type="text" name="hTel3" id="hTel3" class="number-check" maxlength="4" onkeydown="onlyNumber(this);">
                                    </span>
                                    <input type="hidden" name="tel">
                                </dd>
                            </dl>
                            <dl>
                                <dt>이메일 <i class="require">*</i></dt>
                                <dd class="email_cell">
                                    <p>
                                        <span><input type="text" name="hEmail1" class="boxline" id="hEmail1"></span>
                                        <span><input type="text" name="hEmail2" class="boxline" id="hEmail2"></span>
                                    </p>
                                    <p class="pt10">
                                        <span class="select-wrapper">
                                            <select name="mailserver" title="이메일 서버선택" onchange="$('#hEmail2').val( $(this).val() == 'D' ? '' : $(this).val() );">
                                                <option value="D">직접입력</option>
                                                <option value="naver.com">naver.com</option>
                                                <option value="gmail.com">gmail.com</option>
                                                <option value="daum.net">daum.net</option>
                                                <option value="hanmail.net">hanmail.net</option>
                                                <option value="hotmail.com">hotmail.com</option>
                                                <option value="icloud.com">icloud.com</option>
                                                <option value="yahoo.com">yahoo.com</option>
                                            </select>
                                        </span>
                                    </p>
                                    <input type="hidden" name="email">
                                    <ul class="comment">
                                        <li>중요소식 및 뉴스레터 수신을 위해 정확한 이메일 주소를 입력해주세요.</li>
                                    </ul>
                                </dd>
                            </dl>
                            <dl>
                                <dt>성별</dt>
                                <dd>
                                    <label><input type="radio" name="hSec" class="radio" ><span class="label"></span> 남성</label>
                                    <label><input type="radio" name="hSec" class="radio" ><span class="label"></span> 여성</label>
                                    <label><input type="radio" name="hSec" class="radio" ><span class="label"></span> 선택안함</label>
                                </dd>
                            </dl>
                            <dl>
                                <dt>주소 <i class="require"></i></dt>
                                <dd class="check_btn">
                                    <span><input type="text" name="hZip" class="boxline" id="hZip"></span>
                                    <a href="javascript:;" class="btn box-navy" style="vertical-align: top;">우편번호 찾기</a>
                                </dd>
                                <dd class="pt10"><input type="text" name="hZip" class="boxline" id="hZip2" style="width: 100%;"></dd>
                                <dd class="pt10"><input type="text" name="hZip" class="boxline" id="hZip3" style="width: 100%;"></dd>
                            </dl>

                            <h3 class="register_tit">부가정보 입력</h3>

                             <div class="letterchk">
                                <p class="label_box">
                                    <input type="checkbox" name="hLetter" class="checkbox" id="hLetter" >
                                    <span class="label"></span>
                                </p>
                                <p class="txt"><label for="hLetter">경품당첨 소식 및 뉴스레터, 이벤트등의 정보 수신에 동의합니다.</label></p>
                             </div>

                             <dl class="cell_box">
                                <dt>이메일 수신</dt>
                                <dd>
                                    <label>
                                        <input type="radio" name="hAgEmail" class="radio" >
                                        <span class="label"></span> 동의
                                    </label>
                                    <label>
                                        <input type="radio" name="hAgEmail" class="radio" >
                                        <span class="label"></span> 미동의
                                    </label>
                                </dd>
                             </dl>
                             <dl class="cell_box">
                                <dt>SMS 수신</dt>
                                <dd>
                                    <label>
                                        <input type="radio" name="hAgSms" class="radio" >
                                        <span class="label"></span> 동의
                                    </label>
                                    <label>
                                        <input type="radio" name="hAgSms" class="radio" >
                                        <span class="label"></span> 미동의
                                    </label>
                                </dd>
                             </dl>
                        </div>


                        <div class="button_group txt-cnt">
                            <span><a href="javascript:;" class="btn box-white">취소</a></span>
                            <span><a href="javascript:;" class="btn box-blue submit">가입완료</a></span>
                        </div>

                    </form>

                </div>
                <!-- //join_agree -->
            </div>
            <!-- //ly_container -->
        </div>
        <!-- //layout_body -->


<%@include file="../inc/inc_gl_bottom.jsp"%>
