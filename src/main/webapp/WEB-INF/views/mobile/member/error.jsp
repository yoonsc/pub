<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

    <div class="layout_body" style="border-top: 1px solid #d7d7d7;">

        <div class="ly_container">
            <div class="error_icon">
                <i class="ico ico_result_no"></i>
            </div>

            <dl class="error_tit">
                <dt>죄송합니다.<br />요청하신 페이지를 찾을 수 없거나, 오류가 발생하였습니다.</dt>
                <dd>
                    <p>페이지의 주소가 잘못 입력되었거나, 요청하신 페이지의 주소가 변경, 삭제 되어 찾을 수 없습니다.<br />
                   이용에 불편을 드려 죄송합니다. </p>
                </dd>
            </dl>

            <div class="button_group error_btns txt-cnt pb25">
                <span><a href="javascript:history.back();" class="btn box-white">이전 페이지 이동</a></span>
                <span><a href="/" class="btn box-blue submit">홈으로 이동</a></span>
            </div>
        </div>
        <!-- //ly_container -->
    </div>
    <!-- //layout_body -->

<%@include file="../inc/inc_gl_bottom.jsp"%>
