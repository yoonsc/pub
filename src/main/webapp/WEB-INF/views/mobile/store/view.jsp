<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

    <div class="store_title">
        <div class="store_title_con">            
            <h2>CAFE &amp; DESSERT</h2>
            <p class="sub_cate">카페&amp;디저트</p>                            
        </div>            
        <p class="tit_img"><img src="/resources/user/mobile/img/contents/store/layout_topvisual_1.jpg" alt=""></p>
    </div>

    <div class="layout_body store_view">   


        <div class="ly_container store_top">     

            <dl class="store_name">
                <dt>스타벅스 건대스타시티점</dt>
                <dd>Starbucks konkuk City</dd>
            </dl>

            <div class="logoimg">
                <img src="/resources/user/mobile/img/contents/store/shop_logo.jpg" alt="">
            </div>
                
            <div class="store_info">
                <dl>
                    <dt><i class="far fa-clock"></i> 영업시간</dt>
                    <dd>
                        <ul class="detail_list">
                            <li><span class="time">09:00 ~ 23:00</span> <span class="issue">주중 - 월,화,수,목,일</span></li>
                            <li><span class="time">09:00 ~ 23:00</span> <span class="issue">주말 - 금, 토</span></li>
                            <li><span class="time">09:00 ~ 23:00</span> <span class="issue">브레이크 타임</span></li>
                        </ul>
                    </dd>
                </dl>
                <dl>
                    <dt><i class="fas fa-phone"></i> 전화번호</dt>
                    <dd>
                        <ul class="detail_list">
                            <li>02-738-8197</li>
                            <li>010-1234-1234</li>
                        </ul>
                    </dd>
                </dl>
                <dl>
                    <dt><i class="fas fa-map-marker-alt"></i> 매장위치</dt>
                    <dd>스타존 1F</dd>
                </dl>               
            </div>  
            <!-- //store_info -->             


            <div class="button_group txt-cnt">
                <span><a href="javascript:;" class="btn btn-favorite"><i class="far fa-heart"></i> 관심매장</a></span>
                <!-- 관심매장 활성화
                    <span><a href="javascript:;" class="btn btn-favorite on"><i class="far fa-heart"></i> 관심매장</a></span> -->

                <span><a href="javascript:;" class="btn box-white">사이트 바로가기</a></span>
            </div>  
            
        </div>  
        <!-- //store_top -->

        <div class="ly_container store_body">
            <dl class="store_intro store_body_tit">
                <dt>Store Info</dt>
                <dd>
                    <p>스타벅스 건대시티점을 찾아주셔서 감사드립니다.<br /><br />

                    저희 건대스타시티점은 고객여러분께 좀 더 다양한<br />
                    혜택 및 커피에 대 지식을 공유하고자<br />
                    2개월에 한번 커피교실을 운영하고 있습니다. <br /> 
                    많은 고객님들의 참여 부탁드립니다. 
                    </p>
                </dd>
                <dd class="no_editer">
                    모바일 환경에서의 에디터 제공은 지원하지 않습니다.
                </dd>
            </dl>                       

            <dl class="store_gallery store_body_tit">
                <dt>Gallery 
                    <!-- <span class="nav_control">
                        <a href="javascript:;" id="slidePrev"><i class="lnr lnr-chevron-left"></i></a>
                        <a href="javascript:;" id="slideNext"><i class="lnr lnr-chevron-right"></i></a>
                    </span> -->
                </dt>
                <dd>
                    <ul class="gal_list">
                        <li><img src="/resources/user/mobile/img/contents/store/shop_menu_img01.jpg" alt=""></li>
                        <li><img src="/resources/user/mobile/img/contents/store/shop_menu_img01.jpg" alt=""></li>
                        <li><img src="/resources/user/mobile/img/contents/store/shop_menu_img01.jpg" alt=""></li>
                    </ul>
                </dd>
            </dl>

            <div class="map_wrap">
                <dl class="store_location store_body_tit" id="store_body">
                    <dt>Location</dt>
                    <dd>
                        <span>스타벅스 건대시티점의 위치는 </span>
                        <span><a href="javascript:;" class="txt-line-blue">스타존<i class="ico_i_blue">i</i> 1F</a> 에 있습니다.</span>
                    </dd>
                </dl>

                <div class="store_map">
                    <img src="/resources/user/mobile/img/contents/store/shop_location.jpg">                        
                    <i class="pick_icon" style="left: 76%; top: 73.4%;"></i>                       
                </div>

        </div>
        <!-- //store_body -->
        

        <script type="text/javascript">   
            $(function(){          
               shopGallery.init();          
            });
        </script>


    </div>      
    <!-- //store_view -->


<%@include file="../inc/inc_gl_bottom.jsp"%>
