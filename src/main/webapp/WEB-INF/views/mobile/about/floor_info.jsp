<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

    <div class="sub_title">
        <h2>층별안내</h2>
        <p class="en_summary">floor info</p>
    </div>

    <div class="layout_body">
        <div class="sub_contents floor_info">
            <ul class="tab_boxtype tablist_4" id="floorTab">
                <li class="on"><a href="#">시티존</a></li>
                <li><a href="#">스타존</a></li>
                <li><a href="#">영존</a></li>
                <li><a href="#">더 클래식 500</a></li>
            </ul>

            <!-- categoryType
            <span class="ico ico-food"></span>
            <span class="ico ico-cafe"></span>
            <span class="ico ico-life"></span>
            <span class="ico ico-service"></span>
            <span class="ico ico-beauty"></span>
            <span class="ico ico-fashion"></span>
            <span class="ico ico-miscellaneous"></span>
            <span class="ico ico-parking"></span>
            -->

            <div class="tab_content_wrap">
                <div class="tab_content">

                    <div class="floor_info_tbl">
                        <ul class="floor_info_list">
                            <li>
                                <dl>
                                    <dt>3F</dt>
                                    <dd>
                                        <p class="shop_logo"><img src="/resources/user/mobile/img/contents/about/floor_logo_dish.jpg" alt=""></p>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>도레도레</li>
                                                    <li>스쿨푸드</li>
                                                    <li>토끼정</li>
                                                    <li>아비꼬커리</li>
                                                    <li>스시히로바</li>
                                                    <li>한국집</li>
                                                    <li>속초중앙시장 해물짬뽕</li>
                                                    <li>플레이트300</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-cafe"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>앤티앤스</li>
                                                    <li>프레즐&빨라쪼</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>2F</dt>
                                    <dd>
                                        <p class="shop_logo"><img src="/resources/user/mobile/img/contents/about/floor_logo_cinema.jpg" alt=""></p>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-cafe"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>엔제리너스</li>
                                                    <li>스무디킹</li>
                                                    <li>루시카토캔디</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-beauty"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>롭스 </li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>1F</dt>
                                    <dd>
                                        <p class="shop_logo"><img src="/resources/user/mobile/img/contents/about/floor_logo_shinhan.jpg" alt=""></p>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-service"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>폴더</li>
                                                    <li>케이스 갤러리</li>
                                                    <li>필라 언더웨어</li>
                                                    <li>언더피스</li>
                                                    <li>브루 앤 쥬디</li>
                                                    <li>디자인스킨</li>
                                                    <li>헌트이너웨에 </li>
                                                    <li>원더플레이스</li>
                                                    <li>스파오</li>
                                                    <li>메가스테이지</li>
                                                    <li>ABC마켓 그랜드 스테이지</li>
                                                    <li>세이코 </li>
                                                    <li>스태어(STARE)</li>
                                                    <li>오뜨레</li>
                                                    <li>샘소나이트</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-service"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>세븐일레븐</li>
                                                    <li>클라라네일</li>
                                                    <li>SK텔레콤(T world)</li>
                                                    <li>딜라이트</li>
                                                    <li>스태어(STARE)</li>
                                                    <li>오뜨레</li>
                                                    <li>더샾 스타시티약국</li>
                                                    <li>신한은행</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>B1F</dt>
                                    <dd>
                                        <p class="shop_logo"><img src="/resources/user/mobile/img/contents/about/floor_logo_emart.jpg" alt=""></p>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>버스커</li>
                                                    <li>베스킨라빈스</li>
                                                    <li>룩앤룩안경원</li>
                                                    <li>크리스피크림 도넛</li>
                                                    <li>토니모리</li>
                                                    <li>액센트</li>
                                                    <li>롯데리아</li>
                                                    <li>부츠</li>
                                                    <li>나뚜르 팝</li>
                                                    <li>디저트39</li>
                                                    <li>모놀로그</li>
                                                    <li>로이드</li>
                                                    <li>메이드 인 핑크</li>
                                                    <li>클루</li>
                                                    <li>로엠</li>
                                                    <li>더 데이걸즈</li>
                                                    <li>키즈덤</li>
                                                    <li>옥토버훼스트</li>
                                                    <li>타코벨</li>
                                                    <li>이철헤어커커</li>
                                                    <li>놀부부대찌개</li>
                                                    <li>사보텐</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>B2F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-parking"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>주차장</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-miscellaneous"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>세탁 편의점</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>B3F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-parking"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>주차장</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-miscellaneous"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>크린파워 스팀세차</li>
                                                    <li>AJ렌터카</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                        </ul>
                    </div>
                    <!-- //floor_info_tbl -->
                </div>
                <!-- //tab_content : 시티존 -->

                <div class="tab_content">
                    <div class="floor_info_tbl">
                        <ul class="floor_info_list">
                            <li class="lotte_dept">
                                <dl>
                                    <dt>4F<br />~10F</dt>
                                    <dd class="lotte_floor">
                                        <p class="shop_logo"><img src="/resources/user/mobile/img/contents/about/floor_logo_lotte.jpg" alt="" style="height: 30px;"></p>
                                    </dd>
                                </dl>
                            </li>
                            <li class="lotte_dept">
                                <dl>
                                    <dt>3F</dt>
                                    <dd>
                                        <p class="shop_logo logo_lotte2"><img src="/resources/user/mobile/img/contents/about/floor_logo_lotte2.jpg" alt="" style="height: 12px;"></p>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                   <li>에즈블랑</li>
                                                   <li>한상보쌈순두부</li>
                                                   <li>라뷰티코아</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li class="lotte_dept">
                                <dl>
                                    <dt>2F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>BRCD</li>
                                                    <li>제일제면소</li>
                                                    <li>에머이</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li class="lotte_dept">
                                <dl>
                                    <dt>1F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>프리스비</li>
                                                    <li>레스모아</li>
                                                    <li>마노모스 안경</li>
                                                    <li>마노커피</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li class="lotte_dept last">
                                <dl>
                                    <dt>B1</dt>
                                    <dd>
                                        <p class="shop_logo"><img src="/resources/user/mobile/img/contents/about/floor_logo_bandi.jpg" alt="" style="height: 12px;"></p>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>메이썸</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>모스버거</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-beauty"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>양키캔들</li>
                                                    <li>랜즈스토리</li>
                                                    <li>향수&시티</li>
                                                    <li>이야기여섯</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-cafe"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>네스카페</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>B2</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-parking"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>주차장</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-miscellaneous"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>슈펜</li>
                                                    <li>사진무이</li>
                                                    <li>이디야</li>
                                                    <li>원더플레이스</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>B3</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-parking"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>주차장</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>B4</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-parking"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>주차장</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-miscellaneous"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>차나라 스팀세차</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>B5<br />~B6</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-parking"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>주차장</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- //tab_content : 스타존 -->

                <div class="tab_content">

                    <div class="select_box_con mb25 mt25">
                        <span class="select-wrapper">
                            <select id="searchType" name="searchType" onchange="javascript:categorychg(this)">
                                <option value="1a">A동</option>
                                <option value="1b">B동</option>
                            </select>
                        </span>
                    </div>

                    <div class="floor_info_tbl floorinner" id="floorInner_1a">
                        <ul class="floor_info_list">
                            <li>
                                <dl>
                                    <dt>6F<br >~7F</dt>
                                    <dd class="vtop">업무시설</dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>5F</dt>
                                    <dd>스타시티 웨딩홀</dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>4F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>무스쿠스</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>3F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>계절밥상</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>2F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>롯데리아</li>
                                                    <li>하이디라오 훠궈</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>1F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>롯데리아</li>
                                                    <li>청주 오믈렛</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-coffee"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>엔제리너스</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-coffee"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>햇빛약국</li>
                                                    <li>정문대학약국</li>
                                                    <li>세븐일레븐</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>B1</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>알파문구</li>
                                                    <li>CU편의점</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>B2</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>김&장 나라약국</li>
                                                    <li>스타시티플라워</li>
                                                    <li>알뜰 휴대폰 판매점</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>던킨도너츠</li>
                                                    <li>삼끼니</li>
                                                    <li>죽이야기</li>
                                                    <li>브레댄코</li>
                                                    <li>인더그레이</li>
                                                    <li>명동 할머니 국수</li>
                                                    <li>미스터빈</li>
                                                    <li>세계과자할인점</li>
                                                    <li>본가 한식</li>
                                                    <li>다큐프라자</li>
                                                    <li>빠아앙</li>
                                                    <li>싸움의고수</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>B4<br />~B5</dt>
                                    <dd class="vtop">
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-parking"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>주차장</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                        </ul>
                    </div>
                    <!-- //floor_info_tbl -->

                    <div class="floor_info_tbl floorinner" id="floorInner_1b" style="display: none;">
                        <ul class="floor_info_list">
                            <li>
                                <dl>
                                    <dt>6F<br >~7F</dt>
                                    <dd class="vtop">업무시설</dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>5F</dt>
                                    <dd>스타시티 웨딩홀</dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>4F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>애슐리</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>3F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>T.G.I 프라이데이</li>
                                                    <li>곽만근 갈비탕</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>2F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-service"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>KEB 하나은행</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>1F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-service"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>건대의료기</li>
                                                    <li>SK텔레콤</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>B1<br />~B5</dt>
                                    <dd class="vtop">
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-parking"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>주차장</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                        </ul>
                    </div>
                    <!-- //floor_info_tbl -->

                </div>
                <!-- //tab_content : 영존 -->

                <div class="tab_content">


                    <div class="select_box_con mb25 mt25">
                        <span class="select-wrapper">
                            <select id="searchType" name="searchType" onchange="javascript:categorychg2(this)">
                                <option value="2a">A동</option>
                                <option value="2b">B동</option>
                            </select>
                        </span>
                    </div>

                    <div class="floor_info_tbl floorinner2" id="floorInner_2a">
                        <ul class="floor_info_list">
                            <li>
                                <dl>
                                    <dt>21<br />~50F</dt>
                                    <dd class="vtop">주거시설</dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>8<br />~20F</dt>
                                    <dd><p class="shop_logo"><img src="/resources/user/mobile/img/contents/about/floor_logo_pentaz.jpg" alt=""></p>
                                    펜타즈 호텔</dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>7F</dt>
                                    <dd>입주자 전용시설</dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>6F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-life"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>라비앙 로즈</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>5F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-life"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>마실</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>4F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-life"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>14일동안 핫필라</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-life"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>한국투자증권 건대역지점</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>3F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-life"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>메디컬 라이프케어 센터</li>
                                                    <li>건국대병원 스포츠의학센터</li>
                                                    <li>24시 열린의원</li>
                                                    <li>미래에셋대우 건대역WM센터</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>2F</dt>
                                    <dd>통합지원센터</dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>1F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>국민은행 KB센터</li>
                                                    <li>더클래식약국</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>어선정</li>
                                                    <li>어무이</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-coffee"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>스타벅스</li>
                                                    <li>폴 바셋</li>
                                                    <li>투썸플레이스</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>B1</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-miscellaneous"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>GS25 편의점</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-life"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>스파&골프 </li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>B2</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-parking"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>주차장 </li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>B3<br />~B6</dt>
                                    <dd class="vtop">
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-parking"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>주차장</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                        </ul>
                    </div>
                    <!-- //floor_info_tbl -->

                    <div class="floor_info_tbl floorinner2" id="floorInner_2b" style="display: none;">
                        <ul class="floor_info_list">
                            <li>
                                <dl>
                                    <dt>5F<br />~40F</dt>
                                    <dd class="vtop">주거시설</dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>4F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-life"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>체력장 & GX</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>3F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-life"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>아이리스 연회장</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>라구뜨 오뜨 뷔페</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>2F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-life"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>그랜드 볼룸 대연회장</li>
                                                    <li>아젤리아 연회장</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-life"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>봄날의 미소 치과</li>
                                                    <li>하늘담 한의원</li>
                                                    <li>NH투자증권 건대역 WM센터</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>1F</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-food"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>벨피아또</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-coffee"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>커피빈</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-life"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>아가미르</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>B1</dt>
                                    <dd>
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-beauty"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>스위스파 미용실/테라피</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt>B3<br />~B6</dt>
                                    <dd class="vtop">
                                        <dl class="detail_list">
                                           <dt><span class="ico ico-parking"></span></dt>
                                           <dd>
                                                <ul class="item_list">
                                                    <li>주차장</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </li>
                        </ul>
                    </div>
                    <!-- //floor_info_tbl -->

                </div>
                <!-- //tab_content : 클래식 -->
            </div>
            <!-- //tab_content_wrap -->



            <div class="floor_info_link">
                <p class="floor_info_link_title"><strong>스타시티 층별 상세 지도 및 매장위치</strong>를 알고 싶으신가요?</p>
                <p class="floor_info_link_sub">스타시티 가이드에서 스타시티 단지 내 층별 상세 지도 및 매장 위치를 확인하실 수 있습니다.</p>
                <a href="javascript:goGuidePage();">스타시티 가이드</a>
            </div>

        </div>
        <!-- //sub_contents -->


        <script type="text/javascript">
            function categorychg(combo){
                $(".floorinner").hide();
                $("#floorInner_" + combo.value).show();
            }

            function categorychg2(combo){
                $(".floorinner2").hide();
                $("#floorInner_" + combo.value).show();
            }

            $(function(){
               ui_tabMenu($('#floorTab'),$('.tab_content_wrap'));
            });

        </script>

    </div>
    <!-- //layout_body -->


<%@include file="../inc/inc_gl_bottom.jsp"%>
