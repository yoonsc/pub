<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

        <div class="sub_title">
            <h2>오시는길</h2>
            <p class="en_summary">DIRECTIONS</p>
        </div>

        <div class="layout_body location bg_grey pb0">
            <div class="location_list">
                <h3>각 단지별 주소 안내</h3>
                <div class="ly_container">
                    <ul class="location_box">
                        <li>
                            <div class="list_con">
                                <dl>
                                    <dt>시티존<br><strong>(롯데시네마, 이마트 입점 단지)</strong></dt>
                                    <dd>
                                        서울특별시 광진구 아차산로 272<br>
                                        (서울특별시 광진구 자양동 227-7)
                                    </dd>
                                </dl>
                                <div class="button_group txt-cnt">
                                    <span><a href="https://map.kakao.com/?urlX=516160&amp;urlY=1122011&amp;urlLevel=1&amp;map_type=TYPE_MAP&amp;map_hybrid=false" target="_blank" class="btn btn_round_blue">지도에서 보기</a></span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="list_con">
                                <dl>
                                    <dt>스타존<br><strong>(롯데백화점 입점 단지)</strong></dt>
                                    <dd>
                                        서울특별시 광진구 능동로 92<br>(서울특별시 광진구 자양동 227-342)</dd>
                                </dl>
                                <div class="button_group txt-cnt">
                                    <span><a href="https://map.kakao.com/?urlX=515735&amp;urlY=1122086&amp;urlLevel=1&amp;map_type=TYPE_MAP&amp;map_hybrid=false" target="_blank" class="btn btn_round_blue">지도에서 보기</a></span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="list_con">
                                <dl>
                                    <dt>더 클래식 500</dt>
                                    <dd>서울특별시 광진구 능동로 90<br>(서울특별시 광진구 자양동 227-342)</dd>
                                </dl>
                                <div class="button_group txt-cnt">
                                    <span><a href="https://map.kakao.com/?urlX=515670&amp;urlY=1121908&amp;urlLevel=1&amp;map_type=TYPE_MAP&amp;map_hybrid=false" target="_blank" class="btn btn_round_blue">지도에서 보기</a></span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="list_con">
                                <dl>
                                    <dt>영존</dt>
                                    <dd>서울 광진구 능동로 110 스타시티 영존<br>(서울특별시 광진구 화양동 4-20 )</dd>
                                </dl>
                                <div class="button_group txt-cnt">
                                    <span><a href="https://map.kakao.com/?urlX=515760&amp;urlY=1122509&amp;urlLevel=1&amp;map_type=TYPE_MAP&amp;map_hybrid=false" target="_blank" class="btn btn_round_blue">지도에서 보기</a></span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- //ly_container -->
            </div>
            <!-- //location_list -->

            <div class="floor_map_con">
                <div class="floor_zoom">
                    <a href="/resources/user/mobile/img/contents/about/convenience_map.jpg" target="_blank"><i class="ico ico-plus"></i></a>
                </div>
                <div class="floor_map_small">
                    <img src="/resources/user/mobile/img/contents/about/convenience_map.jpg" alt="">
                </div>
            </div>

            <div class="ly_container location_info">
                <div class="locaton_wrap">
                    <p class="tit"><i><img src="/resources/user/mobile/img/contents/about/icon_location_car.png" alt=""></i> 자동차로 오시는 길 </p>
                    <dl>
                        <dt>영동대교 이용 </dt>
                        <dd>영동대교 북단 → 성수사거리 우회전 →  구의로 →  건대입구역 사거리
                        → 스타시티 입구 우회전 </dd>
                        </dl>
                    <dl>
                        <dt>청담대교 이용</dt>
                        <dd>청담대교 북단 → 건대입구역 사거리 우회전 → 스타시티 입구 우회전</dd>
                        </dl>
                    <dl>
                        <dt>잠실대교 이용</dt>
                        <dd>잠실대교 북단 → 자양사거리 좌회전 → 구의로
                        → 건국대학교 일감문 사거리 자회전 </dd>
                        </dl>
                    <dl>
                        <dt>경부고속도로 </dt>
                        <dd>서울톨게이트 → 구리.판교간 고속도로 → 올림픽대로 → 잠실대교 → 구의로
                        → 건국대학교 일감문사거리 좌회전</dd>
                        </dl>
                    <dl>
                        <dt>중부고속도로 </dt>
                        <dd>동서울 톨게이트 → 서하남 → 올림픽대로 → 구의로
                        →  건국대학교 일감문 사거리 좌회전</dd>
                    </dl>
                </div>

                <div class="locaton_wrap">
                    <p class="tit"><i><img src="/resources/user/mobile/img/contents/about/icon_location_subway.png" alt=""></i> 지하철로 오시는 길 </p>
                    <dl>
                        <dt>2호선/7호선 건대입구역 5번 출구에서 190m (도보 3분)</dt>
                    </dl>
                </div>
                <div class="locaton_wrap">
                    <p class="tit"><i><img src="/resources/user/mobile/img/contents/about/icon_location_bus.png" alt=""></i> 버스로 오시는 길 </p>

                    <dl>
                        <dt>건대역(05204) 에서 하차 하여 148m (도보 2분)</dt>
                        <dd>
                            <ul class="bl_line">
                              <li>간선 : 102, 240, 721</li>
                              <li>지선 : 2222, 2223, 2224, 3217, 3220, 4212</li>
                              <li>광역 : 8133</li>
                              <li>공항버스 : 6013</li>
                            </ul>
                        </dd>
                    </dl>
                </div>

            </div>


        </div>
        <!-- //layout_body -->

<%@include file="../inc/inc_gl_bottom.jsp"%>