<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

        <div class="sub_title">
            <h2>편의시설</h2>
            <p class="en_summary">SERVICE FACILITY</p>
        </div>

        <div class="layout_body">

            <div class="select_box_con mb25 bg_grey" id="searchBox"> 
                <p class="search_tit">찾으시는 편의시설을 선택해주세요.</p>               
                <span class="select-wrapper">
                    <select id="searchType" name="searchType" onchange="javascript:categorychg(this)">   
                        <option value="0">전체 편의시설</option>
                        <option value="1">현금인출기</option>                      
                        <option value="2">장애인 화장실</option>                             
                        <option value="3">유모차 대여</option>
                        <option value="4">흡연부스</option>
                        <option value="5">주차 무인정산기</option>
                        <option value="6">무인민원 발급기</option>
                        <option value="7">수유실</option>
                        <option value="8">만남의 광장</option>
                    </select>
                </span>
            </div>

            <div class="ly_container convenience"> 
                <div class="floor_map_con">
                    <div class="floor_zoom">
                        <a href="/resources/user/mobile/img/contents/about/convenience_map.jpg" target="_blank"><i class="ico ico-plus"></i></a>                        
                    </div>
                    <div class="floor_map_small">
                        <img src="/resources/user/mobile/img/contents/about/convenience_map.jpg" alt="">
                    </div>
                </div>

                <div class="conven_info_box">
                    <ul class="comment">
                        <li>편의시설의 상세한 위치는 PC버전에서 확인하실 수 있습니다.</li>
                    </ul>
                    <a href="http://starcity.midashelp.com/about/convenience" class="btn box-navy" target="_blank">PC버전 편의시설 보기</a> 
                </div>

                <div class="conven_wrap">
                    <div class="conven_list" id="conven_1">
                        <h3 class="conv_tit"><i><img src="/resources/user/mobile/img/contents/about/icon_conv_1.png" alt=""></i>현금인출기</h3>
                        <div class="bg_box">
                            단지 내 이용 가능한 현금인출기는 <strong>총19곳</strong> 입니다.
                        </div>
                        <div class="conv_detail">
                            <div class="detail_con">
                                <span class="left_sec">시티존</span>                                
                                <ul>
                                    <li>
                                       <dl>
                                           <dt><span>1F</span></dt>
                                           <dd>신행은행 앞 <em>(설치수량 8대)</em></dd>
                                       </dl> 
                                    </li>
                                    <li>
                                       <dl>
                                           <dt><span>2F</span></dt>
                                           <dd>1,2 호기 E/L홀 內 <em>(설치수량 1대)</em></dd>
                                       </dl> 
                                    </li>
                                </ul>
                            </div>
                            <!-- //detail_con -->

                            <div class="detail_con">
                                <span class="left_sec">스타존</span>                                
                                <ul>
                                    <li>
                                       <dl>
                                           <dt><span>B1</span></dt>
                                           <dd>4,5 호기 E/L홀 내  <em>(설치수량 1대)</em></dd>
                                       </dl> 
                                    </li>                                    
                                    <li>
                                       <dl>
                                           <dt><span>B4</span></dt>
                                           <dd>6,7,8 호기 E/L홀 내  <em>(설치수량 1대)</em></dd>
                                       </dl> 
                                    </li>
                                    <li>
                                       <dl>
                                           <dt><span>8F</span></dt>
                                           <dd>롯데카드센터 내  <em>(설치수량 1대)</em></dd>
                                       </dl> 
                                    </li>
                                </ul>
                            </div>
                            <!-- //detail_con -->

                            <div class="detail_con">
                                <span class="left_sec">영존</span>                                
                                <ul>
                                    <li>
                                       <dl>
                                           <dt><span>2F</span></dt>
                                           <dd>하나은행 <em>(설치수량 3대)</em></dd>
                                       </dl> 
                                    </li>                                                                        
                                </ul>
                            </div>
                            <!-- //detail_con -->

                            <div class="detail_con">
                                <span class="left_sec">더 클래식<br/>500</span>                                
                                <ul>
                                    <li>
                                       <dl>
                                           <dt><span>1F</span></dt>
                                           <dd>북문 출구방향 <em>(설치수량 4대)</em></dd>
                                       </dl> 
                                    </li>                                                                        
                                </ul>
                            </div>
                            <!-- //detail_con -->
                        </div>
                        <!-- //conv_detail -->
                    </div>
                    <!-- //conven_list :: atm-->

                    <div class="conven_list" id="conven_2">
                        <h3 class="conv_tit"><i><img src="/resources/user/mobile/img/contents/about/icon_conv_2.png" alt=""></i>장애인 화장실</h3>
                        <div class="bg_box">
                            단지 내 이용 가능한 장애인 화장실은 <strong>총6곳</strong> 입니다.
                        </div>
                        
                        <ul class="comment">
                            <li>일부 공사 및 수리중인 화장실의 경우 이용에 제한이 있을 수 있습니다.</li>
                        </ul>

                        <div class="conv_detail">
                            <div class="detail_con">
                                <span class="left_sec">시티존</span>                                
                                <ul>
                                    <li>
                                       <dl>
                                           <dt><span>1F</span></dt>
                                           <dd>GATE 복도 남녀공용 <em>(1곳)</em></dd>
                                       </dl> 
                                    </li>                                   
                                </ul>
                            </div>
                            <!-- //detail_con -->

                            <div class="detail_con">
                                <span class="left_sec">스타존</span>                                
                                <ul>
                                    <li>
                                       <dl>
                                           <dt><span>2F</span></dt>
                                           <dd>남녀 일반화장실 內 <em>(1곳)</em></dd>
                                       </dl> 
                                    </li>                                                                       
                                    <li>
                                       <dl>
                                           <dt><span>3F</span></dt>
                                           <dd>남녀 일반화장실 內 <em>(1곳)</em></dd>
                                       </dl> 
                                    </li>
                                </ul>
                            </div>
                            <!-- //detail_con -->

                            <div class="detail_con">
                                <span class="left_sec">영존</span>                                
                                <ul>
                                    <li>
                                       <dl>
                                           <dt><span>1F</span></dt>
                                           <dd>A동 남녀 일반화장실 內 <em>(1곳)</em></dd>
                                       </dl> 
                                    </li>                                                                        
                                </ul>
                            </div>
                            <!-- //detail_con -->

                            <div class="detail_con">
                                <span class="left_sec">더 클래식<br/>500</span>                                
                                <ul>
                                    <li>
                                       <dl>
                                           <dt><span>1F</span></dt>
                                           <dd>A동 북문 출구방향 남녀 개별 화장실 <em>(1곳)</em></dd>
                                       </dl> 
                                    </li>                                                                        
                                </ul>
                            </div>
                            <!-- //detail_con -->
                        </div>
                        <!-- //conv_detail -->
                    </div>
                    <!-- //conven_list :: 장애인 화장실-->

                    <div class="conven_list" id="conven_3">
                        <h3 class="conv_tit"><i><img src="/resources/user/mobile/img/contents/about/icon_conv_3.png" alt=""></i>유모차 대여</h3>
                        <div class="bg_box">
                            단지 내 이용 가능한 유모차대여시설은 <strong>총 1곳</strong> 입니다.
                        </div>
                        
                        <ul class="comment">
                            <li>유모차 대여는 스타존 1F 롯데백화점 고객지원센터에서 대여가 가능합니다.(대여 가능 시간 10:00 ~ 18:00)<br />
                            ※ 대여된 유모차는 롯데백화점 내에서만 이용가능하오니 이점 유의하시기 바랍니다.</li>
                        </ul>
                    </div>
                    <!-- //conven_list :: 유모차대여시설-->

                    <div class="conven_list" id="conven_4">
                        <h3 class="conv_tit"><i><img src="/resources/user/mobile/img/contents/about/icon_conv_4.png" alt=""></i>흡연부스</h3>
                        <div class="bg_box">
                            단지 내 이용 가능한 흡연부스는 <strong>총2곳</strong> 입니다.
                        </div>
                        
                        <ul class="comment">
                            <li>스타시티의 모든 단지는 금연건물 입니다.  흡연은 지정된 아래 장소에서만 가능하며 그 외 흡연 적발 시 [국민건강증진법] 제 9조에 의거과태료가 부가됩니다.</li>
                        </ul>

                        <div class="conv_detail">
                            <div class="detail_con">
                                <span class="left_sec">스타존</span>                                
                                <ul>
                                    <li>
                                       <dl>
                                           <dt><span>B2</span></dt>
                                           <dd>B206 구역</dd>
                                       </dl> 
                                    </li>                                   
                                </ul>
                            </div>
                            <!-- //detail_con -->

                            <div class="detail_con pb25">
                                <span class="left_sec">더 클래식<br/>500 </span>                                
                                <ul>
                                    <li>
                                       <dl>
                                           <dt><span>1F</span></dt>
                                           <dd>북문 외부</dd>
                                       </dl> 
                                    </li>                                                                                                           
                                </ul>
                            </div>
                            <!-- //detail_con -->
                        </div>
                        <!-- //conv_detail -->
                    </div>
                    <!-- //conven_list :: 흡연부스-->

                    <div class="conven_list" id="conven_5">
                        <h3 class="conv_tit"><i><img src="/resources/user/mobile/img/contents/about/icon_conv_5.png" alt=""></i>주차 무인정산기</h3>
                        <div class="bg_box">
                            단지 내 이용 가능한 주차 무인정산기는 <strong>총17곳</strong> 입니다.
                        </div>
                        
                        <ul class="comment">
                            <li>각 단지 내 주차요금은 서로 통합 적용되지 않으니 유의하시기 바랍니다.</li>
                        </ul>

                        <div class="conv_detail">
                            <div class="detail_con">
                                <span class="left_sec">시티존</span>                                
                                <ul>
                                    <li>
                                       <dl>
                                           <dt><span>B3</span></dt>
                                           <dd>
                                                <p>1,2호기 E/L홀 內  <em>(설치수량 2대)</em></p>
                                                <p>3,4호기 E/L홀 內  <em>(설치수량 1대)</em></p>
                                            </dd>
                                       </dl> 
                                    </li>
                                    <li>
                                       <dl>
                                           <dt><span>B2</span></dt>
                                           <dd>
                                                <p>1,2호기 E/L홀 內  <em>(설치수량 1대)</em></p>
                                                <p>3,4호기 E/L홀 內  <em>(설치수량 1대)</em></p>
                                            </dd>
                                       </dl> 
                                    </li>
                                    <li>
                                       <dl>
                                           <dt></dt>
                                           <dd>정산소 출차</dd>
                                       </dl> 
                                    </li>                                                                    
                                </ul>
                            </div>
                            <!-- //detail_con -->

                            <div class="detail_con">
                                <span class="left_sec">스타존 </span>                                
                                <ul>
                                    <li>
                                       <dl>
                                           <dt><span>B2</span></dt>
                                           <dd>E/S홀 內 <em>(설치수량 1대)</em></dd>
                                       </dl> 
                                    </li>
                                    <li>
                                       <dl>
                                           <dt><span>B3</span></dt>
                                           <dd>E/S홀 內 <em>(설치수량 1대)</em></dd>
                                       </dl> 
                                    </li>
                                    <li>
                                       <dl>
                                           <dt><span>B4</span></dt>
                                           <dd>E/S홀 內 <em>(설치수량 1대)</em></dd>
                                       </dl> 
                                    </li>
                                    <li>
                                       <dl>
                                           <dt><span>B5</span></dt>
                                           <dd>E/S홀 內 <em>(설치수량 1대)</em></dd>
                                       </dl> 
                                    </li>
                                    <li>
                                       <dl>
                                           <dt><span>B6</span></dt>
                                           <dd>E/S홀 內 <em>(설치수량 1대)</em></dd>
                                       </dl> 
                                    </li>
                                    <li>
                                       <dl>
                                           <dt></dt>
                                           <dd>정산소 출차</dd>
                                       </dl> 
                                    </li>                                                             
                                </ul>
                            </div>
                            <!-- //detail_con -->

                            <div class="detail_con">
                                <span class="left_sec">영존</span>                                
                                <ul>
                                    <li>
                                       <dl>
                                           <dt><span>B2</span></dt>
                                           <dd>B동 E/L홀 內 <em>(설치수량 1대)</em></dd>
                                       </dl> 
                                    </li>
                                    <li>
                                       <dl>
                                           <dt><span>B3</span></dt>
                                           <dd>B동 E/L홀 <em>(설치수량 1대)</em></dd>
                                       </dl> 
                                    </li>
                                    <li>
                                       <dl>
                                           <dt><span>B4</span></dt>
                                           <dd>B동 E/S홀 內 <em>(설치수량 1대)</em></dd>
                                       </dl> 
                                    </li>
                                    <li>
                                       <dl>
                                           <dt><span>B5</span></dt>
                                           <dd>B동 E/S홀 內 <em>(설치수량 1대)</em></dd>
                                       </dl> 
                                    </li>                                   
                                    <li>
                                       <dl>
                                           <dt></dt>
                                           <dd>정산소 출차</dd>
                                       </dl> 
                                    </li>                                                             
                                </ul>
                            </div>
                            <!-- //detail_con -->

                            <div class="detail_con">
                                <span class="left_sec">더 클래식<br/>500</span>                                
                                <ul>
                                    <li>
                                       <dl>
                                           <dt><span>B2</span></dt>
                                           <dd>A동 B동 E/V홀 內 각 1대 <em>(설치수량 2대)</em></dd>
                                       </dl> 
                                    </li>
                                    <li>
                                       <dl>
                                           <dt><span>B2</span></dt>
                                           <dd>출구 정산소 앞 <em>(설치수량 1대)</em></dd>
                                       </dl> 
                                    </li>                                                                                              
                                </ul>
                            </div>
                            <!-- //detail_con -->
                        </div>
                        <!-- //conv_detail -->
                    </div>
                    <!-- //conven_list :: 주차 무인정산기-->

                    <div class="conven_list" id="conven_6">
                        <h3 class="conv_tit"><i><img src="/resources/user/mobile/img/contents/about/icon_conv_6.png" alt=""></i>무인민원 발급기</h3>
                        <div class="bg_box">
                            단지 내 이용 가능한 무인민원 발급기는 <strong>총 1곳</strong> 입니다.
                        </div>
                        <div class="conv_detail">
                            <div class="detail_con">
                                <span class="left_sec">시티존</span>                                
                                <ul>
                                    <li>
                                       <dl>
                                           <dt><span>1F</span></dt>
                                           <dd>4번 게이트 內 <em>설치수량 1대</em></dd>
                                       </dl> 
                                    </li>                                   
                                </ul>
                            </div>
                            <!-- //detail_con -->
                        </div>
                        <!-- //conv_detail -->
                    </div>
                    <!-- //conven_list :: 무인민원 발급기-->

                    <div class="conven_list" id="conven_7">
                        <h3 class="conv_tit"><i><img src="/resources/user/mobile/img/contents/about/icon_conv_7.png" alt=""></i>수유실</h3>
                        <div class="bg_box">
                            단지 내 이용 가능한 수유실은 <strong>총 1곳</strong> 입니다.
                        </div>   
                        <ul class="comment">
                            <li>수유실은 롯데백화점 8층에 위치하고 있으며, 롯데백화점 이용 고객만 이용가능하오니 유의하시기 바랍니다. </li>
                        </ul>                     
                    </div>
                    <!-- //conven_list :: 수유실 -->

                    <div class="conven_list" id="conven_8">
                        <h3 class="conv_tit"><i><img src="/resources/user/mobile/img/contents/about/icon_conv_8.png" alt=""></i>만남의 광장</h3>
                        <div class="bg_box">
                            단지 내 이용 가능한 만남의 광장은 <strong>총 1곳</strong> 입니다.
                        </div>   
                        <ul class="comment">
                            <li>만남의 광장은 스타존 1층 롯데백화점 앞에 위치하고 있습니다.</li>
                        </ul>                     
                    </div>
                    <!-- //conven_list :: 만남의 광장 -->
                    
                </div>
                <!-- //conven_wrap -->

            </div>
            <!-- //ly_container -->

            <script type="text/javascript">        
                function categorychg(combo){    
                    if (combo.value != 0)
                    {               
                        $(".conven_list").hide();
                        $("#conven_" + combo.value).show();                               
                    } else {
                        $(".conven_list").show();                
                    } 
                }
            </script>  

        </div>
        <!-- //layout_body -->
        
<%@include file="../inc/inc_gl_bottom.jsp"%>