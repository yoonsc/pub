<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

    <div class="sub_title">
        <h2>영업시간</h2>
        <p class="en_summary">BUSINESS HOURS</p>
    </div>

    <div class="layout_body opening">
        <div class="opening_bg_box">
            <dl>
                <dt>
                    <p class="tit">스타시티 영업시간 </p>
                    <p class="time">10:00 ~ 23:00</p>
                </dt>
                <dd>연중무휴</dd>
            </dl>
        </div>


        <div class="ly_container">
            <h3>스타시티 영업시간 외 매장</h3>
            <ul class="opening_list">
                <li>
                    <div class="list_con">
                        <div class="logo_box">
                            <img src="/resources/user/mobile/img/contents/about/logo_lotte.jpg" alt="">
                        </div>
                        <dl>
                            <dt>롯데백화점</dt>
                            <dd>
                                <ul class="shop_detail">
                                    <li>
                                        <span>대표전화</span>
                                        <strong>02-2218-2500</strong>
                                    </li>
                                    <li>
                                        <span>매장위치</span>
                                        <strong>스타존 B1 ~ 10F</strong>
                                    </li>
                                    <li>
                                        <span>영업시간</span>
                                        <strong>10:30 ~ 20:30</strong>
                                    </li>
                                </ul>
                                <div class="button_group txt-cnt">
                                    <span><a href="https://www.lotteshopping.com/branchShopGuide/floorGuideSub?cstr=0028" class="btn btn_round_blue" target="_blank">자세히보기</a></span>
                                </div>
                            </dd>
                        </dl>
                    </div>
                </li>
                <li>
                    <div class="list_con">
                        <div class="logo_box">
                            <img src="/resources/user/mobile/img/contents/about/logo_cinema.jpg" alt="">
                        </div>
                        <dl>
                            <dt>롯데시네마</dt>
                            <dd>
                                <ul class="shop_detail">
                                    <li>
                                        <span>대표전화</span>
                                        <strong>1544-8855</strong>
                                    </li>
                                    <li>
                                        <span>매장위치</span>
                                        <strong>시티존 2F</strong>
                                    </li>
                                    <li>
                                        <span>영업시간</span>
                                        <strong>상영시간에 따라 상의</strong>
                                    </li>
                                </ul>
                                <div class="button_group txt-cnt">
                                    <span><a href="http://www.lottecinema.co.kr" target="_blank" class="btn btn_round_blue">자세히보기</a></span>
                                </div>
                            </dd>
                        </dl>
                    </div>
                </li>
                <li>
                    <div class="list_con">
                        <div class="logo_box">
                            <img src="/resources/user/mobile/img/contents/about/logo_emart2.jpg" alt="">
                        </div>
                        <dl>
                            <dt>이마트</dt>
                            <dd>
                                <ul class="shop_detail">
                                    <li>
                                        <span>대표전화</span>
                                        <strong>02-2024-1234</strong>
                                    </li>
                                    <li>
                                        <span>매장위치</span>
                                        <strong>시티존 B1</strong>
                                    </li>
                                    <li>
                                        <span>영업시간</span>
                                        <strong>10:00 ~ 23:00</strong>
                                    </li>
                                </ul>
                                <div class="button_group txt-cnt">
                                    <span><a href="http://store.emart.com/branch/list.do?id=1106" target="_blank" class="btn btn_round_blue">자세히보기</a></span>
                                </div>
                            </dd>
                        </dl>
                    </div>
                </li>
                <li>
                    <div class="list_con">
                        <div class="logo_box">
                            <img src="/resources/user/mobile/img/contents/about/logo_bandi2.jpg" alt="">
                        </div>
                        <dl>
                            <dt>반디앤루니스</dt>
                            <dd>
                                <ul class="shop_detail">
                                    <li>
                                        <span>대표전화</span>
                                        <strong>02-2218-3050</strong>
                                    </li>
                                    <li>
                                        <span>매장위치</span>
                                        <strong>스타존 B1</strong>
                                    </li>
                                    <li>
                                        <span>영업시간</span>
                                        <strong>09:30 ~ 22:00</strong>
                                    </li>
                                </ul>
                                <div class="button_group txt-cnt">
                                    <span><a href="http://www.bandinlunis.com/front/aboutBandi/aboutBandiMain.do?key=50&amp;map=110" target="_blank" class="btn btn_round_blue">자세히보기</a></span>
                                </div>
                            </dd>
                        </dl>
                    </div>
                </li>
            </ul>
        </div>

    </div>
    <!-- //layout_body -->
        
<%@include file="../inc/inc_gl_bottom.jsp"%>