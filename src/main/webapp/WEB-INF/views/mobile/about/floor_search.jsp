<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

        <div class="sub_title">
            <h2>매장검색</h2>
            <p class="en_summary">search</p>
        </div>

        <div class="layout_body store_search">
            
                <div class="select_box_con mb25" id="searchBox"> 
                    <p class="search_tit">어떤 매장을 찾으시나요?</p>               
                    <span class="select-wrapper">
                        <select id="searchType" name="searchType">   
                            <option value="">전체 카테고리</option>                                
                            <option value="">라이프스타일</option>                             
                            <option value="">푸드</option>
                            <option value="">카페&디저트</option>
                            <option value="">패션</option>
                            <option value="">잡화</option>
                            <option value="">서비스</option>
                            <option value="">푸드</option>
                        </select>
                    </span>
                    <span class="search_in">                                        
                        <input type="text" name="searchTsearchStoreext" id="searchStore" value="" class="btmline" placeholder="매장명 또는 키워드를 입력하세요.">
                        <a href="javascript:;" class="btn btn-search">검색</a>
                    </span>
                </div>



            <div class="ly_container">    

                <div class="floor_select">
                    <span class="result_txt">총 <strong>291</strong>개 매장</span>
                    <span class="select-wrapper">
                        <select id="selectType" name="selectType">   
                            <option value="">전체단지</option> 
                            <option value="">스타존</option> 
                            <option value="">시티존</option> 
                            <option value="">영존</option> 
                            <option value="">더클래식500</option> 
                        </select>
                    </span>
                </div>    


                <ul class="floor_shop_list">
                    <li class="no_result">
                        <p class="icon"><i class="ico ico_result_no"></i></p>
                        <p class="txt">검색어와 일치하는 매장이 없습니다.</p>
                    </li>
                    <li>
                        <div class="list_left">
                            <span class="thumb"><img src="/resources/user/mobile/img/contents/about/logo_shop_list.jpg" alt=""></span>
                        </div>
                        <div class="shop_box_con">
                            <dl>
                                <dt>반디앤루니스반디앤루니스반디앤루니스반디앤루니스반디앤루니스</dt>
                                <dd>
                                   <ul>
                                        <li><strong>대표전화 :</strong><span class="data">02-2218-3050</span></li>
                                        <li><strong>매장위치 :</strong><span class="data">스타존 B1</span></li>
                                    </ul>
                                </dd>
                            </dl>
                            <div class="btn_box">
                                <a href="#" class="btn btn_round_nm">상세보기</a>
                            </div>
                        </div>
                    </li>                    
                    <li>
                        <div class="list_left">
                            <span class="thumb"><img src="/resources/user/mobile/img/contents/about/logo_shop_list.jpg" alt=""></span>
                        </div>
                        <div class="shop_box_con">
                            <dl>
                                <dt>반디앤루니스반디앤루니스반디앤루니스반디앤루니스반디앤루니스</dt>
                                <dd>
                                   <ul>
                                        <li><strong>대표전화 :</strong><span class="data">02-2218-3050</span></li>
                                        <li><strong>매장위치 :</strong><span class="data">스타존 B1</span></li>
                                    </ul>
                                </dd>
                            </dl>
                            <div class="btn_box">
                                <a href="#" class="btn btn_round_nm">상세보기</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="list_left">
                            <span class="thumb"><img src="/resources/user/mobile/img/contents/about/logo_shop_list.jpg" alt=""></span>
                        </div>
                        <div class="shop_box_con">
                            <dl>
                                <dt>반디앤루니스반디앤루니스반디앤루니스반디앤루니스반디앤루니스</dt>
                                <dd>
                                   <ul>
                                        <li><strong>대표전화 :</strong><span class="data">02-2218-3050</span></li>
                                        <li><strong>매장위치 :</strong><span class="data">스타존 B1</span></li>
                                    </ul>
                                </dd>
                            </dl>
                            <div class="btn_box">
                                <a href="#" class="btn btn_round_nm">상세보기</a>
                            </div>
                        </div>
                    </li>
                </ul>

                <div class="pagination">
                    <ul>                    
                        <li><a href="javascript:;" onclick="" class="prev pagbtn">이전</a></li> 
                        <li><strong>1</strong></li>
                        <li><a href="javascript:onPaging(document.listForm, 2)">2</a></li>
                        <li><a href="javascript:;" onclick="onPaging(document.listForm, 2)" class="next pagbtn">다음</a></li>
                    </ul>
                </div>

            </div>            
        </div>

    
<%@include file="../inc/inc_gl_bottom.jsp"%>