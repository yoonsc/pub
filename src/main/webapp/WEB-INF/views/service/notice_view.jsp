<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents bg_white pb100">	

	<div class="ly_container notice_view">
		<dl class="board_tit">
			<dt>
				<span class="tit">제목</span>
				<span class="txt">겨울맞이 슈퍼세일 이벤트</span>
			</dt>
			<dd>
				<span class="tit">작성일</span>
				<span class="date">2019.05.10</span>
			</dd>
		</dl>
		

		<div class="board_data notice_data">
			<div class="data_txt">			
				<p>스타시티 5월 문화공연 안내 내용이 들어갑니다. 스타시티 5월 문화공연 안내 내용이 들어갑니다.</p>
				<p>스타시티 5월 문화공연 안내 내용이 들어갑니다. 스타시티 5월 문화공연 안내 내용이 들어갑니다.</p>
				<p>스타시티 5월 문화공연 안내 내용이 들어갑니다. 스타시티 5월 문화공연 안내 내용이 들어갑니다.</p>
				<p>스타시티 5월 문화공연 안내 내용이 들어갑니다. 스타시티 5월 문화공연 안내 내용이 들어갑니다.</p>
				<p>스타시티 5월 문화공연 안내 내용이 들어갑니다. 스타시티 5월 문화공연 안내 내용이 들어갑니다.</p>
			</div>
		</div>

		<div class="board_file">
			<dl>
				<dt><i class="ico ico_file_clip"></i> 첨부파일</dt>
				<dd>
					<ul>
						<li><a href="#">5월 문화공연 일정표.pdf <i>다운로드</i></a></li>
						<li><a href="#">첨부파일명 첨부파일명이 나옵니다.pdf <i>다운로드</i></a></li>
					</ul>
				</dd>
			</dl>
		</div>

		<div class="board_nav">
            <dl class="data_nav_prev">
                <dt>이전글</dt>
                <dd>등록된 이전글이 없습니다.</dd>
                <dd class="date"></dd> 
            </dl>
            <dl class="data_nav_next">
                <dt>다음글</dt>                   		
       			<dd>
                	<a href="javascript:;" onclick="goMove(73)" class="ellipsis">스타시티 5월 문화공연 안내</a>
                </dd>
                <dd class="date">2019.01.25</dd>                    
            </dl>
        </div>  
              
		<div class="board_btm txt-right bt0">												
			<span><a href="javascript:;" class="btn btn_round_nm"><i class="ico ico_list_box"></i> 목록</a></span>					
		</div>
	</div>
	<!-- //ly_container  -->
</div>
<!-- //sub_contents -->

<script type="text/javascript">
     (function (ns) {
        $(function(){          
          
        });
     })(APP || {});
</script>


<%@include file="../inc/inc_gl_bottom.jsp"%>