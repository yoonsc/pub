<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<p class="contents_title">고객님의 소중한 의견 감사드리며, 궁금하신 사항에 대해 최대한 빠른 답변드리겠습니다.</p>
<div class="sub_contents bg_white ph50">
	<div class="ly_container">

		<div class="register_con">
		
			<table class="common_tb_col register_form">
	         <caption>문의유형, 제목, 내용, 첨부파일내용을 작성하는 등록서식</caption>
	         <colgroup>
	           <col width="137px">
	           <col width="*">
	         </colgroup>                         
	         <tbody>
	             <tr>
	                 <th scope="row"><!-- <i class="require"></i> --> 문의유형</th>
	                 <td>
	                    <span class="select-wrapper line_select">
	                     <select name="type" title="카테고리선택">
	                       <option value="">카테고리선택</option>
	                       <option value="1">일반문의</option>
	                       <option value="2">프로젝트문의</option>
	                       <option value="3">투자문의</option>
	                       <option value="4">기타문의</option>
	                     </select>      
	              		</span>   
	                 </td>
	             </tr>                                                                                         
	             <tr>
	                 <th scope="row"><!-- <i class="require"></i> --> 제목</th>
	                 <td>
	                    <div class="placeholder"style="display: block;">
	                 <label for="hTitle">제목</label>
	                 <input type="text" name="title" id="hTitle" style="width:100%;" >
	              </div>                                    
	                 </td>
	             </tr>
	             <tr>
	                 <th scope="row"><!-- <i class="require"></i> --> 내용</th>
	                 <td class="memo_cell">
	                 	<div class="momo_con">
	                 		<div class="remain_box">글자수 : <span class="remain_txt">0</span> / 2000</div>
	                    	<textarea name="contents" id="hMemo" style="width:100%; height: 280px;" /></textarea>	
	                	</div>

	                	<ul class="comment">
	                 		<li>내용은 2,000자(4000byte)까지 작성하실 수 있습니다.</li>
	                 		<li>문의 주신 내용에 대한 답변은 마이페이지 > 나의 문의내역에서 확인하실 수 있습니다. </li>
	                 	</ul>
	                 </td>
	             </tr>             
	             <tr>
	                 <th scope="row"><!-- <i class="require"></i> --> 첨부파일</th>
	                 <td>
	                 	<div class="addfile">
	                 		<input type="file" name="hFILE_NAME" class="type-file" /><!--<a href="javascript:;" class="add_plus"><i class="fa fa-plus"></i></a>-->
	                 	</div>
	                 	<div class="addfile">
	                 		<input type="file" name="hFILE_NAME" class="type-file" />
	                 	</div>
	                 	<div class="addfile">
	                 		<input type="file" name="hFILE_NAME" class="type-file" />
	                 	</div>

	                 	<ul class="comment">
	                 		<li>5MB(5,120byte)이하의 파일만 등록이 가능합니다.</li>
	                 		<li>첨부파일은 총 3개까지만 등록 가능합니다. </li>
	                 	</ul>

	                 	<!--<ul id="addField_con">                 		
	                 	</ul>
	                 	-->
	                 </td>
	             </tr>
	         </tbody>         
	     </table>  
	    </div>   
	    <!-- //register_con --> 

	    <div class="board_btm txt-right bt0">												
	    	<span><a href="javascript:;" class="btn btn_round_nm"> 취소</a></span>
			<span class="ml10"><a href="javascript:submitPop();" class="btn btn_round_blue_bg"> 등록</a></span>					
		</div>


	</div>
	<!-- //ly_container -->
	 
	<!-- submit 팝업 시작-->
	<div id="confirmLayerBox" class="layerPopup confirm_layer">
		<div class="layerPopup_in">
			<span class="closeLayer">
				<a href="javascript:LayerPopups.close('confirmLayerBox');" class="btn"><i class="lnr lnr-cross"></i></a>
			</span>
			<div class="layer_txt">
				<dl class="submit_txt">
					<dt>문의주신 내용이 <br /><strong>접수 완료</strong>되었습니다.</dt>
					<dd>최대한 빠른 답변을 드릴 수 있도록 노력하겠습니다. <br />
					답변은 <strong class="txt-line-black">마이페이지 > 나의 문의내역</strong> 에서 확인하실 수 있습니다. <br />
					감사합니다. </dd>
				</dl>				
			</div>
			<div class="button_group txt-cnt">
				<span><a href="javascript:LayerPopups.close('confirmLayerBox');" class="btn btn_round_blue_bg">확인</a></span>
			</div>
		</div>
	</div> 
	<!-- //submit 팝업 -->

	<script type="text/javascript">
		
		function submitPop(){
			LayerPopups.open('confirmLayerBox', true);
		}

		(function (ns) {
			$(function(){
			   $('#searchText').keydown(function (key) {
				   if (key.keyCode == 13) {
						$(".btn-search").trigger("click");
				   }
			   });
			   ns.placeholder.init();
			   ns.charLimit.init("#hMemo", 2000);
			   
			   ns.faqAcMenu($("#faq_list"), true);

			   $(".type-file").filestyle({
				   image: "/resources/user/img/btn_file.gif",
				   width:810,		   
				   imageheight : 40,
				   imagewidth : 100
			  });

			});
		 })(APP || {});

 	/*
	$(function(){
	      
	       bind_btn_del();
	       $(".add_plus").click(function () {
	            if ($("#addField_con li").length > 3) {
	                return;
	            }
	            $(".addInput").removeAttr("id");
	            var copy = $('<li class="add_form"></li>').html(
	                '<input type="text" style="width:250px;" class="type-file addInput" name="hFILE_NAME">'
	                + '<a href="javascript:;" class="delfile btn btn_grey_sm">' + '<i class="fa fa-minus">' + '</i>' + '</a>'
	                );
	            $("#addField_con").append(copy);
	            $("#addField_con").find(".addInput").last().val("");
	            $(".delfile").unbind("click");
	            bind_btn_del();
	        });

	        function bind_btn_del() {
	            $(".delfile").click(function () {
	                $(this).parents("li").remove();
	                //if ($(".delfile").length > 1) $(this).parents("li").remove();
	            });
	        }
	        

	});
	*/
	</script>


	

</div>
<!-- //sub_contents -->


<%@include file="../inc/inc_gl_bottom.jsp"%>