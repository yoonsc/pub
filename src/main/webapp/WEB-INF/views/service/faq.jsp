<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<p class="contents_title">스타시티 이용에 대한 궁금증을 해결해 드립니다.</p>
<div class="sub_contents bg_white ph100">
	<div class="ly_container">

		<div class="select_box_top">						
			<span class="select">
				<span class="select-wrapper">
					<select id="type" name="type">
	           			<option value="0">전체</option>							
						<option value="1">제목</option>							
						<option value="2">내용</option>							
					</select>
				</span>
			</span>	
			<span class="search_in">
				<div class="placeholder">
					<label for="text">검색어를 입력해주세요.</label>
					<input type="text" name="text" id="text" class="btmline" value="">
				</div>									
				<a href="javascript:fn_search(document.listForm);" class="btn btn-search">검색</a>
			</span>
		</div>

		<div class="faq">
			<ul class="faq_list" id="faq_list">
				<!-- best 항목 -->
				<li>
					<dl class="ac-cont">
						<dt>
							<a href="javascript:;" title="내용 보기">
								<div class="left_sec">
									<span class="best"></span>
									<span class="cate">고객안내</span>
									<em class="ico_q">Q</em>
								</div>
								<div class="right_sec">
									롯데백화점은 어디에 있나요?
								</div>
								<i></i>
							</a>
						</dt>
						<dd>
							<em class="ico_a">A</em>
							<div>
								<p>이마트는 스타시티 지하 1F에 위치하고 있습니다.</p>
								<p>연락처 : 02-2024-1234</p>
								<p>홈페이지 : http://emart.shinsegae.com/</p>
							</div>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="ac-cont">
						<dt>
							<a href="javascript:;" title="내용 보기">
								<div class="left_sec">
									<span class="best"></span>
									<span class="cate">고객안내</span>
									<em class="ico_q">Q</em>
								</div>
								<div class="right_sec">
									롯데백화점은 어디에 있나요?
								</div>
								<i></i>
							</a>
						</dt>
						<dd>
							<em class="ico_a">A</em>
							<div>
								<p>이마트는 스타시티 지하 1F에 위치하고 있습니다.</p>
								<p>연락처 : 02-2024-1234</p>
								<p>홈페이지 : http://emart.shinsegae.com/</p>
							</div>
						</dd>
					</dl>
				</li>
				<!-- //best 항목 -->
				<li>
					<dl class="ac-cont">
						<dt>
							<a href="javascript:;" title="내용 보기">
								<div class="left_sec">
									<span class="num">5</span>
									<span class="cate">고객안내</span>
									<em class="ico_q">Q</em>
								</div>
								<div class="right_sec">
									롯데백화점은 어디에 있나요?
								</div>
								<i></i>
							</a>
						</dt>
						<dd>
							<em class="ico_a">A</em>
							<div>채용 합격 후 별도로 제출하시면 됩니다. </div>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="ac-cont">
						<dt>
							<a href="javascript:;" title="내용 보기">
								<div class="left_sec">
									<span class="num">5</span>
									<span class="cate">고객안내</span>
									<em class="ico_q">Q</em>
								</div>
								<div class="right_sec">
									롯데백화점은 어디에 있나요?
								</div>
								<i></i>
							</a>
						</dt>
						<dd>
							<em class="ico_a">A</em>
							<div>채용 합격 후 별도로 제출하시면 됩니다. </div>
						</dd>
					</dl>
				</li>
			</ul>
		</div>

		<div class="pagination">
			<ul>
				<!-- <li><a href="#none" onclick="fn_page();" class="first pagbtn">처음</a></li> -->
				<li><a href="#" onclick="fn_page();" class="prev pagbtn">이전</a></li>
				<li><strong>1</strong></li>
				<li><a href="#none" onclick="fn_page(2);">2</a></li>
				<li><a href="#none" onclick="fn_page(3);">3</a></li>
				<li><a href="#none" onclick="fn_page(4);">4</a></li>
				<li><a href="#none" onclick="fn_page(5);">5</a></li>
				<li><a href="#none" onclick="fn_page(6);">6</a></li>
				<li><a href="#none" onclick="fn_page(7);">7</a></li>
				<li><a href="#none" onclick="fn_page(8);">8</a></li>
				<li><a href="#none" onclick="fn_page();" class="next pagbtn">다음</a></li>
				<!-- <li><a href="#" onclick="fn_page();" class="last pagbtn">마지막</a></li> -->
			</ul>
		</div>

	</div>
	<!-- //ly_container -->

	<script type="text/javascript">
		(function (ns) {
			$(function(){
			   $('#searchText').keydown(function (key) {
				   if (key.keyCode == 13) {
						$(".btn-search").trigger("click");
				   }
			   });
			   ns.placeholder.init();
			   ns.faqAcMenu($("#faq_list"), true);
			});
		 })(APP || {});
	</script>

</div>
<!-- //sub_contents -->


<%@include file="../inc/inc_gl_bottom.jsp"%>