<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<p class="contents_title">스타시티 임대 절차 및 방법에 대해 안내해드립니다. </p>
<div class="sub_contents bg_white ph50">
	<div class="ly_container">
		<div class="in_shop_con">
			<h3>스타시티 임대 절차 및 방법 안내</h3>
			<ol class="num_list">
				<li>1. 스타시티의 모든 입찰은 임대차 계약에 따르며, 계약 기간은 협의 후 결정합니다. </li>
				<li>2. 계약자, 사업자등록증, 영업인허가증 명의가 동일해야 하며 전대 등이 원칙적으로 금지되어 있습니다.</li>
				<li>3. 홈페이지상의 입찰공고를 확인하신 후 관련 서류를 준비하시어 지정 기일 내에 제출하시면 됩니다.</li>
				<li>4. 점포별 예정가 이상의 최고금액 응찰자를 낙찰자로 선정하며, 예정가격은 투찰 전까지는 비공개를 원칙으로 합니다.</li>
				<li>5. 투찰은 환산보증금 기준으로 하며, 동일 금액의 최고 응찰자가 2인 이상일 경우 입찰을 실시합니다.</li>
				<li>6. 2인 이상의 유효한 입찰자가 없거나 낙찰자가 없을 경우 재입찰하며, 재입찰에도 낙찰자가 없을 시 수의계약 합니다.</li>
			</ol>

			<div class="line_box">
				<dl>
					<dt>입찰 서류 안내</dt>
					<dd>
						<ul class="bl_line">
							<li>법인 : 입찰참가신청서, 청렴이행서약서, 사업자등록증, 법인 등기부등본, 법인 인감증명서, 사용인감계, 입점의향브랜드소개자료
							<li>개인 : 입찰참가신청서, 청렴이행서약서, 주민등록등본, 인감증명서, 신분증 사본, 입점의향브랜드소개자료</li>
						</ul>
						<p class="mt10">* 입찰제출서류는 입찰공고를 확인해주세요.</p>
					</dd>
				</dl>
				<dl>
					<dt>문의</dt>
					<dd>
						<p>
							<span><strong>전화</strong> 02-2218-5704 </span>
							<span><strong>FAX</strong> 02-2218-5536 </span>
						</p>
						<p><span><strong>이메일</strong> yonggong79@theclassic500.co.kr </span></p>
					</dd>
				</dl>
			</div>

			<div class="button_group txt-cnt">	
				<span><a href="javascript:;" class="btn box-blue">입점 신청서 다운로드 <i class="lnr lnr-download"></i></a></span>			
			</div>

		</div>

	</div>
	<!-- //ly_container -->

</div>
<!-- //sub_contents -->


<%@include file="../inc/inc_gl_bottom.jsp"%>