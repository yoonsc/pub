<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<p class="contents_title">스타시티의 새로운 소식을 알려드립니다.</p>
<div class="sub_contents bg_white ph100">
	<div class="ly_container">

		<div class="select_box_top">						
			<span class="select">
				<span class="select-wrapper">
					<select id="type" name="type">
	           			<option value="0">전체</option>							
						<option value="1">제목</option>							
						<option value="2">내용</option>							
					</select>
				</span>
			</span>	
			<span class="search_in">
				<div class="placeholder">
					<label for="text">검색어를 입력해주세요.</label>
					<input type="text" name="text" id="text" class="btmline" value="">
				</div>									
				<a href="javascript:fn_search(document.listForm);" class="btn btn-search">검색</a>
			</span>
		</div>

		<table class="common_tb_row" >
		  <caption>No, 구분, 제목, 작성일</caption>
		  <colgroup>
		    <col width="100px">
		    <col width="120px">
		    <col width="*">
		    <col width="150px">
		  </colgroup>	
		  <thead>
		  	<tr>
		  		<th>No</th>
		  		<th>구분</th>
		  		<th>제목</th>
		  		<th>작성일</th>
		  	</tr>
		  </thead>				          
		  <tbody>
			  	<tr>
			  		<td colspan="4" class="nodata">등록된 내용이 없습니다.</td>
			  	</tr>
		      	<tr class="notice">
		      		<td><span class="lable_round label_sm box-blue">공지</span></td>
		      		<td>구분</td>
			  		<td class="sbj"><a href="#">HMC 환불진행 관련 문의드려요</a></td>
			  		<td class="date">2019.05.10</td>					              
		      	</tr>	
		      	<tr class="notice">
		      		<td><span class="lable_round label_sm box-blue">공지</span></td>
		      		<td>구분</td>
			  		<td class="sbj"><a href="#">HMC 환불진행 관련 문의드려요</a></td>
			  		<td class="date">2019.05.10</td>					              
		      	</tr>
		      	<tr>
		      		<td class="num">99</td>
		      		<td>구분</td>
			  		<td class="sbj"><a href="#">HMC 환불진행 관련 문의드려요</a></td>
			  		<td class="date">2019.05.10</td>					              
		      	</tr>
		      	<tr>
		      		<td class="num">99</td>
		      		<td>구분</td>
			  		<td class="sbj"><a href="#">HMC 환불진행 관련 문의드려요</a></td>
			  		<td class="date">2019.05.10</td>					              
		      	</tr>
		      	<tr>
		      		<td class="num">99</td>
		      		<td>구분</td>
			  		<td class="sbj"><a href="#">HMC 환불진행 관련 문의드려요</a></td>
			  		<td class="date">2019.05.10</td>					              
		      	</tr>
		      	<tr>
		      		<td class="num">99</td>
		      		<td>구분</td>
			  		<td class="sbj"><a href="#">HMC 환불진행 관련 문의드려요</a></td>
			  		<td class="date">2019.05.10</td>					              
		      	</tr>
		  </tbody>
		</table>

		<div class="pagination">
			<ul>
				<!-- <li><a href="#none" onclick="fn_page();" class="first pagbtn">처음</a></li> -->
				<li><a href="#" onclick="fn_page();" class="prev pagbtn">이전</a></li>
				<li><strong>1</strong></li>
				<li><a href="#none" onclick="fn_page(2);">2</a></li>
				<li><a href="#none" onclick="fn_page(3);">3</a></li>
				<li><a href="#none" onclick="fn_page(4);">4</a></li>
				<li><a href="#none" onclick="fn_page(5);">5</a></li>
				<li><a href="#none" onclick="fn_page(6);">6</a></li>
				<li><a href="#none" onclick="fn_page(7);">7</a></li>
				<li><a href="#none" onclick="fn_page(8);">8</a></li>
				<li><a href="#none" onclick="fn_page();" class="next pagbtn">다음</a></li>
				<!-- <li><a href="#" onclick="fn_page();" class="last pagbtn">마지막</a></li> -->
			</ul>
		</div>

	</div>
	<!-- //ly_container -->

	<script type="text/javascript">
		(function (ns) {
			$(function(){
			   $('#searchText').keydown(function (key) {
				   if (key.keyCode == 13) {
						$(".btn-search").trigger("click");
				   }
			   });
			   ns.placeholder.init();
			});
		 })(APP || {});
	</script>

</div>
<!-- //sub_contents -->


<%@include file="../inc/inc_gl_bottom.jsp"%>