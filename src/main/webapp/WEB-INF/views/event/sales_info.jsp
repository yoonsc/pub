<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<p class="contents_title">스타시티 대관안내 입니다.</p>
<div class="sub_contents bg_white pb100">
	<div class="ly_container">
		<h4 class="content_tit">스타시티 대관 절차</h4>
		<ul class="steplist step_4">			
			<li>				
				<div class="line_wrap">					
					<p class="step_tit">STEP.1</p>
					<p class="icon_img"><img src="/resources/user/img/contents/event/step_icon_1.png" alt=""></p>
					<dl>
						<dt>신청</dt>
						<dd>대관신청서 및 공연계획서를<br />작성하여 담당자 이메일로 <br />접수 하시기 바랍니다. </dd>
					</dl>
				</div>
			</li>
			<li>				
				<div class="line_wrap">					
					<p class="step_tit">STEP.2</p>
					<p class="icon_img"><img src="/resources/user/img/contents/event/step_icon_2.png" alt=""></p>
					<dl>
						<dt>심사</dt>
						<dd>대관 심사 기준에 의거하여<br />
						대관의 가부를 결정하게 되며<br />
						대략 4~10일 정도의 심사기간이<br />
						소요 됩니다. 
						</dd>
					</dl>
				</div>
			</li>	
			<li>				
				<div class="line_wrap">					
					<p class="step_tit">STEP.3</p>
					<p class="icon_img"><img src="/resources/user/img/contents/event/step_icon_3.png" alt=""></p>
					<dl>
						<dt>승인</dt>
						<dd>심사기준 결과<br />
						대관승인이 결정되면<br />
						개별 통보해 드립니다.
						</dd>
					</dl>
				</div>
			</li>
			<li>				
				<div class="line_wrap">					
					<p class="step_tit">STEP.4</p>
					<p class="icon_img"><img src="/resources/user/img/contents/event/step_icon_4.png" alt=""></p>
					<dl>
						<dt>계약</dt>
						<dd>대관일 30일 전까지<br />
						계약을 체결합니다. 
						</dd>
					</dl>
				</div>
			</li>
		</ul>					
	</div>
	<!-- //ly_container  -->

	<div class="ly_container sales_info">
		<div class="linebox lg600">
			<div class="grid_left">
				<div class="linebar_tit">
					스타시티 대관 <br>신청양식 및 접수 안내
				</div>
			</div>
			<div class="grid_right">
				<dl>
					<dt>대관 신청 안내</dt>
					<dd>02-2218-5704</dd>
				</dl>
				<dl>
					<dt>대관 신청 접수 이메일</dt>
					<dd>yonggong79@theclassic500.co.kr </dd>
				</dl>
				<p class="alert_txt"><i class="fas fa-exclamation-circle"></i> 심사기준에 따라 대관료가 발생합니다.</p>
			</div>
			<!-- //grid_right -->
		</div>
	</div>
	<!-- //ly_container  -->

</div>
<!-- //sub_contents -->


<%@include file="../inc/inc_gl_bottom.jsp"%>