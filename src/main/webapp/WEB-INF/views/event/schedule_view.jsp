<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents bg_white pb100">	

	<div class="ly_container">
		<dl class="board_tit">
			<dt>
				<span class="label box-event">공연</span>
				<span class="txt">겨울맞이 슈퍼세일 이벤트</span>
			</dt>
			<dd>
				<ul class="sns_link">
					<li><a href="#"><i class="fab fa-facebook-f"></i>facebook</a></li>
					<li><a href="#"><i class="fab fa-twitter"></i>twitter</a></li>
					<li><a href="#"><i class="fas fa-link"></i>link</a></li>
				</ul>
			</dd>
		</dl>
		<p class="board_subtit">
			<span class="txt"><i class="ico ico_cal_sm"></i> <strong>공연일시</strong> 2018.12.18 (화) 15:00</span>
			<span class="txt"><i class="ico ico_loc_sm"></i> <strong>공연장소</strong> 건대입구역 사거리 라플라스 광장</span>
			<span class="txt"><i class="ico ico_time_sm"></i> <strong>공연시간</strong> 60분공연</span>
		</p>

		<div class="board_data">
			<p class="data_img"><img src="/resources/user/img/contents/event/event_view.jpg" alt=""></p>
			<div class="field_data">
				<dl class="top_bar">
					<dt>공연내용</dt>
					<dd>관리자에 입력된 공연내용이 나옵니다. 관리자에 입력된 공연내용이 나옵니다. 관리자에 입력된 공연내용이 나옵니다. 관리자에 입력된 공연내용이 나옵니다. 
관리자에 입력된 공연내용이 나옵니다. </dd>
				</dl>
				<dl class="top_bar">
					<dt>기타</dt>
					<dd>관리자에 입력된 기타내용이 나옵니다. 관리자에 입력된 기타내용이 나옵니다. </dd>
				</dl>
				<dl class="top_bar">
					<dt>문의</dt>
					<dd>02-1234-1234</dd>
				</dl>
			</div>
		</div>

		<div class="board_btm txt-right">												
			<span><a href="activity" class="btn btn_round_nm"><i class="ico ico_list_box"></i> 목록</a></span>					
		</div>
	</div>
	<!-- //ly_container  -->
</div>
<!-- //sub_contents -->

<script type="text/javascript">
     (function (ns) {
        $(function(){          
          
        });
     })(APP || {});
</script>


<%@include file="../inc/inc_gl_bottom.jsp"%>