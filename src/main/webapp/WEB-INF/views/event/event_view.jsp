<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents bg_white pb100">	

	<div class="ly_container">
		<dl class="board_tit">
			<dt>
				<span class="label box-event">이벤트</span>
				<span class="txt">겨울맞이 슈퍼세일 이벤트</span>
			</dt>
			<dd>
				<ul class="sns_link">
					<li><a href="#"><i class="fab fa-facebook-f"></i>facebook</a></li>
					<li><a href="#"><i class="fab fa-twitter"></i>twitter</a></li>
					<li><a href="#"><i class="fas fa-link"></i>link</a></li>
				</ul>
			</dd>
		</dl>
		<p class="board_subtit">
			<span class="date"><i class="ico ico_cal_sm"></i> <strong>기간</strong> 2018.10.01 ~ 2018.12.31</span>
		</p>

		<div class="board_data">
			<div class="data_txt">
				<p>5월 문화공연을 장식해준 팀은 현대적인 퍼포먼스에 한국의 전통을 가미한 퓨전 난타를 선보인 <퀸즈타>라는 난타 전문 공연팀 입니다.</p>
				<p>5월 문화공연을 장식해준 팀은 현대적인 퍼포먼스에 한국의 전통을 가미한 퓨전 난타를 선보인 <퀸즈타>라는 난타 전문 공연팀 입니다.</p>
				<p>5월 문화공연을 장식해준 팀은 현대적인 퍼포먼스에 한국의 전통을 가미한 퓨전 난타를 선보인 <퀸즈타>라는 난타 전문 공연팀 입니다.</p>
			</div>
		</div>

		<div class="board_btm txt-right">												
			<span><a href="javascript:;" class="btn btn_round_nm"><i class="ico ico_list_box"></i> 목록</a></span>					
		</div>
	</div>
	<!-- //ly_container  -->
</div>
<!-- //sub_contents -->

<script type="text/javascript">
     (function (ns) {
        $(function(){          
          
        });
     })(APP || {});
</script>


<%@include file="../inc/inc_gl_bottom.jsp"%>