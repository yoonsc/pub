<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents bg_white pb100">	

	<div class="ly_container">
		<dl class="board_tit no_tit_label">
			<dt>				
				<span class="txt">겨울맞이 슈퍼세일 이벤트</span>
			</dt>
			<dd>
				<span class="date">2019.05.10</span>
			</dd>
		</dl>		

		<div class="board_data">
			<p class="data_txt">5월 문화공연을 장식해준 팀은 현대적인 퍼포먼스에 한국의 전통을 가미한<br />퓨전 난타를 선보인 <퀸즈타>라는 난타 전문 공연팀 입니다.</p>
			<p class="data_img"><img src="/resources/user/img/contents/event/event_view.jpg" alt=""></p>
			<p class="data_txt">5월 문화공연을 장식해준 팀은 현대적인 퍼포먼스에 한국의 전통을 가미한<br />퓨전 난타를 선보인 <퀸즈타>라는 난타 전문 공연팀 입니다.</p>			
		</div>

		<div class="board_nav bt0">
            <dl class="data_nav_prev">
                <dt>이전글</dt>
                <dd>등록된 이전글이 없습니다.</dd>
                <dd class="date"></dd> 
            </dl>
            <dl class="data_nav_next">
                <dt>다음글</dt>                   		
       			<dd>
                	<a href="javascript:;" onclick="goMove(73)" class="ellipsis">스타시티 5월 문화공연 안내</a>
                </dd>
                <dd class="date">2019.01.25</dd>                    
            </dl>
        </div>


		<div class="board_btm txt-right bt0">												
			<span><a href="activity" class="btn btn_round_nm"><i class="ico ico_list_box"></i> 목록</a></span>					
		</div>
	</div>
	<!-- //ly_container  -->
</div>
<!-- //sub_contents -->

<script type="text/javascript">
     (function (ns) {
        $(function(){          
          
        });
     })(APP || {});
</script>


<%@include file="../inc/inc_gl_bottom.jsp"%>