<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents bg_white pb100">

	<div class="ly_container">

		<div class="calendar_con" style="visibility: hidden;" v-bind:style="'visibility: visible;'">
			<div class="month_nav">
				<a href="#" class="prev_month" @click.prevent="prevCalendar();">이전달</a>
				<span class="nowmonth_txt" id="calendarYM">{{ year }}<strong>년</strong>{{ month }}<strong>월</strong></span>
				<a href="#" class="next_month" @click.prevent="nextCalendar();">다음달</a>
			</div>

			<ul class="tab_choice">
				<li :class="{ on : mode == 'calendar' }"><a href="#" class="btn btn_round_nm" @click.prevent="changeMode( 'calendar' )"><i class="ico ico_cal_box"></i>캘린더 보기</a></li>
				<li :class="{ on : mode == 'list' }"><a href="#" class="btn btn_round_nm" @click.prevent="changeMode( 'list' )"><i class="ico ico_list_box"></i>목록 보기</a></li>
			</ul>

			<div v-if="mode == 'calendar'">
				<table class="carendar_tbl">
					<thead>
						<tr>
							<th v-for="str in week">{{ str }}</th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="( arr, i ) in calendarArr" :key="i">
							<td v-for="( obj, j ) in arr" :key="j">
								<div class="date_box" :class="{ today : obj.today }" data-day="{{ obj.day }}" v-if="obj.day">
									<div class="data_date" :class="{ sun : j == 0 }">{{ obj.day }}</div>
									<div class="data_con" v-for="( dataObj, k ) in list[ obj.dateStr ]" :key="k" v-if="k < 2">
										<a v-bind:href="'/event/schedule_view?seq=' + dataObj.seq">
											<div class="data_cate">
												<span class="cate" :class="{ '공연' : 'cate_play', '전시' : 'cate_view' }[ dataObj.scheduleTypeName ]">{{ dataObj.scheduleTypeName }}</span>
											</div>
											<div class="data_title">{{ dataObj.title }}</div>
										</a>
									</div>
									<a href="#" class="date_more" @click.prevent="more( obj.dateStr )" v-if="list[ obj.dateStr ] && list[ obj.dateStr ].length > 2">+ 더보기</a>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div v-else-if="mode == 'list'">
				<table class="calendar_list" v-if="total > 0">
					<colgroup>
					<col style="width: 230px;">
					</colgroup>
					<tbody>
						<tr :class="{ on: actDateStr == k }" v-for="( arr, k ) in list" :key="k" v-if="arr.length && calendarObj[ k ]">
							<th>{{ getMonthDay( calendarObj[ k ] ) }}<span>({{ calendarObj[ k ].week }})</span></th>
							<td>
								<ul>
									<li v-for="( dataObj, i ) in arr" :key="i">
										<a v-bind:href="'/event/schedule_view?seq=' + dataObj.seq">
											<div class="thumb" :style="{ backgroundImage: 'url(' + dataObj.imgThumPc + ')' }"></div>
											<div class="data_con">
												<div class="data_cate">
													<span class="cate" :class="{ '공연' : 'cate_play', '전시' : 'cate_view' }[ dataObj.scheduleTypeName ]">{{ dataObj.scheduleTypeName }}</span>
												</div>
												<div class="data_title">{{ dataObj.title }}</div>
												<div class="data_des">
													<dl class="time"><dt>시간</dt><dd>{{ dataObj.showTime }}</dd></dl>
													<dl class="location"><dt>장소</dt><dd>{{ dataObj.place }}</dd></dl>
												</div>
											</div>
										</a>
									</li>
								</ul>
							</td>
						</tr>
					</tbody>
				</table>
				<p class="calendar_empty" v-else>
					등록된 공연/문화 일정이 없습니다.
				</p>
			</div>
		</div>
		<!-- //캘린더 영역 -->

	</div>
	<!-- //ly_container  -->
</div>
<!-- //sub_contents -->

<script type="text/javascript" src="/resources/user/js/libs/vue.min.js"></script>
<script type="text/javascript">
	$(function () {
		APP.Schedule.init();
	});
</script>

<%@include file="../inc/inc_gl_bottom.jsp"%>
