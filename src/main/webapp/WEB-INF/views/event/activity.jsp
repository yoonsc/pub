<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<p class="contents_title">스타시티의 공연/전시 활동을 소개합니다.</p>
<div class="sub_contents bg_white pb100">
	<div class="ly_container">

		<ul class="evt_list">			
			<li>
				<a href="#view" onclick="fn_view('1021' )">
					<div class="box_wrap">
						
						<div class="thumb_box">	
							<p class="img"><img src="/resources/user/img/contents/event/event_banner2.jpg" alt=""></p>
						</div>
						<dl>
							<dt>겨울맞이 슈퍼세일 이벤트</dt>
							<dd>기간 : 2018.10.01 ~ 2018.12.31</dd>
						</dl>
					</div>
				</a>
			</li>	
			<li>
				<a href="#view" onclick="fn_view('1021' )">
					<div class="box_wrap">		
																				
						<div class="thumb_box">	
							<p class="img"><img src="/resources/user/img/contents/event/event_banner2.jpg" alt=""></p>
						</div>
						<dl>
							<dt>겨울맞이 슈퍼세일 이벤트</dt>
							<dd>기간 : 2018.10.01 ~ 2018.12.31</dd>
						</dl>
					</div>
				</a>
			</li>
			<li>
				<a href="#view" onclick="fn_view('1021' )">
					<div class="box_wrap">	
																					
						<div class="thumb_box">	
							<p class="img"><img src="/resources/user/img/contents/event/event_banner2.jpg" alt=""></p>
						</div>
						<dl>
							<dt>겨울맞이 슈퍼세일 이벤트</dt>
							<dd>기간 : 2018.10.01 ~ 2018.12.31</dd>
						</dl>
					</div>
				</a>
			</li>
			<li>
				<a href="#view" onclick="fn_view('1021' )">
					<div class="box_wrap">	
																					
						<div class="thumb_box">	
							<p class="img"><img src="/resources/user/img/contents/event/event_banner2.jpg" alt=""></p>
						</div>
						<dl>
							<dt>겨울맞이 슈퍼세일 이벤트</dt>
							<dd>기간 : 2018.10.01 ~ 2018.12.31</dd>
						</dl>
					</div>
				</a>
			</li>
			<li>
				<a href="#view" onclick="fn_view('1021' )">
					<div class="box_wrap">
						
						<div class="thumb_box">	
							<p class="img"><img src="/resources/user/img/contents/event/event_banner2.jpg" alt=""></p>
						</div>
						<dl>
							<dt>겨울맞이 슈퍼세일 이벤트</dt>
							<dd>기간 : 2018.10.01 ~ 2018.12.31</dd>
						</dl>
					</div>
				</a>
			</li>	
			<li>
				<a href="#view" onclick="fn_view('1021' )">
					<div class="box_wrap">		
																				
						<div class="thumb_box">	
							<p class="img"><img src="/resources/user/img/contents/event/event_banner2.jpg" alt=""></p>
						</div>
						<dl>
							<dt>겨울맞이 슈퍼세일 이벤트</dt>
							<dd>기간 : 2018.10.01 ~ 2018.12.31</dd>
						</dl>
					</div>
				</a>
			</li>
			<li>
				<a href="#view" onclick="fn_view('1021' )">
					<div class="box_wrap">	
																					
						<div class="thumb_box">	
							<p class="img"><img src="/resources/user/img/contents/event/event_banner2.jpg" alt=""></p>
						</div>
						<dl>
							<dt>겨울맞이 슈퍼세일 이벤트</dt>
							<dd>기간 : 2018.10.01 ~ 2018.12.31</dd>
						</dl>
					</div>
				</a>
			</li>
			<li>
				<a href="#view" onclick="fn_view('1021' )">
					<div class="box_wrap">	
																					
						<div class="thumb_box">	
							<p class="img"><img src="/resources/user/img/contents/event/event_banner2.jpg" alt=""></p>
						</div>
						<dl>
							<dt>겨울맞이 슈퍼세일 이벤트</dt>
							<dd>기간 : 2018.10.01 ~ 2018.12.31</dd>
						</dl>
					</div>
				</a>
			</li>
		</ul>	

		<!--  더보기  -->
		<div class="btn_page_more_con">           
			<a href="javascript:;" class="more_item">
				<div class="page_more_wrap">
					<div class="data_loading" style="display: none;"><!-- 로딩시 풀어주세요 -->
						<span class="load_img">로딩중..</span>
					</div>
					<div class="page_more_box">
						<span class="more_txt">더보기</span>
					</div>
				</div>
			</a>
		</div>
		<!--  //더보기 -->
				
	</div>
	<!-- //ly_container  -->
</div>
<!-- //sub_contents -->


<%@include file="../inc/inc_gl_bottom.jsp"%>