<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="inc/inc_gl_top.jsp"%>

<section class="popup_wrap">
    <div class="popup_list" id="popList">
        <!-- 신규매장 슬라이드팝업 -->
        <div class="popup" id="pop0">
        	<ul class="popup_slide">
        		<li>
		            <p class="img">
		        		<a href="http://www.naver.com" target="_blank">
		        		   <img src="/resources/user/img/pop_sample.jpg" alt="" />
		                </a>
		            </p>
	        	</li>
	        	<li>
		            <p class="img">
		        		<a href="http://www.naver.com" target="_blank">
		        		   <img src="/resources/user/img/pop_sample.jpg" alt="" />
		                </a>
		            </p>
	        	</li>
	        	<li>
		            <p class="img">
		        		<a href="http://www.naver.com" target="_blank">
		        		   <img src="/resources/user/img/pop_sample.jpg" alt="" />
		                </a>
		            </p>
	        	</li>
        	</ul>
            <div class="today">
                <label><input type="checkbox" id="chkPop0" class="type-checkbox">오늘 이창을 열지 않음</label>
                <a href="javascript:;">닫기</a>
            </div>
        </div>
        <!-- 일반 레이어팝 -->
        <div class="popup nmpop" id="pop1">
            <p class="img">
        		<a href="http://www.naver.com" target="_blank">
        		   <img src="/resources/user/img/main_pop.jpg" alt="" />
                </a>
            </p>
            <div class="today">
                <label><input type="checkbox" id="chkPop1" class="type-checkbox">오늘 이창을 열지 않음</label>
                <a href="javascript:;">닫기</a>
            </div>
        </div>
    </div><!--//popup_list-->
</section><!--//popup_wrap-->



	<section class="main_sec visual_sec" id="visual">

		<div class="visual_contents">
			<div class="ly_container">
				<dl class="main_tit">
					<dt>STARCITY</dt>
					<dd>쇼핑과 문화 스타일을 창조하는 스타시티</dd>
				</dl>

				<div class="search_con">
					<ul class="cate_list">
						<li><a href="javascript:;"><i class="ico ico_nav_all"></i> 전체</a></li>
						<li><a href="javascript:;"><i class="ico ico_nav_shop"></i> 라이프스타일</a></li>
						<li><a href="javascript:;"><i class="ico ico_nav_food"></i> 푸드</a></li>
						<li><a href="javascript:;"><i class="ico ico_nav_fashion"></i> 패션</a></li>
					</ul>

					<div class="select_box_top">
						<span class="search_in">
							<div class="placeholder">
								<label for="text">찾으시는 매장을 입력하세요</label>
								<input type="text" name="text" id="text" value="" class="">
							</div>
							<a href="javascript:fn_search(document.listForm);" class="btn btn-search">검색</a>
						</span>
					</div>
					<!-- //select_box_top -->

					<div class="cate_data_con">
						<ul>
							<li>
								<div class="list_con">
									<div class="detail_box">
						    			<p class="shop_tit">매드포갈릭</p>
						    			<dl>
						    				<dt>대표전화</dt>
						    				<dd>02.2024.1412</dd>
						    			</dl>
						    			<dl>
						    				<dt>매장위치</dt>
						    				<dd>스타존 1층</dd>
						    			</dl>
						    			<div class="btns">
						    				<a href="#" class="f_icon favorit">즐겨찾기</a>
						    				<a href="#" class="f_icon linkgo">바로가기</a>
						    			</div>
						    		</div>
									<dl class="con_default">
										<dt>
											<span class="label box-blue">시티존</span>
											<img src="/resources/user/img/main/store_thumb.jpg" alt="">
										</dt>
										<dd>매드포갈릭</dd>
									</dl>
								</div>
							</li>
							<li>
								<div class="list_con">
									<div class="detail_box">
						    			<p class="shop_tit">매드포갈릭</p>
						    			<dl>
						    				<dt>대표전화</dt>
						    				<dd>02.2024.1412</dd>
						    			</dl>
						    			<dl>
						    				<dt>매장위치</dt>
						    				<dd>스타존 1층</dd>
						    			</dl>
						    			<div class="btns">
						    				<a href="#" class="f_icon favorit on">즐겨찾기</a>
						    				<a href="#" class="f_icon linkgo">바로가기</a>
						    			</div>
						    		</div>
									<dl class="con_default">
										<dt>
											<span class="label box-navy">스타존</span>
											<img src="/resources/user/img/main/store_thumb.jpg" alt="">
										</dt>
										<dd>매드포갈릭</dd>
									</dl>
								</div>
							</li>
							<li>
								<div class="list_con">
									<div class="detail_box">
						    			<p class="shop_tit">매드포갈릭</p>
						    			<dl>
						    				<dt>대표전화</dt>
						    				<dd>02.2024.1412</dd>
						    			</dl>
						    			<dl>
						    				<dt>매장위치</dt>
						    				<dd>스타존 1층</dd>
						    			</dl>
						    			<div class="btns">
						    				<a href="#" class="f_icon favorit on">즐겨찾기</a>
						    				<a href="#" class="f_icon linkgo">바로가기</a>
						    			</div>
						    		</div>
									<dl class="con_default">
										<dt>
											<span class="label box-navy">스타존</span>
											<img src="/resources/user/img/main/store_thumb.jpg" alt="">
										</dt>
										<dd>매드포갈릭</dd>
									</dl>
								</div>
							</li>
							<li>
								<div class="list_con">
									<div class="detail_box">
						    			<p class="shop_tit">매드포갈릭</p>
						    			<dl>
						    				<dt>대표전화</dt>
						    				<dd>02.2024.1412</dd>
						    			</dl>
						    			<dl>
						    				<dt>매장위치</dt>
						    				<dd>스타존 1층</dd>
						    			</dl>
						    			<div class="btns">
						    				<a href="#" class="f_icon favorit on">즐겨찾기</a>
						    				<a href="#" class="f_icon linkgo">바로가기</a>
						    			</div>
						    		</div>
									<dl class="con_default">
										<dt>
											<span class="label box-navy">스타존</span>
											<img src="/resources/user/img/main/store_thumb.jpg" alt="">
										</dt>
										<dd>매드포갈릭</dd>
									</dl>
								</div>
							</li>
							<li>
								<div class="list_con">
									<div class="detail_box">
						    			<p class="shop_tit">매드포갈릭</p>
						    			<dl>
						    				<dt>대표전화</dt>
						    				<dd>02.2024.1412</dd>
						    			</dl>
						    			<dl>
						    				<dt>매장위치</dt>
						    				<dd>스타존 1층</dd>
						    			</dl>
						    			<div class="btns">
						    				<a href="#" class="f_icon favorit">즐겨찾기</a>
						    				<a href="#" class="f_icon linkgo">바로가기</a>
						    			</div>
						    		</div>
									<dl class="con_default">
										<dt>
											<span class="label box-blue">시티존</span>
											<img src="/resources/user/img/main/store_thumb.jpg" alt="">
										</dt>
										<dd>매드포갈릭</dd>
									</dl>
								</div>
							</li>
							<li>
								<div class="list_con">
									<div class="detail_box">
						    			<p class="shop_tit">매드포갈릭</p>
						    			<dl>
						    				<dt>대표전화</dt>
						    				<dd>02.2024.1412</dd>
						    			</dl>
						    			<dl>
						    				<dt>매장위치</dt>
						    				<dd>스타존 1층</dd>
						    			</dl>
						    			<div class="btns">
						    				<a href="#" class="f_icon favorit">즐겨찾기</a>
						    				<a href="#" class="f_icon linkgo">바로가기</a>
						    			</div>
						    		</div>
									<dl class="con_default">
										<dt>
											<span class="label box-navy">스타존</span>
											<img src="/resources/user/img/main/store_thumb.jpg" alt="">
										</dt>
										<dd>매드포갈릭</dd>
									</dl>
								</div>
							</li>
							<li>
								<div class="list_con">
									<div class="detail_box">
						    			<p class="shop_tit">매드포갈릭</p>
						    			<dl>
						    				<dt>대표전화</dt>
						    				<dd>02.2024.1412</dd>
						    			</dl>
						    			<dl>
						    				<dt>매장위치</dt>
						    				<dd>스타존 1층</dd>
						    			</dl>
						    			<div class="btns">
						    				<a href="#" class="f_icon favorit">즐겨찾기</a>
						    				<a href="#" class="f_icon linkgo">바로가기</a>
						    			</div>
						    		</div>
									<dl class="con_default">
										<dt>
											<span class="label box-navy">스타존</span>
											<img src="/resources/user/img/main/store_thumb.jpg" alt="">
										</dt>
										<dd>매드포갈릭</dd>
									</dl>
								</div>
							</li>
						</ul>
					</div>
					<!-- //cate_data_con -->
				</div>
				<!-- //search_con -->
			</div>
			<!-- //ly_container -->
		</div>

		<div class="main_postor">
			<!-- //동영상일경우 -->
			<div class="player" role="application" aria-label="media player">
	            <div id="jquery_jplayer_1" class="jp-jplayer"></div>
	        </div>


	        <!-- //이미지일경우
	        <div class="postor_img" style="background-image:url(/resources/user/img/main/intro_img.jpg);"></div>
	         -->
    	</div>

	</section>
	<!-- //visual_sec -->


	<section class="main_sec event_sec" id="event">
		<div class="event_con">
			<ul class="event_slide">
				<li class="slides" style="background-image: url( /resources/user/img/main/event_banner_img.png );"><!-- 이벤트 배너이미지 통으로 삽입된다고 합니다. bg처리등 협의필요 -->
					<a href="/event/event"><img src="/resources/user/img/main/event_hit.png" alt=""></a>
				</li>
				<li class="slides" style="background-image: url( /resources/user/img/main/event_banner_img.png );">
					<a href="/event/event"><img src="/resources/user/img/main/event_hit.png" alt=""></a>
				</li>
				<li class="slides" style="background-image: url( /resources/user/img/main/event_banner_img.png );">
					<a href="/event/event"><img src="/resources/user/img/main/event_hit.png" alt=""></a>
				</li>
			</ul>
		</div>
		<div class="ly_container">
			<h3>EVENT</h3>
		</div>

	</section>
	<!-- //event_sec -->

	<section class="main_sec insta_sec" id="insta">
		<div class="ly_container ph100">
			<h3>instagram</h3>
			<ul class="insta_list">
				<li>
					<a href="http://www.naver.com" target="_blank">
						<div class="txt_con">
							<i class="ico ico_sns_insta"></i>
							<p>날씨 뚝! 오늘 너무 춥지 않아요? 저녁에 뜨끈~ 한 쌀국수 호로록~ 스타시티가 직접 맛보았다! #포베이 #스타시티포베이#포베이 추천메뉴 #쌀국수C</p>
						</div>
						<div class="thumb"><img src="/resources/user/img/main/insta_thumb.jpg" alt=""></div>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="txt_con">
							<i class="ico ico_sns_insta"></i>
							<p>날씨 뚝! 오늘 너무 춥지 않아요? 저녁에 뜨끈~ 한 쌀국수 호로록~ 스타시티가 직접 맛보았다! #포베이 #스타시티포베이#포베이 추천메뉴 #쌀국수C</p>
						</div>
						<div class="thumb"><img src="/resources/user/img/main/insta_thumb.jpg" alt=""></div>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="txt_con">
							<i class="ico ico_sns_insta"></i>
							<p>날씨 뚝! 오늘 너무 춥지 않아요? 저녁에 뜨끈~ 한 쌀국수 호로록~ 스타시티가 직접 맛보았다! #포베이 #스타시티포베이#포베이 추천메뉴 #쌀국수C</p>
						</div>
						<div class="thumb"><img src="/resources/user/img/main/insta_thumb.jpg" alt=""></div>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="txt_con">
							<i class="ico ico_sns_insta"></i>
							<p>날씨 뚝! 오늘 너무 춥지 않아요? 저녁에 뜨끈~ 한 쌀국수 호로록~ 스타시티가 직접 맛보았다! #포베이 #스타시티포베이#포베이 추천메뉴 #쌀국수C</p>
						</div>
						<div class="thumb"><img src="/resources/user/img/main/insta_thumb.jpg" alt=""></div>
					</a>
				</li>
			</ul>
		</div>
	</section>
	<section class="main_sec guide_sec" id="guide">
		<div class="ly_container">
			<div class="guide_left">
				<dl class="guide_tit">
					<dt>스타시티 <strong>guide</strong></dt>
					<dd>
						<p class="txt">New Life Style을 지향하는<br />
						스타시티의 라이프를<br />
						소개합니다.</p>
						<p class="btn_in">
							<span><a href="/guide/guide#" class="btn btn_round_nm">가이드 바로가기 <i class="bar"></i></a></span>
						</p>
						<ul class="link_list">
							<li><a href="/about/floor_info">층별안내</a></li>
							<li><a href="/about/convenience">편의시설</a></li>
							<li><a href="/about/parking">주차안내</a></li>
							<li><a href="/about/location">오시는길</a></li>
						</ul>
					</dd>
				</dl>
			</div>

			<div class="guide_con">
				<ul>
					<li class="lotte_box">
						<a href="#">
							<dl>
								<dt>롯데백화점 스타시티점</dt>
								<dd>최고의 서비스와 고품격 문화생활 공간</dd>
							</dl>
							<div class="thumb">
								<img src="/resources/user/img/main/shop_banner_00.jpg" alt="">
							</div>
						</a>
					</li>
					<li>
						<a href="#">
							<div class="thumb"><img src="/resources/user/img/main/shop_banner_01.jpg" alt=""></div>
							<dl>
								<dt>롯데시네마 건대입구점</dt>
								<dd>한국형 멀티플렉스를 영화관</dd>
							</dl>
						</a>
					</li>
					<li>
						<a href="#">
							<div class="thumb"><img src="/resources/user/img/main/shop_banner_02.jpg" alt=""></div>
							<dl>
								<dt>이마트 자양점</dt>
								<dd>최고급 수준의 프리미엄 대형마트</dd>
							</dl>
						</a>
					</li>
					<li>
	                    <a href="#">
							<div class="thumb"><img src="/resources/user/img/main/shop_banner_03.jpg" alt=""></div>
							<dl>
								<dt>반디앤루니스 스타시티점</dt>
								<dd>종합 서적과 문구 팬시 서점</dd>
							</dl>
						</a>
	                </li>
	                <li>
	                    <a href="#">
							<div class="thumb"><img src="/resources/user/img/main/shop_banner_04.jpg" alt=""></div>
							<dl>
								<dt>스타시티웨딩홀</dt>
								<dd>넓은 공간과 대규모 웨딩홀</dd>
							</dl>
						</a>
	                </li>

				</ul>
			</div>
		</div>
	</section>

<script type="text/javascript" src="/resources/user/js/libs/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="/resources/user/js/libs/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="/resources/user/js/libs/jquery/jquery.mousewheel.min.js"></script>
<link rel="stylesheet" href="/resources/user/css/jquery.mCustomScrollbar.css" />

<script type="text/javascript">
     (function (ns) {
        $(function(){
        	$('#searchText').keydown(function (key) {
			   if (key.keyCode == 13) {
					$(".btn-search").trigger("click");
			   }
		   });

           designBar();
           $( "ul.insta_list li:nth-child(3), ul.insta_list li:nth-child(4)").addClass("other");
           ns.main.init();
		   ns.placeholder.init();
		   ns.fl_listHover.init(".list_con");
		   ns.mainEvent.init(".event_slide");
		   ns.popup.init();

        });
     })(APP || {});

     function designBar(){
     	var $el = $(".cate_data_con");
     	$el.mCustomScrollbar('destroy');
     	$el.mCustomScrollbar({
             axis:"x",
             autoExpandScrollbar:true,
             mouseWheel:{scrollAmount:120,normalizeDelta:true},
             advanced:{autoExpandHorizontalScroll:true}
         });
     }
</script>


<%@include file="inc/inc_gl_bottom.jsp"%>
