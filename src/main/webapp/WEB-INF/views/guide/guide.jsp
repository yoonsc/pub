<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../inc/inc_gl_top.jsp"%>

<div class="sub_contents bg_white" style="visibility: hidden;" v-bind:style="'visibility: visible;'">
	<div class="ly_container">
		<h3 class="page_title_sub">
			<p class="en_tit">CITY OF DYNAMIC LIFE</p>
			스타시티
		</h3>
		<div class="map_view_sec2">
			<div class="map_info_txt">
				<!-- 존별 텍스트 삽입 -->
				<dl v-show="viewType == 1">
					<dt>시티존</dt>
					<dd>
						국내최대 할인마트 이마트와 대형 멀티플랙스 <br />
						롯데시네마가 입점해있는 시티존에서는 <br />
						쇼핑과 컬처테인먼트가 공존해 트렌디한 <br />
						도시의 감성을 만끽하실 수 있습니다.
					</dd>
				</dl>
				<dl v-show="viewType == 2">
					<dt>스타존</dt>
					<dd>
						젊은 감각의 new life style을 지향하는 스타존은 <br />
						최고의 서비스와 고품격 문화생활을 즐길 수 있는<br />
						롯데백화점 스타시티점이 입점해 있습니다.
					</dd>
				</dl>
				<dl v-show="viewType == 3">
					<dt>영존</dt>
					<dd>
						7호선 건대입구역 3번,4번 출구와 바로 연결되어있는 <br />
						영존에는 스타시티 웨딩홀을 비롯한 다양한 음심점 및 <br />
						카페 그리고 생활편의 매장이 입점해 있습니다.
					</dd>
				</dl>
				<dl v-show="viewType == 4">
					<dt>더 클래식 500</dt>
					<dd>
						도시에서 즐기는 느긋한 여유와 행복 <br />
						‘더 클래식 500 펜타즈 호텔이 위치해 있는 <br />
						더 클래식 500은 최고급 품격과 가치를 지닌 시니어타운 입니다.
					</dd>
				</dl>
			</div>
			<ul class="info_list">
				<li class="cf_num1" :class="{ on : viewType == 1 }">
					<a href="#" @mouseenter="viewOver(1)" @mouseleave="viewOut()" @click.prevent="viewClick(1)">
						<dl>
							<dt>시티존</dt>
							<dd><strong></strong></dd>
						</dl>
					</a>
				</li>
				<li class="cf_num2" :class="{ on : viewType == 2 }">
					<a href="#" @mouseenter="viewOver(2)" @mouseleave="viewOut()" @click.prevent="viewClick(2)">
						<dl>
							<dt>스타존</dt>
							<dd><strong></strong></dd>
						</dl>
					</a>
				</li>
				<li class="cf_num3" :class="{ on : viewType == 3 }">
					<a href="#" @mouseenter="viewOver(3)" @mouseleave="viewOut()" @click.prevent="viewClick(3)">
						<dl>
							<dt>영존</dt>
							<dd><strong></strong></dd>
						</dl>
					</a>
				</li>
				<li class="cf_num4" :class="{ on : viewType == 4 }">
					<a href="#" @mouseenter="viewOver(4)" @mouseleave="viewOut()" @click.prevent="viewClick(4)">
						<dl>
							<dt>더 클래식 500</dt>
							<dd><strong></strong></dd>
						</dl>
					</a>
				</li>
			</ul>
			<p><img :src="'/resources/user/img/contents/guide/guide_map_bg' + viewType + '.jpg'" alt=""></p>
		</div>
		<!-- //map_view_sec -->
	</div>
	<!-- //ly_container -->

	<div class="ly_container tab_map_con pt50" v-if="zoneType != 0" id="detail">
		<ul class="tab_boxtype tablist_4 mb50">
			<li :class="{ on : zoneType == 1 }"><a href="#1">시티존</a></li>
			<li :class="{ on : zoneType == 2 }"><a href="#2">스타존</a></li>
			<li :class="{ on : zoneType == 3 }"><a href="#3">영존</a></li>
			<li :class="{ on : zoneType == 4 }"><a href="#4">더 클래식 500</a></li>
		</ul>

		<div class="floor_map_con">
			<div class="floor_map_small">
				<img :src="imgSmall">
				<!-- 매장위치아이콘 -->
				<i class="pick_icon" :style="{ left: mapLocX + '%', top: mapLocY + '%' }"></i>
			</div>
			<div class="floor_map_big" ref="mapBig" @mousedown.prevent="mousedown" v-show="zoom">
				<img :src="imgBig">
				<!-- 매장위치아이콘 -->
				<i class="pick_icon" :style="{ left: mapLocX + '%', top: mapLocY + '%' }"></i>
			</div>
			<p class="floor_num">{{ floorType.codeName }}</p>

			<ul class="floor_navi">
				<li v-for="(obj, k) in floorTypeActObj" v-bind:key="k" :class="{ on : obj.code == floorType.code }">
					<a href="#" @click.prevent="changeFloorType( obj )">{{ obj.codeName }}</a>
				</li>
			</ul>

			<div class="floor_zoom">
				<a href="#" @click.prevent="zoomIn"><i class="ico ico-plus"></i></a>
				<a href="#" @click.prevent="zoomOut"><i class="ico ico-minus"></i></a>
			</div>
			<!-- //floor_zoom -->

			<div class="map_pop_box" v-show="storeData.seq">
				<a href="#" class="closex" @click.prevent="storeData = {}"><i class="lnr lnr-cross"></i></a>
				<dl>
					<dt>{{ storeData.name }}</dt>
					<dd>
						<ul>
							<li><i class="ico ico_store_phone"></i>대표전화	<strong>{{ storeData.tel }}</strong></li>
							<li><i class="ico ico_store_map"></i>매장위치 <strong>{{ storeData.location }}</strong></li>
						</ul>
					</dd>
				</dl>
				<div class="button_group">
					<a :href="'/store/view?seq=' + storeData.seq" class="btn btn_round_nm">상세보기</a>
				</div>
			</div>
			<!-- //map_pop_box -->
		</div>
		<!-- //floor_map_con -->

	</div>
	<!-- //tab_map_con -->

	<!-- 매장 상세 리스트 -->
	<div class="ly_container floor_info_wrap" v-if="zoneType != 0">
		<div class="select_box_top">
			<h3>{{ floorType.codeName }}</h3>
			<span class="select">
				<span class="select-wrapper">
                    <select v-model="categoryType">
						<option value="">전체 카테고리</option>
						<option value="1">라이프스타일</option>
						<option value="2">푸드</option>
						<option value="3">카페&디저트</option>
						<option value="4">패션</option>
						<option value="5">뷰티</option>
						<option value="6">잡화</option>
						<option value="7">서비스</option>
                    </select>
				</span>
			</span>
		</div>

		<div class="floor_info_con">
			<ul class="shop_ac_list">
				<my-list v-for="(obj, i) in storeList" :store-data="obj" :id="i" :toggle="myListActId == i" @toggle="detailToggle" @show="showInfo" v-if="i % 2 == 0 && ( categoryType == obj.categoryType || categoryType == '' )"></my-list>
			</ul>
			<ul class="shop_ac_list">
				<my-list v-for="(obj, i) in storeList" :store-data="obj" :id="i" :toggle="myListActId == i" @toggle="detailToggle" @show="showInfo" v-if="i % 2 == 1 && ( categoryType == obj.categoryType || categoryType == '' )"></my-list>
			</ul>
		</div>
	</div>

</div>

<script type="text/javascript" src="/resources/user/js/libs/vue.min.js"></script>
<script type="text/x-template" id="list-template">
	<!-- 아이콘리스트
	쇼핑 : <i class="ico ico-life"></i>
	음식점 : <i class="ico ico-food"></i>
	카페 : <i class="ico ico-cafe"></i>
	뷰티 : <i class="ico ico-beauty"></i>
	상점 : <i class="ico ico-fashion"></i>
	잡화 : <i class="ico ico-miscellaneous"></i>
	서비스 : <i class="ico ico-service"></i>
	-->
	<li>
		<dl class="ac-cont" :class="{ on : toggle }">
			<dt>
				<a href="#" title="내용보기" @click.prevent="detailToggle">
					<div class="cell_tit">
						<i class="ico" :class="[ '', 'ico-life', 'ico-food', 'ico-cafe', 'ico-beauty', 'ico-fashion', 'ico-miscellaneous', 'ico-service' ][ storeData.categoryType ]"></i>
						{{ storeData.name }}
					</div>
					<div class="cell_info">
						<button class="btn btn_round_blue" @click.prevent.stop="showInfo">위치보기</button>
						<span class="txt_phone">{{ storeData.tel }}</span>
						<i class="lnr lnr-chevron-down"></i>
					</div>
				</a>
			</dt>
			<dd v-show="toggle">
				<div class="detail_con">
					<div class="left_cell">
						<span class="logo_box"><img :src="'/resources/upload/' + storeData.folderName + '/' + storeData.fileStoredName" :alt="storeData.name"></span>
					</div>
					<div class="detail_txt">
						<dl>
							<dt><i class="ico ico_store_time"></i> 영업시간</dt>
							<dd>
								<ul class="detail_list">
									<li v-for="str in detailTime" v-if="str">
										<span class="time" v-if="checkTime( str ).bool">{{ checkTime( str ).txt }}</span>
										<span class="issue" v-if="checkIssue( str ).bool">{{ checkIssue( str ).txt }}</span>
									</li>
								</ul>
							</dd>
						</dl>
						<dl>
							<dt><i class="ico ico_store_map"></i> 매장위치</dt>
							<dd>
								<ul class="detail_list">
									<li>{{ storeData.location }}</li>
								</ul>
							</dd>
						</dl>
						<div class="button_group"><a class="btn btn_round_navy_bg" :href="'/store/view?seq=' + storeData.seq">상세보기</a></div>
					</div>
				</div>
			</dd>
		</dl>
	</li>
</script>
<script type="text/javascript">
    $(function(){
		APP.Guide.init();
    });
</script>

<%@include file="../inc/inc_gl_bottom.jsp"%>
