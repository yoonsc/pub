if (typeof console === "undefined" || console === null) {
    console = {
      log: function() {}
    };
  }

var APP = APP || {};

APP.register = function(ns_name){
    var parts = ns_name.split('.'),
    parent = APP;
    for(var i = 0; i < parts.length; i += 1){
        if(typeof parent[parts[i]] === "undefined"){
               parent[parts[i]] = {};
        }else {
            throw new Error( ( parts[ i - 1 ] || "MYAPP" ) + " 부모객체에 이미 " + parts[ i ] + " 객체가 존재합니다." );
        }

        parent = parent[parts[i]];
    }
    return parent;
};

//글자수제한
Array.prototype.valueIndex=function(pval)
{
     var idx = -1;
     if(this==null || this==undefined || pval==null || pval==undefined){
     }else{
      for(var i=0;i<this.length;i++){
       if(this[i]==pval){
        idx = i;
        break;
       }
      }
     }
     return idx
};


APP.isAlphaTween = true;

var browser = navigator.userAgent;
if(browser.toLowerCase().indexOf("msie 8")>0 || browser.toLowerCase().indexOf("msie 7")>0 ){
    APP.isAlphaTween = false;
}

(function(ns, $,undefined){

    ns.register('chkScreen');
    ns.chkScreen = function(){
        var _init = function() {
            chkScreen();
        };
        var chkScreen = function(){
            var winW = $(window).width();
            if(winW > 1280){
                $("body").removeClass("smallscreen").addClass("widescreen");
            }else{
                $("body").removeClass("widescreen").addClass("smallscreen");
            }
        };

        $(window).resize(chkScreen);

        return {
            init: _init
        }
    }();

    ns.register('gnb');
    ns.gnb = function(){
        var element, depth2Bg, depth2ConArr, depth1TotalNum, viewDepth2 = false, depth1Arr, depth2Arr=[], depth2ConArr=[], reSetTimer;

        var _init = function(){
            var i, max;
            var pathName = location.pathname;
            var activeOk = false;
            var depth3Idx, curUrl, remoteUrl, folderDir, resultUrl, family;

            element = $('.gnb_depth_1');
            depth2Bg = $('#navbg');
            depth1Arr = element.find('> li > a');
            depth1TotalNum = depth1Arr.length;
            depth2ConArr = $('.gnb_depth_2');
            depth2Arr = depth2ConArr.find('>li>a');
            //family = $(".family_site_box");
            //var $tabMenu = $(".tab_menu_box");
            depth1Arr.each(function(index, item){
                $(item).attr('name', 'depth1_'+index);
            });

            depth1Arr.on('mouseenter focusin mouseleave focusout', depth1Handler);
            for(i = 0, max = depth2ConArr.length; i<max; i++){
                depth2Arr[i] =  $(depth2ConArr[i]).find('a');
                depth2Arr[i].on('mouseenter focusin mouseleave focusout', depth2Handler);
            }

            depth2ConArr.on('mouseenter mouseleave', depth2Handler);
            depth2Bg.on('mouseenter mouseleave', function(e) {
                switch ( e.type ) {
                    case 'mouseenter':
                       stopTimer();
                        break;
                    case 'mouseleave':
                       startTimer();
                        break;
                    }
            });
        };

        var depth1Handler = function(e){
            var num = e.currentTarget.getAttribute('name').substr(7,1);
            switch ( e.type ) {
                case 'mouseenter':
                case 'focusin':
                    stopTimer();
                    depth1Over(num);
                    TweenLite.to($("#header"), 0.3, {css:{className:'+=open'}});
                    break;
                case 'focusout':
                case 'mouseleave':
                    startTimer();
                    break;
            }
        };
        var depth1Over = function(num){
            for(var i = 0; i < depth1TotalNum; i++){
                if(num == i){
                    $(depth1Arr[num]).addClass('on');

                }else{
                    $(depth1Arr[i]).removeClass('on');
                }
            }

            if(!viewDepth2){
                TweenLite.to($("#wrap"), 0.3, {css:{className:'+=bkon'}});
                TweenLite.to(depth2Bg, 0.3, {css:{className:'+=on'}});
                TweenLite.to(depth2Bg, 0.5, {css:{height:300}, ease:Cubic.easeOut});
                depth2ConArr.fadeIn();
            }
            viewDepth2 = true;
        };
        var depth2Handler = function(e){
            var name = e.currentTarget.getAttribute('name');
            if(name != null){
                var num = name.substr(7,1);
            }

            switch ( e.type ) {
                case 'mouseenter':
                case 'focusin':
                    TweenLite.to($(e.currentTarget), 0.2, {css:{className:'+=on'}});
                    stopTimer();
                    depth1Over(num);
                    break;
                case 'focusout':
                case 'mouseleave':
                    TweenLite.to($(e.currentTarget), 0.2, {css:{className:'-=on'}});
                    startTimer();
                    break;
            }
        };
        var startTimer = function(){
            clearTimeout( reSetTimer );
            reSetTimer = setTimeout (reSetMenu, 500 );
        };
        var stopTimer = function(){
            clearTimeout( reSetTimer );
        };
        var reSetMenu = function(){
            depth1Over(null);
            TweenLite.to($("#wrap"), 0.3, {css:{className:'-=bkon'}});
            TweenLite.to(depth2Bg, 0.3, {css:{className:'-=on'}});
            TweenLite.to(depth2Bg, 0.5, {css:{height:0}, ease:Cubic.easeOut});
            TweenLite.to($("#header"), 0.3, {css:{className:'-=open'}});
            depth2ConArr.fadeOut();
            viewDepth2 = false;
        };
         return {
            init: _init
        }
    }();


    // Numbering
    ns.register("Numbering");
    ns.Numbering = function(){
        var $gnb;
        var localNum1, localNum2, pathName, titleName;

        var _init = function () {
            $gnb = $(".gnb");
            pathName = location.pathname + location.search;

            //개발경로에 붙이면 경로 다시 맞춰야 함.
            fordurl = (window.location.pathname).replace("http://", "");
            var uarr = fordurl.split("/");
            var s = false;
            if (uarr.length > 1 && !s) {
                if (uarr[1].length > 0) s = true;
            }

            if (s) {
                uarr[1] = decodeURIComponent(uarr[1]);
                if (uarr[1] == "member") {
                    $(".layout_visual").hide();
                    $(".layout_lnb").hide();
                    $(".page_title").hide();
                    $(".aside_menu").addClass("fixed");
                    $("#container").addClass("bg_container");
                } else if(uarr[1] == "mypage" || uarr[1] == "store") {
                    $(".layout_lnb").hide();
                    $(".page_title").hide();
                    $(".aside_menu").css("top","0");
                } else if(uarr[1] == "guide") {
                    $(".page_title").hide();
                }
            }


            $gnb.find(".gnb_depth_1>li").each(function (i) {
                var href = $(this).find(">a").attr("href");

                if (location.href.match(href)) {
                    localNum1 = i;
                }

                if(pathName === "/" || pathName === "/index"){
                    $("body").attr("data-page", "main");
                    $("header").addClass("main");
                    $(".layout_visual").remove();
                    $(".layout_lnb").remove();
                    $(".page_title").remove();
                    $(".page_container").addClass("main_container");

                    $(".aside_menu").addClass("fixed");
                    $(".main_quick_nav").show();

                } else{
                    $("body").attr("data-page", "sub");
                }

                $(this).find('.gnb_depth_2>li').each(function (j) {
                    var href = $(this).find('>a').attr('href');

                    if (location.href.match(href)) {
                        localNum1 = i;
                        localNum2 = j;
                        titleName = $(this).find("a").text();
                    } else {
                        if($(this).find('>a').data("url") != undefined){
                            if(pathName.match($(this).find('>a').data("url"))){
                                localNum1 = i;
                                localNum2 = j;
                            }
                        }

                        var url = $(this).find('>a').data("url"),
                            urlArr = [];

                        if (url != undefined) {
                            if (url.indexOf(",") > -1) {
                                for (var k = 0; k < url.split(",").length; k++) {
                                    urlArr[k] = url.split(",")[k];
                                }
                            } else {
                                urlArr[0] = url;
                            }

                            for (var m = 0; m < urlArr.length; m++) {
                                if (pathName == urlArr[m]) {
                                    localNum1 = i;
                                    localNum2 = j;
                                }
                            }
                        }
                    }

                });
            });
            localNum1 = localNum1;
            localNum2 = localNum2;


            /*if ($("body").data("page") === "sub") {
                console.log("sub");
            } else {
            } */
            _settingEle(localNum1, localNum2);
            console.log(localNum1, localNum2);
        };

        var _settingEle = function (localNum1, localNum2) {
            var num1 = localNum1,
                num2 = localNum2;
            var $depth1Target = $gnb.find(".gnb_depth_1>li").eq(num1),
                $depth2Target = $depth1Target.find(".gnb_depth_2>li").eq(num2);
            var $titleH2 = $("#titleKor"),
                $titleH3 = $("#titleH3"),
                $titleEng = $("#titleH2");

            titleName = $depth2Target.find(">a").text();

            if (num1 != undefined) {
                $depth1Target.addClass("on");
                $titleEng.text($depth1Target.find(">a").attr("data-title"));

                $titleH2.text($depth1Target.find(">a").text());
                $(".layout_visual").attr("id", "subVis_"+num1);

                _makeLocation(num1, num2);
                if (titleName != undefined || floortype != undefined) $titleH3.text(titleName);

                var categoryType = $("#categoryType").prop("value");
                //console.log(floortype);
                if (categoryType != undefined) {
                    $(".layout_visual").attr("id", "subVis_5"+categoryType);
                }
                if (categoryType == 1) {
                    $titleEng.text("lifestyle");
                    $titleH2.text("라이프스타일");
                } else if (categoryType == 2) {
                    $titleEng.text("food");
                    $titleH2.text("푸드");
                } else if (categoryType == 3) {
                    $titleEng.text("cafe&dessert");
                    $titleH2.text("카페&디저트");
                } else if (categoryType == 4) {
                    $titleEng.text("fashion");
                    $titleH2.text("패션");
                } else if (categoryType == 5) {
                    $titleEng.text("beauty");
                    $titleH2.text("뷰티");
                } else if (categoryType == 6) {
                    $titleEng.text("general goods");
                    $titleH2.text("잡화");
                } else if (categoryType == 7) {
                    $titleEng.text("service");
                    $titleH2.text("서비스");
                }
            }

            //title 세팅
            var $title = $("title");
            var remoteH2 = $(".layout_top").find("h2").text();
            nowTit1 = $depth1Target.find(">a").text(),
            nowTit2 = $depth2Target.find(">a").text();

            if (nowTit2.length > 0 && nowTit1.length > 0) {
                $("title").text("스타시티 | " + nowTit1 + " | " + nowTit2);
                if (nowTit2 != remoteH2) {
                    $("title").text("스타시티 | " + nowTit1 + " | " + nowTit2 + " | " + remoteH2);
                }
            } else if (nowTit2.length == 0 && nowTit1.length > 0) {
                $("title").text("스타시티 | " + nowTit1);
            } else {
                $("title").text("스타시티");
            }
        }

        var _makeLocation = function (num1, num2) {
            var $lnb = $("#cloneLnb");
            var html = "", html2;

            $gnb.find(".gnb_depth_1>li").each(function () {
                html += "<li>";
                html += $(this).find(">a")[0].outerHTML;
                html += "</li>";
            });
            html2 = $gnb.find(".gnb_depth_1>li").eq(num1).find(".gnb_depth_2>li").clone();
            $lnb.html(html2);
            $lnb.find(">li").eq(num2).addClass("on");
            //etc
            /*var currentUrl = document.location.href.split("/"),
                folderDir = currentUrl[5];
                if (folderDir == 'etc') {
                    $locDepth1.remove();
                }
                console.log(num1, num2)*/
        };

       return {
            init: _init
        }
    }();

    ns.register('faqAcMenu');
    ns.faqAcMenu = function(ele){

        var element, btn, isOpen=false, listArr;
        var i, max;

        element=ele;
        listArr = $(element).find('>li>dl');

        btn = $(listArr).find('>dt>a');
        btn.on('click', openList);

        function listHandler(e) {
            switch ( e.type ) {
                case 'mouseenter':
                case 'focusin':
                    break;
                case 'focusout':
                case 'mouseleave':
                    break;
            }
        }

       function openList(e){
            var parent = $(e.currentTarget).parent().parent()
            var viewCon = parent.find('>dd')
            if(parent.hasClass('on')){
                parent.removeClass('on');
                viewCon.css('display', 'none')
            }else{
                //listArr.removeClass('on');
                $(listArr).removeClass('on')
                $(listArr).find('>dd').css('display', 'none');
                parent.addClass('on');
                viewCon.css('display', 'block');
                TweenLite.from(viewCon, 0.3, {css:{opacity:0}});
            }

        }
    };

    /* 20190308 * ***************/
    ns.register('familybox');
    ns.familybox = function(){
        var _init = function() {
            var $btn = $(".family_wrap").find(">a");
                $con = $(".family_wrap").find(".list_con");
            $btn.click(function(e){
                e.preventDefault();
                if($(this).hasClass("on")){
                    $(this).removeClass("on");
                    $con.fadeOut();
                    $con.focus();
                }else{
                    $(this).addClass("on");
                    $con.fadeIn();
                }
            });
        };
        return {
            init: _init
        }
    }();

    ns.register('navHover');
    ns.navHover = function(){
        var _init = function() {
           var $btn = $(".quick_info li a");
           $btn.on('mouseenter focusin mouseleave focusout', function(e) {
            switch ( e.type ) {
                case 'mouseenter':
                case 'focusin':
                    TweenLite.to($(this), 0.5, {width:"180px", backgroundColor:"#2253b8", ease:Cubic.easeOut});
                    TweenLite.to($(this).find("span"), 0.7, {left:30, opacity:1, ease:Cubic.easeOut});
                    break;
                case 'mouseleave':
                case 'focusout':
                    TweenLite.to($(this), 0.5, {width:"60px", backgroundColor:"#343434", ease:Cubic.easeOut});
                    TweenLite.to($(this).find("span"), 0.7, {left:0, opacity:0, ease:Cubic.easeOut});
                    break;
                }
            });
        };
        return {
            init: _init
        }
    }();
    /* 20190308 * ***************/

    ns.register('fl_listHover');
    ns.fl_listHover = function(){
        var _init = function(ele) {
           var $ele = $(ele);
           $ele.find('.detail_box').css("opacity", 0);
           $ele.on('mouseenter mouseleave', function(e) {
            switch ( e.type ) {
                case 'mouseenter':
                    TweenLite.to($(this).find('.detail_box'), 0.5, {css:{autoAlpha:1},ease:Cubic.easeOut});
                    break;
                case 'mouseleave':
                    TweenLite.to($(this).find('.detail_box'), 0.5, {css:{autoAlpha:0},ease:Cubic.easeOut});
                    break;
                }
            });
        };
        return {
            init: _init
        }
    }();

    ns.register('shopgnb');
    ns.shopgnb = function(){
        var element, setDepth1, depth2Bg, depth1Arr, depth1OverImgArr, depth1OverBg, depth2Arr, depth2ConArr, reSetTimer, overDepth1, overDepth2, overDepth2Con;

        var _init = function(){
            var i, max;
            element = $('.floor_nav_menu');
            depth2Bg = $('.sub_menu_bg');

            depth1Arr = element.find('> li > a');
            //depth1OverImgArr=[];
            depth1OverBg=[];
            depth2Arr = [];
            depth2ConArr = element.find('> li > ul');

            depth1Arr.each(function(index, item){
                $(item).attr('name', 'depth1_'+index);
                //depth1OverImgArr[index] = $(item).parent().contents('img:last');
                depth1OverBg[index] = $(item).parent();

            });
            depth1Arr.on('mouseenter focusin mouseleave focusout', depth1Handler);
            element.on('mouseover mouseout', depth2BgHandler);

            for(i = 0, max = depth2ConArr.length; i<max; i++){
                depth2Arr[i] =  $(depth2ConArr[i]).find('> li > a');
                depth2Arr[i].on('mouseenter focusin mouseleave focusout', depth2Handler);
                depth2Arr[i].each(function(index, item){
                    $(item).attr('name', 'depth2_'+i+'_'+index);
               });
            }

        };
        var depth2BgHandler = function(e){
            switch ( e.type ) {
                case 'mouseover':
                    stopTimer();
                    break;
                case 'mouseout':
                    startTimer();
                    break;
            }
        };
        var depth1Handler = function(e){
            var num = e.currentTarget.getAttribute('name').substr(7,1);

            switch ( e.type ) {
                case 'mouseenter':
                case 'focusin':
                    stopTimer();
                    depth1Over(num);
                    break;
                case 'focusout':
                case 'mouseleave':
                    startTimer();
                    break;
            }
        };

        var depth1Over = function(num){
            if(overDepth1 == depth1Arr[num])return;
            if(overDepth1){
                TweenLite.to(overDepth1, 0.2, {css:{className:'-=on'}});
                $(overDepth2Con).css('display','none')
                //TweenLite.to(depth1OverImgArr, 0.2, {css:{opacity:0}});
                TweenLite.to(depth1OverBg, 0.2,  {css:{className:'-=on'}});
            }else{
                TweenLite.to(depth1Arr, 0.2, {css:{className:'-=on'}});
                //TweenLite.to(depth1OverImgArr, 0.2, {css:{opacity:0}});
                TweenLite.to(depth1OverBg, 0.2,  {css:{className:'-=on'}});
            }

            if(num == null || num == 0){
                TweenLite.to(depth1Arr[num], 0.2, {css:{className:'+=on'}});
                TweenLite.to(depth2Bg, 0.2, {css:{height:0}});
                TweenLite.to($(".floor_nav > ul"), 0.3, {css:{paddingBottom:0}});
                overDepth1 = null;
            }else{
                TweenLite.to(depth1Arr[num], 0.2, {css:{className:'+=on'}});
                //TweenLite.to(depth1OverImgArr[num], 0.2, {css:{opacity:1}});
                TweenLite.to(depth1OverBg[num], 0.2,  {css:{className:'+=on'}});
                TweenLite.to($(".floor_nav > ul"), 0.3, {css:{paddingBottom:60}});
                TweenLite.to(depth2Bg, 0.3, {css:{height:60}});

                $(depth2ConArr[num]).css({'display':'block', opacity:0})
                TweenLite.to($(depth2ConArr[num]), 0.2, {css:{opacity:1}, ease:Cubic.easeIn});
                overDepth1 = depth1Arr[num];
                overDepth2Con = depth2ConArr[num];
                overDepth2 = depth2Arr[num][0];
            }
        };

        var setDepth1Over = function(){
            if(setDepth1 != null)
            {
                TweenLite.to(depth1Arr[setDepth1], 0.2, {css:{className:'+=on'}});
                //TweenLite.to(depth1OverImgArr[setDepth1], 0.2, {css:{opacity:1}});
                TweenLite.to(depth1OverBg[setDepth1], 0.2,  {css:{className:'+=on'}});
                overDepth1 = null;
            }
        }
        var depth2Handler = function(e){
            var num = e.currentTarget.getAttribute('name').substr(7,1);
            var num2 = e.currentTarget.getAttribute('name').slice(9);

            switch ( e.type ) {
                case 'mouseenter':
                case 'focusin':
                    //TweenLite.to(e.currentTarget, 0.3, {className:'+=on'});
                    stopTimer();
                    // depth2Over(num, num2);
                    break;
                case 'focusout':
                case 'mouseleave':
                    //TweenLite.to(e.currentTarget, 0.3, {className:'-=on'});
                    startTimer();
                    break;
                }
        };

        var depth2Over = function(num, num2){
            // if(overDepth2 == depth2Arr[num][num2])return;
            overDepth2 = depth2Arr[num][num2];
            depth2Arr.forEach( function( arr ){
                TweenLite.set(arr, {css:{className:'-=current'}});
            } );
            if( overDepth2 ) TweenLite.set(overDepth2, {css:{className:'+=current'}});
        };


        var startTimer = function(){
            clearTimeout( reSetTimer );
            // reSetTimer = setTimeout (reSetMenu, 1000 );
            reSetTimer = setTimeout (depth1Over, 500, setDepth1 );
        };

        var stopTimer = function(){
            clearTimeout( reSetTimer );
        };

        var reSetMenu = function(){
            depth1Over(null);
            setDepth1Over();
            overDepth2 = null;
        };

        var _setMenu = function(num, num2){
            setDepth1 = num;
            depth1Over(num);
            depth2Over(num, num2 || 0);
        }
        return{
            init:_init,
            setMenu:_setMenu

        };
    }();





    ns.register('mainvis');
    ns.mainvis = function(){
        var _init = function(){
            _mainVisual();
        };

        var _mainVisual = function(){
            var $visualcon, $copy1, $copy2, $copy3;
            var $mainVisual = $(".visual_slide");
            var wHeight = $(window).height();

            $(window).on("load resize", function(){
                $mainVisual.find(".img").css("height", wHeight + "px");
            });


            $mainVisual.on("init reInit afterChange", function (event, slick, currentSlide, nextSlide) {
                var $paging = $('.visual .page_info');

                var i = (currentSlide ? currentSlide : 0) + 1;

                $paging.html('<strong>'+i+'</strong>' + '/' + slick.slideCount);


            });


            $mainVisual.on("init", function(slick){
                _bgMotion(0);
            });


            $mainVisual.slick({
                fade:true,
                slidesToShow:1,
                slidesToScroll:1,
                arrows:true,
                dots:false,
                infinite: true,
                autoplay:true,
                autoplaySpeed:6000,
                draggable:false,
                speed:1000,
                zIndex:10,
                pauseOnHover:false
            });



            $mainVisual.on("beforeChange", function(event, slick, currentSlide, nextSlide){
                _bgMotion(nextSlide);
            });


            function _bgMotion(num){
                var $nextLi = $mainVisual.find(".slick-slide").eq(num);

                TweenMax.set($nextLi.find(".img"), {autoAlpha:.5, scale:1.3, skewX:0.001});
                TweenMax.set($nextLi.find(".txt span"), {autoAlpha:0, y:40});
                TweenMax.set($nextLi.find(".txt .copy"), {autoAlpha:0, y:40});
                TweenMax.set($nextLi.find(".txt .copys"), {autoAlpha:0, y:40});

                TweenMax.to($nextLi.find(".img"), 2, {autoAlpha:1, ease:Cubic.easeOut});
                TweenMax.to($nextLi.find(".img"), 7, {scale:1.01, ease:Linear.easeNone});
                TweenMax.to($nextLi.find(".txt span"), 2, {delay:.8, autoAlpha:1, y:0, ease:Power2.easeOut});
                TweenMax.to($nextLi.find(".txt .copy"), 2, {delay:1.4, autoAlpha:1, y:0, ease:Power2.easeOut});
                TweenMax.to($nextLi.find(".txt .copys"), 2, {delay:2.2, autoAlpha:1, y:0, ease:Power2.easeOut});
            };
        };

        return {
            init: _init
        }
    }();


    ns.register('main');
    ns.main = function(){
        var _init = function(){
           playerVideo();
           $("#jquery_jplayer_1").jPlayer("mute");
           cateFn();
           parallaxMain();
        };

        var parallaxMain = function() {

            $(".main_nav").find("li").each(function(index){
                $(this).attr("data-index",index);
            }).click(function() {
                var index = $(this).attr("data-index");
                var href = $(this).find("a").attr("href");
                var startTop = $(href).offset().top;
                if (href == "#visual") {
                    startTop =  startTop - 89;
                } else if (href == "#guide"){
                   startTop =  startTop + 99;
                }
                TweenMax.to($("html, body"), 0.8, {scrollTop:startTop, ease:Cubic.easeInOut});
                return false;
            });

            $(".main_sec").each(function(index){
                $(this).attr("data-index",index);
            })

            function changeClass(index) {
                $(".main_nav li[data-index!="+index+"]").removeClass("on");
                $(".main_nav li[data-index="+index+"]").addClass("on");
            }
            function scroll() {
                var size = $(".main_nav li").size()-1;
                var top = $(window).scrollTop();
                for(var index = size; index >= 0; index--) {
                    if(top >= $(".main_sec[data-index="+index+"]").offset().top-200) {
                        changeClass(index);
                        return false;
                    }
                }

            }
            $(window).scroll(function(){
                scroll();
            });
        };


        var parallax = function() {

            $quickNav = $(".main_nav").find("li");
            $(window).on({
                "load":function(){
                    $(".main_sec").each(function(i){
                        var obj = {}
                        obj.top = parseInt($(this).offset().top);
                        obj.height = parseInt($(this).height());
                        sOffset[i] = obj;
                    });
                    sOffset.push({top:950, height:500});
                },
                "scroll":function(){
                    var posS, posE, prev;
                    posS = $(window).scrollTop();
                    posE = $(window).scrollTop()+ $(window).height()- ($(window).height()/2)-200;
                    for(var i = 0; i < sOffset.length-1; i++)
                    {
                        var offsetTop = sOffset[i].top;
                        var offsetEnd = sOffset[i].top + sOffset[i].height - ( sOffset[i].height/2);
                        if((posS<= offsetTop && offsetTop <= posE) || (posS<= offsetEnd && offsetEnd <= posE)){
                            $quickNav.find("a").removeClass("on");
                            $quickNav.eq(i).find("a").addClass("on");
                            if(tNum == i)return;
                            motionControll(i);
                            return;
                        }
                        if(tNum != i){
                        }
                    }
                }
            });

            $(".main_nav").find("a").click(function(e){
                e.preventDefault();
                quickNav($(this).parent().index());
            });
            motionSet();

            function motionSet(){
                var tl0 = new TimelineMax({paused:true});
                motionArr[0] = tl0;

                var tl1 = new TimelineMax({paused:true});
                tl1.from($(".event_con").eq(0), 0.5, {opacity:0, ease:Cubic.easeOut});
                motionArr[1] = tl1;

                var tl2 = new TimelineMax({paused:true});
                tl2.from($(".insta_list li").eq(0), 0.7, {opacity:0, y:80, ease:Cubic.easeOut})
                    .from($(".insta_list li").eq(1), 0.7, {opacity:0, y:80, ease:Cubic.easeOut}, '-=0.4')
                    .from($(".insta_list li").eq(2), 0.7, {opacity:0, y:80, ease:Cubic.easeOut}, '-=0.4')
                    .from($(".insta_list li").eq(3), 0.7, {opacity:0, y:80, ease:Cubic.easeOut}, '-=0.4');

                motionArr[2] = tl2;
                var tl3 = new TimelineMax({paused:true});
                tl3.from($(".guide_left"), 0.7, {opacity:0,  x:80, ease:Cubic.easeOut})
                    .from($(".guide_con"), 0.7, {opacity:0, x:-80, ease:Cubic.easeOut}, 0.3);

                motionArr[3] = tl3;
            }
            function motionControll(num){
                if(tNum == num)return;
                tNum=num;
                for (var i = 0; i < motionArr.length; i++) {
                    if( i == num){
                        motionArr[num].play();
                        motionIsPlay[num] = true;
                    }else{
                        if(motionIsPlay[i] == true){
                                motionArr[i].reverse();
                                motionIsPlay[i] = false;
                        }
                    }
                };
            }


            function quickNav(index){
                var quickA = $quickNav.eq(index).children("a");
                var href = quickA.attr("href");
                var startTop = $(href).offset().top;
                if (href == "#visual") {
                    startTop =  startTop - 89;
                } else if (href == "#guide"){
                    startTop =  startTop + 89;
                }

                TweenMax.to($("html, body"), 0.8, {scrollTop:startTop, ease:Cubic.easeInOut});
            }
        }

        var cateFn = function() {
            var TxtTit = $(".main_tit"),
                searchTop = $(".search_con"),
                cateBtn = $(".cate_list>li>a"),
                dataCon = $(".cate_data_con"),
                playerBg = $(".main_postor");


            cateBtn.each(function(index, item){
                $(item).attr('data-cate', index);
            });

            cateBtn.on("click", function(){
               cateBtn.removeClass("on");
               $(this).addClass("on");
               playerBg.addClass("bgon")
               dataCon.show();
               TweenMax.to(TxtTit, 0.8, {autoAlpha:0, y:-200, ease:Power2.easeOut});
               TweenMax.to(searchTop, 0.8, {y:-350, ease:Power2.easeOut});
            });
        }
        var playerVideo = function() {
            var vWidth = $(".visual_sec").width();
            var vHeight = Math.round(vWidth * 0.5825);
            $("#jquery_jplayer_1").jPlayer({
                ready: function () {
                    $(this).jPlayer("setMedia", {
                        m4v: "/resources/user/img/main/movie.mp4"
                    }).jPlayer("play");
                    $(".jp-jplayer").find("video").css({"width":"auto", "height":"auto"});
                    $(".jp-jplayer").css({"width":"auto", "height":"auto"});
                },
               ended: function() {
                    $("#jquery_jplayer_1").jPlayer("play");
                },
                swfPath: "http://jplayer.org/latest/dist/jplayer",
                wmode: "window",
                supplied: "m4v",
                smoothPlayBar: true
            });

            $('#btnMoviePlay').on('click', function(){
                 $("#jquery_jplayer_1").jPlayer("play");
            });

            $('#btnMovieStop').on('click', function(){
                 $("#jquery_jplayer_1").jPlayer("pause");
            });

            $(window).on("load resize", function(){
                var vWidth = $(window).width();
                var vHeight = Math.round(vWidth * 0.5650);
                //if(vHeight > $(window).height()){
                    //$(".main_video").css("height", $(window).height() + "px");
                    $(".jp-jplayer").find("video").css({"width":"auto", "height":"auto"});
                    $(".jp-jplayer").css({"width":"auto", "height":"auto"});
                    //$(".visual_sec").css({"height":vHeight});

            });

        };

        return {
            init: _init
        }
    }();

    /* popup */
    ns.register('popup');
    ns.popup = function(){
        var $popup, $popupList;

        var _init = function(){
            var $popup = $(".popup");
            var $popupList = $("#popList");

            $popup.css("display", "block");

            $popup.each(function(i){
                if(getCookie("pop"+i) != "checked"){
                    $("#pop"+i).css("display", "block");

                }else{
                    $("#pop"+i).css("display", "none");
                }
            });

            $popup.find(".today a").on("click", function(e){
                e.preventDefault();
                var layerId = $(this).parent().parent().attr("id");
                closeCookie(layerId);
            });
            popSlide();
        };

        var popSlide = function(){
            var $popup_slide = $(".popup_slide");

            $popup_slide.slick({
                fade: false,
                infinite: false,
                autoplay: false,
                arrows: false,
                draggable: true,
                autoplaySpeed: 4000,
                speed: 1500,
                dots:true,
                    customPaging: function(slider, i) {
                        var inum = i+1;
                        return '<a href="javascript:;">' +'0'+ inum + '</a>';
                    },
                pauseOnHover: false,
                pauseOnFocus: false,
                focusOnSelect: false
            });
        }


        var getCookie = function(name){
            var nameOfCookie = name + "=";
            var x = 0;
            while ( x <= document.cookie.length )
            {
                var y = (x+nameOfCookie.length);
                if ( document.cookie.substring( x, y ) == nameOfCookie ) {
                    if ( (endOfCookie=document.cookie.indexOf( ";", y )) == -1 )
                        endOfCookie = document.cookie.length;
                    return unescape( document.cookie.substring( y, endOfCookie ) );
                }
                x = document.cookie.indexOf( " ", x ) + 1;
                if ( x == 0 )
                    break;
            }
            return "";
        };

        var setCookie = function(name, value, expiredays){
            var todayDate = new Date();
            todayDate.setDate( todayDate.getDate() + expiredays );
            document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";"
        };

        var closeCookie = function(layerId){
            var i = layerId.split("pop")[1];

            if ( document.getElementById("chkPop"+i).checked ) {
                setCookie( layerId, "checked" ,1 );
                console.log(layerId);
            }
            $("#"+layerId).css("display", "none");
        };
        return {
            init:_init
        }
    }();
    /* faq */
    ns.register('faq');
    ns.faq = function(){
        var $accordion, $accordionLink;

        var _init = function(){
            $accordionList = $(".faq_list");
            $accordionLink = $(".faq_list dl dt a");
            var $prev;

            $accordionLink.click(function(e){
                e.preventDefault();
                if($(this).hasClass("on")){
                    _accordionList.close($(this));
                } else {
                    if($prev){
                        if($prev.hasClass("on")){
                            _accordionList.close($prev);
                        }
                    }
                    _accordionList.open($(this));
                }


                $prev = $(this);

            });

            $accordionList.find("li:nth-child(1) a").trigger("click");
        };

        var _accordionList = {
            open:function(ele){
                var $ele = ele;

                $ele.parent().parent().find("dd").slideDown(400);
                $ele.addClass("on");
            },
            close:function(ele){
                var $ele = ele;

                $ele.parent().parent().find("dd").slideUp(400);
                $ele.removeClass("on");
            }
        };

        return {
            init:_init
        }
    }();

    /* tab */
    ns.register('tab');
    ns.tab = function(){
        var _init = function(ele){
            var $this = ele;
            var prev;

            $this.find("a").click(function(e){
                e.preventDefault();
                var $this = $(this);

                if(prev){
                    prev.parent().removeClass("on");
                    TweenMax.set($(prev.attr("href")), {"opacity":0, "display":"none"});

                }

                $(this).parent().addClass("on");
                TweenMax.set($($this.attr("href")), {"display":"block"});
                TweenMax.to($($this.attr("href")), 0.8, {"opacity":1});

                prev = $this;
            });

            $this.find("li:nth-child(1) a").trigger("click");
        };

        return {
            init: _init
        }
    }();


    ns.register('tabMenufloor');
    ns.tabMenufloor = function(ele, targetEle){
        var element, targetElement, tNum=0, tabContainer, tabBtn, tabBtnCon, contentsArr, totalTabNum, infoTab;
        infoTab = $(".info_list>li");
        element = ele;
        targetElement = targetEle;
        tabBtn = element.find(">li:not(.deactive)");
        tabBtnCon = element.find(">li");
        totalTabNum = tabBtn.length;
        contentsArr= targetElement.find(">div");
        tabBtn.each(function(index, item){
            $(item).attr('name', 'tab_'+index);

        });
        tabBtn.on('mouseenter focusin mouseleave focusout click', tabHandler);

        function tabHandler(e){
            var num = e.currentTarget.getAttribute('name').substr(4,1);
            if(tNum == num)return;

            switch ( e.type ) {
                case 'mouseenter':
                case 'focusin':
                   // tabOver(num);
                    break;
                case 'focusout':
                case 'mouseleave':
                  //  tabOver(tNum);
                    break;
               case 'click':
                    tabSelect(num);
                    break;
            }
        };

        function tabOver(num){
            for(var i = 0; i<totalTabNum; i++){
                if(i== num){
                    TweenLite.to($(tabBtn[num]), 0, {className:'+=on'});
                    TweenLite.to($(tabBtnCon[num]), 0, {className:'+=on'});
                    TweenLite.to($(infoTab[num]), 0, {className:'+=on'});
                }else{
                    TweenLite.to($(tabBtn[i]), 0, {className:'-=on'});
                    TweenLite.to($(tabBtnCon[i]), 0, {className:'-=on'});
                    TweenLite.to($(infoTab[i]), 0, {className:'-=on'});
                }
            }

        };

        function tabSelect(num){
            tabOver(num)
            tNum = num;
            $(contentsArr[num]).siblings().removeClass('current');
            $(contentsArr[num]).addClass('current')
        }
        tabOver(tNum);
        tabSelect(tNum)
    };


    /* innerTab */
    ns.register('innerTab');
    ns.innerTab = function(){
        var _init = function(ele, active){
            var $this = ele;
            var prev;
            var active;

            $this.find("a").click(function(e){
                e.preventDefault();
                var $this = $(this);

                if(prev){
                    prev.parent().removeClass("on");
                    TweenMax.set($(prev.attr("href")), {"display":"none"});

                }

                $(this).parent().addClass("on");
                TweenMax.set($($this.attr("href")), {"display":"block"});

                prev = $this;
            });

            $this.find("li:nth-child("+ active +") a").trigger("click");
        };

        return {
            init: _init
        }
    }();

     /* ajaxTab */
    ns.register('ajaxTab');
    ns.ajaxTab = function(){
        var _init = function(ele){
            var $this = ele;
            var prev;

            $this.find("a").click(function(e){
                e.preventDefault();
                var $this = $(this);
                var loadURL = $(this).attr("data-load-url");

                if(prev){
                    prev.parent().removeClass("on");
                }

                $(this).parent().addClass("on");
                _loadData(loadURL);

                prev = $this;

            });

            $this.find("li:nth-child(1) a").trigger("click");

        };

        var _loadData = function(loadURL){
            var $viewCon = $(".type_tab_con");

             TweenMax.set($viewCon, {"display":"block"});

            $.ajax({
                url:loadURL+".asp",
                method: 'GET',
                cache: false,
                success:function(data){
                    $viewCon.empty().append(data);
                },
                error:function(xhr, status, error){
                }
            });
        };

        return {
            init: _init
        }
    }();




    /* paramTab */
    ns.register('paramTab');
    ns.paramTab = function(){

        var _init = function(ele){
            var $this = ele;

            function getUrlParameter(sParam) {
                var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            };
            var sParam = getUrlParameter('tab');
            $(".type_tab_con").not('#'+sParam).css("display", "none");
            $("#"+sParam).fadeIn();
            $this.find("."+sParam).addClass('on');
        };
        return {
            init: _init
        }
    }();



    /* selecttype */
    ns.register('selectbox');
    ns.selectbox = function(){
        var _init = function(ele) {
            var $ele = $(ele);
            var $btn = $ele.find('>a');
            var $list = $ele.find('>div');
            $btn.click(function(e){
                e.preventDefault();
                if($(this).hasClass("open")){
                    $(this).removeClass("open");
                    $list.hide();
                }else{
                    $(this).addClass("open");
                    $list.show();
                }
            });
        };
        return {
            init: _init
        }
    }();

    /* placeholder */
    ns.register('placeholder');
    ns.placeholder = function(){
        var _init = function() {
          var $placeholder = $("body").find('.placeholder'),
            $inTxt = $placeholder.find('input, textarea');
            $inTxt.each(function () {
                if ($(this).val() != '') {
                    $(this).addClass('focus');
                };
            });

            $inTxt.on('focusin', function () {
                $(this).addClass('focus');
            });

            $inTxt.on('focusout', function () {
                if ($(this).val() === '') {
                    $(this).removeClass('focus');
                } else {
                    $(this).addClass('focus');
                }
            });

            $placeholder.on('click', function () {
                $(this).find('input').focus();
            });
        };
        return {
            init: _init
        }
    }();

    ns.register('selectie');
    ns.selectie = function(){
        var _init = function() {
            if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                $("body").find(".select-wrapper").addClass("iestyle");
            }
        };
        return {
            init: _init
        }
    }();

    /* datePicker */
    ns.register('datePicker');
    ns.datePicker = function(){
        var _init = function(inputId) {
            $(inputId).datepicker({
                showOn: "both", // focus / button / both
                dayNamesMin: ["일", "월", "화", "수", "목", "금", "토"],
                monthNames:[ "1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월" ],
                monthNamesShort: ["1월","2월","3월","4월","5월","6월", "7월","8월","9월","10월","11월","12월"],
                buttonText: "<i class='fa fa-calendar'></i>",
                dateFormat: "yy-mm-dd",
                changeMonth: true,
                changeYear: true,
                yearRange: "1960:+nn",
                isRTL: false,
                yearSuffix: '',
                firstDay: 0

                //yearRange: '0+50'
            });
        };
        return {
            init: _init
        }
    }();

    // fileStyle function
    ns.register('fileStyle');
    ns.fileStyle = function(){
        var _init = function() {
            var fileTarget = $('.filebox .upload-hidden');
                fileTarget.on('change', function () { // 값이 변경되면
                    if (window.FileReader) { // modern browser
                        var filename = $(this)[0].files[0].name;
                    } else { // old IE var
                        filename = $(this).val().split('/').pop().split('\\').pop(); // 파일명만 추출
                    }
                    // 추출한 파일명 삽입
                    $(this).siblings('.upload-name').val(filename);
                });
        };
        return {
            init: _init
        }
    }();

    ns.register('linkHover');
    ns.linkHover = function(){
        var _init = function(ele) {
           var $ele = $(ele);
           $ele.on('mouseenter focusin mouseleave focusout', function(e) {
            switch ( e.type ) {
                case 'mouseenter':
                case 'focusin':
                    $(this).addClass("hover");
                    break;
                case 'focusout':
                case 'mouseleave':
                    $(this).removeClass("hover");
                    break;
                }
            });
        };
        return {
            init: _init
        }
    }();


    ns.register('selectUpBox');
    ns.selectUpBox = function(ele){

        var element, btn, isOpen=false, listCon, listHeight, closeTimer, listWrap;
        var i, max;

        element=ele;
        listWrap = $(element).find('div');
        listCon = listWrap.find('ul');
        btn = $(element).find('>a');
        $(element).find('>a').on('mouseenter focusin mouseleave focusout', listHandler);
        $(element).find('>a').on('click', openList);
        listHeight = $(listCon).outerHeight(true)
        listWrap.css('height', 0)
        listCon.find('li>a').on('mouseenter focusin mouseleave focusout', listHandler);
        listCon.css('display', 'none');
        listCon.css('top', listHeight);
        function listHandler(e) {
            switch ( e.type ) {
                //case 'mouseenter':
                case 'focusin':
                    stopTimer();
                    break;
                case 'focusout':
                //case 'mouseleave':
                    startTimer();
                    break;
            }
        }
        function startTimer(){
            clearTimeout( closeTimer );
            closeTimer = setTimeout (close, 700 );
        };
        function stopTimer(){
            clearTimeout( closeTimer );
        };
        function close(){
            isOpen=true;
            openList()
        };

        function openList(){
            listHeight = $(listCon).outerHeight(true);
            if(isOpen){
                isOpen = false;
                listWrap.css('height', 0);
                listCon.css('display', 'none');
                $(btn).removeClass('on');
                TweenLite.to(listCon, 0, {css:{top:listHeight}});
            }else{
                isOpen = true;
                listWrap.css('height', listHeight);
                listCon.css('display', 'block');
                $(btn).addClass('on');
                TweenLite.to(listCon, 0.3, {css:{top:0}});
            }
        }
    };

     // 글자수제한
    ns.register('charLimit');
    ns.charLimit = function(){
        var _init = function(ele, charNum) {
            var $ele = $(ele);
            _calTxt(ele, charNum); //최초로딩시 추가
            // 키이벤트
            $ele.on("keyup", function(e){
                _calTxt(ele, charNum);
            });

            // 마우스 우클릭 복붙
            $ele[0].onpaste = function(){
                setTimeout(function(){
                    _calTxt(ele, charNum);
                }, 100);
            }
        };

        var _calTxt = function(ele, charNum){
            var $ele = $(ele);
            var $remainTxt = $ele.parents().find('.remain_txt');
            var val = $ele.val();
            var limitCnt = charNum;
            var length = val.length;
            var newVal;

            if(length > limitCnt){
                alert(limitCnt + "자이상 입력하실 수 없습니다.");
                newVal = val.substr(0, limitCnt);
                $ele.val(newVal);
                $remainTxt.html(limitCnt);
            }else{
                $remainTxt.html(length);
            }
        };

        return {
            init: _init
        }
    }();

    ns.register('ui.tabMenu');
    ns.ui.tabMenu = function(ele, targetEle){
        var element, targetElement, tNum=0, tabContainer, tabBtn, tabBtnCon, contentsArr, totalTabNum;
        element = ele;
        targetElement = targetEle;
        tabBtn = element.find(">li:not(.deactive)");
        tabBtnCon = element.find(">li");
        totalTabNum = tabBtn.length;
        contentsArr= targetElement.find(">div");
        tabBtn.each(function(index, item){
            $(item).attr('name', 'tab_'+index);

        });
        tabBtn.on('mouseenter focusin mouseleave focusout click', tabHandler);

        function tabHandler(e){
            var num = e.currentTarget.getAttribute('name').substr(4,1);
            if(tNum == num)return;

            switch ( e.type ) {
                case 'mouseenter':
                case 'focusin':
                   // tabOver(num);
                    break;
                case 'focusout':
                case 'mouseleave':
                  //  tabOver(tNum);
                    break;
               case 'click':
                    tabSelect(num);
                    break;
            }
        };

        function tabOver(num){
            for(var i = 0; i<totalTabNum; i++){
                if(i== num){
                    TweenLite.to($(tabBtn[num]), 0, {className:'+=on'});
                    TweenLite.to($(tabBtnCon[num]), 0, {className:'+=on'});
                }else{
                    TweenLite.to($(tabBtn[i]), 0, {className:'-=on'});
                    TweenLite.to($(tabBtnCon[i]), 0, {className:'-=on'});
                }
            }

        };

        function tabSelect(num){
            tabOver(num)
            tNum = num;
            $(contentsArr[num]).siblings().removeClass('current');
            $(contentsArr[num]).addClass('current')
        }
        tabOver(tNum);
        tabSelect(tNum)
    };

     // 메인 이벤트
    ns.register('mainEvent');
    ns.mainEvent = function(){
        var _init = function(selector) {
            var $ele = $(selector);
            $ele.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: true,
                infinite : true,
                focusOnSelect: true
            });

            $('#slidePrev').on('click', function(){
                $ele.slick('slickPrev');
            });

            $('#slideNext').on('click', function(){
                $ele.slick('slickNext');
            });
        };
        return {
            init: _init
        }
    }();


    // 상점 갤러리
    ns.register('shopGallery');
    ns.shopGallery = function(){
        var _init = function(selector) {
            var $ele = $(selector);
            $ele.slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                dots: false,
                arrows: false,
                centerMode: false,
                focusOnSelect: true
            });

            $('#slidePrev').on('click', function(){
                $ele.slick('slickPrev');
            });

            $('#slideNext').on('click', function(){
                $ele.slick('slickNext');
            });
        };
        return {
            init: _init
        }
    }();

        ns.register('gasMapcontrol');
        ns.gasMapcontrol = function(){
            var leady = true, tNum = -1, totalNum, menuCon, pointBg, tl, contents, maps, overTimer;
            var _init = function() {

                menuCon = $('.highway_label').find('li>a');
                pointBg = $('.highway_list').find('.hw_point');
                menuCon.on('mouseenter focusin mouseleave focusout click', menuHandler);
                maps = $('.highway_map>div').find(".mapline");
                contents = $('.highway_map>div');
                totalNum = pointBg.length;


                /*$('.index0').css({"opacity":"1", "display":"block"});
                tl = new TimelineMax({});
                tl.from(index0.find('.visual_img'), .7, { scale:1.1, autoAlpha:0, ease:Cubic.easeOut})
                  .from(index0.find('.txt_1'), 1, {x:-150, autoAlpha:0, ease:Cubic.easeOut, delay:0.5})
                  .from(index0.find('.txt_2'), 0.6, { y:50, autoAlpha:0, ease:Cubic.easeOut}, "-=0.2")
                  .from(index0.find('.btngo'), 0.6, { y:50, autoAlpha:0, ease:Cubic.easeOut}, "-=0.2")
                tl.restart();*/
            };

            var menuHandler = function(e){
                e.preventDefault();
                var num = $(e.currentTarget).parent().index();
                switch ( e.type ) {
                    case 'mouseenter':
                    case 'focusin':
                        clearTimeout( overTimer );
                        menuOver(num);
                        break;
                    case 'focusout':
                    case 'mouseleave':
                        overTimer = setTimeout(function(){menuOver(tNum);}, 500) ;
                        break;
                    case 'click':
                        menuSelect(num);
                        break;
                }
            };

            var menuSelect = function(num){
                if(!leady)return;
                //if(tNum == num)return;
                movePage(num);
                menuOver(num);
                changepointBg(num);
            };

            var changepointBg = function(num){
                for(var i=0; i < totalNum; i++){
                    if(num == i){
                       //TweenMax.to($(pointBg[num]), 0.3, {opacity:1, ease:Cubic.easeOut});
                       $(pointBg[num]).addClass("on");
                    }else{
                       //TweenMax.to($(pointBg[i]), 0.3, {opacity:.5, ease:Cubic.easeIn});
                       $(pointBg[num]).removeClass("on");
                    }
                };
            };

            var menuOver = function(num){
                for(var i=0; i < totalNum; i++){
                    if(num == i){
                        $(menuCon[num]).addClass('on')
                    }else{
                         $(menuCon[i]).removeClass('on')
                    }
                };
            };

            var movePage = function(num, arrow){
                if(!leady) return;
                //if(num == tNum)return;
                leady = false;

                //TweenLite.to($(visualImg[tNum]), 0.5, {css:{scale:.95}, ease:Cubic.easeOut});
                TweenLite.to( $(maps[tNum]), 0.3, {opacity:0, onComplete:function(){

                    $(maps[tNum]).css('display', 'none');
                    $(maps[num]).css('display', 'block');
                    TweenLite.set( $(maps[num]), {opacity:0});
                    TweenLite.to( $(contents).find(".hw_point>a"), 0.5, {css:{scale:1}, ease:Cubic.easeOut});
                    //TweenLite.set( $(visualImg[num]), {scale:.95});
                    TweenLite.to( $(maps[num]), 0.3, {opacity:1, onComplete:function(){
                        //TweenLite.to( $(visualImg[num]), 0.5, {css:{scale:1}, ease:Cubic.easeOut}); //
                        TweenLite.to( $(contents[num]).find(".hw_point>a"), 0.5, {css:{scale:2}, ease:Cubic.easeOut});
                        leady = true;
                        tNum = num;
                        menuOver(tNum);
                    }});
                }});
                menuOver(num);
                changepointBg(num);
            };

            var pointOver = function(){
                /*function pointHandler(e){
                    var num = e.currentTarget.getAttribute('name').substr(8,1);
                    switch ( e.type ) {
                        case 'mouseenter':
                            status = true;
                            $(this).find(">a").addClass("on");
                            $(this).find("ul").show();
                            break;
                        case 'mouseleave':
                            status = false;
                             $(this).find(">a").removeClass("on");
                            $(this).find("ul").hide();
                            break;
                    }
                }; */
            };
            return {
                init: _init
            }
        }();

        ns.register('mapPointer');
        ns.mapPointer = function(){
            var element, elementDiv, depth2ConArr, depth1TotalNum, viewDepth2 = false, depth1Arr, depth2Arr=[], depth2ConArr=[], reSetTimer, depth1Over, num;
            var _init = function(selector) {
                var $ele = $(selector);
                var i, max;
                element = $('.highway_map');
                elementDiv = element.children();
                depth1Arr = elementDiv.find('>.hw_point > a');
                depth1TotalNum = depth1Arr.length;
                depth2ConArr = elementDiv.find('ul');
                depth2Arr = depth2ConArr.find('>li>a');

                $ele.next().css("visibility", "hidden");
                $ele.css("cursor","default");

                depth1Arr.each(function(index, item){
                    $(item).attr('name', 'pointer'+index);
                });

               depth1Arr.on('mouseenter focusin mouseleave focusout', depth1Handler);
               for(i = 0, max = depth2ConArr.length; i<max; i++){
                   depth2Arr[i] =  $(depth2ConArr[i]).find('li>a');
                   depth2Arr[i].on('mouseenter focusin mouseleave focusout', depth2Handler);
                   depth2Arr[i].each(function(index, item){
                        $(item).attr('name', 'depth2_'+i+'_'+index);
                   });
               }
               depth2ConArr.on('mouseenter mouseleave', depth2Handler);
            };

            var depth1Handler = function(e){
               var num = e.currentTarget.getAttribute('name').substr(7,2);
               switch ( e.type ) {
                   case 'mouseenter':
                   case 'focusin':
                       stopTimer();
                       depth1Over(num);
                       break;
                   case 'focusout':
                   case 'mouseleave':
                       startTimer();
                       break;
               }
           };

           var depth1Over = function(num){
               for(var i = 0; i < depth1TotalNum; i++){
                   if(num == i){
                       $(depth1Arr[num]).addClass('on');
                       $(depth1Arr[num]).parent().css("z-index","91");
                       $(depth1Arr[num]).next("ul").fadeIn();
                       $(depth1Arr[num]).next("ul").css("z-index","99");
                   }else{
                       $(depth1Arr[i]).removeClass('on');
                       $(depth1Arr[i]).parent().css("z-index","20");
                       $(depth1Arr[i]).next("ul").fadeOut();
                       $(depth1Arr[i]).next("ul").css("z-index","21");
                   }
               }
               if(!viewDepth2){
                   //depth2ConArr.fadeIn();
               }
               viewDepth2 = true;
           };

           var depth2Handler = function(e){
               //var num = e.currentTarget.getAttribute('name').substr(7,2);
               switch ( e.type ) {
                   case 'mouseenter':
                   case 'focusin':
                       //TweenLite.to($(e.currentTarget), 0.2, {css:{className:'+=on'}});
                       stopTimer();
                       //depth1Over(num);
                       $(this).parent().parent().fadeIn();
                       break;
                   case 'focusout':
                   case 'mouseleave':
                       //TweenLite.to($(e.currentTarget), 0.2, {css:{className:'-=on'}});
                       startTimer();
                       break;
               }
           };

           var startTimer = function(){
               clearTimeout( reSetTimer );
               reSetTimer = setTimeout (reSetMenu, 500 );
           };
           var stopTimer = function(){
               clearTimeout( reSetTimer );
           };
           var reSetMenu = function(){
               depth1Over(null);
               //depth2ConArr.fadeOut();
               viewDepth2 = false;
           };
            return {
                init: _init
            }
        }();


}(APP || {}, jQuery));

function GoTop() {
    TweenMax.to($('body, html'), 0.5, {scrollTop:0, ease:"Cubic.easeOut"});
}


var LayerPopups = {
    find: function (id) {
        if (typeof (id) === 'string')
            return $((id.match(/^#/)) ? id : '#' + id);
        else
            return $(id).parents('.layerPopup');
    },
    open: function (id, closeOthers) {
        var $id = this.find(id);
        if ($id.length == 0)
            return;
        //GoTop(); //맨위로
        //$("html, body").stop().animate({scrollTop:(thisPos.top)-600}, 400);


        this.showScreen();
        if (closeOthers) {
            $('.layerPopup').each(function () {
                if (this.id == $id[0].id)
                    $(this).show();
                else
                    $(this).hide();
            });
        }
        else {
            $id.show();
        }
    },
    close: function (id) {
        this.find(id).hide();
        this.hideScreen();
    },
    closeAll: function () {
        $('.layerPopup').hide();
        this.hideScreen();
    },
    opened: function () {
        var opened = false;
        $('.layerPopup').each(function () {
            if ($(this).css('display') != 'none')
                opened = true;
        });
        return opened;
    },
    showScreen: function () {
        $('#layerScreen').show();
    },
    hideScreen: function () {
        if (!this.opened())
            $('#layerScreen').hide();
    },
    closeId: function (id) {
        var $id = this.find(id);
        $id.hide();
        this.hideScreen();
        return;
    },
    openAlert: function (id, closeOthers, target, txt) {
        var $id = this.find(id);
        if ($id.length == 0)
            return;

        //GoTop(); //맨위로
        this.showScreen();
        if (closeOthers) {
            $('.layerPopup').each(function () {
                if (this.id == $id[0].id){
                    $(this).attr("data-target", target);
                    $(this).find(".layer_txt").html(txt);
                    $(this).show();
                }else{
                    $(this).hide();
                }
            });
        }
        else {
            $id.show();
        }
    },
    closeAlert: function (id) {
        var $id = this.find(id);

        $id.hide();
        this.hideScreen();
        if($id.attr("data-target") != "") {
            $($id.attr("data-target")).focus();
        }
        return;
    }
};


if( typeof String.prototype.startsWith !== 'function' )
{
    /**
     * 문자열이 해당 suffix 로 시작하는지 체크
     *
     * @param suffix
     * @returns {boolean}
     */
    String.prototype.startsWith = function (suffix) {

        return this.indexOf(suffix) == 0;
    };
}


function onlyNumber(obj) {
    $(obj).keyup(function () {
        $(this).val($(this).val().replace(/[^0-9]/g, ""));
    });
}

function emailLayer() {
    LayerPopups.open('emailReject', true);
}
