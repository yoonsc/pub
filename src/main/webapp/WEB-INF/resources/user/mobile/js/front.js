var LayerPopups = {
    find: function (id) {
        if (typeof (id) === 'string')
            return $((id.match(/^#/)) ? id : '#' + id);
        else
            return $(id).parents('.layerPopup');
    },
    open: function (id, closeOthers) {
        var $id = this.find(id);
        if ($id.length == 0)
            return;
        //GoTop(); //맨위로
        //$("html, body").stop().animate({scrollTop:(thisPos.top)-600}, 400);
        $("body").css("overflow","hidden");

        this.showScreen();
        if (closeOthers) {
            $('.layerPopup').each(function () {
                if (this.id == $id[0].id)
                    $(this).show();
                else
                    $(this).hide();
            });
        }
        else {
            $id.show();
        }
    },
    close: function (id) {
        this.find(id).hide();
        this.hideScreen();
        $("body").css("overflow","inherit");
    },
    closeAll: function () {
        $('.layerPopup').hide();
        this.hideScreen();
    },
    opened: function () {
        var opened = false;
        $('.layerPopup').each(function () {
            if ($(this).css('display') != 'none')
                opened = true;
        });
        return opened;
    },
    showScreen: function () {
        $('#layerScreen').show();
    },
    hideScreen: function () {
        if (!this.opened())
            $('#layerScreen').hide();
    },
    closeId: function (id) {
        var $id = this.find(id);
        $id.hide();
        this.hideScreen();
        return;
    },
    openAlert: function (id, closeOthers, target, txt) {
        var $id = this.find(id);
        if ($id.length == 0)
            return;

        //GoTop(); //맨위로
        this.showScreen();
        if (closeOthers) {
            $('.layerPopup').each(function () {
                if (this.id == $id[0].id){
                    $(this).attr("data-target", target);
                    $(this).find(".layer_txt").html(txt);
                    $(this).show();
                }else{
                    $(this).hide();
                }
            });
        }
        else {
            $id.show();
        }
    },
    closeAlert: function (id) {
        var $id = this.find(id);

        $id.hide();
        this.hideScreen();
        if($id.attr("data-target") != "") {
            $($id.attr("data-target")).focus();
        }
        return;
    }
};



/**
 * @method	: locationId
 * @param	: $depth0Li - 뎁스0 li 제이쿼리
 * @param	: $depth1Ul - 뎁스1 ul 제이쿼리
 * @return	: array - [ id0, id1 ]
 */
var locationId = ( function() {
	var r = null;
	var locationId;

	locationId = function( $depth0Li, $depth1Ul ) {
		// $depth0Li 뎁스0 li 제이쿼리
		// $depth1Ul 뎁스1 ul 제이쿼리

		var pathName = location.pathname; // 현재 페이지 주소
		var id0 = -1, id1 = -1;

		// index 페이지 일때
		if( pathName === "/" || pathName === "/index" ) {
			return [ id0, id1 ];
		}

		$depth0Li.each( function( i ) {
			// li>a 구조
			var $a = $( this ).find( '>a' );
			var aPathname = $a.prop( 'pathname' );
			var href = $a.attr( 'href' );
			var dataLink = $a.attr( 'data-link' ) || '';

			if( href == '#' || href == '/' ) return true;
			if( pathName.indexOf( aPathname ) > -1 || dataLink.indexOf( pathName ) > -1 ) {
				id0 = i;
				return false;
			}
		} );

		$depth1Ul.each( function( i ) {
			// ul>li>a 구조
			$( this ).find( '>li' ).each( function( j ) {
				var $a = $( this ).find( '>a' );
				var aPathname = $a.prop( 'pathname' );
				var href = $a.attr( 'href' );
				var dataLink = $a.attr( 'data-link' ) || '';
				// gnb에 노출되지 않는 페이지 처리
				// attribute data-link 값으로 처리 ( , 로 연결 )
				// <a href="url0" data-link="url1,url2"></a>

				if( href == '#' || href == '/' ) return true;
				if( pathName.indexOf( aPathname ) > -1 || dataLink.indexOf( pathName ) > -1 ) {
					id0 = i;
					id1 = j;
					return false;
				}
			} );
		} );

		var $title = $(".sub_title");
		var $h2 = $title.find("h2");
		var $h3 = $(".layout_body").find(".body_tit");
		var txtD1 = $depth0Li.eq( id0 ).find("> a").text(); //1depth text
		var txtD2 = $depth0Li.eq( id0 ).find("li").eq( id1 ).find("> a").text(); //2depth text

		//$depth0Li.removeClass( 'on' );
		if(id0 != -1){
			//$depth0Li.eq( id0 ).addClass( 'on' );
			if(id1 != -1){
				//$h2.text(txtD1);
				//$h3.text(txtD2);
				//$('.visimg').append('<img src="/resources/sweetm/mobile/img/sub_visual_'+ id0 +'.jpg" alt="">');
			}else {
				//$h2.text(txtD1);
			}
		}

		//예외처리
	       var url = (window.location.pathname).replace("http://", "");
	       var uarr = url.split("/");
	       uarr[2] = decodeURIComponent(uarr[2]);
	       if(uarr[2] == "expected_view") {
	          localNum1 = 1;
	          localNum2 = 1;
	          //$(".body_tit").remove();
	          //$(".sales_view_top").addClass("tpmg");	          
	          
	       } else if(uarr[2] == "ing_view") {
	          localNum1 = 1;
	          localNum2 = 2;
	          //$(".body_tit").remove();
	          //$(".sales_view_top").addClass("tpmg");

	       } else if(uarr[2] == "end_view") {
	          localNum1 = 1;
	          localNum2 = 3;
	          //$(".body_tit").remove();
	          //$(".sales_view_top").addClass("tpmg");
	       }


		//2depth 타이틀 예외처리
		/*if(id0 == 1){
			$h2.text(txtD2);
		}*/

		return [ id0, id1 ];
	};

	return function( $depth0Li, $depth1Ul ) {
		if( r === null ) r = locationId( $depth0Li, $depth1Ul ); // 결과값이 없을때만 계산
		return r.slice();
	}
} )();
//---------------------------------------------------------------------------------------//

var actId = locationId( $( '#gnb .depth0>li.remote' ), $( '#gnb .depth0 li.remote ul.depth1' ) );
//console.log( actId );
//console.log( actId[ 0 ] );
//console.log( actId[ 1 ] );

//---------------------------------------------------------------------------------------//


//gnb
var gnb = (function($){
	var $gnb, $gnbMask;
	var tl;
	var $searchBtn, $searchCon, $conMask;

	var init = function(){
		$gnb = $("#gnb");
		$gnbMask = $("div.gnb_mask");
		$searchBtn = $("#search_open");
        $searchCon = $(".header_search");
        $conMask = $("#container");

		gnbViewSetting();

		$("#gnb_open").on("click", function(){
			$gnbMask.fadeIn(200);
			$gnb.css({"display":"block"});
			tl.play();
		});      
        	
    	$searchBtn.click(function(e){
            e.preventDefault();
            if($(this).hasClass("on")){
                $(this).removeClass("on");
                $searchCon.fadeOut(200);   
                $conMask.removeClass("mask");                             
            }else{
                $(this).addClass("on");
                $searchCon.fadeIn(200);
                $("#searchText").focus();
                $conMask.addClass("mask");
            }
        });

        $(".search_close a").click(function(){
        	$searchBtn.removeClass("on");
        	$searchCon.fadeOut();
        	$conMask.removeClass("mask");
        });



		$("#gnb_close, div.gnb_mask").click(function(){
			tl.eventCallback("onReverseComplete", reverseComplete);
			tl.reverse();
		});

		_openDepth2(actId[0], actId[1]); //2depth있을때
	};

	var _openDepth2 = function(num1, num2){
		var $el, $depth1, $depth2, $menu;

		$el = $("#gnb");
		$depth1 = $el.find("ul.depth0");
		$depth2 = $el.find("ul.depth1");
		$menu = $depth1.find(">li>a");
		$menu2 = $depth2.find(">li>a");

		//로딩시 1depth메뉴 활성화
		if (num1 != -1) {
			$menu.eq(num1).next($depth2).show();
			$menu.eq(num1).next($depth2).find("li").eq(num2).children().addClass("on");
		}
		console.log(num1, num2)

		$menu.on("click", function(e){
			if(!$(this).next("ul.depth1").is(".no_child")){
				e.preventDefault();
				var $parent = $(this).parent();
				if($parent.is(".on")){
					$parent.removeClass("on").find(".depth1").slideUp(200);
				}else{
					$depth1.find(">li").removeClass("on");
					$depth2.slideUp(200);
					$parent.addClass("on").find(".depth1").slideDown(200);
				}
			}
		});
	};

	function gnbViewSetting(){
		tl = new TimelineMax({paused:true});
		tl.to($gnb, 0.3, {css:{left:0}, ease:Cubic.easeOut});
	}

	function reverseComplete(){
		$gnb.css("display", "none");
		$gnbMask.fadeOut(200);
	}

	return {
		init: init
	}
})(jQuery);


var selectUpBox = (function($){
	var init = function(ele){
        var element, btn, isOpen=false, listCon, listHeight, closeTimer, listWrap;
        var i, max;
        element=ele;
        listWrap = $(element).find('>div');
        listCon = listWrap.find('ul');
        btn = $(element).find('>a');
        $(element).find('>a').on('mouseenter focusin mouseleave focusout', listHandler);
        $(element).find('>a').on('click', openList);
        listHeight = $(listCon).outerHeight(true)
        listWrap.css('height', 0)
        listCon.find('li>a').on('mouseenter focusin mouseleave focusout', listHandler);
        listCon.css('display', 'none');
        listCon.css('top', listHeight);
        function listHandler(e) {
            switch ( e.type ) {
                //case 'mouseenter':
                case 'focusin':
                    stopTimer();
                    break;
                case 'focusout':
                //case 'mouseleave':
                    startTimer();
                    break;
            }
        }
        function startTimer(){
            clearTimeout( closeTimer );
            closeTimer = setTimeout (close, 700 );
        };
        function stopTimer(){
            clearTimeout( closeTimer );
        };
        function close(){
            isOpen=true;
            openList()
        };
        function openList(){
            listHeight = $(listCon).outerHeight(true);
            if(isOpen){
                isOpen = false;
                console.log(false);
                listWrap.css('height', 0);
                listCon.css('display', 'none');
                $(btn).removeClass('on');
                TweenLite.to(listCon, 0, {css:{top:listHeight}});
            }else{
                isOpen = true;
                console.log(true);
                listWrap.css('height', listHeight);
                listCon.css('display', 'block');
                $(btn).addClass('on');
                TweenLite.to(listCon, 0.3, {css:{top:0}});
            }
        }
	};
	return {
		init: init
	}
})(jQuery);



var ui_tabMenu = function(ele, targetEle){
    var element, targetElement, tNum=0, tabContainer, tabBtn, tabBtnCon, contentsArr, totalTabNum;
    element = ele;
    targetElement = targetEle;
    tabBtn = element.find(">li:not(.deactive)");
    tabBtnCon = element.find(">li");
    totalTabNum = tabBtn.length;
    contentsArr= targetElement.find(">div");
    tabBtn.each(function(index, item){
        $(item).attr('name', 'tab_'+index);

    });
    tabBtn.on('mouseenter focusin mouseleave focusout click', tabHandler);

    function tabHandler(e){
        var num = e.currentTarget.getAttribute('name').substr(4,1);
        if(tNum == num)return;

        switch ( e.type ) {
            case 'mouseenter':
            case 'focusin':
               // tabOver(num);
                break;
            case 'focusout':
            case 'mouseleave':
              //  tabOver(tNum);
                break;
           case 'click':
                tabSelect(num);
                break;
        }
    };

    function tabOver(num){
        for(var i = 0; i<totalTabNum; i++){
            if(i== num){
                TweenLite.to($(tabBtn[num]), 0, {className:'+=on'});
                TweenLite.to($(tabBtnCon[num]), 0, {className:'+=on'});
            }else{
                TweenLite.to($(tabBtn[i]), 0, {className:'-=on'});
                TweenLite.to($(tabBtnCon[i]), 0, {className:'-=on'});
            }
        }

    };

    function tabSelect(num){
        tabOver(num)
        tNum = num;
        $(contentsArr[num]).siblings().removeClass('current');
        $(contentsArr[num]).addClass('current')
    }
    tabOver(tNum);
    tabSelect(tNum)
};


var ui_faqAcMenu = function(ele){
    var element, btn, isOpen=false, listArr;
    var i, max;
    element=ele;
    listArr = $(element).find('>li>dl');
    btn = $(listArr).find('>dt>a');
    btn.on('click', openList);

    function listHandler(e) {
        switch ( e.type ) {
            case 'mouseenter':
            case 'focusin':
                break;
            case 'focusout':
            case 'mouseleave':
                break;
        }
    }
   function openList(e){
        var parent = $(e.currentTarget).parent().parent()
        var viewCon = parent.find('>dd')
        if(parent.hasClass('on')){
            parent.removeClass('on');
            viewCon.css('display', 'none')
        }else{
            //listArr.removeClass('on');
            $(listArr).removeClass('on')
            $(listArr).find('>dd').css('display', 'none');
            parent.addClass('on');
            viewCon.css('display', 'block');
            TweenLite.from(viewCon, 0.3, {css:{opacity:0}});
        }

    }
};


        
var salesViewSlide = (function($){
    var _init = function() {  
      $('#slider-for').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          fade: false,
          draggable:true,
          adaptiveHeight: true,
          asNavFor: '#slider-nav'          
        });

        $('#slider-nav').slick({
          slidesToShow: 3,
          slidesToScroll: 1,             
          infinite: true,
          variableWidth: true,
          asNavFor: '#slider-for',
          dots: false,
          arrows: false,              
          focusOnSelect: true
        });
        $('#slidePrev').on('click', function(){
            $('.slider-view').slick('slickPrev');
        });
        $('#slideNext').on('click', function(){
            $('.slider-view').slick('slickNext');
        });
        
        var DataLength = $(".sales_view_body").find(".view_img").length;
        if (DataLength < 2) {
        	$(".sales_view_body").find(".fn_nav").hide();
        }

    };
    return {
        init: _init
    }
})(jQuery);


var charLimit = (function($){
    var _init = function(ele, charNum) {
        var $ele = $(ele);
        _calTxt(ele, charNum); //최초로딩시 추가
        // 키이벤트
        $ele.on("keyup", function(e){
            _calTxt(ele, charNum);
        });

        // 마우스 우클릭 복붙
        $ele[0].onpaste = function(){
            setTimeout(function(){
                _calTxt(ele, charNum);
            }, 100);
        }
    };

    var _calTxt = function(ele, charNum){
        var $ele = $(ele);
        var $remainTxt = $ele.parents().find('.remain_txt');
        var val = $ele.val();
        var limitCnt = charNum;
        var length = val.length;
        var newVal;

        if(length > limitCnt){
            alert(limitCnt + "자이상 입력하실 수 없습니다.");
            newVal = val.substr(0, limitCnt);
            $ele.val(newVal);
            $remainTxt.html(limitCnt);
        }else{
            $remainTxt.html(length);
        }
    };

    return {
        init: _init
    }
})(jQuery);

var selectbox = (function($){
    var _init = function(ele) {  
        var $ele = $(ele);
        var $btn = $ele.find('>a');
        var $list = $ele.find('>div');
        $btn.click(function(e){
            e.preventDefault();
            if($(this).hasClass("open")){
                $(this).removeClass("open");
                $list.hide();
            }else{
                $(this).addClass("open");
                $list.show();
            }
        });
    };
    return {
        init: _init
    }
})(jQuery);


var shopGallery = (function($){
    var _init = function() {  
      var $ele = $(".store_gallery").find(".gal_list");
	      $ele.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            centerMode: false,
            focusOnSelect: true
        });
    };
    return {
        init: _init
    }
})(jQuery);


var youtubeframe = (function($){
	var btn_view = false;
    var _init = function() {    	
        var $allVideos = $("#movieBox").find("iframe"),
        	$fluidEl = $("#movieBox");        	
	        $allVideos.each(function() {

	          	$(this)
	            .data('aspectRatio', this.height / this.width)
	            .removeAttr('height')
	            .removeAttr('width');
	        	});

		        $(window).resize(function() {

		          var newWidth = $fluidEl.width();
		          $allVideos.each(function() {

		            var $el = $(this);
		            $el
		              .width(newWidth)
		              .height(newWidth * $el.data('aspectRatio'));

		        });
		    }).resize();         
            
    	};
	    return {
	    init: _init
    }
})(jQuery);


var subFunc = (function($){
	var _tab = function(num){
		var $tab = $(".tab>ul");
		var prev;

		$tab.find("a").click(function(e){
			e.preventDefault();
			var url = $(this).attr("href");
			if(prev){
				prev.removeClass("on");
				$(prev.attr("href")).css("display", "none");
			}
			$(this).addClass("on");
			$(url).css("display", "block");

			prev = $(this);
		});
		$tab.find("li").eq(num).find("a").trigger("click");
	};
	return{
		tab:_tab
	}
})(jQuery);


var popControl = (function($){
    var init;
    var $popup, $popupList;   

    init = function(){
        var $popup = $(".popup");
        var $popupList = $("#popList");

        $popup.css("display", "block");

        $popup.each(function(i){
            if(getCookie("pop"+i) != "checked"){
                $("#pop"+i).css("display", "block");

            }else{
                $("#pop"+i).css("display", "none");
            }
        });

        $popup.find(".today a").on("click", function(e){
            e.preventDefault();
            var layerId = $(this).parent().parent().attr("id");
            closeCookie(layerId);
        });
        popSlide();
    };
    var popSlide = function(){
        var $popup_slide = $(".popup_slide");

        $popup_slide.slick({
            fade: false,
            infinite: false,
            autoplay: false,
            arrows: false,
            draggable: true,
            autoplaySpeed: 4000,
            speed: 1500,
            dots:true,
                customPaging: function(slider, i) {
                    var inum = i+1;
                    return '<a href="javascript:;">' +'0'+ inum + '</a>';
                },
            pauseOnHover: false,
            pauseOnFocus: false,
            focusOnSelect: false
        });
    }


    var getCookie = function(name){
        var nameOfCookie = name + "=";
        var x = 0;
        while ( x <= document.cookie.length )
        {
            var y = (x+nameOfCookie.length);
            if ( document.cookie.substring( x, y ) == nameOfCookie ) {
                if ( (endOfCookie=document.cookie.indexOf( ";", y )) == -1 )
                    endOfCookie = document.cookie.length;
                return unescape( document.cookie.substring( y, endOfCookie ) );
            }
            x = document.cookie.indexOf( " ", x ) + 1;
            if ( x == 0 )
                break;
        }
        return "";
    };

    var setCookie = function(name, value, expiredays){
        var todayDate = new Date();
        todayDate.setDate( todayDate.getDate() + expiredays );
        document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";"
    };

    var closeCookie = function(layerId){
        var i = layerId.split("pop")[1];

        if ( document.getElementById("chkPop"+i).checked ) {
            setCookie( layerId, "checked" ,1 );
            console.log(layerId);
        }
        $("#"+layerId).css("display", "none");
    };

    return{
        init: init
    }
})(jQuery);

